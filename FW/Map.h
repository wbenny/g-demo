#pragma once
#include <map>

namespace FW {

template<class T1, class T2>
class Map
{
	public:
		struct Pair
		{
			T1 First;
			T2 Second;

			Pair(){}
			Pair(const T1& p1, const T2& p2) : First(p1), Second(p2) {}
		};

		Map() {}

 		void Add(const Pair& pair) { m_Map[pair.First] = pair.Second; }
		void Add(const T1& p1, const T2& p2) { m_Map[p1] = p2; }

		void Remove(const T1& p1) { m_Map.erase(p1); }

		void Clear() { m_Map.clear(); }
		
		T2& operator[] (const T1& p1) { return m_Map[p1]; }

		T2& GetMappedElement(const T1& p1) { return m_Map[p1]; }
		T2& GetElementByIndex(u32 index) { return m_Map.at(index); }

		u32 GetCount() const { return m_Map.size(); }

		bool ContainsElement(const T2& p2)
		{
			for (u32 i = 0; i < GetCount(); i++)
			{
				if (GetElementByIndex(i) == p2)
					return true;
			}

			return false;
		}

	protected:
		std::map<T1, T2> m_Map;
};

}