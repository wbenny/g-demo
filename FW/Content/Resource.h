#pragma once
#include "../RefCounter.h"
#include "../String.h"

namespace FW { namespace Content {

enum ResourceType
{
	None,
	Geometry,
	Texture,
	CubeMap,
	Shader,
	Material,
	RenderMode
};

class Resource : public RefCounter
{
	public:
		virtual void SetName(const String& name) { m_Name = name; }
		virtual void SetName(const char* name) { m_Name = name; }

		const String& GetName() const { return m_Name; }

		u32 GetID() const { return m_ID; }

		ResourceType GetType() const { return m_Type; }

	protected:
		ResourceType m_Type;

		String m_Name;
		u32 m_ID;

		Resource() : m_Type(ResourceType::None) { m_ID = GetNextUniqueID(); m_Name = String::Format("resource_%u", m_ID); }
		Resource(ResourceType type) : m_Type(type) { m_ID = GetNextUniqueID(); m_Name = String::Format("resource_%u", m_ID); }
		virtual ~Resource() {}

		static u32 GetNextUniqueID() { static u32 id = 0; return id++; }
};

} }
