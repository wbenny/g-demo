#pragma once
#include "../String.h"
#include "../Render/VertexFormats/PositionNormalTexture.h"
#include "../Render/GeometryBuilder.h"
#include "../Render/Texture.h"
#include "../Render/Shader.h"
#include "../Utils/LoadFile.h"
#include "Resource.h"

#include "../Scene/MeshNode.h"
#include "../Utils/lib3ds/lib3ds.h"
#include "../Materials/PhongMaterial.h"

#include "../Render/CubeMap.h"

namespace FW { namespace Content {

template <class TRes> class ResourceLoader {};

template <>
class ResourceLoader<Render::CubeMap>
{
	private:
		static u8* ReadTga(const char* filename, u32* w, u32* h)
		{
			FILE *file;

			fopen_s(&file, filename, "rb");
			if (file == NULL)
			{
				printf("Could not open the file: %s\n", filename);
				exit(0);
			}

			u8 header[20];

			// Read all 18 bytes of the header.
			fread(header, sizeof(u8), 18, file);

			// Should be image type 2 (color) or type 10 (rle compressed color).
			if (header[2] != 2 && header[2] != 10)
			{
				fclose(file);
				exit(0);
			}

			if (header[0])
			{
				fseek(file, header[0], SEEK_CUR);
			}

			// Get the size and bitdepth from the header.
			u32 m_width  = header[13] * 256 + header[12];
			u32 m_height = header[15] * 256 + header[14];
			u32 m_bpp    = header[16] / 8;
			*w = m_width;
			*h = m_height;

			if (m_bpp != 3 && m_bpp != 4)
			{
				fclose(file);
				exit(0);
			}

			int imageSize = m_width * m_height * 4;

			// Allocate memory for image data.
			u8* data = new u8[imageSize];

			// Read the uncompressed image data if type 2.
			if (header[2] == 2)
			{
				long ctpixel = 0;

				// Stores the temp color data.
				u8 color[4];

				while (ctpixel < imageSize)
				{
					// Read what color we should use.
					fread(&color[0], 1, m_bpp, file);

					data[ctpixel+0] = color[0];
					data[ctpixel+1] = color[1];
					data[ctpixel+2] = color[2];

					if (m_bpp == 4)
					{
						data[ctpixel+3] = color[3];
					}
					else
					{
						data[ctpixel+3] = 255;
					}

					ctpixel += 4;
				}
			}

			// Read the compressed image data if type 10.
			else if (header[2] == 10)
			{
				long ctpixel = 0,
					ctloop = 0;
				// Stores the rle header and the temp color data.
				u8 rle;
				u8 color[4];

				while (ctpixel < imageSize)
				{
					// Reads the the RLE header.
					fread(&rle, 1, 1, file);

					// If the rle header is below 128 it means that what folows is just raw data with rle+1 pixels.
					if (rle < 128)
					{
						fread(&data[ctpixel], m_bpp, rle+1, file);
						ctpixel += m_bpp*(rle+1);
					}

					// If the rle header is equal or above 128 it means that we have a string of rle-127 pixels
					// that use the folowing pixels color.
					else
					{
						// Read what color we should use.
						fread(&color[0], 1, m_bpp, file);

						// Insert the color stored in tmp into the folowing rle-127 pixels.
						ctloop = 0;
						while (ctloop < (rle-127))
						{
							data[ctpixel+0] = color[0];
							data[ctpixel+1] = color[1];
							data[ctpixel+2] = color[2];

							if (m_bpp == 4)
							{
								data[ctpixel+3] = color[3];
							}
							else
							{
								data[ctpixel+3] = 255;
							}

							ctpixel += 4;
							ctloop++;
						}
					}
				}
			}

			// Close file.
			fclose(file);

			return data;
		}

		enum CubeMapSides
		{
			XP = 0, XN = 1, YP = 2, YN = 3, ZP = 4, ZN = 5
		};

	public:
		static Render::CubeMap* Read(const String& dirName, bool mipmap = true)
		{
			u32 width[6], height[6];
			u8* dataPtr[6];

			dataPtr[CubeMapSides::XP] = ReadTga((dirName + "\\xp.tga").CharArray(), &(width[CubeMapSides::XP]), &(height[CubeMapSides::XP]));
			dataPtr[CubeMapSides::XN] = ReadTga((dirName + "\\xn.tga").CharArray(), &(width[CubeMapSides::XN]), &(height[CubeMapSides::XN]));
			dataPtr[CubeMapSides::YP] = ReadTga((dirName + "\\yp.tga").CharArray(), &(width[CubeMapSides::YP]), &(height[CubeMapSides::YP]));
			dataPtr[CubeMapSides::YN] = ReadTga((dirName + "\\yn.tga").CharArray(), &(width[CubeMapSides::YN]), &(height[CubeMapSides::YN]));
			dataPtr[CubeMapSides::ZP] = ReadTga((dirName + "\\zp.tga").CharArray(), &(width[CubeMapSides::ZP]), &(height[CubeMapSides::ZP]));
			dataPtr[CubeMapSides::ZN] = ReadTga((dirName + "\\zn.tga").CharArray(), &(width[CubeMapSides::ZN]), &(height[CubeMapSides::ZN]));

			GLuint texture;

			glGenTextures(1, &texture);
			glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA8, width[CubeMapSides::XP], height[CubeMapSides::XP], 0, GL_BGRA, GL_UNSIGNED_BYTE, dataPtr[CubeMapSides::XP]);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA8, width[CubeMapSides::XN], height[CubeMapSides::XN], 0, GL_BGRA, GL_UNSIGNED_BYTE, dataPtr[CubeMapSides::XN]);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA8, width[CubeMapSides::YP], height[CubeMapSides::YP], 0, GL_BGRA, GL_UNSIGNED_BYTE, dataPtr[CubeMapSides::YP]);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA8, width[CubeMapSides::YN], height[CubeMapSides::YN], 0, GL_BGRA, GL_UNSIGNED_BYTE, dataPtr[CubeMapSides::YN]);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA8, width[CubeMapSides::ZP], height[CubeMapSides::ZP], 0, GL_BGRA, GL_UNSIGNED_BYTE, dataPtr[CubeMapSides::ZP]);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA8, width[CubeMapSides::ZN], height[CubeMapSides::ZN], 0, GL_BGRA, GL_UNSIGNED_BYTE, dataPtr[CubeMapSides::ZN]);

			Render::CubeMap* cm = new Render::CubeMap(texture);

			if (mipmap)
				cm->GenerateMipmaps();

			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

			return cm;
		}
};


template <>
class ResourceLoader<Render::Texture>
{
	private:
		static u8* ReadTga(const char* filename, u32* w, u32* h)
		{
			FILE *file;

			fopen_s(&file, filename, "rb");
			if (file == NULL)
			{
				printf("Could not open the file: %s\n", filename);
				exit(0);
			}

			u8 header[20];

			// Read all 18 bytes of the header.
			fread(header, sizeof(u8), 18, file);

			// Should be image type 2 (color) or type 10 (rle compressed color).
			if (header[2] != 2 && header[2] != 10)
			{
				fclose(file);
				exit(0);
			}

			if (header[0])
			{
				fseek(file, header[0], SEEK_CUR);
			}

			// Get the size and bitdepth from the header.
			u32 m_width  = header[13] * 256 + header[12];
			u32 m_height = header[15] * 256 + header[14];
			u32 m_bpp    = header[16] / 8;
			*w = m_width;
			*h = m_height;

			if (m_bpp != 3 && m_bpp != 4)
			{
				fclose(file);
				exit(0);
			}

			int imageSize = m_width * m_height * 4;

			// Allocate memory for image data.
			u8* data = new u8[imageSize];

			// Read the uncompressed image data if type 2.
			if (header[2] == 2)
			{
				long ctpixel = 0;

				// Stores the temp color data.
				u8 color[4];

				while (ctpixel < imageSize)
				{
					// Read what color we should use.
					fread(&color[0], 1, m_bpp, file);

					data[ctpixel+0] = color[0];
					data[ctpixel+1] = color[1];
					data[ctpixel+2] = color[2];

					if (m_bpp == 4)
					{
						data[ctpixel+3] = color[3];
					}
					else
					{
						data[ctpixel+3] = 255;
					}

					ctpixel += 4;
				}
			}

			// Read the compressed image data if type 10.
			else if (header[2] == 10)
			{
				long ctpixel = 0,
					ctloop = 0;
				// Stores the rle header and the temp color data.
				u8 rle;
				u8 color[4];

				while (ctpixel < imageSize)
				{
					// Reads the the RLE header.
					fread(&rle, 1, 1, file);

					// If the rle header is below 128 it means that what folows is just raw data with rle+1 pixels.
					if (rle < 128)
					{
						fread(&data[ctpixel], m_bpp, rle+1, file);
						ctpixel += m_bpp*(rle+1);
					}

					// If the rle header is equal or above 128 it means that we have a string of rle-127 pixels
					// that use the folowing pixels color.
					else
					{
						// Read what color we should use.
						fread(&color[0], 1, m_bpp, file);

						// Insert the color stored in tmp into the folowing rle-127 pixels.
						ctloop = 0;
						while (ctloop < (rle-127))
						{
							data[ctpixel+0] = color[0];
							data[ctpixel+1] = color[1];
							data[ctpixel+2] = color[2];

							if (m_bpp == 4)
							{
								data[ctpixel+3] = color[3];
							}
							else
							{
								data[ctpixel+3] = 255;
							}

							ctpixel += 4;
							ctloop++;
						}
					}
				}
			}

			// Close file.
			fclose(file);

			return data;
		}

	public:
		static Render::Texture* Read(const String& fileName, bool repeat = false, bool mipmap = true)
		{
			u32 width, height, textureBits;
			u8* dataPtr = ReadTga(fileName.CharArray(), &width, &height);
			
			textureBits =	Render::TextureBit::BlueGreenRedAlpha |
							Render::TextureBit::Byte |
							(repeat ? Render::TextureBit::Repeat : Render::TextureBit::Clamp) |
							(mipmap ? Render::TextureBit::LinearFilteringWithMipmap : Render::TextureBit::NoFiltering) |
							(mipmap ? Render::TextureBit::Mipmap : 0);

			return new Render::Texture(textureBits, width, height, dataPtr);
		}
};

// template <>
// class ResourceLoader<Render::Shader>
// {
// 	public:
// 		static Render::Shader* Read(const String& vertexShaderFileName, const String& fragmentShaderFileName)
// 		{
// 			Render::Shader* result = new Render::Shader();
// 			result->Init(Utils::LoadFile(vertexShaderFileName), Utils::LoadFile(fragmentShaderFileName));
// 			return result;
// 		}
// };

template <>
class ResourceLoader<Render::Geometry>
{
	public:
		template <class TVertex>
		static Render::Geometry* Read(const String& fileName)
		{
			FW_DEBUG_PRINT("[-] Loading geometry '%s'...\n", fileName.CharArray());

			Render::GeometryBuilder<TVertex> gb;

			FILE *fr;
			fopen_s(&fr, fileName.CharArray(), "rb");

			if (!fr)
				return false;

			TVertex vertex[3];

			while (fread(&vertex, 3 * sizeof(TVertex), 1, fr) != 0)
			{
				gb.AddTriangle(vertex[0], vertex[1], vertex[2]);
			}

			fclose(fr);

			FW_DEBUG_PRINT("    Done! (%i vertices, %i triangles)\n", gb.GetVertexCount(), gb.GetIndexCount() / 3);

			Render::Geometry* result = gb.ToGeometry();
			result->UpdateBuffers();

			return result;
		}

		static Render::Geometry* Read(const String& fileName)
		{
			return Read<Render::VertexFormats::PositionNormalTexture>(fileName);
		}
};

template <>
class ResourceLoader<Scene::MeshNode>
{
	public:
		static Scene::MeshNode* Read(const String& fileName)
		{
			List<Materials::PhongMaterial*> materialList;
			List<Scene::MeshNode*> meshList;
			List<Render::Texture*> textureList;
			List<Render::Texture*> normalmapList;

			FW_DEBUG_PRINT("[-] Loading model '%s'...\r\n", fileName.CharArray());

			// Open .3ds file.
			Lib3dsFile* file3ds = lib3ds_file_open(fileName.CharArray());

			if (!file3ds)
			{
				FW_DEBUG_PRINT("[X] FAILED - file not found\r\n");
				return NULL;
			}
			else
			{
				FW_DEBUG_PRINT("[-]    Loaded into memory...\r\n");
			}

			FW_DEBUG_PRINT("[-]    Building materials... ");

			// Loop through materials.
			for (int i = 0; i < file3ds->nmaterials; i++)
			{
				Lib3dsMaterial* material3ds = file3ds->materials[i];
				Materials::PhongMaterial* material = new Materials::PhongMaterial();

// 				FW_DEBUG_PRINT("[-]       - '%s'...\r\n", material3ds->name);

				material->SetName(material3ds->name);
				material->SetColor(Math::Vector3(material3ds->diffuse));
				material->SetSpecularShininess((1.0f - material3ds->shininess) * 128.0f);
				material->SetDiffuseReflection(0.8f);

				// Load texture, if available.
				if (material3ds->texture1_map.name[0] != '\0') // if (strlen(material3ds->texture1_map.name) == 0)
				{
					Render::Texture* texture;

// 					FW_DEBUG_PRINT("[-]          - Contains texture '%s', loading...\r\n", material3ds->name);

					String textureFileName = material3ds->texture1_map.name;

					textureFileName.StrReplace(".jpg", ".tga", String::Case::IgnoreCase);

					textureFileName = "textures\\" + textureFileName;
					
					texture = ResourceLoader<Render::Texture>::Read(textureFileName, true);
					texture->SetName(material3ds->texture1_map.name);

					material->SetTexture(texture);
				}

				// Load normal/bump map, if available.
				if (0 && material3ds->bump_map.name[0] != '\0')	// if (strlen(material3ds->bump_map.name) == 0)
				{
					Render::Texture* normalMap;

// 					FW_DEBUG_PRINT("[-]          - Contains bump map '%s', loading...\r\n", material3ds->name);

					String normalMapFileName = material3ds->bump_map.name;

					normalMapFileName.StrReplace(".jpg", "_normal.tga", String::Case::IgnoreCase);

					normalMapFileName = "textures\\" + normalMapFileName;

					normalMap = ResourceLoader<Render::Texture>::Read(normalMapFileName, true);
					normalMap->SetName(material3ds->bump_map.name);

					material->SetNormalMap(normalMap);
				}

				materialList.Add(material);

// 				FW_DEBUG_PRINT("[-]       Done...\r\n", material3ds->name);
			}

			FW_DEBUG_PRINT("done!\r\n");

			FW_DEBUG_PRINT("[-]    Loading nodes... ");

			// Loop through meshes.
			for (int i = 0; i < file3ds->nmeshes; i++)
			{
				Lib3dsMesh* mesh3ds = file3ds->meshes[i];
				Render::GeometryBuilder<Render::VertexFormats::PositionNormalTexture> geometryBuilder;
				int materialIndex = -1;
				
// 				FW_DEBUG_PRINT("[-]       - '%s'...\r\n", mesh3ds->name);

// 				FW_DEBUG_PRINT("[-]          Loading vertices...\r\n");

				for (int j = 0; j < mesh3ds->nvertices; j++)
				{
					Render::VertexFormats::PositionNormalTexture v;

					//v.Position = Math::Vector3(mesh3ds->vertices[j]);

					Math::Matrix4 m = Math::Matrix4(mesh3ds->matrix).Transposed();
					Math::Vector3 u(mesh3ds->vertices[j]);
					
					Utils::Swap(m.Cell[13], m.Cell[14]);
					Utils::Swap(u.Y, u.Z);

					u.Z = -u.Z;
					m.Cell[14] = -m.Cell[14]; // 12X, 13Y, 14Z, 15W
					
					v.Position = m * u;

					if (mesh3ds->texcos)
						v.TexCoord = Math::Vector2(mesh3ds->texcos[j]);

					geometryBuilder.AddVertex(v);
				}

// 				FW_DEBUG_PRINT("[-]          Loading Faces...\r\n");

				// Material is set by face - set material of 1st face for the whole mesh.
				if (mesh3ds->nfaces > 0)
					materialIndex = mesh3ds->faces[0].material;

				for (int j = 0; j < mesh3ds->nfaces; j++)
				{
					geometryBuilder.AddFace3(	(u32)mesh3ds->faces[j].index[0],
												(u32)mesh3ds->faces[j].index[1],
												(u32)mesh3ds->faces[j].index[2]);
				}

// 				FW_DEBUG_PRINT("[-]          Calculating normals...\r\n");

				geometryBuilder.CalculateNormals();

				Scene::MeshNode* mesh = new Scene::MeshNode();
				//mesh->SetMatrix(Math::Matrix4(mesh3ds->matrix).Transposed());	// 3ds holds row-major matrices.

				// Set material, if exists.
				if (materialIndex != -1)
					mesh->SetMaterial(materialList[materialIndex]);

				mesh->SetName(mesh3ds->name);
				mesh->SetGeometry(geometryBuilder.ToGeometry());
				mesh->GetGeometry()->UpdateBuffers();

				meshList.Add(mesh);

// 				FW_DEBUG_PRINT("[-]       Done...\r\n");
			}

			FW_DEBUG_PRINT("done!\r\n");

			lib3ds_file_free(file3ds);

			// Build a final node.
			Scene::MeshNode* finalNode = new Scene::MeshNode();

			for (u32 i = 0; i < meshList.GetCount(); i++)
				finalNode->Add(meshList[i]);

// 			FW_DEBUG_PRINT("[-] Done...\r\n");

			return finalNode;
		}
};


} }
