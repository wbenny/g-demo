/**
 * @file	fw/String.h
 *
 * @brief	Declares the string class.
 */

#pragma once
#include "Types.h"
#include <string>
#include <algorithm>
#include <cstdio>
#include <cctype>
#include <locale>
#include <vector>
#include <stdarg.h>
#include <windows.h>
#include "Types.h"
#include "List.h"
#include "RefCounter.h"

namespace FW {

/**
 * @enum StringSplitOptions
 *
 * @brief	Values that represent split options.
 */
enum StringSplitOptions
{
	None = 0,
	RemoveEmptyEntries = 1
};

/**
 * @class	BasicString<T>
 *
 * @brief	The base string class.
 *
 * @date	2.10.2011
 */
template <class T>
class BasicString
{
	public:
		// Static constant members.
		static const int NoPosition = -1;	///< No position constant.
		static const T* Whitespaces;		///< Default whitespaces.

		/**
		 * @typedef	List< BasicString<T> > Collection
		 *
		 * @brief	Defines an alias representing the string collection.
		 */
		typedef List< BasicString<T> >	Collection;

		/**
		 * @enum	Case
		 *
		 * @brief	Values that represent letter casing.
		 */
		enum Case
		{
			NoticeCase = 0,
			IgnoreCase = 1
		};

		//////////////////////////////////////////////////////////////////////////
		// HELPING STRUCTURES
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @struct	Tokenizer
		 *
		 * @brief	Tokenizer structure. 
		 *
		 * @date	3.10.2011
		 */
		struct Tokenizer
		{
			/**
			 * @fn	inline Tokenizer(const BasicString<T> &str, const BasicString<T> &separator = ' ',
			 * 		StringSplitOptions options = StringSplitOptions::None)
			 *
			 * @brief	Constructor.
			 *
			 * @date	3.10.2011
			 *
			 * @param	str		 	The string to tokenize.
			 * @param	separator	(optional) the separator.
			 * @param	options  	(optional) options for controlling the operation.
			 */
			inline Tokenizer(const BasicString<T> &str, const BasicString<T> &separator = ' ',
				StringSplitOptions options = StringSplitOptions::None) :
					m_sString(str),
					m_iPosition(0)
			{
				Tokenize(separator, options);
			}

			/**
			 * @fn	inline Tokenizer(const BasicString<T> &str, const Array< BasicString<T> > &separators,
			 * 		StringSplitOptions options = StringSplitOptions::None)
			 *
			 * @brief	Constructor.
			 *
			 * @date	3.10.2011
			 *
			 * @param	str		  	The string.
			 * @param	separators	The separators.
			 * @param	options   	(optional) options for controlling the operation.
			 */
			inline Tokenizer(const BasicString<T> &str, const Array< BasicString<T> > &separators,
				StringSplitOptions options = StringSplitOptions::None) :
					m_sString(str),
					m_iPosition(0)
			{
				Tokenize(separators, options);
			}

			/**
			 * @fn	inline Tokenizer &Tokenize(const BasicString<T> &separator = ' ',
			 * 		StringSplitOptions options = StringSplitOptions::None)
			 *
			 * @brief	Tokenizes the string.
			 *
			 * @date	3.10.2011
			 *
			 * @param	separator	(optional) the separator.
			 * @param	options  	(optional) options for controlling the operation.
			 *
			 * @return	This tokenizer structure.
			 */
			inline Tokenizer &Tokenize(const BasicString<T> &separator = ' ', StringSplitOptions options = StringSplitOptions::None)
			{
				m_oCollection = m_sString.Split(separator, options);
				return (*this);
			}

			/**
			 * @fn	inline Tokenizer &Tokenize(const Array< BasicString<T> > &separators,
			 * 		StringSplitOptions options = StringSplitOptions::None)
			 *
			 * @brief	Tokenizes the string.
			 *
			 * @date	3.10.2011
			 *
			 * @param	separators	The separators.
			 * @param	options   	(optional) options for controlling the operation.
			 *
			 * @return	This tokenizer structure.
			 */
			inline Tokenizer &Tokenize(const Array< BasicString<T> > &separators, StringSplitOptions options = StringSplitOptions::None)
			{
				m_oCollection = m_sString.Split(separators, options);
				return (*this);
			}

			/**
			 * @fn	BasicString<T> GetNextToken()
			 *
			 * @brief	Gets the next token.
			 *
			 * @date	3.10.2011
			 *
			 * @return	The next token.
			 */
			BasicString<T> GetNextToken()
			{
				if (m_iPosition == m_oCollection.Count())
					return BasicString<T>(false); // Return invalid string.
				else
					return m_oCollection[m_iPosition++];
			}

			/**
			 * @fn	BasicString<T> ActualToken() const
			 *
			 * @brief	Gets the token at actual position.
			 * 			If the position is out of range, returns invalid string.
			 *
			 * @date	3.10.2011
			 *
			 * @return	The actual token.
			 */
			BasicString<T> ActualToken() const
			{
				if (!IsValid())
					return BasicString<T>(false);
				else
					return m_oCollection[m_iPosition];
			}

			/**
			 * @fn	inline int operator++()
			 *
			 * @brief	Increment operator. Moves pointer to the next token.
			 *
			 * @date	3.10.2011
			 *
			 * @return	The next token position.
			 */
			inline int operator++()
			{
				return ++m_iPosition;
			}

			/**
			 * @fn	inline int operator--()
			 *
			 * @brief	Decrement operator. Moves pointer to the previous token.
			 *
			 * @date	3.10.2011
			 *
			 * @return	The previous token position.
			 */
			inline int operator--()
			{
				return --m_iPosition;
			}

			/**
			 * @fn	inline int operator++(int)
			 *
			 * @brief	Increment operator. Moves pointer to the next token.
			 *
			 * @date	3.10.2011
			 *
			 * @param	_unused	Unused.
			 * 
			 * @return	The next token position.
			 */
			inline int operator++(int _unused)
			{
				return m_iPosition++;
			}

			/**
			 * @fn	inline int operator--(int)
			 *
			 * @brief	Decrement operator. Moves pointer to the previous token.
			 *
			 * @date	3.10.2011
			 *
			 * @param	_unused	Unused.
			 *
			 * @return	The previous token position.
			 */
			inline int operator--(int _unused)
			{
				return m_iPosition--;
			}

			/**
			 * @fn	BasicString<T> operator*() const
			 *
			 * @brief	Indirection operator. Gets the token at actual position.
			 * 			If the position is out of range, returns invalid string.
			 *
			 * @date	3.10.2011
			 *
			 * @return	The token at actual position.
			 */
			BasicString<T> operator *() const
			{
				return ActualToken();
			}

			/**
			 * @fn	inline bool IsValid() const
			 *
			 * @brief	Query if exists valid token at current position.
			 *
			 * @date	3.10.2011
			 *
			 * @return	true if valid, false if not.
			 */
			inline bool IsValid() const
			{
				return m_iPosition < m_oCollection.Count() && m_iPosition >= 0;
			}

			/**
			 * @fn	inline void Reset()
			 *
			 * @brief	Resets the current position to zero.
			 *
			 * @date	3.10.2011
			 */
			inline void Reset()
			{
				m_iPosition = 0;
			}

			private:
				// Members.
				int m_iPosition;					///< Actual position in the string collection.
				Collection m_oCollection;			///< String collection of tokens.
				const BasicString<T> &m_sString;	///< Reference to the input string.
		};

		/**
		 * @struct	Unicode
		 *
		 * @brief	Structure for manipulating Unicode strings.
		 *
		 * @date	2.10.2011
		 */
		struct Unicode
		{
			/**
			 * @fn	static inline int GetFinalStringLength(const wchar_t *utf8)
			 *
			 * @brief	Gets a final length of string.
			 *
			 * @date	2.10.2011
			 *
			 * @param	utf8	The Unicode string.
			 *
			 * @return	The final length of string.
			 */
			static inline int GetFinalStringLength(const wchar_t *utf8)
			{
				return WideCharToMultiByte(CP_UTF8, 0, utf8, -1, NULL, 0, NULL, NULL);
			}

			/**
			 * @fn	static bool ToAnsi(const wchar_t *utf8, char *ansi, int ansiBuffLen)
			 *
			 * @brief	Converts given Unicode string to an ANSI string.
			 *
			 * @date	2.10.2011
			 *
			 * @param	utf8			The Unicode string.
			 * @param [out]	ansi		Pointer to the ANSI string buffer.
			 * @param	ansiBuffLen 	Length of the ANSI string buffer.
			 *
			 * @return	true if it succeeds, false if it fails.
			 */
			static bool ToAnsi(const wchar_t *utf8, char *ansi, int ansiBuffLen)
			{
				if (ansi == NULL || utf8 == NULL)
					return false;

				WideCharToMultiByte(CP_UTF8, 0, utf8, -1, ansi, ansiBuffLen, NULL, NULL);
				ansi[ansiBuffLen] = 0;
				return true;
			}
		};

		/**
		 * @struct	Ansi
		 *
		 * @brief	Structure for manipulating ANSI strings.
		 *
		 * @date	2.10.2011
		 */
		struct Ansi
		{
			/**
			 * @fn	static inline int GetFinalStringLength(const char *ansi)
			 *
			 * @brief	Gets a final length of string.
			 *
			 * @date	2.10.2011
			 *
			 * @param	ansi	The ANSI string.
			 *
			 * @return	The final length of string.
			 */
			static inline int GetFinalStringLength(const char *ansi)
			{
				return MultiByteToWideChar(CP_ACP, 0, ansi, -1, NULL, 0);
			}

			/**
			 * @fn	static bool ToUnicode(const char *ansi, wchar_t *utf8, int utf8BuffLen)
			 *
			 * @brief	Converts given ANSI string to a Unicode string.
			 *
			 * @date	2.10.2011
			 *
			 * @param	ansi			The ANSI string.
			 * @param [out]	utf8		Pointer to the Unicode string buffer.
			 * @param	utf8BuffLen 	Length of the Unicode string buffer.
			 *
			 * @return	true if it succeeds, false if it fails.
			 */
			static bool ToUnicode(const char *ansi, wchar_t *utf8, int utf8BuffLen)
			{
				if (ansi == NULL || utf8 == NULL)
					return false;

				MultiByteToWideChar(CP_ACP, 0, ansi, -1, utf8, utf8BuffLen);
				utf8[utf8BuffLen] = 0;
				return true;
			}
		};

		//////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline BasicString::BasicString()
		 *
		 * @brief	Default constructor.
		 *
		 * @date	2.10.2011
		 */
		inline BasicString() :
			m_bValid(true)
		{

		}

		/**
		 * @fn	inline BasicString::BasicString(bool valid)
		 *
		 * @brief	Constructor. Can create invalid string.
		 *
		 * @date	3.10.2011
		 *
		 * @param	valid	false to invalid string.
		 */
		inline BasicString(bool valid) :
			m_bValid(valid)
		{

		}

		/**
		 * @fn	BasicString::BasicString(signed int s);
		 *
		 * @brief	Constructor. Creates string from a signed integer values.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	Integer.
		 */
		BasicString(signed int s);

		/**
		 * @fn	BasicString::BasicString(unsigned int s);
		 *
		 * @brief	Constructor. Creates string from an unsigned integer values.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	Integer.
		 */
		BasicString(unsigned int s);

		/**
		 * @fn	BasicString::BasicString(double s);
		 *
		 * @brief	Constructor. Creates string from a floating point number.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	Double.
		 */
		BasicString(double s);

		/**
		 * @fn	BasicString::BasicString(T s)
		 *
		 * @brief	Constructor. Creates string from an initial letter.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	The letter.
		 */
		BasicString(T s) :
			m_bValid(true)
		{
			T tmp[2] = { s, 0 };
			m_String = tmp;
		}

		/**
		 * @fn	BasicString::BasicString(const T *s)
		 *
		 * @brief	Constructor. Creates string from a zero-terminated string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	The zero-terminated string.
		 */
		BasicString(const T *s) :
			m_bValid(true),
			m_String(s)
		{

		}

		/**
		 * @fn	BasicString::BasicString(const std::basic_string<T> &s)
		 *
		 * @brief	Constructor. Creates string from a std::(w)string object.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	The std::(w)string object.
		 */
		BasicString(const std::basic_string<T> &s) :
			m_bValid(true),
			m_String(s)
		{

		}

		/**
		 * @fn	BasicString::BasicString(const BasicString<T> &s)
		 *
		 * @brief	Copy constructor.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	String.
		 */
		BasicString(const BasicString<T> &s) :
			m_bValid(s.m_bValid),
			m_String(s.m_String)
		{

		}

		/**
		 * @fn	virtual BasicString::~BasicString()
		 *
		 * @brief	Destructor.
		 *
		 * @date	2.10.2011
		 */
		virtual ~BasicString()
		{

		}

		//////////////////////////////////////////////////////////////////////////
		// operator +=
		//////////////////////////////////////////////////////////////////////////
 
		/**
		 * @fn	inline BasicString<T> &BasicString::operator+= (T r)
		 *
		 * @brief	Addition assignment operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of 'TCHAR' type.
		 *
		 * @return	The reference of this string.
		 */
		inline BasicString<T> &operator += (T r)
		{
			m_String.append(static_cast<std::basic_string<T>::size_type>(1), r);
			return (*this);
		}

		/**
		 * @fn	inline BasicString<T> &BasicString::operator+= (const T *r)
		 *
		 * @brief	Addition assignment operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of 'TCHAR *' type.
		 *
		 * @return	The reference of this string.
		 */
		inline BasicString<T> &operator += (const T *r)
		{
			m_String.append(r);
			return (*this);
		}

		/**
		 * @fn	inline BasicString<T> &BasicString::operator+= (const BasicString<T> &r)
		 *
		 * @brief	Addition assignment operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of string type.
		 *
		 * @return	The reference of this string.
		 */
		inline BasicString<T> &operator += (const BasicString<T> &r)
		{
			m_String.append(r.m_String);
			return (*this);
		}

		//////////////////////////////////////////////////////////////////////////
		// operator =
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline BasicString<T> BasicString::operator= (T r)
		 *
		 * @brief	Assignment operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of 'TCHAR' type.
		 *
		 * @return	A shallow copy of this object.
		 */
		inline BasicString<T> operator = (T r)
		{
			m_String.assign(1, r);
			return (*this);
		}

		/**
		 * @fn	inline BasicString<T> BasicString::operator= (const T *r)
		 *
		 * @brief	Assignment operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of 'TCHAR *' type.
		 *
		 * @return	A shallow copy of this object.
		 */
		inline BasicString<T> operator = (const T *r)
		{
			m_String.assign(r);
			return (*this);
		}

		/**
		 * @fn	inline BasicString<T> BasicString::operator= (const BasicString<T> &r)
		 *
		 * @brief	Assignment operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of string type.
		 *
		 * @return	A shallow copy of this object.
		 */
		inline BasicString<T> operator = (const BasicString<T> &r)
		{
			m_String.assign(r.m_String);
			m_bValid = r.m_bValid;
			return (*this);
		}

		//////////////////////////////////////////////////////////////////////////
		// operator +
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline BasicString<T> BasicString::operator+ (T r) const
		 *
		 * @brief	Addition operator. Concatenates strings.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of 'TCHAR' type.
		 *
		 * @return	The result of the operation.
		 */
		inline BasicString<T> operator + (T r) const
		{
			BasicString<T> tmp(this);
			tmp.m_String += r;
			return tmp;
		}

		/**
		 * @fn	friend inline BasicString<T> BasicString::operator+ (T l, const BasicString<T> &r)
		 *
		 * @brief	Addition operator. Concatenates strings.
		 *
		 * @date	2.10.2011
		 *
		 * @param	l	The left value of TCHAR type.
		 * @param	r	A right value of string type.
		 *
		 * @return	The result of the operation.
		 */
		friend inline BasicString<T> operator + (T l, const BasicString<T> &r)
		{
			BasicString<T> tmp(l);
			tmp += r;
			return tmp;
		}

		/**
		 * @fn	friend inline BasicString<T> BasicString::operator+ (const T *l, const BasicString<T> &r)
		 *
		 * @brief	Addition operator. Concatenates strings.
		 *
		 * @date	2.10.2011
		 *
		 * @param	l	The left value of 'TCHAR *' type.
		 * @param	r	A right value of string type.
		 *
		 * @return	The result of the operation.
		 */
		friend inline BasicString<T> operator + (const T *l, const BasicString<T> &r)
		{
			BasicString<T> tmp(l);
			tmp += r;
			return tmp;
		}

		/**
		 * @fn	friend inline BasicString<T> BasicString::operator+ (const std::basic_string<T> &l,
		 * 		const BasicString<T> &r)
		 *
		 * @brief	Addition operator. Concatenates strings.
		 *
		 * @date	2.10.2011
		 *
		 * @param	l	The left value of std::(w)string type.
		 * @param	r	A right value of string type.
		 *
		 * @return	The result of the operation.
		 */
		friend inline BasicString<T> operator + (const std::basic_string<T> &l,
			const BasicString<T> &r)
		{ 
			BasicString<T> tmp(l);
			tmp += r;
			return tmp;
		}

		/**
		 * @fn	friend inline BasicString<T> BasicString::operator+ (const BasicString<T> &l,
		 * 		const BasicString<T> &r)
		 *
		 * @brief	Addition operator. Concatenates strings.
		 *
		 * @date	4.10.2011
		 *
		 * @param	l	A left value of string type.
		 * @param	r	A right value of string type.
		 *
		 * @return	The result of the operation.
		 */
		friend inline BasicString<T> operator + (const BasicString<T> &l,
			const BasicString<T> &r)
		{ 
			BasicString<T> tmp(l);
			tmp += r;
			return tmp;
		}

		/**
		 * @fn	friend inline BasicString<T> BasicString::operator+ (const BasicString<T> &l, const T *r)
		 *
		 * @brief	Addition operator.
		 *
		 * @date	5.10.2011
		 *
		 * @param	l	A left value of string type.
		 * @param	r	A right value of 'TCHAR *' type.
		 *
		 * @return	The result of the operation.
		 */
		friend inline BasicString<T> operator + (const BasicString<T> &l,
			const T *r)
		{ 
			BasicString<T> tmp(l);
			tmp += r;
			return tmp;
		}
		//////////////////////////////////////////////////////////////////////////
		// operator ==
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	friend inline bool BasicString::operator== (T l, const BasicString<T> &r)
		 *
		 * @brief	Equality operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	l	The first instance of type TCHAR to compare.
		 * @param	r	The second instance of string to compare.
		 *
		 * @return	true if the parameters are considered equivalent.
		 */
		friend inline bool operator == (T l, const BasicString<T> &r)
		{
			return r.EqualsTo(l);
		}

		/**
		 * @fn	friend inline bool BasicString::operator== (const T *l, const BasicString<T> &r)
		 *
		 * @brief	Equality operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	l	The first instance of type 'TCHAR *' to compare.
		 * @param	r	The second instance of string to compare.
		 *
		 * @return	true if the parameters are considered equivalent.
		 */
		friend inline bool operator == (const T *l, const BasicString<T> &r)
		{
			return r.EqualsTo(l);
		}

		/**
		 * @fn	friend inline bool BasicString::operator== (const BasicString<T> &l,
		 * 		const BasicString<T> &r)
		 *
		 * @brief	Equality operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	l	The first string to compare.
		 * @param	r	The second string to compare.
		 *
		 * @return	true if the parameters are considered equivalent.
		 */
		friend inline bool operator == (const BasicString<T> &l, const BasicString<T> &r)
		{
			return r.EqualsTo(l);
		}

		//////////////////////////////////////////////////////////////////////////
		// operator !=
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline bool BasicString::operator!= (T r)
		 *
		 * @brief	Inequality operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of 'TCHAR' type.
		 */
		inline bool operator != (T r)
		{
			return !EqualsTo(r);
		}

		/**
		 * @fn	inline bool BasicString::operator!= (const T *r)
		 *
		 * @brief	Inequality operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of 'TCHAR *' type.
		 */
		inline bool operator != (const T *r)
		{
			return !EqualsTo(r);
		}

		/**
		 * @fn	inline bool BasicString::operator!= (const BasicString<T> &r)
		 *
		 * @brief	Inequality operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	r	The right value of string type.
		 */
		inline bool operator != (const BasicString<T> &r)
		{
			return !EqualsTo(r);
		}

		/**
		 * @fn	friend inline bool BasicString::operator!= (T l, const BasicString<T> &r)
		 *
		 * @brief	Inequality operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	l	The first instance of type TCHAR to compare.
		 * @param	r	The second instance of type string to compare.
		 */
		friend inline bool operator != (T l, const BasicString<T> &r)
		{
			return !r.EqualsTo(l);
		}

		/**
		 * @fn	friend inline bool BasicString::operator!= (const T *l, const BasicString<T> &r)
		 *
		 * @brief	Inequality operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	l	The first instance of type 'TCHAR *' to compare.
		 * @param	r	The second instance of type string to compare.
		 */
		friend inline bool operator != (const T *l, const BasicString<T> &r)
		{
			return !r.EqualsTo(l);
		}

		friend inline bool operator != (const BasicString<T> &l, const T *r)
		{
			return !l.EqualsTo(r);
		}

		//////////////////////////////////////////////////////////////////////////
		// operator <
		//////////////////////////////////////////////////////////////////////////
		friend inline bool operator < (const T *l, const BasicString<T> &r)
		{
			return (l < r.m_String);
		}

		friend inline bool operator < (const BasicString<T> &l, const BasicString<T> &r)
		{
			return (l.m_String < r.m_String);
		}

		//////////////////////////////////////////////////////////////////////////
		// operator []
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline T &BasicString::operator[] (int index)
		 *
		 * @brief	Array indexer operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	index	Zero-based index of the character.
		 *
		 * @return	The character at specified index.
		 */
		inline T &operator[] (int index)
		{ 
			return m_String[index];
		}

		/**
		 * @fn	inline const T &BasicString::operator[] (int index) const
		 *
		 * @brief	Array indexer operator.
		 *
		 * @date	2.10.2011
		 *
		 * @param	index	Zero-based index of the character.
		 *
		 * @return	The character at specified index.
		 */
		inline const T &operator[] (int index) const
		{
			return m_String[index];
		}

		//////////////////////////////////////////////////////////////////////////
		// FOR OUTPUT STREAMS
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @property	friend inline std::ostream &operator<< (std::ostream &out, const BasicString<T> &str)
		 *
		 * @brief	Passes given string to the output stream.
		 *
		 * @return	The stream.
		 */
		friend inline std::ostream &operator << (std::ostream &out,
			const BasicString<T> &str)
		{
			return (out << str.m_String);
		}

		/**
		 * @property	friend inline std::wostream &operator<< (std::wostream &out, const BasicString<T> &str)
		 *
		 * @brief	Passes given string to the output stream.
		 *
		 * @return	The stream.
		 */
		friend inline std::wostream &operator << (std::wostream &out,
			const BasicString<T> &str)
		{
			return (out << str.m_String);
		}

		//////////////////////////////////////////////////////////////////////////
		// COMPARE
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	bool BasicString::EqualsTo(T s, Case caseType = NoticeCase) const
		 *
		 * @brief	Query if this string equals to a specified string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s			The string (TCHAR) to compare to.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	true if it equals, false if it fails.
		 */
		bool EqualsTo(T s, Case caseType = NoticeCase) const
		{
			return (caseType == IgnoreCase ?
					Internal_EqualsToIgnoreCase(s) :
					m_String[0] == s && m_String.length() == 1);
		}

		/**
		 * @fn	bool BasicString::EqualsTo(const T *s, Case caseType = NoticeCase) const
		 *
		 * @brief	Query if this string equals to a specified string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s			The string (TCHAR *) to compare to.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	true if it equals, false if it fails.
		 */
		bool EqualsTo(const T *s, Case caseType = NoticeCase) const
		{
			return (caseType == IgnoreCase ?
					Internal_EqualsToIgnoreCase(s) :
					m_String.compare(s) == 0);
		}

		/**
		 * @fn	bool BasicString::EqualsTo(const BasicString &s, Case caseType = NoticeCase) const
		 *
		 * @brief	Query if this string equals to a specified string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s			The string to compare to.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	true if it equals, false if it fails.
		 */
		bool EqualsTo(const BasicString &s, Case caseType = NoticeCase) const
		{
			return EqualsTo(s.m_String.c_str(), caseType);
		}

		//////////////////////////////////////////////////////////////////////////
		// FIND HACKS
		//////////////////////////////////////////////////////////////////////////
	
#define D_UNIVERSAL_FIND_METHOD(t, method) \
	static_cast<int>(caseType == IgnoreCase ?\
		Internal_FindAndIgnoreCase(\
			needle,\
			offset,\
			static_cast<size_t (std::basic_string<T>::*)(t, size_t) const>(\
				&std::basic_string<T>::method)\
		) : m_String.method(needle, offset))

#define D_UNIVERSAL_STRING_FIND_METHOD(t, method) \
	static_cast<int>(caseType == IgnoreCase ?\
		Internal_FindAndIgnoreCase(\
			needle.Convert<t>(),\
			offset,\
			static_cast<size_t (std::basic_string<T>::*)(t, size_t) const>(\
				&std::basic_string<T>::method)\
		) : m_String.method(needle.Convert<t>(), offset))

		/**
		 * @fn	inline int BasicString::Find(const BasicString<T> &needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the first match.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle  	The needle.
		 * @param	caseType	(optional) casing type.
		 * @param	offset  	(optional) the offset.
		 *
		 * @return	The index of first match.
		 */
		inline int Find(const BasicString<T> &needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_STRING_FIND_METHOD(const T *, find);
		}

		/**
		 * @fn	inline int BasicString::Find(const T *needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the first match.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle  	The needle.
		 * @param	caseType	(optional) casing type.
		 * @param	offset  	(optional) the offset.
		 *
		 * @return	The index of first match.
		 */
		inline int Find(const T *needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(const T *, find);
		}

		/**
		 * @fn	inline int BasicString::Find(T needle, Case caseType = NoticeCase, int offset = 0) const
		 *
		 * @brief	Searches for the first match.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle  	The needle.
		 * @param	caseType	(optional) casing type.
		 * @param	offset  	(optional) the offset.
		 *
		 * @return	The index of first match.
		 */
		inline int Find(T needle, Case caseType = NoticeCase, int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(T, find);
		}

		/**
		 * @fn	inline int BasicString::ReverseFind(const BasicString<T> &needle,
		 * 		Case caseType = NoticeCase, int offset = 0) const
		 *
		 * @brief	Searches for the last match.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle  	The needle.
		 * @param	caseType	(optional) casing type.
		 * @param	offset  	(optional) the offset.
		 *
		 * @return	The index of last match.
		 */
		inline int ReverseFind(const BasicString<T> &needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_STRING_FIND_METHOD(const T *, rfind);
		}

		/**
		 * @fn	inline int BasicString::ReverseFind(const T *needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the last match.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle  	The needle.
		 * @param	caseType	(optional) casing type.
		 * @param	offset  	(optional) the offset.
		 *
		 * @return	The index of last match.
		 */
		inline int ReverseFind(const T *needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(const T *, rfind);
		}

		/**
		 * @fn	inline int BasicString::ReverseFind(T needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the last match.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle  	The needle.
		 * @param	caseType	(optional) casing type.
		 * @param	offset  	(optional) the offset.
		 *
		 * @return	The index of last match.
		 */
		inline int ReverseFind(T needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(T, rfind);
		}

		/**
		 * @fn	inline int BasicString::FindFirstOf(const BasicString<T> &needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the first occurrence of needle in this string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) casing type.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindFirstOf(const BasicString<T> &needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
 			return D_UNIVERSAL_STRING_FIND_METHOD(const T *, find_first_of);
		}

		/**
		 * @fn	inline int BasicString::FindFirstOf(const T *needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the first occurrence of needle in this string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) casing type.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindFirstOf(const T *needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(const T *, find_first_of);
		}

		/**
		 * @fn	inline int BasicString::FindFirstOf(T needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the first occurrence of needle in this string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) casing type.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindFirstOf(T needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(T, find_first_of);
		}

		/**
		 * @fn	inline int BasicString::FindFirstNotOf(const BasicString<T> &needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the first character in the object
		 * 			which is not part of needle.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindFirstNotOf(const BasicString<T> &needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_STRING_FIND_METHOD(const T *, find_first_not_of);
		}

		/**
		 * @fn	inline int BasicString::FindFirstNotOf(const T *needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the first character in the object
		 * 			which is not part of needle.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindFirstNotOf(const T *needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(const T *, find_first_not_of);
		}

		/**
		 * @fn	inline int BasicString::FindFirstNotOf(T needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the first character in the object
		 * 			which is not part of needle.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindFirstNotOf(T needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(T, find_first_not_of);
		}

		/**
		 * @fn	inline int BasicString::FindLastOf(const BasicString<T> &needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches the string from the end
		 * 			for any of the characters that are part of needle
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindLastOf(const BasicString<T> &needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_STRING_FIND_METHOD(const T *, find_last_of);
		}

		/**
		 * @fn	inline int BasicString::FindLastOf(const T *needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches the string from the end
		 * 			for any of the characters that are part of needle
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindLastOf(const T *needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(const T *, find_last_of);
		}

		/**
		 * @fn	inline int BasicString::FindLastOf(T needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches the string from the end
		 * 			for any of the characters that are part of needle
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindLastOf(T needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(T, find_last_of);
		}

		/**
		 * @fn	inline int BasicString::FindLastNotOf(const BasicString<T> &needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the last character in the object
		 * 			which is not part of needle.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindLastNotOf(const BasicString<T> &needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_STRING_FIND_METHOD(const T *, find_last_not_of);
		}

		/**
		 * @fn	inline int BasicString::FindLastNotOf(const T *needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the last character in the object
		 * 			which is not part of needle.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindLastNotOf(const T *needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(const T *, find_last_not_of);
		}

		/**
		 * @fn	inline int BasicString::FindLastNotOf(T needle, Case caseType = NoticeCase,
		 * 		int offset = 0) const
		 *
		 * @brief	Searches for the last character in the object
		 * 			which is not part of needle.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle			The needle.
		 * @param	caseType	  	(optional) type of the case.
		 * @param	offset		  	(optional) the offset.
		 *
		 * @return	The position of match.
		 */
		inline int FindLastNotOf(T needle, Case caseType = NoticeCase,
			int offset = 0) const
		{
			return D_UNIVERSAL_FIND_METHOD(T, find_last_not_of);
		}

#undef D_UNIVERSAL_FIND_METHOD
#undef D_UNIVERSAL_STRING_FIND_METHOD

		//////////////////////////////////////////////////////////////////////////
		// ERASE
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline BasicString<T> &BasicString::Erase(int offset = 0, int count = NoPosition)
		 *
		 * @brief	Erases a part of the string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	offset	(optional) the offset.
		 * @param	count 	(optional) amount of characters to be removed.
		 *
		 * @return	A reference to this object.
		 */
		inline BasicString<T> &Erase(int offset = 0, int count = NoPosition)
		{
			m_String.erase(offset, count);
			return (*this);
		}

		/**
		 * @fn	inline BasicString<T> &BasicString::EraseFirst(int count = 1)
		 *
		 * @brief	Erases a part of the string from begin.
		 *
		 * @date	2.10.2011
		 *
		 * @param	count 	(optional) amount of characters to be removed.
		 *
		 * @return	A reference to this object.
		 */
		inline BasicString<T> &EraseFirst(int count = 1) 
		{
			return Erase(0, count);
		}

		/**
		 * @fn	inline BasicString<T> &BasicString::EraseLast(int count = 1)
		 *
		 * @brief	Erases a part of the string from the end.
		 *
		 * @date	2.10.2011
		 *
		 * @param	count 	(optional) amount of characters to be removed.
		 *
		 * @return	A reference to this object.
		 */
		inline BasicString<T> &EraseLast(int count = 1)
		{
			return Erase(Length() - count, count);
		}

		//////////////////////////////////////////////////////////////////////////
		// REPLACE
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline int BasicString::StrReplace(const BasicString<T> &search,
		 * 		const BasicString<T> &replace, Case caseType = NoticeCase)
		 *
		 * @brief	String replacing.
		 *
		 * @date	2.10.2011
		 *
		 * @param	search  	String to search.
		 * @param	replace 	String to replace.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	Count of replaced matches.
		 */
		inline int StrReplace(const BasicString<T> &search,
			const BasicString<T> &replace, Case caseType = NoticeCase)
		{
			return Internal_StrReplace(search, replace, caseType);
		}

		/**
		 * @fn	inline int BasicString::Replace(const BasicString<T> &search,
		 * 		const BasicString<T> &replace, Case caseType = NoticeCase)
		 *
		 * @brief	String replacing.
		 *
		 * @date	2.10.2011
		 *
		 * @param	search  	String to search.
		 * @param	replace 	String to replace.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	Count of replaced matches.
		 */
		inline int Replace(const BasicString<T> &search,
			const BasicString<T> &replace, Case caseType = NoticeCase)
		{
			return Internal_StrReplace(search, replace, caseType);
		}

		/**
		 * @fn	inline BasicString<T> BasicString::Replace(int offset, int count,
		 * 		const BasicString<T> &str)
		 *
		 * @brief	Replaces a section of the current string by some other content.
		 *
		 * @date	2.10.2011
		 *
		 * @param	offset	The offset.
		 * @param	count 	Length of the section to be replaced.
		 * @param	str   	The string.
		 *
		 * @return	New string.
		 */
		inline BasicString<T> Replace(int offset, int count, const BasicString<T> &str)
		{
			m_String.replace(offset, count, str.m_String);
		}

		//////////////////////////////////////////////////////////////////////////
		// TRIM
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline BasicString<T> &BasicString::TrimBegin(const BasicString<T> &whitespaces = Whitespaces)
		 *
		 * @brief	Trims string from begin.
		 *
		 * @date	2.10.2011
		 *
		 * @param	whitespaces	(optional) the whitespaces.
		 *
		 * @return	A reference to this object.
		 */
		inline BasicString<T> &TrimBegin(const BasicString<T> &whitespaces = Whitespaces)
		{
			return Internal_TrimBegin(whitespaces);
		}

		/**
		 * @fn	inline BasicString<T> &BasicString::TrimEnd(const BasicString<T> &whitespaces = Whitespaces)
		 *
		 * @brief	Trims string from the end.
		 *
		 * @date	2.10.2011
		 *
		 * @param	whitespaces	(optional) the whitespaces.
		 *
		 * @return	A reference to this object.
		 */
		inline BasicString<T> &TrimEnd(const BasicString<T> &whitespaces = Whitespaces)
		{
			return Internal_TrimEnd(whitespaces);
		}

		/**
		 * @fn	inline BasicString<T> &BasicString::Trim(const BasicString<T> &whitespaces = Whitespaces)
		 *
		 * @brief	Trims edges of the string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	whitespaces	(optional) the whitespaces.
		 *
		 * @return	A reference to this object.
		 */
		inline BasicString<T> &Trim(const BasicString<T> &whitespaces = Whitespaces)
		{
			return Internal_Trim(whitespaces);
		}

		//////////////////////////////////////////////////////////////////////////
		// LEFT/RIGHT
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline BasicString<T> BasicString::Left(int count) const
		 *
		 * @brief	Returns a substring from begin to the specified length.
		 *
		 * @date	2.10.2011
		 *
		 * @param	count	Amount of characters.
		 *
		 * @return	Substring.
		 */
		inline BasicString<T> Left(int count) const
		{
			return Substring(0, count);
		}

		/**
		 * @fn	inline BasicString<T> BasicString::Right(int count) const
		 *
		 * @brief	Returns a substring from the end
		 * 			to the specified length (way to begin).
		 *
		 * @date	2.10.2011
		 *
		 * @param	count	Amount of characters.
		 *
		 * @return	Substring.
		 */
		inline BasicString<T> Right(int count) const
		{
			return Substring(Length() - count);
		}

		/**
		 * @fn	inline bool BasicString::StartsWith(const BasicString &str,
		 * 		Case caseType = NoticeCase) const
		 *
		 * @brief	Query if this string starts with specified substring.
		 *
		 * @date	2.10.2011
		 *
		 * @param	str			The string.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	true if it succeeds, false if it fails.
		 */
		inline bool StartsWith(const BasicString &str, Case caseType = NoticeCase) const
		{
			return Left(str.Length()).EqualsTo(str, caseType);
		}

		/**
		 * @fn	inline bool BasicString::EndsWith(const BasicString &str,
		 * 		Case caseType = NoticeCase) const
		 *
		 * @brief	Query if this string ends with specified substring.
		 *
		 * @date	2.10.2011
		 *
		 * @param	str			The string.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	true if it succeeds, false if it fails.
		 */
		inline bool EndsWith(const BasicString &str, Case caseType = NoticeCase) const
		{
			return Right(str.Length()).EqualsTo(str, caseType);
		}
		
		//////////////////////////////////////////////////////////////////////////
		// TO UPPER/TO LOWER
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	BasicString &BasicString::ToLower()
		 *
		 * @brief	Converts all characters in string to lowercase.
		 *
		 * @date	2.10.2011
		 *
		 * @return	A reference to this object.
		 */
		BasicString &ToLower()
		{
			std::transform(m_String.begin(), m_String.end(), m_String.begin(),
				::tolower);
			return (*this);
		}

		/**
		 * @fn	BasicString &BasicString::ToUpper()
		 *
		 * @brief	Converts all characters in string to uppercase.
		 *
		 * @date	2.10.2011
		 *
		 * @return	A reference to this object.
		 */
		BasicString &ToUpper()
		{
			std::transform(m_String.begin(), m_String.end(), m_String.begin(),
				::toupper);
			return (*this);
		}

		//////////////////////////////////////////////////////////////////////////
		// IS* & CONVERT
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	inline bool BasicString::IsNumeric() const
		 *
		 * @brief	Query if this string represents a number.
		 *
		 * @date	2.10.2011
		 *
		 * @return	true if represents number, false if not.
		 */
		inline bool IsNumeric() const
		{
			double i;
			return (sscanf_s(m_String.c_str(), "%lf", &i) == 1);
		}

		/**
		 * @fn	inline bool BasicString::IsHex() const
		 *
		 * @brief	Query if this string represents hexadecimal number.
		 *
		 * @date	2.10.2011
		 *
		 * @return	true if represents hexadecimal number, false if not.
		 */
		inline bool IsHex() const 
		{
			char c;

			for (int i = 0; i < m_String.length(); i++)
			{
				if (!isxdigit(m_String.at(i)) &&
					m_String.at(i) != Whitespaces[4] && // '\n'
					m_String.at(i) != Whitespaces[5])	// '\r'
				{
					return false;
				}
				else
				{
					
				}
			}

			return true;
		}
		
		/**
		 * @fn	inline bool BasicString::IsEmpty() const
		 *
		 * @brief	Query if this object is empty.
		 *
		 * @date	2.10.2011
		 *
		 * @return	true if empty, false if not.
		 */
		inline bool IsEmpty() const
		{
			return m_String.empty();
		}

		/**
		 * @fn	inline bool BasicString::IsValid() const
		 *
		 * @brief	Query if this string is valid.
		 *
		 * @date	3.10.2011
		 *
		 * @return	true if valid, false if not.
		 */
		inline bool IsValid() const
		{
			return m_bValid;
		}

		/**
		 * @fn	template <class T3> inline T BasicString::Convert() const
		 *
		 * @brief	Converts this string to a specified type.
		 *
		 * @date	2.10.2011
		 *
		 * @return	The string value in specified type.
		 */
		template <class T3>
		inline T3 Convert() const;

		/**
		 * @fn	template <> inline bool BasicString::Convert<bool>() const
		 *
		 * @brief	Converts this string to a bool.
		 *
		 * @date	2.10.2011
		 *
		 * @tparam	bool	Type of the bool.
		 *
		 * @return	true if it string contains "true", false if not.
		 */
		template <>
		inline bool Convert<bool>() const
		{
			return (m_String == _TrueStr);
		}

		/**
		 * @fn	template <> inline T BasicString::Convert<T>() const
		 *
		 * @brief	Converts this object to a specified character type (char or wchar_t).
		 *
		 * @date	2.10.2011
		 *
		 * @tparam	T	Generic type parameter, representing a character.
		 *
		 * @return	The first character in this string.
		 */
		template <>
		inline T Convert<T>() const
		{
			return m_String[0];
		}

		/**
		 * @fn	template <> inline const T *Convert<const T *>() const
		 *
		 * @brief	Converts this object to a const character (char or wchar_t) pointer type.
		 *
		 * @date	2.10.2011
		 *
		 * @tparam	const T*	Type of the const character pointer.
		 *
		 * @return	The pointer to the zero-terminated string.
		 */
		template <>
		inline const T *Convert<const T *>() const
		{
			return m_String.c_str();
		}

		/**
		 * @fn	template <> inline int BasicString::Convert<int>() const
		 *
		 * @brief	Converts this object to a signed int.
		 *
		 * @date	2.10.2011
		 *
		 * @tparam	int	Type of the int.
		 *
		 * @return	This string as a signed int.
		 */
		template <>
		inline int Convert<int>() const
		{
			return static_cast<int>(ToDouble());
		}

		/**
		 * @fn	template <> inline unsigned int Convert<unsigned int>() const
		 *
		 * @brief	Converts this object to an unsigned int.
		 *
		 * @date	2.10.2011
		 *
		 * @tparam	unsigned int	Type of the unsigned int.
		 *
		 * @return	This string as an unsigned int.
		 */
		template <>
		inline unsigned int Convert<unsigned int>() const
		{
			return static_cast<unsigned int>(ToDouble());
		}

		/**
		 * @fn	template <> inline double BasicString::Convert<double>() const
		 *
		 * @brief	Converts this string to a double.
		 *
		 * @date	2.10.2011
		 *
		 * @tparam	double	Type of the double.
		 *
		 * @return	This string as a double.
		 */
		template <>
		inline double Convert<double>() const
		{
			double i = 0.0;

			sscanf_s(ToAnsi().RefString().c_str(), "%lf", &i);

			return i;
		}

		/**
		 * @fn	template <> inline std::basic_string<T> Convert< std::basic_string<T> >() const
		 *
		 * @brief	Converts this string to a std::(w)string.
		 *
		 * @date	2.10.2011
		 *
		 * @tparam	std::basic_string<T>	Standard string type.
		 *
		 * @return	This string as a std::(w)string.
		 */
		template <>
		inline std::basic_string<T> Convert< std::basic_string<T> >() const
		{
			return m_String;
		}

		/**
		 * @fn	template <> inline std::basic_string<T> &Convert< std::basic_string<T> &>() const
		 *
		 * @brief	Returns a reference to the internal std::(w)string.
		 *
		 * @date	2.10.2011
		 *
		 * @tparam	std::basic_string<T> &	Reference to a standard string type.
		 *
		 * @return	Reference to the internal std::(w)string.
		 */
		template <>
		inline std::basic_string<T> &Convert<std::basic_string<T> &>() const
		{
			return m_String;
		}

		/**
		 * @fn	inline const T BasicString::*CharArray() const
		 *
		 * @brief	Gets a null-terminated sequence of characters
		 * 			with the same content as the string object.
		 *
		 * @date	2.10.2011
		 *
		 * @return	Pointer to the array of characters.
		 */
		inline const T *CharArray() const
		{
			return m_String.c_str();
		}

		/**
		 * @fn	inline T BasicString::*GetTempBuffer(int n)
		 *
		 * @brief	Gets temporary buffer for a string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	n	The count of space for characters to allocate.
		 *
		 * @return	null if it fails, else the temporary buffer.
		 */
		inline T *GetTempBuffer(int n)
		{
			// sooo dirty direct access

			m_String.clear();
			m_String.reserve(n+1);

			return const_cast<T *>(m_String.data());
		}

		/**
		 * @fn	inline BasicString<T> &BasicString::ApplyTempBuffer()
		 *
		 * @brief	Applies the temporary buffer.
		 *
		 * @date	2.10.2011
		 *
		 * @return	Reference to this object.
		 */
		inline BasicString<T> &ApplyTempBuffer()
		{
			m_String._Mysize = Internal_StrLen(m_String.data());
			return (*this);
		}

		/**
		 * @fn	BasicString<wchar_t> BasicString::ToUnicode() const;
		 *
		 * @brief	Converts this string to a Unicode string.
		 *
		 * @date	2.10.2011
		 *
		 * @return	This object as a WString.
		 */
		BasicString<wchar_t> ToUnicode() const;

		/**
		 * @fn	BasicString<char> BasicString::ToAnsi() const;
		 *
		 * @brief	Converts this object to an ANSI string.
		 *
		 * @date	2.10.2011
		 *
		 * @return	This object as a String
		 */
		BasicString<char> ToAnsi() const;

		//////////////////////////////////////////////////////////////////////////
		// MISCELLANEOUS METHODS...
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	template <class T2> inline BasicString<T> &BasicString::AddLine(T2 s)
		 *
		 * @brief	Adds a line to the string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	The line to add.
		 *
		 * @return	A reference to this object.
		 */
		template <class T2>
		inline BasicString<T> &AddLine(T2 s)
		{
			operator+=(s);
			operator+=(Whitespaces[5]); // '\r'
			operator+=(Whitespaces[4]); // '\n'

			return (*this);
		}

		/**
		 * @fn	template <class T2> inline bool BasicString::Contains(T2 s,
		 * 		Case caseType = NoticeCase) const
		 *
		 * @brief	Query if this object contains the given string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s			The needle.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	true if the needle is in this string, false if not.
		 */
		template <class T2>
		inline bool Contains(T2 s, Case caseType = NoticeCase) const
		{
			int tmp = Find(s, caseType);
			return (tmp > 0 && tmp != NoPosition);
		}

		/**
		 * @fn	template <class T2> inline BasicString<T> &BasicString::Insert(int offset, const T2 str)
		 *
		 * @brief	Inserts string into the specified offset.
		 *
		 * @date	2.10.2011
		 *
		 * @param	offset	The offset.
		 * @param	str   	The string.
		 *
		 * @return	A reference to this object.
		 */
		template <class T2>
		inline BasicString<T> &Insert(int offset, const T2 str)
		{
			m_String.insert(offset, str);
			return (*this);
		}

		/**
		 * @fn	inline void BasicString::Clear()
		 *
		 * @brief	Clears this string.
		 *
		 * @date	2.10.2011
		 */
		inline void Clear()
		{
			m_String.clear();
		}

		/**
		 * @fn	inline void BasicString::Swap(BasicString<T> &str)
		 *
		 * @brief	Swaps two strings.
		 *
		 * @date	2.10.2011
		 *
		 * @param [in,out]	str	The string.
		 */
		inline void Swap(BasicString<T> &str)
		{
			bool tmp = m_bValid;
			str.m_bValid = tmp;
			m_bValid = tmp;
			m_String.swap(str.m_String);
		}

		/**
		 * @fn	inline int BasicString::CountOf(const BasicString<T> &s, Case caseType = NoticeCase) const
		 *
		 * @brief	Count of substrings in this string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s			The needle.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	The total number of corresponding matches.
		 */
		inline int CountOf(const BasicString<T> &s, Case caseType = NoticeCase) const
		{
			return Internal_CountOf(s, caseType);
		}

		/**
		 * @fn	inline int BasicString::CountOf(T s, Case caseType = NoticeCase) const
		 *
		 * @brief	Count of substrings in this string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s			The needle.
		 * @param	caseType	(optional) casing type.
		 *
		 * @return	The total number of corresponding matches.
		 */
		inline int CountOf(T s, Case caseType = NoticeCase) const
		{
			int result = 0;

			if (caseType == NoticeCase)
			{
				for (int i = 0; i < this->Length(); i++)
				{
					if (m_String[i] == s)
						result++;
				}
			}
			else
			{
				for (int i = 0; i < this->Length(); i++)
				{
					if (std::tolower(m_String[i]) == std::tolower(s))
						result++;
				}
			}

			return result;
		}

		/**
		 * @fn	inline BasicString<T> BasicString::Substring(int offset = 0, int count = NoPosition) const
		 *
		 * @brief	Return a substring from this string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	offset	(optional) the offset.
		 * @param	count 	(optional) number of.
		 *
		 * @return	A substringed string.
		 */
		inline BasicString<T> Substring(int offset = 0, int count = NoPosition) const
		{
			return BasicString<T>(m_String.substr(offset, count));
		}

		/**
		 * @fn	inline void BasicString::Reverse()
		 *
		 * @brief	Reverses this string.
		 *
		 * @date	2.10.2011
		 */
		inline void Reverse()
		{
			std::reverse(m_String.begin(), m_String.end());
		}

		/**
		 * @fn	inline void BasicString::Shuffle()
		 *
		 * @brief	Shuffles the characters in this string.
		 *
		 * @date	2.10.2011
		 */
		inline void Shuffle()
		{
			std::random_shuffle(m_String.begin(), m_String.end(),
				[](int i) { return rand()%i; });
		}

		/**
		 * @fn	inline bool BasicString::Reserve(int count)
		 *
		 * @brief	Reserves a specified amount of space for the string.
		 *
		 * @date	2.10.2011
		 *
		 * @param	count	New buffer length.
		 */
		inline void Reserve(int count)
		{
			m_String.reserve(count);
		}

		/**
		 * @fn	inline int BasicString::Length() const
		 *
		 * @brief	Gets the length of this string
		 *
		 * @date	2.10.2011
		 *
		 * @return	The length in characters.
		 */
		inline int Length() const
		{
			return m_String.length();
		}

		/**
		 * @fn	inline int BasicString::LinesCount() const
		 *
		 * @brief	Gets the count of lines in this string.
		 *
		 * @date	2.10.2011
		 *
		 * @return	The count of lines.
		 */
		inline int LinesCount() const
		{
			return CountOf(Whitespaces[4] /* '\n' */ );
		}

		/**
		 * @fn	static BasicString<T> BasicString::Format(const T *format, ...);
		 *
		 * @brief	Creates new string from specified printf-like format.
		 *
		 * @date	2.10.2011
		 *
		 * @param	format	Describes the format to use.
		 */
		static BasicString<T> Format(const T *format, ...);

		/**
		 * @fn	Collection BasicString::Split(T separator = Whitespaces[0],
		 * 		StringSplitOptions options = StringSplitOptions::None, int count = -1) const
		 *
		 * @brief	Splits this string.
		 *
		 * @date	3.10.2011
		 *
		 * @param	separator	(optional) the separator.
		 * @param	options  	(optional) options for controlling the operation.
		 * @param	count	 	(optional) number of maximum substrings to return.
		 *
		 * @return	Collection of substrings.
		 */
		Collection Split(T separator = Whitespaces[0],
			StringSplitOptions options = StringSplitOptions::None, int count = -1) const
		{
			int l_iPosition = 0,
				l_iLastPosition = 0;
			Collection l_oResult;
			
			while (l_iPosition != NoPosition)
			{
				l_iPosition = this->Find(separator, NoticeCase, l_iLastPosition);

				BasicString<T> l_sMatch = this->Substring(l_iLastPosition, (l_iPosition - l_iLastPosition));

				if (!(l_sMatch.IsEmpty() && options == StringSplitOptions::RemoveEmptyEntries))
					l_oResult.Add(l_sMatch);

				l_iLastPosition = l_iPosition + 1;

				if (count != -1 && l_oResult.Count() == count)
					break;
			}

			return l_oResult;
		}

		/**
		 * @fn	Collection BasicString::Split(const BasicString<T> &separator = Whitespaces[0],
		 * 		StringSplitOptions options = StringSplitOptions::None, int count = -1) const
		 *
		 * @brief	Splits this string.
		 *
		 * @date	3.10.2011
		 *
		 * @param	separator	(optional) the separator.
		 * @param	options  	(optional) options for controlling the operation.
		 * @param	count	 	(optional) number of maximum substrings to return.
		 *
		 * @return	Collection of substrings.
		 */
		Collection Split(const BasicString<T> &separator = Whitespaces[0],
			StringSplitOptions options = StringSplitOptions::None, int count = -1) const
		{
			int l_iPosition = 0,
				l_iLastPosition = 0;
			Collection l_oResult;

			while (l_iPosition != NoPosition)
			{
				l_iPosition = this->Find(separator, NoticeCase, l_iLastPosition);

				BasicString<T> l_sMatch = this->Substring(l_iLastPosition, (l_iPosition - l_iLastPosition));

				if (!(l_sMatch.IsEmpty() && options == StringSplitOptions::RemoveEmptyEntries))
					l_oResult.Add(l_sMatch);

				l_iLastPosition = l_iPosition + separator.Length();

				if (count != -1 && l_oResult.Count() == count)
					break;
			}

			return l_oResult;
		}

		/**
		 * @fn	Collection BasicString::Split(const Array<T> &separators,
		 * 		StringSplitOptions options = StringSplitOptions::None, int count = -1) const
		 *
		 * @brief	Splits this string.
		 *
		 * @date	3.10.2011
		 *
		 * @param	separators	Separators.
		 * @param	options  	(optional) options for controlling the operation.
		 * @param	count	 	(optional) number of maximum substrings to return.
		 *
		 * @return	Collection of substrings.
		 */
		Collection Split(const Array<T> &separators,
			StringSplitOptions options = StringSplitOptions::None, int count = -1) const
		{
			int l_iPosition = 0,
				l_iLastPosition = 0;
			Collection l_oResult;

			while (l_iPosition != NoPosition)
			{
				// HACK: Array.Data() returns pointer to array of elements
				// + FindFirstOf returns first occurrence of any character
				// in the needle.
				l_iPosition = this->FindFirstOf(separators.Data(), NoticeCase, l_iLastPosition);

				BasicString<T> l_sMatch = this->Substring(l_iLastPosition, (l_iPosition - l_iLastPosition));

				if (!(l_sMatch.IsEmpty() && options == StringSplitOptions::RemoveEmptyEntries))
					l_oResult.Add(l_sMatch);

				l_iLastPosition = l_iPosition + 1;

				if (count != -1 && l_oResult.Count() == count)
					break;
			}

			return l_oResult;
		}

		/**
		 * @fn	Collection BasicString::Split(const Array< BasicString<T> > &separators,
		 * 		StringSplitOptions options = StringSplitOptions::None, int count = -1) const
		 *
		 * @brief	Splits this string.
		 *
		 * @date	3.10.2011
		 *
		 * @param	separators	Separators.
		 * @param	options  	(optional) options for controlling the operation.
		 * @param	count	 	(optional) number of maximum substrings to return.
		 *
		 * @return	Collection of substrings.
		 */
		Collection Split(const Array< BasicString<T> > &separators,
			StringSplitOptions options = StringSplitOptions::None, int count = -1) const
		{
			// TODO: optimize

			int l_iPosition = 0,
				l_iTmpPosition = 0,
				l_iLastPosition = 0;
			Collection l_oResult;

			while (l_iPosition != NoPosition)
			{
				int i = 0,	// Iterator.
					l_iAdditionalSeparatorLength = 0;	// Space to add to Find method
														// for overjump found substring.

				do 
				{
					// Just put into the temporary variable, due of finding
					// the minimal substring position from the separators array.
					l_iTmpPosition = this->Find(separators[i], NoticeCase, l_iLastPosition);
					
					// First loop - set the l_iPosition value.
					if (i == 0)
					{
						l_iPosition = l_iTmpPosition;
						l_iAdditionalSeparatorLength = separators[i].Length();
					}
					
					// If current separator was found.
					if (l_iTmpPosition != NoPosition)
					{
						// If any of separators was not found yet, this is our
						// initial position.
						if (l_iPosition == NoPosition)
						{
							l_iPosition = l_iTmpPosition;
							l_iAdditionalSeparatorLength = separators[i].Length();
						}
						else
						{
							// If any of separators was found yet, compare the actual
							// position with l_iPosition, and the smallest value
							// set to the l_iPosition variable.
							l_iPosition = min(l_iTmpPosition, l_iPosition);
							
							// If l_iPosition was changed, refresh
							// the l_iAdditionalSeparatorLength variable.
							if (l_iPosition == l_iTmpPosition)
								l_iAdditionalSeparatorLength = separators[i].Length();
						}
					}
				} while (++i != separators.Count());

				BasicString<T> l_sMatch = this->Substring(l_iLastPosition, (l_iPosition - l_iLastPosition));

				if (!(l_sMatch.IsEmpty() && options == StringSplitOptions::RemoveEmptyEntries))
					l_oResult.Add(l_sMatch);

				l_iLastPosition = l_iPosition + l_iAdditionalSeparatorLength;

				if (count != -1 && l_oResult.Count() == count)
					break;
			}

			return l_oResult;
		}

		/**
		 * @fn	inline Tokenizer BasicString::GetTokenizer(const BasicString<T> &separator = ' ',
		 * 		StringSplitOptions options = StringSplitOptions::None) const
		 *
		 * @brief	Gets a tokenizer.
		 *
		 * @date	3.10.2011
		 *
		 * @param	separator	(optional) the separator.
		 * @param	options  	(optional) options for controlling the operation.
		 *
		 * @return	The tokenizer.
		 */
		inline Tokenizer GetTokenizer(const BasicString<T> &separator = T(' '),
			StringSplitOptions options = StringSplitOptions::None) const
		{
			return Tokenizer(*this, separator, options);
		}

		/**
		 * @fn	inline Tokenizer BasicString::GetTokenizer(const Array< BasicString<T> > &separators,
		 * 		StringSplitOptions options = StringSplitOptions::None) const
		 *
		 * @brief	Gets a tokenizer.
		 *
		 * @date	3.10.2011
		 *
		 * @param	separators	The separators.
		 * @param	options   	(optional) options for controlling the operation.
		 *
		 * @return	The tokenizer.
		 */
		inline Tokenizer GetTokenizer(const Array< BasicString<T> > &separators,
			StringSplitOptions options = StringSplitOptions::None) const
		{
			return Tokenizer(*this, separators, options);
		}

	protected:
		// Members.
		std::basic_string<T> m_String;	///< Internal string.
		static const T *_TrueStr;		///< The value representing "true".
		bool m_bValid;					///< Value representing if this string is valid.

		//////////////////////////////////////////////////////////////////////////
		// INTERNAL METHODS...
		//////////////////////////////////////////////////////////////////////////

		/**
		 * @fn	BasicString<T> &BasicString::Internal_TrimBegin(const BasicString<T> &whitespaces)
		 *
		 * @brief	Internal TrimBegin method.
		 *
		 * @date	2.10.2011
		 *
		 * @param	whitespaces	The whitespaces.
		 *
		 * @return	A reference to this object.
		 */
		BasicString<T> &Internal_TrimBegin(const BasicString<T> &whitespaces)
		{
			int pos;

			pos = m_String.find_first_not_of(whitespaces.CharArray());

			if (pos != NoPosition)
				m_String = m_String.substr(pos);
			else
				m_String.clear();

			return (*this);
		}

		/**
		 * @fn	BasicString<T> &BasicString::Internal_TrimEnd(const BasicString<T> &whitespaces)
		 *
		 * @brief	Internal TrimEnd method.
		 *
		 * @date	2.10.2011
		 *
		 * @param	whitespaces	The whitespaces.
		 *
		 * @return	A reference to this object.
		 */
		BasicString<T> &Internal_TrimEnd(const BasicString<T> &whitespaces)
		{
			int pos;

			pos = m_String.find_last_not_of(whitespaces.CharArray());

			if (pos != NoPosition)
				m_String = m_String.erase(pos + 1);
			else
				m_String.clear();

			return (*this);
		}

		/**
		 * @fn	BasicString<T> &BasicString::Internal_Trim(const BasicString<T> &whitespaces)
		 *
		 * @brief	Internal Trim method.
		 *
		 * @date	2.10.2011
		 *
		 * @param	whitespaces	The whitespaces.
		 *
		 * @return	A reference to this object.
		 */
		BasicString<T> &Internal_Trim(const BasicString<T> &whitespaces)
		{
			return TrimBegin(whitespaces).TrimEnd(whitespaces);
		}

		/**
		 * @fn	int BasicString::Internal_StrReplace(const BasicString<T> &search,
		 * 		const BasicString<T> &replace, Case caseType)
		 *
		 * @brief	Internal StrReplace method.
		 *
		 * @date	2.10.2011
		 *
		 * @param	search  	The search.
		 * @param	replace 	The replace.
		 * @param	caseType	The casing type.
		 *
		 * @return	Count of replacements.
		 */
		int Internal_StrReplace(const BasicString<T> &search, const BasicString<T> &replace, Case caseType)
		{
			BasicString<T> buffer;

			int sealeng = search.Length(),
				strleng = m_String.length(),
				counter = 0;

			if (sealeng == 0)
				return 0;

			for (int i = 0, j = 0; i<strleng; j = 0)
			{
				if (caseType == NoticeCase)
				{
					while (i+j < strleng &&
						   j < sealeng &&
						   m_String[i+j] == search[j])
					{
						j++;
					}
				}
				else
				{
					while (i+j < strleng &&
						   j < sealeng &&
						   std::tolower(m_String[i+j]) == std::tolower(search[j]))
					{
						j++;
					}
				}

				if (j == sealeng)
				{
					buffer.m_String.append(replace.m_String);
					i += sealeng;
					counter++;
				}
				else
				{
					buffer.m_String.append(&m_String[i++], 1);
				}
			}

			m_String = buffer.m_String;

			return counter;
		}

		/**
		 * @fn	int BasicString::Internal_CountOf(const BasicString &needle,
		 * 		Case caseType = NoticeCase) const
		 *
		 * @brief	Internal CountOf method.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle  	The needle.
		 * @param	caseType	(optional) case type.
		 *
		 * @return	Count of corresponding matches.
		 */
		int Internal_CountOf(const BasicString &needle, Case caseType = NoticeCase) const
		{
			int sealeng = needle.Length(),
				strleng = m_String.length(),
				counter = 0;

			if (sealeng == 0)
				return 0;

			for (int i = 0, j = 0; i<strleng; j = 0)
			{
				if (caseType == NoticeCase)
				{
					while (i+j < strleng &&
						   j < sealeng &&
						   m_String[i+j] == needle[j])
					{
						j++;
					}
				}
				else
				{
					while (i+j < strleng &&
						   j < sealeng &&
						   std::tolower(m_String[i+j]) == std::tolower(needle[j]))
					{
						j++;
					}
				}

				if (j == sealeng)
					counter++, i += sealeng;
				else
					i++;
			}

			return counter;
		}

		/**
		 * @fn	template<class TNeedle, class TMethod> int BasicString::Internal_FindAndIgnoreCase(TNeedle needle,
		 * 		int offset, TMethod method) const
		 *
		 * @brief	Internal Find* method regardless of the case.
		 *
		 * @date	2.10.2011
		 *
		 * @param	needle	The needle.
		 * @param	offset	The offset.
		 * @param	method	The method.
		 *
		 * @return	Position of the match.
		 */
		template<class TNeedle, class TMethod>
		int Internal_FindAndIgnoreCase(TNeedle needle, int offset, TMethod method) const
		{
			BasicString lowerNeedle = needle,
				lowerThis = (*this);

			lowerNeedle.ToLower();
			lowerThis.ToLower();

			return (lowerThis.m_String.*(method))(lowerNeedle.Convert<TNeedle>(), offset);
		}

		/**
		 * @fn	bool BasicString::Internal_EqualsToIgnoreCase(const BasicString<T> &s) const
		 *
		 * @brief	Internal EqualsTo method regardless of the case.
		 *
		 * @date	2.10.2011
		 *
		 * @param	s	The string to compare.
		 *
		 * @return	true if equals, false if not.
		 */
		bool Internal_EqualsToIgnoreCase(const BasicString<T> &s) const
		{
			BasicString ls1 = (*this),
				ls2 = s;

			ls1.ToLower();
			ls2.ToLower();

			return (ls1.m_String == ls2.m_String);
		}


		/**
		 * @fn	int BasicString::Internal_StrLen(const T *str) const;
		 *
		 * @brief	Internal StrLen method.
		 *
		 * @date	2.10.2011
		 *
		 * @param	str	The string.
		 *
		 * @return	The length of string.
		 */
		int Internal_StrLen(const T *str) const;
};

//////////////////////////////////////////////////////////////////////////
// TYPE DEFINITIONS
//////////////////////////////////////////////////////////////////////////

/**
 * @typedef	BasicString<char> String
 *
 * @brief	Defines an alias representing the ANSI string.
 */
typedef BasicString<char>		String;

/**
 * @typedef	BasicString<wchar_t> WString
 *
 * @brief	Defines an alias representing the Unicode string.
 */
typedef BasicString<wchar_t>	WString;

/**
 * @typedef	BasicString<TCHAR> string_t
 *
 * @brief	Defines an alias representing the string type
 * 			depending on UNICODE define.
 */
typedef BasicString<TCHAR>		string_t;

/**
 * @typedef	string_t string
 *
 * @brief	Defines an alias representing the string type
 * 			depending on UNICODE define.
 */
typedef string_t				string;

} // namespace

