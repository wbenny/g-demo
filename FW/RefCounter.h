/**
 * @file	fw/RefCounter.h
 *
 * @brief	Declares the RefCounter class.
 */

#pragma once
#include "Types.h"

namespace FW {

/**
 * @class	RefCounter
 *
 * @brief	Reference counter. 
 *
 * @date	30.9.2011
 */
class RefCounter
{
	public:
		/**
		 * @fn	inline RefCounter::RefCounter()
		 *
		 * @brief	Default constructor.
		 *
		 * @date	30.9.2011
		 */
		inline RefCounter() : 
			m_iRefCount(0)
		{
			
		}

		/**
		 * @fn	inline void RefCounter::Ref() const
		 *
		 * @brief	References this object.
		 *
		 * @date	30.9.2011
		 */
		inline void Ref() const
		{
			++m_iRefCount;
		}

		/**
		 * @fn	inline void RefCounter::Unref() const
		 *
		 * @brief	Dereferences this object. Deletes if reference
		 * 			count is 0.
		 *
		 * @date	30.9.2011
		 */
		inline void Unref() const
		{
			if ((--m_iRefCount) <= 0)
				delete this;
		}

		/**
		 * @fn	inline s32 RefCounter::GetRefCount() const
		 *
		 * @brief	Gets the reference count.
		 *
		 * @date	30.9.2011
		 *
		 * @return	The reference count.
		 */
		inline s32 GetRefCount() const
		{
			return m_iRefCount;
		}

	protected:
		/**
		 * @fn	virtual RefCounter::~RefCounter()
		 *
		 * @brief	Destructor.
		 *
		 * @date	30.9.2011
		 */
		inline virtual ~RefCounter()
		{
		
		}

	private:
		// Members.
		mutable s32 m_iRefCount;	///< Number of references.
};

} // namespace
