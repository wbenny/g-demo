/**
 * @file	fw/List.h
 *
 * @brief	Declares the list class.
 */

#pragma once
#include "Assert.h"
#include "IList.h"
#include <vector>
#include <algorithm>

namespace FW {

/**
 * @class	List<T>
 *
 * @brief	Represents a non-generic collection
 * 			of objects that can be individually accessed by index.
 *
 * @date	1.10.2011
 */
template<class T>
class List : public IList<T>
{
	public:
		/**
		 * @fn	List::List()
		 *
		 * @brief	Default constructor.
		 *
		 * @date	1.10.2011
		 */
		List()
		{

		}

		/**
		 * @fn	List::List(const List<T>& list)
		 *
		 * @brief	Copy constructor.
		 *
		 * @date	1.10.2011
		 *
		 * @param	list	The list to construct from.
		 */
		List(const List<T>& list)
		{
			m_Vector = list.m_Vector;
		}

		/**
		 * @fn	List<T> &List::operator= (List<T>& list)
		 *
		 * @brief	Assignment operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param [in,out]	list	The list to copy from.
		 *
		 * @return	A reference to this object.
		 */
		List<T> &operator = (List<T>& list)
		{
			m_Vector = list.m_Vector;
			return *this;
		}

		/**
		 * @fn	void List::Add(const T& object)
		 *
		 * @brief	Adds object..
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to add.
		 */
		void Add(const T& object)
		{
			m_Vector.push_back(object);
		}

		/**
		 * @fn	void List::Clear()
		 *
		 * @brief	Clears this object to its blank state.
		 *
		 * @date	1.10.2011
		 */
		void Clear()
		{
			m_Vector.clear();
		}

		/**
		 * @fn	void List::Sort(FnCmp compareFunc) 
		 *
		 * @brief	Sorts elements in list.
		 *
		 * @date	7.2.2012
		 *
		 * @param	compareFunc	The compare function.
		 */
		template <class FnCmp>
		void Sort(FnCmp compareFunc)
		{
			if (m_Vector.size() < 2)	// Sorting one element does not make any sense.
				return;

			std::sort(m_Vector.begin(), m_Vector.end(), compareFunc);
		}

		/**
		 * @fn	void List::CopyTo(Array<T>* array, u32 index = 0) const
		 *
		 * @brief	Copies this list to an array.
		 *
		 * @date	1.10.2011
		 *
		 * @param [in,out]	array	The array.
		 * @param	index		 	(optional) zero-based index of the first element
		 * 							in the array.
		 */
		void CopyTo(Array<T>* array, u32 index = 0) const
		{
			FW_ASSERT(array != NULL);
			FW_ASSERT(array->Count() <= static_cast<u32>(m_Vector.size()), "List is larger than Array");

			array->FromArray(m_Vector.data(), m_Vector.size(), index);
		}

		/**
		 * @fn	void List::RemoveAt(u32 index)
		 *
		 * @brief	Removes object at described index.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 */
		void RemoveAt(u32 index)
		{
			if (index >= static_cast<u32>(m_Vector.size()))
				return;

			m_Vector.erase(m_Vector.begin() + index);
		}

		/**
		 * @fn	T& List::At(u32 index)
		 *
		 * @brief	Return object at specified position.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		T& At(u32 index)
		{
			return m_Vector.at(static_cast<std::vector<T>::size_type>(index));
		}

		/**
		 * @fn	const T& List::At(u32 index) const
		 *
		 * @brief	Return object at specified position.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		const T& At(u32 index) const
		{
			return m_Vector.at(static_cast<std::vector<T>::size_type>(index));
		}

		/**
		 * @fn	T &List::operator[](u32 index)
		 *
		 * @brief	Array indexer operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		T& operator[](u32 index)
		{
			return At(index);
		}

		/**
		 * @fn	const T& List::operator[](u32 index) const
		 *
		 * @brief	Array indexer operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		const T& operator[](u32 index) const
		{
			return At(index);
		}

		/**
		 * @fn	u32 List::GetCount() const
		 *
		 * @brief	Gets the number of elements contained in the list.
		 *
		 * @date	1.10.2011
		 *
		 * @return	Count of elements.
		 */
		u32 GetCount() const
		{
			return m_Vector.size();
		}

		/**
		 * @fn	T* List::GetDataPointer()
		 *
		 * @brief	Gets the data stream.
		 *
		 * @date	1.10.2011
		 *
		 * @return	Pointer to the data.
		 */
		T* GetDataPointer()
		{
			return const_cast<T*>(m_Vector.data());
		}

		/**
		 * @fn	const T* List::GetDataPointer() const
		 *
		 * @brief	Gets the data stream.
		 *
		 * @date	1.10.2011
		 *
		 * @return	Pointer to the data.
		 */
		const T* GetDataPointer() const
		{
			return m_Vector.data();
		}

		void Reserve(u32 count)
		{
			m_Vector.reserve(static_cast<std::vector<T>::size_type>(count));
		}

		void Resize(u32 count, T e = T())
		{
			m_Vector.resize(static_cast<std::vector<T>::size_type>(count), e);
		}

	protected:
		// Members.
		std::vector<T> m_Vector; ///< Vector.
};

} // namespace
