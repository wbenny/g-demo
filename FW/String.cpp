#include "String.h"

namespace FW {

//////////////////////////////////////////////////////////////////////////
// WHITESPACES STRING DEFINITIONS
//////////////////////////////////////////////////////////////////////////

template <> const char *BasicString<char>::Whitespaces = " \t\f\v\n\r";
template <> const wchar_t *BasicString<wchar_t>::Whitespaces = L" \t\f\v\n\r";
template <> const char *BasicString<char>::_TrueStr = "true";
template <> const wchar_t *BasicString<wchar_t>::_TrueStr = L"true";

//////////////////////////////////////////////////////////////////////////
// FROM-NUMBER CONSTRUCTORS
//////////////////////////////////////////////////////////////////////////

template <>
BasicString<char>::BasicString(signed int s) :
	m_bValid(true)
{
	char tmpBuff[32];
	sprintf_s(tmpBuff, "%i", s);
	m_String = tmpBuff;
}

template <>
BasicString<wchar_t>::BasicString(signed int s) :
	m_bValid(true)
{
	wchar_t tmpBuff[32];
	swprintf_s(tmpBuff, L"%i", s);
	m_String = tmpBuff;
}

template <>
BasicString<char>::BasicString(unsigned int s) :
	m_bValid(true)
{
	char tmpBuff[32];
	sprintf_s(tmpBuff, "%u", s);
	m_String = tmpBuff;
}

template <>
BasicString<wchar_t>::BasicString(unsigned int s) :
	m_bValid(true)
{
	wchar_t tmpBuff[32];
	swprintf_s(tmpBuff, L"%u", s);
	m_String = tmpBuff;
}

template <>
BasicString<char>::BasicString(double s) :
	m_bValid(true)
{
	char tmpBuff[32];
	sprintf_s(tmpBuff, "%lf", s);
	m_String = tmpBuff;
}

template <>
BasicString<wchar_t>::BasicString(double s) :
	m_bValid(true)
{
	wchar_t tmpBuff[32];
	swprintf_s(tmpBuff, L"%lf", s);
	m_String = tmpBuff;
}

//////////////////////////////////////////////////////////////////////////
// FORMAT STRING DEFINITION
//////////////////////////////////////////////////////////////////////////
		
template <>
BasicString<char> BasicString<char>::Format(const char *format, ...) 
{
	va_list argList;
	va_start(argList, format);

	int len = _vscprintf(format, argList)+1;

	char *buff = new char[len];

	vsprintf_s(buff, len, format, argList);

	va_end(argList);

	BasicString<char> result(buff);

	delete[] buff;

	return result;
}

template <>
BasicString<wchar_t> BasicString<wchar_t>::Format(const wchar_t *format, ...) 
{
	va_list argList;
	va_start(argList, format);

	int len = _vscwprintf(format, argList)+1;

	wchar_t *buff = new wchar_t[len];

	vswprintf_s(buff, len, format, argList);

	va_end(argList);

	BasicString<wchar_t> result(buff);

	delete[] buff;

	return result;
}

//////////////////////////////////////////////////////////////////////////
// INTERNAL STRLEN FUNCTION
//////////////////////////////////////////////////////////////////////////

template <>
int BasicString<char>::Internal_StrLen(const char *str) const 
{
	return lstrlenA(str);
}

template <>
int BasicString<wchar_t>::Internal_StrLen(const wchar_t *str) const
{
	return lstrlenW(str);
}

//////////////////////////////////////////////////////////////////////////
// CONVERSION METHODS
//////////////////////////////////////////////////////////////////////////

template <>
BasicString<wchar_t> BasicString<char>::ToUnicode() const
{
	size_t buffLen = Ansi::GetFinalStringLength(m_String.c_str());

	wchar_t *tmp = new wchar_t[buffLen + sizeof(wchar_t)];

	Ansi::ToUnicode(m_String.c_str(), tmp, buffLen);

	BasicString<wchar_t> r(tmp);

	delete[] tmp;

	return r;
}

template <>
BasicString<wchar_t> BasicString<wchar_t>::ToUnicode() const
{
	return (*this);
}

template <>
BasicString<char> BasicString<char>::ToAnsi() const
{
	return (*this);
}

template <>
BasicString<char> BasicString<wchar_t>::ToAnsi() const
{
	size_t buffLen = Unicode::GetFinalStringLength(m_String.c_str());

	char *tmp = new char[buffLen + sizeof(char)];

	Unicode::ToAnsi(m_String.c_str(), tmp, buffLen);

	BasicString<char> r(tmp);

	delete[] tmp;

	return r;
}

}