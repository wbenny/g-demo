/**
 * @file	fw/PtrList.h
 *
 * @brief	Declares the list class.
 */

#pragma once
#include "Assert.h"
#include "IPtrList.h"
#include <vector>
#include <algorithm>
#include "Ptr.h"

namespace FW {

/**
 * @class	PtrList<T>
 *
 * @brief	Represents a non-generic collection
 * 			of objects that can be individually accessed by index.
 * 			This class is especially defined for pointer types
 * 			and using garbage collecting.
 *
 * @date	1.10.2011
 */
template<class T>
class PtrList : public IPtrList<T>
{
	public:
		/**
		 * @fn	PtrList::PtrList()
		 *
		 * @brief	Default constructor.
		 *
		 * @date	1.10.2011
		 */
		PtrList()
		{

		}

		/**
		 * @fn	PtrList::PtrList(const PtrList<T> &list)
		 *
		 * @brief	Copy constructor.
		 *
		 * @date	1.10.2011
		 *
		 * @param	list	The list to construct from.
		 */
		PtrList(const PtrList<T> &list)
		{
			m_oVector = list.m_oVector;
		}

		/**
		 * @fn	PtrList<T> &PtrList::operator= (PtrList<T> &list)
		 *
		 * @brief	Assignment operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param [in,out]	list	The list to copy from.
		 *
		 * @return	A reference to this object.
		 */
		PtrList<T> &operator = (PtrList<T> &list)
		{
			m_oVector = list.m_oVector;
			return *this;
		}

		/**
		 * @fn	void PtrList::Add(T *object)
		 *
		 * @brief	Adds object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to add.
		 */
		void Add(T *object)
		{
			m_oVector.push_back(object);
		}

		/**
		 * @fn	void PtrList::Clear()
		 *
		 * @brief	Clears this object to its blank state.
		 *
		 * @date	1.10.2011
		 */
		void Clear()
		{
			m_oVector.clear();
		}

		/**
		 * @fn	bool PtrList::Contains(const T *object) const
		 *
		 * @brief	Query if this object contains the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to test for containment.
		 *
		 * @return	true if the object is in this collection, false if not.
		 */
		bool Contains(const T *object) const
		{
			return (std::find(m_oVector.cbegin(), m_oVector.cend(), object) !=
						m_oVector.cend());
		}

		/**
		 * @fn	u32 PtrList::IndexOf(const T *object) const
		 *
		 * @brief	Index of the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object.
		 *
		 * @return	The index.
		 */
		u32 IndexOf(const T *object) const
		{
			std::vector< Ptr<T> >::const_iterator it;
			it = std::find(m_oVector.cbegin(), m_oVector.cend(), object);

			if (it == m_oVector.cend())
				return static_cast<u32>(-1);

			return (it - m_oVector.cbegin());
		}

		/**
		 * @fn	void PtrList::Remove(const T *object)
		 *
		 * @brief	Removes the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to remove.
		 */
		void Remove(const T *object)
		{
			std::vector< Ptr<T> >::iterator it;
			it = std::find(m_oVector.begin(), m_oVector.end(), object);

			if (it != m_oVector.end())
				m_oVector.erase(it);
		}

		/**
		 * @fn	void PtrList::RemoveAt(u32 index)
		 *
		 * @brief	Removes object at described index.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 */
		void RemoveAt(u32 index)
		{
			if (index >= static_cast<u32>(m_oVector.size()))
				return;

			m_oVector.erase(m_oVector.begin() + index);
		}

		/**
		 * @fn	T *PtrList::At(u32 index)
		 *
		 * @brief	Return object at specified position.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		T *At(u32 index)
		{
			return m_oVector.at(static_cast<std::vector< Ptr<T> >::size_type>(index)).Get();
		}

		/**
		 * @fn	const T *PtrList::At(u32 index) const
		 *
		 * @brief	Return object at specified position.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		const T *At(u32 index) const
		{
			return m_oVector.at(static_cast<std::vector< Ptr<T> >::size_type>(index)).Get();
		}

		/**
		 * @fn	T *PtrList::operator[](u32 index)
		 *
		 * @brief	Array indexer operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		T *operator[](u32 index)
		{
			return At(index);
		}

		/**
		 * @fn	const T *PtrList::operator[](u32 index) const
		 *
		 * @brief	Array indexer const operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		const T *operator[](u32 index) const
		{
			return At(index);
		}

		/**
		 * @fn	u32 PtrList::GetCount() const
		 *
		 * @brief	Gets the number of elements contained in the list.
		 *
		 * @date	1.10.2011
		 *
		 * @return	Count of elements.
		 */
		u32 GetCount() const
		{
			return m_oVector.size();
		}

	protected:
		// Members.
		std::vector< Ptr<T> > m_oVector; ///< Vector.
};

} // namespace
