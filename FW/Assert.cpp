#include "Assert.h"

namespace FW
{
	void __Assert(int expression, const char* filename, int line, const char* fn, const char* expr, const char* msg)
	{
		if (!expression)
		{
			char l_sMessage[1024];

			if (msg)
			{
				wsprintfA(l_sMessage, 
					"An exception has occurred!\n\n"
					"%s\n\n"
					"Function: %s\n\n"
					"File: \"%s\"\n\n"
					"Line: %d\n\n"
					"Expr.: (%s) has returned false\n\n",
					msg, fn, filename, line, expr);
			}
			else
			{
				wsprintfA(l_sMessage, 
					"An exception has occurred!\n\n"
					"Function: %s\n\n"
					"File: \"%s\"\n\n"
					"Line: %d\n\n"
					"Expr.: (%s) has returned false\n\n",
					fn, filename, line, expr);
			}

			MessageBoxA(NULL, l_sMessage, "Exception", MB_ICONWARNING | MB_OK);
		}
	}

}