#pragma once
#include "../Singleton.h"
#include "../String.h"
#include "../Map.h"

namespace FW { namespace Render {

class ShaderChunkManager : public Singleton<ShaderChunkManager>, public Map<String, String>
{
	friend class Singleton<ShaderChunkManager>;

	public:
		String& GetChunkByName(const String& name) { return GetMappedElement(name); }

	private:
		ShaderChunkManager() {}
};

} }
