#pragma once
#include "../Types.h"
#include "../GL/GL.h"
#include "../Utils/Swap.h"
#include "../Content/Resource.h"
#include "Viewer.h"

namespace FW { namespace Render {

class CubeMap : public Content::Resource
{
	public:
		CubeMap(GLuint handle) :
			Content::Resource(Content::ResourceType::CubeMap),
			m_CubeMapHandle(handle)
		{
			Init();
		}

		CubeMap() :
			Content::Resource(Content::ResourceType::CubeMap),
			m_CubeMapHandle(-1)
		{
			Init();
		}
			
		virtual ~CubeMap()
		{
			Done();
		}

		virtual bool Init()
		{
			return true;
		}

		virtual void Done()
		{
			if (m_CubeMapHandle != -1)
			{
				GL::DeleteTexture(m_CubeMapHandle);
				Unbind();
			}
		}

		void Bind() { GL::BindTexture(GL_TEXTURE_CUBE_MAP, m_CubeMapHandle); }
		void Unbind() { GL::BindTexture(GL_TEXTURE_CUBE_MAP, 0); }

		void GenerateMipmaps() { Bind(); GL::GenerateMipmap(GL_TEXTURE_CUBE_MAP); }

		GLuint GetHandle() const { return m_CubeMapHandle; }
		bool IsInitialized() const { return (m_CubeMapHandle != -1); }

		static void SetActiveTexture(u8 index) { GL::ActiveTexture(GL_TEXTURE0 + index); }

	protected:
		GLuint m_CubeMapHandle;
};

} }
