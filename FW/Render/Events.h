#pragma once
#include "../Types.h"

namespace FW { namespace Render { namespace Events {

enum Keys
{
	ArrowLeft  = 0,
	ArrowRight = 1,
	ArrowUp	   = 2,
	ArrowDown  = 3,

	Space    = 4,
	
	PageUp	 = 5,
	PageDown = 6,

	Num0 = 40,
	Num1 = 41,
	Num2 = 42,
	Num3 = 43,
	Num4 = 44,
	Num5 = 45,
	Num6 = 46,
	Num7 = 47,
	Num8 = 48,
	Num9 = 49,

	A = 65,
	B = 66,
	C = 67,
	D = 68,
	E = 69,
	F = 70,
	G = 71,
	H = 72,
	I = 73,
	J = 74,
	K = 75,
	L = 76,
	M = 77,
	N = 78,
	O = 79,
	P = 80,
	Q = 81,
	R = 82,
	S = 83,
	T = 84,
	U = 85,
	V = 86,
	W = 87,
	X = 88,
	Y = 89,
	Z = 90,

	LeftShift  = 100,
	RightShift = 101,
	LeftCtrl   = 102,
	RightCtrl  = 103,
};

enum MouseButton
{
	None = 0,
	Left = 1,
	Right = 2,
};

struct MouseInfo
{
	s32 DX, DY;
	s32 X, Y;
	bool Buttons[2];
};

struct EventInfo
{
	bool Key[256];
	MouseInfo Mouse;
};

} } }
