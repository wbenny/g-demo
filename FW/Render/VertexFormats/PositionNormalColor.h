#pragma once
#include "../../Math/Vector3.h"
#include "VertexBase.h"

namespace FW { namespace Render { namespace VertexFormats {

#include <PshPack1.h>
struct PositionNormalColor : VertexBase
{
	Math::Vector3 Position;
	Math::Vector3 Normal;
	Math::Vector3 Color;

	PositionNormalColor() :
		Position(Math::Vector3::Zero),
		Normal(Math::Vector3::Zero),
		Color(Math::Vector3::Zero)
	{

	}

	PositionNormalColor(const Math::Vector3& position) :
		Position(position),
		Normal(Math::Vector3::Zero),
		Color(Math::Vector3(Math::Vector3::Zero))
	{

	}

	PositionNormalColor(const Math::Vector3& position,
						const Math::Vector3& color) :
		Position(position),
		Normal(Math::Vector3::Zero),
		Color(color)
	{

	}

	PositionNormalColor(const Math::Vector3& position,
						const Math::Vector3& normal,
						const Math::Vector3& color) :
		Position(position),
		Normal(normal),
		Color(color)
	{

	}

	static VertexFormat GetVertexFormat()
	{
		return VertexFormat(3,
							12,	0,
							12,	12,
							12,	24);
	}
};
#include <PopPack.h>

} } }
