#pragma once
#include "../../Math/Vector3.h"
#include "../../Memory.h"
#include <cstdarg>

namespace FW { namespace Render { namespace VertexFormats {

#include <PshPack1.h>
struct VertexFormat
{
	u32  Elements;
	u32* Sizes;
	u32* Indices;

	VertexFormat() :
		Elements(0),
		Sizes(NULL),
		Indices(NULL)
	{

	}

	VertexFormat(u32 elements, ...)
	{
		va_list vl;
		va_start(vl, elements);

		Elements   = elements;
		Sizes      = new u32[elements];
		Indices    = new u32[elements];

		for (u32 i = 0; i<elements; i++)
		{
			Sizes[i]    = va_arg(vl, u32);
			Indices[i]  = va_arg(vl, u32);
		}

		va_end(vl);
	}

	VertexFormat(const VertexFormat& vf)
	{
		Elements = vf.Elements;

		Sizes   = new u32[Elements];
		Indices = new u32[Elements];

		Memory::Copy(Sizes,   vf.Sizes,   Elements * sizeof(u32));
		Memory::Copy(Indices, vf.Indices, Elements * sizeof(u32));
	}

	VertexFormat& operator = (const VertexFormat& vf)
	{
		if (Sizes && Indices)
		{
			delete[] Sizes;
			delete[] Indices;
		}

		Elements = vf.Elements;

		Sizes   = new u32[Elements];
		Indices = new u32[Elements];

		Memory::Copy(Sizes,   vf.Sizes,   Elements * sizeof(u32));
		Memory::Copy(Indices, vf.Indices, Elements * sizeof(u32));

		return (*this);
	}

	~VertexFormat()
	{
		delete[] Sizes;
		delete[] Indices;
	}

	void Apply()
	{
		GLsizei vertexFormatSize = 0;

		for (u8 i = 0; i < Elements; i++)
		{
			vertexFormatSize += Sizes[i];
		}

		for (u32 i = 0; i < Elements; i++)
		{
			GL::EnableVertexAttribArray(i);

			GL::VertexAttribPointer(i,
				static_cast<GLint>(Sizes[i]) / sizeof(float),
				GL_FLOAT,
				GL_FALSE,
				vertexFormatSize,
				reinterpret_cast<const GLvoid*>((u64)Indices[i]));
		}
	}
};
#include <PopPack.h>

} } }
