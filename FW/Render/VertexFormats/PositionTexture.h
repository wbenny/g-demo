#pragma once
#include "../../Math/Vector2.h"
#include "../../Math/Vector3.h"
#include "VertexBase.h"

namespace FW { namespace Render { namespace VertexFormats {

#include <PshPack1.h>
struct PositionTexture : VertexBase
{
	Math::Vector3 Position;
	Math::Vector2 TexCoord;

	PositionTexture() :
		Position(Math::Vector3::Zero),
		TexCoord(Math::Vector2::Zero)
	{

	}

	PositionTexture(const Math::Vector3& position) :
		Position(position),
		TexCoord(Math::Vector2::Zero)
	{

	}

	PositionTexture(const Math::Vector3& position,
						const Math::Vector2& texCoord) :
		Position(position),
		TexCoord(texCoord)
	{

	}

	static VertexFormat GetVertexFormat()
	{
		return VertexFormat(2,
							12,	0,
							 8,	12);
	}
};
#include <PopPack.h>

} } }
