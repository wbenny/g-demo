#pragma once
#include "../../Math/Vector2.h"
#include "../../Math/Vector3.h"
#include "VertexBase.h"

namespace FW { namespace Render { namespace VertexFormats {

#include <PshPack1.h>
struct PositionNormalTexture : VertexBase
{
	Math::Vector3 Position;
	Math::Vector3 Normal;
	Math::Vector2 TexCoord;

	PositionNormalTexture() :
		Position(Math::Vector3::Zero),
		Normal(Math::Vector3::Zero),
		TexCoord(Math::Vector2::Zero)
	{

	}

	PositionNormalTexture(const Math::Vector3& position) :
		Position(position),
		Normal(Math::Vector3::Zero),
		TexCoord(Math::Vector2::Zero)
	{

	}

	PositionNormalTexture(const Math::Vector3& position,
						const Math::Vector2& texCoord) :
		Position(position),
		Normal(Math::Vector3::Zero),
		TexCoord(texCoord)
	{

	}

	PositionNormalTexture(const Math::Vector3& position,
						const Math::Vector3& normal,
						const Math::Vector2& texCoord) :
		Position(position),
		Normal(normal),
		TexCoord(texCoord)
	{

	}

	static VertexFormat GetVertexFormat()
	{
		return VertexFormat(3,
							12,	0,
							12,	12,
							 8,	24);
	}
};
#include <PopPack.h>

} } }
