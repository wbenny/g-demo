#pragma once
#include "../../Math/Vector2.h"
#include "../../Math/Vector3.h"
#include "VertexBase.h"

namespace FW { namespace Render { namespace VertexFormats {

#include <PshPack1.h>
struct PositionNormalColorTexture : VertexBase
{
	Math::Vector3 Position;
	Math::Vector3 Normal;
	Math::Vector3 Color;
	Math::Vector2 TexCoord;

	PositionNormalColorTexture() :
		Position(Math::Vector3::Zero),
		Normal(Math::Vector3::Zero),
		Color(Math::Vector3::Zero),
		TexCoord(Math::Vector2::Zero)
	{

	}

	PositionNormalColorTexture(const Math::Vector3& position) :
		Position(position),
		Normal(Math::Vector3::Zero),
		Color(Math::Vector3::Zero),
		TexCoord(Math::Vector2::Zero)
	{

	}

	PositionNormalColorTexture(const Math::Vector3& position,
						const Math::Vector2& texCoord) :
		Position(position),
		Normal(Math::Vector3::Zero),
		Color(Math::Vector3::Zero),
		TexCoord(texCoord)
	{

	}

	PositionNormalColorTexture(const Math::Vector3& position,
						const Math::Vector3& normal,
						const Math::Vector2& texCoord) :
		Position(position),
		Normal(normal),
		Color(Math::Vector3::Zero),
		TexCoord(texCoord)
	{

	}

	PositionNormalColorTexture(const Math::Vector3& position,
						const Math::Vector3& color) :
		Position(position),
		Normal(Math::Vector3::Zero),
		Color(color),
		TexCoord(Math::Vector2::Zero)
	{

	}

	PositionNormalColorTexture(const Math::Vector3& position,
						const Math::Vector2& texCoord,
						const Math::Vector3& color) :
		Position(position),
		Normal(Math::Vector3::Zero),
		Color(color),
		TexCoord(texCoord)
	{

	}

	PositionNormalColorTexture(const Math::Vector3& position,
				const Math::Vector3& normal,
				const Math::Vector3& color) :
		Position(position),
		Normal(normal),
		Color(color),
		TexCoord(Math::Vector2::Zero)
	{

	}
	static VertexFormat GetVertexFormat()
	{
		return VertexFormat(4,
							12,	0,
							12,	12,
							12,	24,
							 8, 36);
	}
};
#include <PopPack.h>

} } }
