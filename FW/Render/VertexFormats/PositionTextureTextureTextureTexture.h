#pragma once
#include "../../Math/Vector2.h"
#include "../../Math/Vector3.h"
#include "VertexBase.h"

namespace FW { namespace Render { namespace VertexFormats {

#include <PshPack1.h>
struct PositionTextureTextureTextureTexture : VertexBase
{
	Math::Vector3 Position;
	Math::Vector2 TexCoord1;
	Math::Vector2 TexCoord2;
	Math::Vector2 TexCoord3;
	Math::Vector2 TexCoord4;

	friend bool operator == (const PositionTextureTextureTexture&, const PositionTextureTextureTexture&) { return true; }

	PositionTextureTextureTextureTexture() :
		Position(Math::Vector3::Zero)
	{

	}

	PositionTextureTextureTextureTexture(const Math::Vector3& position) :
		Position(position)
	{

	}

	PositionTextureTextureTextureTexture(const Math::Vector3& position,
								  const Math::Vector2& texCoord) :
		Position(position),
		TexCoord1(texCoord)
	{

	}

	PositionTextureTextureTextureTexture(const Math::Vector3& position,
								  const Math::Vector2& texCoord1,
								  const Math::Vector2& texCoord2) :
		Position(position),
		TexCoord1(texCoord1),
		TexCoord2(texCoord2)
	{

	}

	PositionTextureTextureTextureTexture(const Math::Vector3& position,
								  const Math::Vector2& texCoord1,
								  const Math::Vector2& texCoord2,
								  const Math::Vector2& texCoord3) :
		Position(position),
		TexCoord1(texCoord1),
		TexCoord2(texCoord2),
		TexCoord3(texCoord3)
	{

	}

	PositionTextureTextureTextureTexture(const Math::Vector3& position,
								  const Math::Vector2& texCoord1,
								  const Math::Vector2& texCoord2,
								  const Math::Vector2& texCoord3,
								  const Math::Vector2& texCoord4) :
		Position(position),
		TexCoord1(texCoord1),
		TexCoord2(texCoord2),
		TexCoord3(texCoord3),
		TexCoord4(texCoord4)
	{

	}

	static VertexFormat GetVertexFormat()
	{
		return VertexFormat(5,
							12,	0,
							 8,	12,
							 8,	20,
							 8,	28,
							 8,	36);
	}
};
#include <PopPack.h>

} } }
