#pragma once
#include "VertexFormat.h"

namespace FW { namespace Render { namespace VertexFormats {

#include <PshPack1.h>
struct VertexBase
{
	static VertexFormat GetVertexFormat() {}
};
#include <PopPack.h>

} } }
