#include "RenderTarget.h"

namespace FW { namespace Render {

void RenderTarget::AttachTexture(Texture* texture)
{
	DetachTexture();

	m_Texture = texture;

	if (!texture)
		return;

	m_Texture->Bind();

	m_FrameBufferHandle = GL::GenFramebuffer();
	GL::BindFramebuffer(m_FrameBufferHandle);

	if (m_Texture->GetTextureFormat().ExternalFormat == GL_DEPTH_COMPONENT)
	{
		GL::FramebufferTexture2D(GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_Texture->GetHandle(), 0);

		// Disable drawing to any buffers, we only want the depth.
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
	}
	else
	{
		m_RenderBufferHandle = GL::GenRenderbuffer();
		GL::BindRenderbuffer(m_RenderBufferHandle);

		GL::RenderbufferStorage(GL_DEPTH_COMPONENT, m_Texture->GetTextureFormat().Width, m_Texture->GetTextureFormat().Height);
		GL::FramebufferRenderbuffer(GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_RenderBufferHandle);

		GL::FramebufferTexture2D(GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_Texture->GetHandle(), 0);
	}

	GL::BindFramebuffer(0);
}

void RenderTarget::DetachTexture()
{
	if (m_FrameBufferHandle != -1)
	{
		GL::DeleteFramebuffer(m_FrameBufferHandle);
		GL::DeleteRenderbuffer(m_RenderBufferHandle);

		m_FrameBufferHandle = m_RenderBufferHandle = -1;

		m_Texture = NULL;
	}
}

void RenderTarget::Bind()
{
	//Shader::LastBoundShader = NULL;

	GL::PushState();

	if (m_Texture.IsValid())
	{
		GL::BindFramebuffer(m_FrameBufferHandle);

		m_RenderState->m_ViewportWidth  = m_Texture->GetTextureFormat().Width;
		m_RenderState->m_ViewportHeight = m_Texture->GetTextureFormat().Height;

		if (m_Texture->GetTextureFormat().ExternalFormat == GL_DEPTH_COMPONENT)
		{
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); 

			glDrawBuffer(GL_NONE);
			glReadBuffer(GL_NONE);
		}
	}
	else
	{
		glDrawBuffer(GL_BACK);
		glReadBuffer(GL_BACK);
	}

	Renderer::GetInstance().SetRenderState(m_RenderState);
}

void RenderTarget::Unbind()
{
	if (m_Texture.IsValid())
		GL::BindFramebuffer(0);

	GL::PopState();

	if (m_Texture.IsValid() && m_Texture->GetTextureFormat().ExternalFormat == GL_DEPTH_COMPONENT)
	{
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

		glDrawBuffer(GL_BACK);
		glReadBuffer(GL_BACK);
	}
}

} }
