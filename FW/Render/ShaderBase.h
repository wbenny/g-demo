#pragma once
#include "../GL/GL.h"
#include "../Content/Resource.h"

namespace FW { namespace Render {

class ShaderBase : public Content::Resource
{
	public:
		ShaderBase();
		~ShaderBase();

		bool Init(const String& vertexSource, const String& fragmentSource);
		void End();

		virtual void Bind();
		virtual void Unbind();

		GLint GetAttribLocation(const char* name);
		GLint GetUniformLocation(const char* name);

	protected:
		void Debug(GLuint object, const char* type);

		GLuint	m_iVertexShader,
				m_iFragmentShader,
				m_iProgram;
};

} }
