#include "RenderState.h"

namespace FW { namespace Render {

const DrawMode DrawMode::Default  = DrawMode();
const DrawMode DrawMode::Solid    = DrawMode(GL_TRIANGLES, GL_FILL);
const DrawMode DrawMode::Wired    = DrawMode(GL_TRIANGLES, GL_LINE);
const DrawMode DrawMode::Particle = DrawMode(GL_POINTS, GL_FILL);

} }
