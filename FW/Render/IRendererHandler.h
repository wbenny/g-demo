#pragma once

namespace FW { namespace Render { 

class IRendererHandler
{
	public:
		virtual bool Init() = 0;
		virtual void End() = 0;

		virtual bool Update(float time, float delta) = 0;

		virtual void OnMouseMove(unsigned buttons, int x, int y, int dx, int dy) = 0;
		virtual void OnMouseDown(unsigned button, int x, int y) = 0;
		virtual void OnMouseUp(unsigned button, int x, int y) = 0;

		virtual void OnKeyDown(char key, int x, int y) = 0;
		virtual void OnKeyUp(char key, int x, int y) = 0;

	protected:
		IRendererHandler() { }
};

} } 
