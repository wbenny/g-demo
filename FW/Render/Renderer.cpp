#include "Renderer.h"
#include "Events.h"
#include "IRendererHandler.h"
#include "Viewer.h"
#include "Shader.h"
#include "Texture.h"
#include "../Scene/CameraNode.h"
#include "../Scene/MeshNode.h"
#include "../Scene/RootNode.h"
#include "../Materials/MaterialManager.h"
#include "Postprocess/VerticalBlur.h"
#include "Postprocess/HorizontalBlur.h"
#include "FullScreenQuad.h"
#include "../Scene/DirectionalLightNode.h"

namespace FW { namespace Render {

void Renderer::Init()
{
	// Depth
	GL::Enable(GL_DEPTH_TEST);
	GL::DepthFunc(GL_LEQUAL);
	GL::DepthMask(GL_TRUE);

	// Face culling
	GL::Enable(GL_CULL_FACE);
	GL::FrontFace(GL_CCW);

	// Blend mode - TODO
	GL::Disable(GL_BLEND);
}

Texture* Renderer::Draw(Scene::RootNode* scene)
{
	m_ReflectionCamera = NULL;

	if (scene->m_Cameras.GetCount() > 1)
	{
		// Shadow & reflect cameras first!
		scene->m_Cameras.Sort(
			[](Scene::CameraNode* c1, Scene::CameraNode* c2) -> bool
			{
				Scene::CameraType ct1 = c1->GetCameraType();
				Scene::CameraType ct2 = c2->GetCameraType();

				if (ct1 == Scene::CameraType::Main)
					return false;

				if (ct1 == Scene::CameraType::Reflect && ct2 == Scene::CameraType::Shadow)
					return true;

				if (ct1 == Scene::CameraType::Shadow && ct2 == Scene::CameraType::Reflect)
					return false;

				return true;
			}
		);
	}

	List<Scene::LightNode*> _noLights;
	Math::Vector3 _ambientLightColor(1.0f, 1.0f, 1.0f);

	for (u32 i = 0; i < scene->m_Cameras.GetCount(); i++)
	{
		Scene::CameraNode* camera = scene->m_Cameras[i];
		
		if (camera->IsLightingEnabled())
		{
			m_Lights = &(scene->m_Lights);
			m_AmbientLightColor = &(scene->m_AmbientLightColor);
		}
		else
		{
			m_Lights = &_noLights;
			m_AmbientLightColor = &_ambientLightColor;
		}

		m_ActualCameraType = camera->GetCameraType();

		if (m_ActualCameraType == Scene::CameraType::Reflect)
			m_ReflectionCamera = camera;

		camera->UpdateLightCamera();

		camera->GetFramebuffer()->Bind();
		{
			m_RenderState->ApplyRenderState();

			m_ViewMatrix = camera->GetWorldMatrix();
			const Math::Matrix4& inverseViewMatrix = m_ViewMatrix.Inverted();

			m_ProjectionMatrix = camera->GetProjectionMatrix();
			
			m_EyeDirection = inverseViewMatrix.GetTranslation().Negated().Normalized();
			m_EyePosition = inverseViewMatrix.GetTranslation();

			m_NearFar = camera->GetNearFar();

			DrawObjectArray(scene->m_Meshes);
		}
		camera->GetFramebuffer()->Unbind();
	}

	return scene->GetMainCamera()->GetFramebuffer()->GetTexture();
}

void Renderer::DrawObject(Scene::MeshNode* mesh)
{
	if(!mesh->GetMaterial())
		return;

	RenderModes::RenderModeBase* tmp = m_RenderState->m_RenderMode;

 	if (m_ActualCameraType == Scene::CameraType::Shadow && !mesh->CastsShadow())
 		return;

	if (m_ActualCameraType == Scene::CameraType::Reflect && mesh->IsReflective())
		return;

	if (mesh->GetMaterial()->GetMaterialType() == Materials::MaterialType::SkyDome && !m_Lights->GetCount())
		return;

	if (mesh->GetName()[0] == 's' && mesh->GetName()[1] == 'u' && mesh->GetName()[2] == 'n' && !m_Lights->GetCount())
		m_RenderState->m_RenderMode = (RenderModes::RenderModeBase*)mesh->GetMaterial();

	// Setup render settings for actual object.
#define SET_IF_ENABLED(s1, s2) !!s1 ? s1 : s2

	Materials::MaterialBase* meshMaterial	= SET_IF_ENABLED(m_RenderState->m_RenderMode,    mesh->GetMaterial());
	DepthTestMode            meshDepthTest	= SET_IF_ENABLED(m_RenderState->m_DepthTestMode, mesh->GetDepthTestMode());
	DrawMode                 meshDrawMode	= SET_IF_ENABLED(m_RenderState->m_DrawMode,      mesh->GetDrawMode());

#undef SET_IF_ENABLED

	ScenePassContext rpd(
		mesh,
		*m_AmbientLightColor,
		m_ActualCameraType == Scene::CameraType::Main ? m_ReflectionCamera : NULL,
		*m_Lights,
		m_ProjectionMatrix,
		mesh->GetWorldMatrix(),
		m_ViewMatrix);

	//if (!meshMaterial->GetShader()->IsBound())
	{
		meshMaterial->SetScenePassContext(&rpd);

		meshMaterial->Bind();
		
		meshMaterial->p_uProjectionViewMatrix = m_ProjectionMatrix * m_ViewMatrix;

		meshMaterial->p_uProjectionMatrix = m_ProjectionMatrix;
		meshMaterial->p_uViewMatrix = m_ViewMatrix;

		meshMaterial->p_uEyePosition = m_EyePosition;
		meshMaterial->p_uEyeDirection = m_EyeDirection;

		meshMaterial->p_uWindowSize = Math::Vector2(
			static_cast<float>(Viewer::GetInstance().GetInfo().Width),
			static_cast<float>(Viewer::GetInstance().GetInfo().Height)
		);

		meshMaterial->p_uNearFar = m_NearFar;
	}

	meshMaterial->p_uProjectionViewModelMatrix = m_ProjectionMatrix * m_ViewMatrix * mesh->GetWorldMatrix();
	meshMaterial->p_uModelMatrix = mesh->GetWorldMatrix();
	meshMaterial->p_uNormalMatrix = mesh->GetNormalMatrix();

	GL::PolygonMode(GL_FRONT_AND_BACK, meshDrawMode.m_PolygonMode);

	GL::DepthFunc(meshDepthTest);

	if (mesh->GetGeometry())
	{
		mesh->GetGeometry()->GetVertexArray()->Bind();

		GL::DrawElements(meshDrawMode.m_DrawMode, mesh->GetGeometry()->GetIndexBuffer()->GetSize(), GL_UNSIGNED_INT, 0);
	}

	m_RenderState->m_RenderMode = tmp;
}
		
void Renderer::DrawObjectArray(List<Scene::Node*>& renderArray)
{
	int renderables = renderArray.GetCount();
	Scene::MeshNode* renderable = NULL;

	while (renderables--)
	{
		renderable = renderArray[renderables]->As<Scene::MeshNode>();
		DrawObject(renderable);
	}
}


} }
