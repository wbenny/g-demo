#pragma once
#include "../Types.h"
#include "../List.h"
#include "../Math/Vector3.h"
#include "../Math/Matrix4.h"

FW_CLASS_FWD_DECL2(FW, Scene, MeshNode)
FW_CLASS_FWD_DECL2(FW, Scene, LightNode)
FW_CLASS_FWD_DECL2(FW, Scene, CameraNode)

namespace FW { namespace Render {

struct ScenePassContext
{
	Scene::MeshNode*					Mesh;

	const Math::Vector3&				AmbientLightColor;
	Scene::CameraNode*					ReflectionCamera;
	List<Scene::LightNode*>&			Lights;

	const Math::Matrix4&				ProjectionMatrix;
	const Math::Matrix4&				ModelMatrix;
	const Math::Matrix4&				ViewMatrix;

	ScenePassContext(
					Scene::MeshNode* mesh,
					const Math::Vector3& ambientLightColor,
					Scene::CameraNode* reflectionCamera,
					List<Scene::LightNode*>& lights,
					const Math::Matrix4& projectionMatrix,
					const Math::Matrix4& modelMatrix,
					const Math::Matrix4& viewMatrix) :
		Mesh(mesh),
		AmbientLightColor(ambientLightColor),
		ReflectionCamera(reflectionCamera),
		Lights(lights),
		ProjectionMatrix(projectionMatrix),
		ModelMatrix(modelMatrix),
		ViewMatrix(viewMatrix)
	{

	}
};

} }
