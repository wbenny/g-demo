#pragma once
#include "../Types.h"
#include "../RefCounter.h"
#include "BufferObject.h"

namespace FW { namespace Render {

class IBufferArray : public RefCounter
{
	public:
		IBufferArray(BufferObjectTypes bufferType) :
			m_Data(NULL),
			m_DataSize(0),
			m_BufferObject(bufferType)
		{

		}

		virtual ~IBufferArray() { }

		virtual void UpdateBuffer() = 0;

		virtual u32 GetSize() const = 0;
		virtual void SetSize(u32 newSize) = 0;

		virtual const void* GetDataPointer() const = 0;
		virtual void SetData(const void* dataPtr, u32 count = 0) = 0;

	protected:
		void*						m_Data;

		u32							m_DataSize;

		BufferObject				m_BufferObject;
};

} }
