#include "Viewer.h"
#include "../GL/GL.h"
#include "../Memory.h"
#include "../Math/MathBase.h"

LRESULT CALLBACK __Viewer_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return FW::Render::Viewer::GetInstance().WndProc(hWnd, uMsg, wParam, lParam);
}

namespace FW { namespace Render {

static const PIXELFORMATDESCRIPTOR _pixelformatDescriptor =
{
    sizeof(PIXELFORMATDESCRIPTOR),
    1,
    PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
    PFD_TYPE_RGBA,
    32,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0,
    32,             // Z-buffer.
    0,              // Stencil buffer.
    0,
    PFD_MAIN_PLANE,
    0, 0, 0, 0
};

Viewer::Viewer() :
	m_szClassName("g_demo")
{
	m_StartTime = timeGetTime();
	m_FrameTime = timeGetTime();

	m_Active = true;

	Memory::Set(&m_RendererInfo, 0, sizeof(m_RendererInfo));
}

bool Viewer::Init(int width, int height, bool fullscreen)
{
	unsigned int	PixelFormat;
	DWORD			dwExStyle, dwStyle;
	WNDCLASS		wc = { 0 };
	RECT			rec = { 0 };

	m_RendererInfo.hInstance = GetModuleHandle(NULL);
	m_RendererInfo.FullScreen = fullscreen;
	m_RendererInfo.Width = width;
	m_RendererInfo.Height = height;

	wc.style         = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = ::__Viewer_WndProc;
	wc.hInstance     = m_RendererInfo.hInstance;
	wc.lpszClassName = m_szClassName;

	if (!RegisterClass(&wc))
		return false;

	if(fullscreen)
	{
		DEVMODE dmScreenSettings = { 0 };

		dmScreenSettings.dmSize       = sizeof(DEVMODE);
		dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmPelsWidth  = width;
		dmScreenSettings.dmPelsHeight = height;

		if (ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
		{
			return false;
		}

		dwExStyle = WS_EX_APPWINDOW;
		dwStyle   = WS_VISIBLE | WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

		while (ShowCursor(0) >= 0);	// hide cursor
	}
	else
	{
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle   = WS_VISIBLE | WS_CAPTION | WS_CLIPSIBLINGS |
			WS_CLIPCHILDREN | WS_SYSMENU;
	}

	rec.left   = 0;
	rec.top    = 0;
	rec.right  = width;
	rec.bottom = height;

	AdjustWindowRect(&rec, dwStyle, 0);

	m_RendererInfo.hWnd = CreateWindowEx(dwExStyle, wc.lpszClassName, wc.lpszClassName, dwStyle,
		((GetSystemMetrics(SM_CXSCREEN) - rec.right + rec.left) >> 1) /*- 200*/,
		((GetSystemMetrics(SM_CYSCREEN) - rec.bottom + rec.top) >> 1) - 20/*- 30*/,
		rec.right - rec.left, rec.bottom - rec.top, 0, 0, m_RendererInfo.hInstance, 0);

	if (!m_RendererInfo.hWnd)
		return false;

	if (!(m_RendererInfo.hDC = GetDC(m_RendererInfo.hWnd)))
		return false;

	if (!(PixelFormat = ChoosePixelFormat(m_RendererInfo.hDC, &_pixelformatDescriptor)))
		return false;

	if (!SetPixelFormat(m_RendererInfo.hDC, PixelFormat, &_pixelformatDescriptor))
		return false;

	if (!(m_RendererInfo.hRC = wglCreateContext(m_RendererInfo.hDC)))
		return false;

	if (!wglMakeCurrent(m_RendererInfo.hDC, m_RendererInfo.hRC))
		return false;

	GL::Init();

	return true;
}

void Viewer::End()
{
	if (m_RendererInfo.hRC)
	{
		wglMakeCurrent(0, 0);
		wglDeleteContext(m_RendererInfo.hRC);
	}

	if (m_RendererInfo.hDC)
		ReleaseDC(m_RendererInfo.hWnd, m_RendererInfo.hDC);

	if (m_RendererInfo.hWnd)
		DestroyWindow(m_RendererInfo.hWnd);

	UnregisterClass(m_szClassName, m_RendererInfo.hInstance);

	if (m_RendererInfo.FullScreen)
	{
		ChangeDisplaySettings(0, 0);
		while (ShowCursor(1) < 0); // show cursor
	}
}

void Viewer::Run()
{
	bool running = true;
	int frames = 0;

	if(!m_RendererHandler->Init())
	{
		End();
		MessageBox(0, "Chyba pri inicializaci intra.", 0, MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	m_StartTime = timeGetTime();

	while (running)
	{
		if (!ProcessMessage(&running))
		{
			float frameTime   = 0.001f * static_cast<float>(timeGetTime() - m_FrameTime);
			float elapsedTime = 0.001f * static_cast<float>(timeGetTime() - m_StartTime);

			if (GetActiveWindow() == m_RendererInfo.hWnd)
			{
				ProcessMouseInput();
				ProcessKeyboardInput();
			}
			else
			{
				m_Active = false;
			}

			m_FrameTime = timeGetTime();

			running = m_RendererHandler->Update(elapsedTime, frameTime);

			SwapBuffers(m_RendererInfo.hDC);

			DrawTime(elapsedTime, frameTime);

			if (!frames)
				PlaySoundA("music\\music.wav", NULL, SND_ASYNC | SND_FILENAME);

			frames++;
		}
	}

	m_RendererHandler->End();

	End();
}


void Viewer::SetHandler(IRendererHandler *glHandler)
{
	m_RendererHandler = glHandler;
}

const RendererInfo& Viewer::GetInfo()
{
	return m_RendererInfo;
}

void Viewer::ProcessMouseInput()
{
	POINT p;
	GetCursorPos(&p);

	if (!m_Active)
	{
		m_RendererInfo.Events.Mouse.DX = 0;
		m_RendererInfo.Events.Mouse.DY = 0;
		m_Active = true;
	}
	else
	{
		m_RendererInfo.Events.Mouse.DX = p.x - m_RendererInfo.Events.Mouse.X;
		m_RendererInfo.Events.Mouse.DY = p.y - m_RendererInfo.Events.Mouse.Y;
	}

	m_RendererInfo.Events.Mouse.X = p.x;
	m_RendererInfo.Events.Mouse.Y = p.y;

	bool mbl = m_RendererInfo.Events.Mouse.Buttons[Events::MouseButton::Left];
	bool mbr = m_RendererInfo.Events.Mouse.Buttons[Events::MouseButton::Right];

	m_RendererInfo.Events.Mouse.Buttons[Events::MouseButton::Left]  = !!(GetAsyncKeyState(VK_LBUTTON) & 0x8000);
	m_RendererInfo.Events.Mouse.Buttons[Events::MouseButton::Right] = !!(GetAsyncKeyState(VK_RBUTTON) & 0x8000);

	unsigned button = m_RendererInfo.Events.Mouse.Buttons[Events::MouseButton::Left]  ? Events::MouseButton::Left :
					  m_RendererInfo.Events.Mouse.Buttons[Events::MouseButton::Right] ? Events::MouseButton::Right : 0;

	if (mbl != m_RendererInfo.Events.Mouse.Buttons[Events::MouseButton::Left])
	{
		if (mbl)
			m_RendererHandler->OnMouseUp(1, p.x, p.y);
		else
			m_RendererHandler->OnMouseDown(1, p.x, p.y);
	}
	else if (mbr != m_RendererInfo.Events.Mouse.Buttons[Events::MouseButton::Right])
	{
		if (mbl)
			m_RendererHandler->OnMouseUp(2, p.x, p.y);
		else
			m_RendererHandler->OnMouseDown(2, p.x, p.y);
	}

	if (m_RendererInfo.Events.Mouse.DX || m_RendererInfo.Events.Mouse.DY)
		m_RendererHandler->OnMouseMove(button, p.x, p.y, m_RendererInfo.Events.Mouse.DX, m_RendererInfo.Events.Mouse.DY);
}

void Viewer::ProcessKeyboardInput()
{
	bool tmp[256];
	Memory::Copy(&tmp, &(m_RendererInfo.Events.Key), 256);

	m_RendererInfo.Events.Key[Events::Keys::ArrowLeft]  = !!(GetAsyncKeyState(VK_LEFT)		& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::ArrowRight] = !!(GetAsyncKeyState(VK_RIGHT)		& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::ArrowUp]    = !!(GetAsyncKeyState(VK_UP)		& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::ArrowDown]  = !!(GetAsyncKeyState(VK_DOWN)		& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::PageUp]     = !!(GetAsyncKeyState(VK_PRIOR)		& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::PageDown]   = !!(GetAsyncKeyState(VK_NEXT)		& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::Space]      = !!(GetAsyncKeyState(VK_SPACE)		& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::RightShift] = !!(GetAsyncKeyState(VK_RSHIFT)	& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::RightCtrl]  = !!(GetAsyncKeyState(VK_RCONTROL)	& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::LeftShift]  = !!(GetAsyncKeyState(VK_LSHIFT)	& 0x8000);
	m_RendererInfo.Events.Key[Events::Keys::LeftCtrl]   = !!(GetAsyncKeyState(VK_LCONTROL)	& 0x8000);

	for (int i = Events::Keys::Num0; i <= Events::Keys::Num9; i++)
		m_RendererInfo.Events.Key[i] = !!(GetAsyncKeyState(i) & 0x8000);

	for (int i = Events::Keys::A; i <= Events::Keys::Z; i++)
		m_RendererInfo.Events.Key[i] = !!(GetAsyncKeyState(i) & 0x8000);

	for (int i = 0; i < 256; i++)
	{
		if (tmp[i] && tmp[i] == m_RendererInfo.Events.Key[i])
		{
			m_RendererHandler->OnKeyDown(char(i&0xFF), m_RendererInfo.Events.Mouse.X, m_RendererInfo.Events.Mouse.Y);
		}
		else if (!tmp[i] && tmp[i] != m_RendererInfo.Events.Key[i])
		{
			m_RendererHandler->OnKeyUp(char(i&0xFF), m_RendererInfo.Events.Mouse.X, m_RendererInfo.Events.Mouse.Y);
		}
	}
}

LRESULT Viewer::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if ((uMsg == WM_SYSCOMMAND && (wParam == SC_SCREENSAVE || wParam == SC_MONITORPOWER)))
	{
		return 0;
	}

	if (uMsg == WM_CLOSE || uMsg == WM_DESTROY)
	{
		PostQuitMessage(0);
		return 0;
	}

	if (uMsg == WM_CHAR && wParam == 0x1B)
	{
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void Viewer::DrawTime(float elapsedTime, float frameTime)
{
	static int      frame = 0;
	static float    to    = 0.0f;
	static int      fps   = 0;
	char            str[64];
	int             s, m, c;

	if (elapsedTime < 0.0f)
		return;

	if (m_RendererInfo.FullScreen)
		return;

	frame++;
	if ((elapsedTime - to) > 1.0f)
	{
		fps = frame;
		to = elapsedTime;
		frame = 0;
	}

	if (!(frame & 3))
	{
		m = Math::IFloor(elapsedTime / 60.0f);
		s = Math::IFloor(elapsedTime - 60.0f * static_cast<float>(m));
		c = Math::IFloor(elapsedTime * 100.0f) % 100;
		sprintf_s(str, "%02d:%02d:%02d  [%d fps] [frametime: %i ms]", m, s, c, fps, static_cast<int>(frameTime*1000.0f));
		SetWindowText(m_RendererInfo.hWnd, str);
	}
}

bool Viewer::ProcessMessage(bool* runFlag)
{
	static MSG msg;

	if (runFlag)
		*runFlag = true;

	if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
	{
		if (runFlag && msg.message == WM_QUIT)
			*runFlag = false;

		TranslateMessage(&msg);
		DispatchMessage(&msg);

		return true;
	}
	else
	{
		return false;
	}
}

LRESULT WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return 0;
}


} }
