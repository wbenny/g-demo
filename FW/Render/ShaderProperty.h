#pragma once
#include "../Math/Vector2.h"
#include "../Math/Vector3.h"
#include "../Math/Vector4.h"
#include "../Math/Matrix3.h"
#include "../Math/Matrix4.h"
#include "../String.h"
#include "../PtrList.h"
#include "../Ptr.h"
#include "../RefCounter.h"
#include "Shader.h"

namespace FW { namespace Render {

class ShaderUniform
{
	public:
		ShaderUniform() :
			m_Shader(NULL)
		{

		}

		ShaderUniform(const String& name) :
			m_Name(name)
		{
			Init(name);
		}

		bool Init(const String& name)
		{
			m_Shader = Shader::LastBoundShader;
			m_Name = name;

			m_Location = m_Shader->GetUniformLocation(m_Name.CharArray());

			return (m_Location != -1);
		}

		String GetName() const
		{
			return m_Name;
		}

		GLint GetLocation() const
		{
			return m_Location;
		}

		void Set(bool value)				 { if (m_Location != -1) GL::Uniform1i(m_Location, value ? true : false); }
		void Set(int value)					 { if (m_Location != -1) GL::Uniform1i(m_Location, value); }
		void Set(float value)				 { if (m_Location != -1) GL::Uniform1f(m_Location, value); }
		void Set(const Math::Vector2& value) { if (m_Location != -1) GL::Uniform2fv(m_Location, value.Cell); }
		void Set(const Math::Vector3& value) { if (m_Location != -1) GL::Uniform3fv(m_Location, value.Cell); }
		void Set(const Math::Vector4& value) { if (m_Location != -1) GL::Uniform4fv(m_Location, value.Cell); }
		void Set(const Math::Matrix3& value) { if (m_Location != -1) GL::UniformMatrix3f(m_Location, value.Cell); }
		void Set(const Math::Matrix4& value) { if (m_Location != -1) GL::UniformMatrix4f(m_Location, value.Cell); }

		void operator = (bool value)					{ Set(value); }
		void operator = (int value)						{ Set(value); }
		void operator = (float value)					{ Set(value); }
		void operator = (const Math::Vector2& value)	{ Set(value); }
		void operator = (const Math::Vector3& value)	{ Set(value); }
		void operator = (const Math::Vector4& value)	{ Set(value); }
		void operator = (const Math::Matrix3& value)	{ Set(value); }
		void operator = (const Math::Matrix4& value)	{ Set(value); }

	protected:
		Ptr<ShaderBase>	m_Shader;
		String			m_Name;
		GLint			m_Location;
};

} }
