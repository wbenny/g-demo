#pragma once
#include "../GL/GL.h"
#include "../Math/Vector3.h"
#include "../RefCounter.h"
#include "../Ptr.h"
#include "Texture.h"
#include "Viewer.h"
#include "Renderer.h"

namespace FW { namespace Render {

enum FramebufferPreset
{
	ByteBGRA	= TextureBit::BlueGreenRedAlpha | TextureBit::Byte	| TextureBit::NoFiltering,
	ByteBGR		= TextureBit::BlueGreenRed	    | TextureBit::Byte	| TextureBit::NoFiltering,
	ByteRG		= TextureBit::RedGreen			| TextureBit::Byte	| TextureBit::NoFiltering,
	ByteR		= TextureBit::Red				| TextureBit::Byte	| TextureBit::NoFiltering,
	FloatBGRA   = TextureBit::BlueGreenRedAlpha | TextureBit::Float | TextureBit::NoFiltering,
	FloatBGR    = TextureBit::BlueGreenRed	    | TextureBit::Float | TextureBit::NoFiltering,
	FloatRG		= TextureBit::RedGreen			| TextureBit::Float	| TextureBit::NoFiltering,
	FloatR		= TextureBit::Red				| TextureBit::Float | TextureBit::NoFiltering,
	DepthOnly   = TextureBit::Depth				|					  TextureBit::NoFiltering
};

class RenderTarget : public RefCounter
{
	public:
		RenderTarget() :
			m_FrameBufferHandle(-1),
			m_RenderBufferHandle(-1)
		{

		}


		RenderTarget(u32 textureFlags, u32 width = 0, u32 height = 0) :
			m_FrameBufferHandle(-1),
			m_RenderBufferHandle(-1)
		{
			Init(textureFlags, width, height);
		}

		virtual ~RenderTarget()
		{
			Done();
		}

		virtual bool Init(u32 textureFlags = FramebufferPreset::ByteBGRA, u32 width = 0, u32 height = 0)
		{
			m_Texture = new Texture(textureFlags, width, height);
			AttachTexture(m_Texture);

			m_RenderState = new RenderState(m_Texture->GetTextureFormat().Width, m_Texture->GetTextureFormat().Height);

			return true;
		}

		virtual void Done()
		{
			DetachTexture();
		}

		void AttachTexture(Texture* texture);
		void DetachTexture();

		const Texture* GetTexture() const { return m_Texture; }
		Texture* GetTexture() {	return m_Texture; }
		void SetTexture(Texture* texture) { m_Texture = texture; }

		void Bind();
		void Unbind();

		const RenderState* GetRenderState() const { return m_RenderState; }
		RenderState* GetRenderState() { return m_RenderState; }
		void SetRenderState(RenderState* renderState) { m_RenderState = renderState; }

	protected:
		GLuint			 m_FrameBufferHandle,
						 m_RenderBufferHandle;

		Ptr<RenderState> m_RenderState;

		Ptr<Texture>	 m_Texture;
};

} }
