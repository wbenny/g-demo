#pragma once
#include "../../String.h"
#include "../Texture.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class VolumeLight : public FullScreenQuadPostprocess
{
	public:
		VolumeLight()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("volumelight");

			InitShaderProperties();
		}

		virtual void InitShaderProperties()
		{
			FullScreenQuadPostprocess::InitShaderProperties();

			p_uCameraDepthTexture.Init("uCameraDepthTexture");
			p_uLightDepthTexture.Init("uLightDepthTexture");
		}

		virtual void Bind()
		{
			FullScreenQuadPostprocess::Bind();

			Render::Texture::SetActiveTexture(1);
			m_CameraDepthTexture->Bind();
			p_uCameraDepthTexture = 1;

			Render::Texture::SetActiveTexture(2);
			m_LightDepthTexture->Bind();
			p_uLightDepthTexture = 2;
		}

		const Texture* GetLightDepthTexture() const { return m_LightDepthTexture; }
		Texture* GetLightDepthTexture() { return m_LightDepthTexture; }
		void SetLightDepthTexture(Texture* texture) { m_LightDepthTexture = texture; }

		const Texture* GetCameraDepthTexture() const { return m_CameraDepthTexture; }
		Texture* GetCameraDepthTexture() { return m_CameraDepthTexture; }
		void SetCameraDepthTexture(Texture* texture) { m_CameraDepthTexture = texture; }

		ShaderUniform	p_uLightDepthTexture,
						p_uCameraDepthTexture;

	protected:
		Ptr<Texture>	m_LightDepthTexture,
						m_CameraDepthTexture;


};

} } }
