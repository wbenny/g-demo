#pragma once
#include "../../String.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class FXAA : public FullScreenQuadPostprocess
{
	public:
		FXAA()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("fxaa");

			SetInputSourcesCount(1);
			InitShaderProperties();
		}
};

} } }
