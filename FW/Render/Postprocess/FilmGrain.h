#pragma once
#include "../../String.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class FilmGrain : public FullScreenQuadPostprocess
{
	public:
		FilmGrain()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("filmgrain");

			m_Time = 0.0f;
			m_Strength = 0.8f;
			m_Intensity = 1.0f;

			SetInputSourcesCount(1);
			InitShaderProperties();
		}

		void InitShaderProperties()
		{
			FullScreenQuadPostprocess::InitShaderProperties();

			p_uTime.Init("uTime");
			p_uStrength.Init("uStrength");
			p_uIntensity.Init("uIntensity");
		}

		virtual void Bind()
		{
			FullScreenQuadPostprocess::Bind();

			p_uTime = m_Time;
			p_uStrength = m_Strength;
			p_uIntensity = m_Intensity;
		}

		float GetTime() const { return m_Time; }
		void SetTime(float time) { m_Time = time; }

		float GetStrength() const { return m_Strength; }
		void SetStrength(float strength) { m_Strength = strength; }

		float GetIntensity() const { return m_Intensity; }
		void SetIntensity(float intensity) { m_Intensity = intensity; }

		ShaderUniform	p_uTime,
						p_uStrength,
						p_uIntensity;


	protected:
		float m_Time, m_Strength, m_Intensity;
};

} } }
