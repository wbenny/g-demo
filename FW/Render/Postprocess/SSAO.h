#pragma once
#include "../../String.h"
#include "../Texture.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class SSAO : public FullScreenQuadPostprocess
{
	public:
		SSAO()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("ssao");

			//m_NoiseTexture = Content::ResourceLoader<Texture>::Read("textures\\noise.tga", true);

			m_AOInitColor = 1.0f;
			m_AOScale = 0.1f;
			
			//SetInputSourcesCount(1);
			InitShaderProperties();
		}

		virtual void InitShaderProperties()
		{
			FullScreenQuadPostprocess::InitShaderProperties();

			p_uUseColorTexture.Init("uUseColorTexture");
			//p_uNoiseTexture.Init("uNoiseTexture");
			p_uNormalDepthTexture.Init("uNormalDepthTexture");
			p_uAOInitColor.Init("uAOInitColor");
			p_uAOScale.Init("uAOScale");
		}

		virtual void Bind()
		{
			FullScreenQuadPostprocess::Bind();

			p_uUseColorTexture = m_Texture[0].IsValid();

			if (m_Texture[0].IsValid())
			{
				p_uUseColorTexture = true;

				Render::Texture::SetActiveTexture(0);
				m_Texture[0]->Bind();
				p_uInputTexture[0] = static_cast<int>(0);
			}

			Render::Texture::SetActiveTexture(1);
			m_NormalDepthTexture->Bind();
			p_uNormalDepthTexture = 1;

			p_uAOInitColor = m_AOInitColor;
			p_uAOScale = m_AOScale;

// 			Render::Texture::SetActiveTexture(2);
// 			m_NoiseTexture->Bind();
// 			p_uNoiseTexture = 2;
		}

		const Texture* GetNormalDepthTexture() const { return m_NormalDepthTexture; }
		Texture* GetNormalDepthTexture() { return m_NormalDepthTexture; }
		void SetNormalDepthTexture(Texture* texture) { m_NormalDepthTexture = texture; }

		float GetAOInitColor() const { return m_AOInitColor; }
		void SetAOInitColor(float color) { m_AOInitColor = color; }

		float GetAOScale() const { return m_AOScale; }
		void SetAOScale(float scale) { m_AOScale = scale; }

		ShaderUniform	p_uNormalDepthTexture,
// 						p_uNoiseTexture,
						p_uUseColorTexture,
						p_uAOInitColor,
						p_uAOScale;

	protected:
		Ptr<Texture>	m_NormalDepthTexture;
// 						m_NoiseTexture;

		float			m_AOInitColor,
						m_AOScale;


};

} } }
