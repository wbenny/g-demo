#pragma once
#include "../../String.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class RadialBlur : public FullScreenQuadPostprocess
{
	public:
		RadialBlur()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("radialblur");

			SetInputSourcesCount(1);
			InitShaderProperties();
		}
};

} } }
