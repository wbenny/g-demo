#pragma once
#include "../../String.h"
#include "../Texture.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class ToneMapping : public FullScreenQuadPostprocess
{
	public:
		ToneMapping()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("tonemapping");

			m_Exposure = 0.75f;

			SetInputSourcesCount(1);
			InitShaderProperties();
		}

		virtual void InitShaderProperties()
		{
			FullScreenQuadPostprocess::InitShaderProperties();

			p_uBloomTexture.Init("uBloomTexture");
			p_uExposure.Init("uExposure");
		}

		virtual void Bind()
		{
			FullScreenQuadPostprocess::Bind();

			Render::Texture::SetActiveTexture(1);
			m_BloomTexture->Bind();
			p_uBloomTexture = 1;

			//p_uExposure = m_Exposure;
		}

		const Texture* GetBloomTexture() const { return m_BloomTexture; }
		Texture* GetBloomTexture() { return m_BloomTexture; }
		void SetBloomTexture(Texture* texture) { m_BloomTexture = texture; }

		float GetExposure() const { return m_Exposure; }
		void SetExposure(float exposure) { m_Exposure = exposure; }

		ShaderUniform	p_uBloomTexture,
						p_uExposure;

	protected:
		Ptr<Texture>	m_BloomTexture;

		float			m_Exposure;
};

} } }
