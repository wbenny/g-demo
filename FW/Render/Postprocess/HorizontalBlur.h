#pragma once
#include "../../String.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class HorizontalBlur : public FullScreenQuadPostprocess
{
	public:
		HorizontalBlur()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("hblur");

			SetInputSourcesCount(1);
			InitShaderProperties();
		}
};

} } }
