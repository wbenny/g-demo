#pragma once
#include "../../Assert.h"
#include "../../String.h"
#include "../../Materials/MaterialBase.h"
#include "../FullScreenQuad.h"
#include "../ShaderManager.h"
#include "../Texture.h"

namespace FW { namespace Render { namespace Postprocess {

class FullScreenQuadPostprocess : public Materials::MaterialBase
{
	public:
		FullScreenQuadPostprocess(u32 inputSources = 0) :
			m_InputSources(inputSources)
		{
			FW_ASSERT(inputSources >= 0 && inputSources <= 4);

			m_FullScreenQuad = new FullScreenQuad(this);

			//SetInputSourcesCount(inputSources);

			if (!m_InputSources)
				return;

			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("quad");

			InitShaderProperties();
		}

		virtual void InitShaderProperties()
		{
			if (!m_Shader)
				return;

			MaterialBase::InitShaderProperties();

			p_uTextureCount.Init("uTextureCount");

			for (u32 i = 0; i < m_InputSources; i++)
			{
				p_uInputTexture[i].Init(String::Format("uInputTexture%i", i));
			}
		}

		virtual void Bind()
		{
			if (!m_Shader)
				return;

			MaterialBase::Bind();

			p_uTextureCount = static_cast<int>(m_InputSources);

			for (u32 i = 0; i < m_InputSources; i++)
			{
				Render::Texture::SetActiveTexture(i);
				m_Texture[i]->Bind();
				p_uInputTexture[i] = static_cast<int>(i);
			}
		}

		const Render::Texture* const GetInputTexture(u32 index = 0) const { return m_Texture[index]; }
		Render::Texture* GetInputTexture(u32 index = 0) { return m_Texture[index]; }
		void SetInputTexture(Render::Texture* texture, u32 index = 0) { m_Texture[index] = texture; }

		const Texture* GetOutputTexture() const { return m_FullScreenQuad->GetOutputTexture(); }
		Texture* GetOutputTexture()		{ return m_FullScreenQuad->GetOutputTexture(); }

		void SetAsFinal(bool final = true) { m_FullScreenQuad->SetAsFinal(final); }

		Texture* Draw() { return m_FullScreenQuad->Draw(); }

		//! Properties.
		Render::ShaderUniform	p_uInputTexture[4], p_uTextureCount;

	protected:
		void SetInputSourcesCount(u32 inputSources)
		{
			m_InputSources = inputSources;
		}

		u32						m_InputSources;

		Ptr<Texture>			m_Texture[4];
		Ptr<FullScreenQuad>		m_FullScreenQuad;

};

} } }
