#pragma once
#include "../../String.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class VerticalBlur : public FullScreenQuadPostprocess
{
	public:
		VerticalBlur()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("vblur");

			SetInputSourcesCount(1);
			InitShaderProperties();
		}
};

} } }
