#pragma once
#include "../../String.h"
#include "../Texture.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class DepthOfField : public FullScreenQuadPostprocess
{
	public:
		DepthOfField()
		{		
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("dof");

			m_ShowFocus = false;
			m_AutoFocus = false;

			m_Focus = Math::Vector2(0.5f, 0.2f);
			m_FocalDepth = 115.0f;

			m_NearDofStart = 3.0f;
			m_NearDofDistance = 30.0f;

			m_FarDofStart = 20.0f;
			m_FarDofDistance = 20.0f;

			m_Blur = 3.2f;

			InitShaderProperties();
		}

		virtual void InitShaderProperties()
		{
			FullScreenQuadPostprocess::InitShaderProperties();

			p_uRenderedTexture.Init("uRenderedTexture");
			p_uDepthTexture.Init("uDepthTexture");

			p_uShowFocus.Init("uShowFocus");
			p_uAutoFocus.Init("uAutoFocus");
			p_uFocus.Init("uFocus");
			p_uFocalDepth.Init("uFocalDepth");
			p_uNearDofStart.Init("uNearDofStart");
			p_uNearDofDistance.Init("uNearDofDistance");
			p_uFarDofStart.Init("uFarDofStart");
			p_uFarDofDistance.Init("uFarDofDistance");
			p_uBlur.Init("uBlur");
		}

		virtual void Bind()
		{
			FullScreenQuadPostprocess::Bind();

			Render::Texture::SetActiveTexture(0);
			m_DepthTexture->Bind();
			p_uDepthTexture = 0;

			Render::Texture::SetActiveTexture(1);
			m_RenderedTexture->Bind();
			p_uRenderedTexture = 1;

			p_uShowFocus = m_ShowFocus;
			p_uAutoFocus = m_AutoFocus;
			p_uFocus = m_Focus;
			p_uFocalDepth = m_FocalDepth;
			p_uNearDofStart = m_NearDofStart;
			p_uNearDofDistance = m_NearDofDistance;
			p_uFarDofStart = m_FarDofStart;
			p_uFarDofDistance = m_FarDofDistance;
			p_uBlur = m_Blur;
		}

		const Texture* GetDepthTexture() const { return m_DepthTexture; }
		Texture* GetDepthTexture() { return m_DepthTexture; }
		void SetDepthTexture(Texture* texture) { m_DepthTexture = texture; }
	
		const Texture* GetRenderedTexture() const { return m_RenderedTexture; }
		Texture* GetRenderedTexture() { return m_RenderedTexture; }
		void SetRenderedTexture(Texture* texture) { m_RenderedTexture = texture; }

		bool IsFocusShown() const { return m_ShowFocus; }
		void SetFocusVisibility(bool visibility = true) { m_ShowFocus = visibility; }

		bool IsAutoFocused() const { return m_AutoFocus; }
		void SetAutoFocusState(bool state = true) { m_AutoFocus = state; }

		const Math::Vector2& GetFocus() const { return m_Focus; }
		void SetFocus(const Math::Vector2& focus) { m_Focus = focus; }

		float GetFocalDepth() const { return m_FocalDepth; }
		void SetFocalDepth(float focalDepth) { m_FocalDepth = focalDepth; }

		float GetNearStart() const { return m_NearDofStart; }
		void SetNearStart(float nearStart) { m_NearDofStart = nearStart; }

		float GetNearDistance() const { return m_NearDofDistance; }
		void SetNearDistance(float nearDistance) { m_NearDofDistance = nearDistance; }

		float GetFarStart() const { return m_FarDofStart; }
		void SetFarStart(float farStart) { m_FarDofStart = farStart; }

		float GetFarDistance() const { return m_FarDofDistance; }
		void SetFarDistance(float farDistance) { m_FarDofDistance = farDistance; }

		float GetBlur() const { return m_Blur; }
		void SetBlur(float blur) { m_Blur = blur; }

		ShaderUniform	p_uDepthTexture,
						p_uRenderedTexture,
						p_uShowFocus,
						p_uAutoFocus,
						p_uFocus,
						p_uFocalDepth,
						p_uNearDofStart,
						p_uNearDofDistance,
						p_uFarDofStart,
						p_uFarDofDistance,
						p_uBlur;

	protected:
		Ptr<Texture>	m_DepthTexture,
						m_RenderedTexture;

		bool m_ShowFocus, m_AutoFocus;

		Math::Vector2 m_Focus;

		float	m_FocalDepth,
				m_NearDofStart,
				m_NearDofDistance,
				m_FarDofStart,
				m_FarDofDistance,
				m_Blur;


};

} } }
