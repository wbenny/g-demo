#pragma once
#include "PostprocessBase.h"

namespace FW { namespace Render { namespace Postprocess {

class NormalDepth : public PostprocessBase
{
	public:
		NormalDepth()
		{
			m_Shader = new Render::Shader();
			m_Shader->Init(
				Render::ShaderChunkManager::GetInstance().GetChunkByName("vp_normaldepth"),
				Render::ShaderChunkManager::GetInstance().GetChunkByName("fp_normaldepth")
				);

			InitShaderProperties();
		}
};

} } }
