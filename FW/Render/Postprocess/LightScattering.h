#pragma once
#include "../../String.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class LightScattering : public FullScreenQuadPostprocess
{
	public:
		LightScattering()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("lightscattering");

			m_Exposure = 0.0034f;
			m_Decay    = 1.0f;
			m_Density  = 0.84f;
			m_Weight   = 5.65f;

			m_DefaultBlurPos = Math::Vector2(0.5f, 0.15f);

			SetInputSourcesCount(1);
			InitShaderProperties();
		}

		void InitShaderProperties()
		{
			FullScreenQuadPostprocess::InitShaderProperties();

			p_uLightPositionOnScreen.Init("uLightPositionOnScreen");
			p_uExposure.Init("uExposure");
			p_uDecay.Init("uDecay");
			p_uDensity.Init("uDensity");
			p_uWeight.Init("uWeight");
		}

		virtual void Bind()
		{
			FullScreenQuadPostprocess::Bind();

			if (m_SunNode)
			{
				Math::Vector4 pos = Unproject(m_SunNode->GetWorldMatrix().GetTranslation());			
				p_uLightPositionOnScreen = (Math::Vector2(pos.X, pos.Y) * 0.5f + 0.5f);
			}
			else
			{
				p_uLightPositionOnScreen = m_DefaultBlurPos;
			}

			p_uExposure = m_Exposure;
			p_uDecay = m_Decay;
			p_uDensity = m_Density;
			p_uWeight = m_Weight;		
		}

		const Scene::Node* GetAssignedSunNode() const { return m_SunNode; }
		Scene::Node* GetAssignedSunNode() { return m_SunNode; }
		void SetAssignedSunNode(Scene::Node* sunNode) { m_SunNode = sunNode; }

		const Scene::CameraNode* GetAssignedCamera() const { return m_Camera; }
		Scene::CameraNode* GetAssignedCamera() { return m_Camera; }
		void SetAssignedCamera(Scene::CameraNode* camera) { m_Camera = camera; }

		const Math::Vector2& GetDefaultBlurPosition() const { return m_DefaultBlurPos; }
		void SetDefaultBlurPosition(const Math::Vector2& p) { m_DefaultBlurPos = p; }

		float GetExposure() const { return m_Exposure; }
		void SetExposure(float exposure) { m_Exposure = exposure; }

		float GetDecay() const { return m_Decay; }
		void SetDecay(float decay) { m_Decay = decay; }

		float GetDensity() const { return m_Density; }
		void SetDensity(float density) { m_Density = density; }

		float GetWeight() const { return m_Weight; }
		void SetWeight(float weight) { m_Weight = weight; }

		ShaderUniform	p_uLightPositionOnScreen,
						p_uExposure,
						p_uDecay,
						p_uDensity,
						p_uWeight;


	protected:
		Ptr<Scene::Node> m_SunNode;
		Ptr<Scene::CameraNode> m_Camera;

		Math::Vector2 m_DefaultBlurPos;

		float m_Exposure, m_Decay, m_Density, m_Weight;

		Math::Vector4 Unproject(const Math::Vector3& worldPos)
		{
			Math::Matrix4 projection;
			Math::Matrix4 view;
			Math::Vector4 result;
			Math::Vector4 wp = Math::Vector4(worldPos.X, worldPos.Y, worldPos.Z, 1.0f);

			projection = m_Camera->GetProjectionMatrix();
			view  = m_Camera->GetWorldMatrix();

			result = projection * view * wp;
			result *= 1.0f / result.W;
			return result;
		}

		Math::Vector4 Project(const Math::Vector4& ndcPos)
		{
			Math::Matrix4 projection;
			Math::Matrix4 view;
			Math::Vector4 result;
			Math::Vector4 wp = ndcPos;

			projection = m_Camera->GetProjectionMatrix();
			view  = m_Camera->GetWorldMatrix();
			result = (projection * view).Inverted() * wp;

			result *= 1.0f / result.W;

			return result;
		}
};

} } }
