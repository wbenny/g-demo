#pragma once
#include "../../String.h"
#include "FullScreenQuadPostprocess.h"

namespace FW { namespace Render { namespace Postprocess {

class Bloom : public FullScreenQuadPostprocess
{
	public:
		Bloom()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("bloom");

			SetInputSourcesCount(1);
			InitShaderProperties();
		}
};

} } }
