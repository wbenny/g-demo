#pragma once
#include "../Types.h"
#include "../GL/GL.h"
#include "../Utils/Swap.h"
#include "../Content/Resource.h"
#include "Viewer.h"

namespace FW { namespace Render {

enum TextureBit
{
	// Color channels.
	Red			= 1,
	RedGreen	= 2,
	BlueGreenRed = 4,
	BlueGreenRedAlpha = 8,

	Depth		= 16,

	// Clamp or repeat?
	Clamp		= 32,
	Repeat		= 64,

	// Antialiasing.
	NoFiltering	= 128,
	LinearFiltering = 256,
	LinearFilteringWithMipmap = 512,	// Must include Mipmap bit!

	// Format type.
	Byte		= 1024,
	Float		= 2048,

	// Mipmapping.
	Mipmap		= 4096
};

class Texture : public Content::Resource
{
	public:
		struct TextureFormat
		{
			u16   Width;
			u16   Height;

			u16   MinFilter;
			u16   MagFilter;

			u16   WrapS;
			u16   WrapT;

			u16   InternalFormat;
			u16   ExternalFormat;

			u16   Type;
			
			bool  MipMap;

			u8*   DataPointer;

			TextureFormat() : DataPointer(NULL) {}
			TextureFormat(u32 createFlags, u32 width = 0, u32 height = 0, const u8* dataPointer = NULL) { Init(createFlags, width, height, dataPointer); }
			virtual ~TextureFormat() { Done(); }

			virtual bool Init(u32 createFlags, u32 width = 0, u32 height = 0, const u8* dataPointer = NULL)
			{
				Width = width;
				Height = height;

				InternalFormat = GL_RGBA8;
				ExternalFormat = GL_BGRA;

				Type = GL_UNSIGNED_BYTE;

				MinFilter   = MagFilter = GL_NEAREST;
				WrapS       = WrapT = GL_CLAMP_TO_EDGE;
				MipMap      = false;
				DataPointer = const_cast<u8*>(dataPointer);

				// Depth... Color channels are defined for each type.
				if (createFlags & TextureBit::Depth)
				{
					InternalFormat = ExternalFormat = GL_DEPTH_COMPONENT;
					Type = GL_UNSIGNED_BYTE;
				}

				// Clamp or repeat?
				if (createFlags & TextureBit::Clamp)
				{
					WrapS = WrapT = GL_CLAMP_TO_EDGE;
				}

				else if (createFlags & TextureBit::Repeat)
				{
					WrapS = WrapT = GL_REPEAT;
				}

				// Antialiasing.
				if (createFlags & TextureBit::NoFiltering)
				{
					MinFilter = MagFilter = GL_NEAREST;
				}

				else if (createFlags & TextureBit::LinearFiltering)
				{
					MinFilter = MagFilter = GL_LINEAR;
				}

				else if (createFlags & TextureBit::LinearFilteringWithMipmap)
				{
					MinFilter = GL_LINEAR_MIPMAP_LINEAR;
					MagFilter = GL_LINEAR;
				}

				// Format type + color channel.
				if (createFlags & TextureBit::Byte)
				{
					Type = GL_UNSIGNED_BYTE;

					if (createFlags & TextureBit::Red)
					{
						InternalFormat = GL_R8;
						ExternalFormat = GL_RED;
					}

					else if (createFlags & TextureBit::RedGreen)
					{
						InternalFormat = GL_RG8;
						ExternalFormat = GL_RG;
					}

					else if (createFlags & TextureBit::BlueGreenRed)
					{
						InternalFormat = GL_RGB8;
						ExternalFormat = GL_BGR;
					}

					else if (createFlags & TextureBit::BlueGreenRedAlpha)
					{
						InternalFormat = GL_RGBA8;
						ExternalFormat = GL_BGRA;
					}
				}

				else if (createFlags & TextureBit::Float)
				{
					Type = GL_FLOAT;

					if (createFlags & TextureBit::Red)
					{
						InternalFormat = GL_R32F;
						ExternalFormat = GL_RED;
					}

					else if (createFlags & TextureBit::RedGreen)
					{
						InternalFormat = GL_RG32F;
						ExternalFormat = GL_RG;
					}

					else if (createFlags & TextureBit::BlueGreenRed)
					{
						InternalFormat = GL_RGB32F;
						ExternalFormat = GL_BGR;
					}

					else if (createFlags & TextureBit::BlueGreenRedAlpha)
					{
						InternalFormat = GL_RGBA32F;
						ExternalFormat = GL_BGRA;
					}
				}

				// Mipmapping.
				if (createFlags & TextureBit::Mipmap)
				{
					MipMap = true;
				}

				return true;
			}

			virtual void Done()
			{
				if (DataPointer)
					delete[] DataPointer;
			}
		};

		Texture() :
			Content::Resource(Content::ResourceType::Texture),
			m_TextureHandle(-1)
		{

		}

		Texture(u32 createFlags, u32 width = 0, u32 height = 0, const u8* dataPointer = NULL) :
			Content::Resource(Content::ResourceType::Texture),
			m_TextureHandle(-1)
		{
			Init(createFlags, width, height, dataPointer);
		}
			
		virtual ~Texture()
		{
			Done();
		}

		virtual bool Init(u32 createFlags, u32 width = 0, u32 height = 0, const u8* dataPointer = NULL)
		{
			if (!width || !height)
			{
				width  = static_cast<u16>(Viewer::GetInstance().GetInfo().Width );
				height = static_cast<u16>(Viewer::GetInstance().GetInfo().Height);
			}

			m_TextureFormat.Init(createFlags, width, height, dataPointer);
			Internal_CreateTexture();

			return true;
		}

		virtual void Done()
		{
			if (m_TextureHandle != -1)
			{
				GL::DeleteTexture(m_TextureHandle);
				Unbind();
			}

			m_TextureFormat.Done();
		}

		void Bind() { GL::BindTexture(GL_TEXTURE_2D, m_TextureHandle); }
		void Unbind() { GL::BindTexture(GL_TEXTURE_2D, 0); }

		void GenerateMipmaps() { Bind(); GL::GenerateMipmap(GL_TEXTURE_2D); }

		GLuint GetHandle() const { return m_TextureHandle; }
		bool IsInitialized() const { return (m_TextureHandle != -1); }

		const TextureFormat& GetTextureFormat() const { return m_TextureFormat; }

		static void SetActiveTexture(u8 index) { GL::ActiveTexture(GL_TEXTURE0 + index); }

	protected:
		GLuint m_TextureHandle;
		TextureFormat m_TextureFormat;

		void Internal_CreateTexture()
		{
			m_TextureHandle = GL::GenTexture();

			Bind();

			GL::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,	static_cast<GLint>(m_TextureFormat.MinFilter));
			GL::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,	static_cast<GLint>(m_TextureFormat.MagFilter));

			GL::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,		static_cast<GLint>(m_TextureFormat.WrapS));
			GL::TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,		static_cast<GLint>(m_TextureFormat.WrapT));

			GL::TexImage2D(
				GL_TEXTURE_2D,
				0,
				static_cast<GLint>       (m_TextureFormat.InternalFormat),
				static_cast<GLsizei>     (m_TextureFormat.Width),
				static_cast<GLsizei>     (m_TextureFormat.Height),
				static_cast<GLenum>      (m_TextureFormat.ExternalFormat),
				static_cast<GLenum>      (m_TextureFormat.Type),
				static_cast<GLvoid*>     (m_TextureFormat.DataPointer)
			);

			if (m_TextureFormat.MipMap)
				GenerateMipmaps();

			// glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		}
};

} }
