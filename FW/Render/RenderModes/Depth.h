#pragma once
#include "../../Ptr.h"
#include "../../String.h"
#include "../ShaderChunkManager.h"
#include "RenderModeBase.h"

namespace FW { namespace Render { namespace RenderModes {

class Depth : public RenderModeBase
{
	public:
		Depth()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("depth");

			InitShaderProperties();
		}

		static RenderModeBase* GetInstance()
		{
			static Ptr<Depth> instance = NULL;

			if (!instance)
				instance = new Depth();

			return instance;
		}
};

} } }
