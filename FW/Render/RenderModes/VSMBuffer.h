#pragma once
#include "../../Ptr.h"
#include "../../String.h"
#include "../ShaderChunkManager.h"
#include "RenderModeBase.h"

namespace FW { namespace Render { namespace RenderModes {

class VSMBuffer : public RenderModeBase
{
	public:
		VSMBuffer()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("vsmbuffer");

			InitShaderProperties();
		}

		static RenderModeBase* GetInstance()
		{
			static Ptr<VSMBuffer> instance = NULL;

			if (!instance)
				instance = new VSMBuffer();

			return instance;
		}
};

} } }
