#pragma once
#include "../../Ptr.h"
#include "../../String.h"
#include "../ShaderChunkManager.h"
#include "RenderModeBase.h"

namespace FW { namespace Render { namespace RenderModes {

class Position : public RenderModeBase
{
	public:
		Position()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("position");

			InitShaderProperties();
		}

		static RenderModeBase* GetInstance()
		{
			static Ptr<Position> instance = NULL;

			if (!instance)
				instance = new Position();

			return instance;
		}
};

} } }
