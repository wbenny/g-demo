#pragma once
#include "../../Ptr.h"
#include "../../String.h"
#include "../ShaderManager.h"
#include "RenderModeBase.h"

namespace FW { namespace Render { namespace RenderModes {

class NoShading : public RenderModeBase
{
	public:
		NoShading()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("noshading");
			InitShaderProperties();
		}

		void InitShaderProperties()
		{
			MaterialBase::InitShaderProperties();

			p_uColor.Init("uColor");
		}

		void Bind()
		{
			MaterialBase::Bind();

			p_uColor = m_Color;
		}

		static RenderModeBase* GetInstance()
		{
			static Ptr<NoShading> instance = NULL;

			if (!instance)
				instance = new NoShading();

			return instance;
		}

		const Math::Vector3& GetColor() const { return m_Color; }
		void SetColor(const Math::Vector3& color) { m_Color = color; }

		Render::ShaderUniform p_uColor;

	protected:
		Math::Vector3 m_Color;
};

} } }
