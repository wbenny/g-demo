#pragma once
#include "../../Ptr.h"
#include "../../String.h"
#include "../ShaderChunkManager.h"
#include "RenderModeBase.h"

namespace FW { namespace Render { namespace RenderModes {

class NormalDepth : public RenderModeBase
{
	public:
		NormalDepth()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("normaldepth");

			InitShaderProperties();
		}

		static RenderModeBase* GetInstance()
		{
			static Ptr<NormalDepth> instance = NULL;

			if (!instance)
				instance = new NormalDepth();

			return instance;
		}
};

} } }
