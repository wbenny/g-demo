#pragma once
#include "../GL/GL.h"
#include "../Types.h"
#include "../Memory.h"
#include "../RefCounter.h"
#include "../Math/Vector3.h"
#include "RenderModes/RenderModeBase.h"

#include "../Utils/Resolution.h"

namespace FW { namespace Render {

enum FaceCullingMode
{
	NoCulling		= 0,
	Front			= GL_FRONT,
	Back			= GL_BACK,
	FrontAndBack	= GL_FRONT_AND_BACK,
};

enum DepthTestMode
{
	Enabled		= GL_LEQUAL,
	Disabled	= GL_ALWAYS
};

enum ClearMode
{
	DepthBuffer   = GL_DEPTH_BUFFER_BIT,
	ColorBuffer   = GL_COLOR_BUFFER_BIT,
	StencilBuffer = GL_STENCIL_BUFFER_BIT
};

struct DrawMode
{
	GLenum m_DrawMode;
	GLenum m_PolygonMode;

	DrawMode() : m_DrawMode(0), m_PolygonMode(0) {}
	DrawMode(GLenum drawMode, GLenum polygonMode) : m_DrawMode(drawMode), m_PolygonMode(polygonMode) {}

	operator bool() const { return (m_DrawMode != 0); }

	static const DrawMode Default;
	static const DrawMode Solid;
	static const DrawMode Wired;
	static const DrawMode Particle;
};

class RenderState : public RefCounter
{
	public:
		Math::Vector3					m_ClearColor;
		RenderModes::RenderModeBase*	m_RenderMode;
	
		DrawMode						m_DrawMode;
		DepthTestMode					m_DepthTestMode;
		FaceCullingMode					m_FaceCullingMode;

		u32								m_ClearMode,
										m_ViewportWidth,
										m_ViewportHeight;

		RenderState()
		{
			m_ClearColor = Math::Vector3(1.0f, 1.0f, 1.0f);
			m_RenderMode = NULL;
			m_DepthTestMode = (DepthTestMode)NULL;
			m_DrawMode = DrawMode::Default;
			m_ClearMode = ClearMode::ColorBuffer | ClearMode::DepthBuffer;
			m_FaceCullingMode = FaceCullingMode::Back;
			m_ViewportWidth = FW_VIEWPORT_WIDTH;
			m_ViewportHeight = FW_VIEWPORT_HEIGHT;
		}

		RenderState(u32 width, u32 height)
		{
			m_ClearColor = Math::Vector3(1.0f, 1.0f, 1.0f);
			m_RenderMode = NULL;
			m_DepthTestMode = (DepthTestMode)NULL;
			m_DrawMode = DrawMode::Default;
			m_ClearMode = ClearMode::ColorBuffer | ClearMode::DepthBuffer;
			m_FaceCullingMode = FaceCullingMode::Back;
			m_ViewportWidth = width;
			m_ViewportHeight = height;
		}

		RenderState(u32 width, u32 height,
			const Math::Vector3& clearColor,
			RenderModes::RenderModeBase* renderMode,
			FaceCullingMode faceCullingMode
			)
		{
			m_ClearColor = clearColor;
			m_RenderMode = renderMode;
			m_DepthTestMode = (DepthTestMode)NULL;
			m_DrawMode = DrawMode::Default;
			m_ClearMode = ClearMode::ColorBuffer | ClearMode::DepthBuffer;
			m_FaceCullingMode = faceCullingMode;
			m_ViewportWidth = width;
			m_ViewportHeight = height;
		}

		void ApplyRenderState()
		{
			ApplyCullFace();
			ApplyResize();
			ApplyClear();
		}

	protected:
		void ApplyCullFace()
		{
			if (m_FaceCullingMode == FaceCullingMode::NoCulling)
			{
				GL::Disable(GL_CULL_FACE);
			}
			else
			{
				GL::Enable(GL_CULL_FACE);
				GL::CullFace(m_FaceCullingMode);
			}
		}

		void ApplyClear()
		{
			if (!m_ClearMode)
			{
				return;
			}
			else if (m_ClearMode & ClearMode::ColorBuffer)
			{
				GL::ClearColor(
					m_ClearColor.X,
					m_ClearColor.Y,
					m_ClearColor.Z,
					1.0f);
			}

			GL::Clear(static_cast<GLbitfield>(m_ClearMode));
		}

		void ApplyResize()
		{
			GL::Viewport(
				0,
				0,
				static_cast<GLsizei>(m_ViewportWidth),
				static_cast<GLsizei>(m_ViewportHeight));
		}
};

} }
