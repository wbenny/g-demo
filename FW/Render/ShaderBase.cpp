#include "ShaderBase.h"
#include "ShaderChunkManager.h"
#include "../Utils/LoadFile.h"
#include <cstdio>

#ifdef _DEBUG
#	define PRINTSTATUS(obj, type)	Debug(obj, type)
#else
#	define PRINTSTATUS(obj, type)
#endif

namespace FW { namespace Render {

ShaderBase::ShaderBase() :
	Content::Resource(Content::ResourceType::Shader),
	m_iVertexShader(0),
	m_iFragmentShader(0),
	m_iProgram(0)
{

}

ShaderBase::~ShaderBase()
{
	End();
}

bool ShaderBase::Init(const String& vertexSource, const String& fragmentSource)
{
	const GLchar *v = static_cast<const GLchar*>(vertexSource.CharArray());
	const GLchar *f = static_cast<const GLchar*>(fragmentSource.CharArray());

	// Zkompilovat vertex shader.
	m_iVertexShader = GL::CreateShader(GL_VERTEX_SHADER);
	GL::ShaderSource(m_iVertexShader, 1, &v, NULL);
	GL::CompileShader(m_iVertexShader);
	PRINTSTATUS(m_iVertexShader, "vertex shader");

	// Zkompilovat fragment shader.
	m_iFragmentShader = GL::CreateShader(GL_FRAGMENT_SHADER);
	GL::ShaderSource(m_iFragmentShader, 1, &f, NULL);
	GL::CompileShader(m_iFragmentShader);
	PRINTSTATUS(m_iFragmentShader, "fragment shader");

	// Slinkovat dohromady
	m_iProgram = GL::CreateProgram();
	GL::AttachShader(m_iProgram, m_iVertexShader);
	GL::AttachShader(m_iProgram, m_iFragmentShader);
	GL::LinkProgram(m_iProgram);
	PRINTSTATUS(m_iProgram, "program");

	return true;
}

void ShaderBase::End()
{
	if (!m_iProgram)
		GL::DeleteProgram(m_iProgram);

	if (!m_iVertexShader)
		GL::DeleteShader(m_iVertexShader);

	if (!m_iFragmentShader)
		GL::DeleteShader(m_iFragmentShader);
}

void ShaderBase::Bind()
{
	GL::UseProgram(m_iProgram);
}

void ShaderBase::Unbind()
{
	GL::UseProgram(0);
}

void ShaderBase::Debug(GLuint object, const char* type)
{
	const int MAX_LOG_LENGTH = 4096;
	char log[MAX_LOG_LENGTH];
	int  logLength = 0;

	if (GL::IsShader(object))
		GL::GetShaderInfoLog(object, MAX_LOG_LENGTH, &logLength, log);
	else
		GL::GetProgramInfoLog(object, MAX_LOG_LENGTH, &logLength, log);

	if (logLength > 0)
	{
		FW_DEBUG_PRINT("[*] Info log of %s:\r\n    %s\r\n    ----------------\r\n", type, log);
	}
}

GLint ShaderBase::GetAttribLocation(const char* name)
{
	return GL::GetAttribLocation(m_iProgram, name);
}

GLint ShaderBase::GetUniformLocation(const char* name)
{
	return GL::GetUniformLocation(m_iProgram, name);
}

} }

#undef PRINTSTATUS
