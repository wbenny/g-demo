#pragma once
#include "../Memory.h"
#include "../GL/GL.h"
#include "BufferObject.h"
#include "IBufferArray.h"

namespace FW { namespace Render {

class IndexBuffer : public IBufferArray
{
	public:
		IndexBuffer() :
			IBufferArray(BufferObjectTypes::ElementBuffer)
		{

		}
		
		void Bind()
		{
			m_BufferObject.Bind();
		}

		void Unbind()
		{
			m_BufferObject.Unbind();
		}

		void UpdateBuffer()
		{
			m_BufferObject.Bind();
			m_BufferObject.Init(m_DataSize * sizeof(u32), reinterpret_cast<GLvoid*>(m_Data));
		}

		u32 GetSize() const
		{
			return m_DataSize;
		}

		void SetSize(u32 newSize)
		{
			m_Data = reinterpret_cast<u32*>(Memory::Realloc(m_Data, m_DataSize * sizeof(u32), newSize * sizeof(u32)));
			m_DataSize = newSize;
		}

		const void* GetDataPointer() const
		{
			return m_Data;
		}

		void SetData(const void* dataPtr, u32 count = 0)
		{
			if (!count)
				count = m_DataSize;

			count *= sizeof(u32);

			Memory::Copy(m_Data, dataPtr, count);
		}
};

} }
