#pragma once
#include "../Singleton.h"

namespace FW { namespace Render {

class MeshManager : Singleton<MeshManager>
{
	friend class Singleton<MeshManager>;

	public:
		void AddMesh() {}

	private:
		MeshManager() {}
};

} }
