#pragma once
#include "../RefCounter.h"
#include "../Scene/RootNode.h"
#include "../Scene/CameraNode.h"
#include "../Scene/Primitives/FullScreenQuad.h"

namespace FW { namespace Render {

class Texture;

class FullScreenQuad : public RefCounter
{
	public:
		FullScreenQuad(Materials::MaterialBase* postProcess) :
			m_Final(false)
		{
			m_Scene    = new Scene::RootNode();
			m_Camera   = new Scene::CameraNode();
			m_Quad	   = new Scene::Primitives::FullScreenQuad();
			
			m_Quad->SetMaterial(postProcess);

			m_Scene->Add(m_Camera);
			m_Scene->Add(m_Quad);
		}

		~FullScreenQuad()
		{

		}

		const Texture* GetOutputTexture() const { return m_Camera->GetFramebuffer()->GetTexture(); }
			  Texture* GetOutputTexture()		{ return m_Camera->GetFramebuffer()->GetTexture(); }

		const RenderTarget* GetFramebuffer() const { return m_Camera->GetFramebuffer(); }
		RenderTarget* GetFramebuffer() { return m_Camera->GetFramebuffer(); }

		bool IsFinal() const { return m_Final; }
		void SetAsFinal(bool final = true)
		{
			if (final && !m_Final)
			{
				m_Texture = m_Camera->GetFramebuffer()->GetTexture();
				m_Camera->GetFramebuffer()->AttachTexture(NULL);
			}
			else if (!final && m_Final)
			{
				m_Camera->GetFramebuffer()->AttachTexture(m_Texture);
			}
			
			m_Final = final;
		}

		const Scene::RootNode* GetScene() const { return m_Scene; }
			  Scene::RootNode* GetScene()		{ return m_Scene; }

		Texture* Draw() { Renderer::GetInstance().Draw(m_Scene); return GetOutputTexture(); }

	protected:
		Ptr<Scene::RootNode>						m_Scene;
		Ptr<Scene::CameraNode>						m_Camera;
		Ptr<Scene::Primitives::FullScreenQuad>		m_Quad;

		Ptr<Texture>								m_Texture;

		bool										m_Final;
};

} }
