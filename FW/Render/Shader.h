#pragma once
#include "ShaderBase.h"

namespace FW { namespace Render {

class Shader : public ShaderBase
{
	public:
		static Shader* LastBoundShader;

		Shader()
		{

		}

		void Bind()
		{
			if (IsBound())
				return;
			
			ShaderBase::Bind();

			LastBoundShader = this;
		}

		void Unbind()
		{
			if (IsBound())
				ShaderBase::Unbind();

			LastBoundShader = NULL;
		}

		bool IsBound() const
		{
			return (LastBoundShader == this);
		}
};

} }
