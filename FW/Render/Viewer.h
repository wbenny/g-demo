#pragma once
#include "../Types.h"
#include "../Singleton.h"
#include "Events.h"
#include "IRendererHandler.h"

static LRESULT CALLBACK __Viewer_WndProc(HWND hWnd, UINT uMsg,
	WPARAM wParam, LPARAM lParam);

namespace FW { namespace Render {

struct RendererInfo
{
	bool FullScreen;
	int  Width, Height;

	Events::EventInfo Events;

	HINSTANCE hInstance;
	HWND      hWnd;
	HDC		  hDC;
	HGLRC     hRC;
};

class Viewer : public Singleton<Viewer>
{
	friend class Singleton<Viewer>;
	friend LRESULT CALLBACK ::__Viewer_WndProc(HWND hWnd, UINT uMsg,
		WPARAM wParam, LPARAM lParam);

	public:
		bool Init(int width, int height, bool fullscreen);
		void End();

		void Run();

		static bool ProcessMessage(bool* runFlag = NULL);

		void SetHandler(IRendererHandler* handler);

		const RendererInfo& GetInfo();

	protected:
		Viewer();
		void DrawTime(float elapsedTime, float frameTime);

		void ProcessMouseInput();
		void ProcessKeyboardInput();

		LRESULT WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

		u32 m_StartTime, m_FrameTime;

		bool m_Active;

		const char* m_szClassName;
		IRendererHandler* m_RendererHandler;
		RendererInfo m_RendererInfo;
};

} }
