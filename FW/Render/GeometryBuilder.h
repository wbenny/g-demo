#pragma once
#include "../List.h"
#include "../Math/BoundingBox.h"
#include "Geometry.h"

namespace FW { namespace Render {

template<class T>
class GeometryBuilder
{
	public:
		GeometryBuilder()
		{

		}

		void AddQuad(const T& v1, const T& v2, const T& v3, const T& v4)
		{
			u32 i1 = AddVertex(v1),
				i2 = AddVertex(v2),
				i3 = AddVertex(v3),
				i4 = AddVertex(v4);

			AddFace4(i1, i2, i3, i4);
		}

		void AddTriangle(const T& v1, const T& v2, const T& v3)
		{
			u32 i1 = AddVertex(v1),
				i2 = AddVertex(v2),
				i3 = AddVertex(v3);

			AddFace3(i1, i2, i3);
		}

		u32 AddVertex(const T& vertex)
		{
			m_Vertices.Add(vertex);
			return (m_Vertices.GetCount() - 1);
		}

		void AddFace3(u32 i1, u32 i2, u32 i3)
		{
			m_Indices.Add(i1);
			m_Indices.Add(i2);
			m_Indices.Add(i3);
		}

		void AddFace4(u32 i1, u32 i2, u32 i3, u32 i4)
		{
			m_Indices.Add(i1);
			m_Indices.Add(i2);
			m_Indices.Add(i3);
			m_Indices.Add(i1);
			m_Indices.Add(i3);
			m_Indices.Add(i4);
		}

		void Clear()
		{
			m_Vertices.Clear();
			m_Indices.Clear();
		}

		void CalculateNormals()
		{
			Math::Vector3 vertexPosition[3], normal;

			for (u32 i = 0; i < m_Vertices.GetCount(); i++)
			{
				m_Vertices[i].Normal = Math::Vector3::Zero;
			}

			for (u32 i = 0, j = 0; i < m_Indices.GetCount() / 3; i++, j += 3)
			{
				vertexPosition[0] = m_Vertices[m_Indices[j + 0]].Position;
				vertexPosition[1] = m_Vertices[m_Indices[j + 1]].Position;
				vertexPosition[2] = m_Vertices[m_Indices[j + 2]].Position;

				vertexPosition[0] -= vertexPosition[1];
				vertexPosition[1] -= vertexPosition[2];

				normal = Math::Vector3::Cross(vertexPosition[0], vertexPosition[1]);

				m_Vertices[m_Indices[j + 0]].Normal += normal;
				m_Vertices[m_Indices[j + 1]].Normal += normal;
				m_Vertices[m_Indices[j + 2]].Normal += normal;
			}

			for (u32 i = 0; i < m_Vertices.GetCount(); i++)
			{
				m_Vertices[i].Normal.Normalize();
			}
		}

		void SmoothNormals()
		{
			List<Math::Vector3> normals;
			normals.Reserve(m_Vertices.GetCount());

			for (size_t i = 0; i < m_Vertices.GetCount(); ++i)
				normals.Add(Math::Vector3());

			T *vertices = &m_Vertices[0];
			for (u32 i = 0; i < m_Vertices.GetCount() - 1; ++i, ++vertices)
			{
				T *vertices2 = vertices;
				for (u32 j = i; j < m_Vertices.GetCount(); ++j, ++vertices2)
				{
					if (vertices->Position == vertices2->Position)
					{
						normals[i] += vertices2->Normal;
						normals[j] += vertices->Normal;
					}
				}
			}

			vertices = &m_Vertices[0];
			for (u32 i = 0; i < m_Vertices.GetCount(); ++i, ++vertices)
			{
				vertices->Normal = normals[i].Normalized();
			}
		}

		void CalculateBoundingBox()
		{
			if (m_Vertices.GetCount() > 0)
			{
				T* vertices = &m_Vertices[0];

				Math::Vector3 minBB  = vertices->Position;
				Math::Vector3 maxBB = vertices->Position;

				for (u32 i = 0; i < m_Vertices.GetCount(); ++i, ++vertices)
				{
					Math::Vector3 position = vertices->Position;

					if (position.X < minBB.X)
					{
						minBB.X = position.X;
					}
					if (position.Y < minBB.Y)
					{
						minBB.Y = position.Y;
					}
					if (position.Z < minBB.Z)
					{
						minBB.Z = position.Z;
					}
					if (position.X > maxBB.X)
					{
						maxBB.X = position.X;
					}
					if (position.Y > maxBB.Y)
					{
						maxBB.Y = position.Y;
					}
					if (position.Z > maxBB.Z)
					{
						maxBB.Z = position.Z;
					}
				}

				m_BoundingBox = Math::BoundingBox::FromMinMax(minBB, maxBB);
			}
		}

		T& operator[] (u32 index)
		{
			return m_Vertices[index];
		}

		const T& operator[] (u32 index) const
		{
			return m_Vertices[index];
		}

		u32 GetVertexCount() const
		{
			return m_Vertices.GetCount();
		}

		u32 GetIndexCount() const
		{
			return m_Indices.GetCount();
		}

		Geometry* ToGeometry()
		{
			Geometry* result = new Geometry();

			CalculateBoundingBox();

			result->InitBuffers();

			result->SetBoundingBox(m_BoundingBox);

			result->GetVertexBuffer()->SetSize(m_Vertices.GetCount() * sizeof(T));
			result->GetVertexBuffer()->SetData(m_Vertices.GetDataPointer());
			result->GetVertexBuffer()->SetVertexFormat(T::GetVertexFormat());

			result->GetIndexBuffer()->SetSize(m_Indices.GetCount());
			result->GetIndexBuffer()->SetData(m_Indices.GetDataPointer());

			return result;
		}

	protected:
		List<T>   m_Vertices;
		List<u32> m_Indices;

		Math::BoundingBox m_BoundingBox;
};

} }
