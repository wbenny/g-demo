#include "Geometry.h"
#include "GeometryBuilder.h"
#include "VertexFormats/PositionNormalTexture.h"

namespace FW { namespace Render {

Geometry* Geometry::LoadFromFile(const String& fileName)
{
	GeometryBuilder<VertexFormats::PositionNormalTexture> gb;

	FILE *fr;
	fopen_s(&fr, fileName.CharArray(), "rb");

	if (!fr)
		return false;

	VertexFormats::PositionNormalTexture vertex[3];

	while (fread(&vertex, 3 * sizeof(VertexFormats::PositionNormalTexture), 1, fr) != 0)
	{
		gb.AddTriangle(vertex[0], vertex[1], vertex[2]);
	}

	fclose(fr);

	Geometry* result = gb.ToGeometry();
	result->UpdateBuffers();

	return result;
}

} }
