#pragma once
#include "../Types.h"
#include "../RefCounter.h"
#include "../GL/GL.h"

namespace FW { namespace Render {

enum BufferObjectTypes
{
	ArrayBuffer   = GL_ARRAY_BUFFER,
	ElementBuffer = GL_ELEMENT_ARRAY_BUFFER,
	UniformBuffer = GL_UNIFORM_BUFFER
};

class BufferObject : public RefCounter
{
	public:
		BufferObject(BufferObjectTypes type) :
			m_Type(static_cast<GLenum>(type)),
			m_BufferSize(0)
		{
			m_BufferHandle = GL::GenBuffer();
		}

		~BufferObject()
		{
			GL::DeleteBuffer(m_BufferHandle);
			GL::BindBuffer(m_Type, 0);
		}

		void Init(GLsizeiptr size, const GLvoid* data, GLenum usage = GL_STATIC_DRAW)
		{
			m_BufferSize = size;
			GL::BufferData(m_Type, size, data, usage);
		}

		void Update(const GLvoid* data, GLintptr offset, GLsizeiptr size)
		{
			GL::BufferSubData(m_Type, offset, size, data);
		}

		void Bind()
		{
			GL::BindBuffer(m_Type, m_BufferHandle);
		}

		void Unbind()
		{
			GL::BindBuffer(m_Type, 0);
		}

		GLsizeiptr GetSize() const
		{
			return m_BufferSize;
		}

		BufferObjectTypes GetType() const
		{
			return static_cast<BufferObjectTypes>(m_Type);
		}

	protected:
		GLenum m_Type;

		GLuint m_BufferHandle;

		GLsizeiptr m_BufferSize;
};

} }
