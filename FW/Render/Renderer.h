#pragma once
#include "../Types.h"
#include "../GL/GL.h"
#include "../List.h"
#include "../Singleton.h"
#include "../Math/Vector2.h"
#include "../Math/Vector3.h"
#include "../Math/Matrix4.h"
#include "../Scene/Node.h"
#include "RenderState.h"

FW_CLASS_FWD_DECL2(FW, Scene, RootNode);
FW_CLASS_FWD_DECL2(FW, Scene, LightNode);
FW_CLASS_FWD_DECL2(FW, Scene, MeshNode);

FW_CLASS_FWD_DECL3(FW, Render, Postprocess, VerticalBlur);
FW_CLASS_FWD_DECL3(FW, Render, Postprocess, HorizontalBlur);
FW_CLASS_FWD_DECL2(FW, Render, FullScreenQuad);

FW_TYPE_FWD_DECL2(FW, Scene, enum, CameraType);

namespace FW { namespace Render {

class Texture;

class Renderer : public Singleton<Renderer>
{
	friend class Singleton<Renderer>;

	public:
		void Init();

		Texture* Draw(Scene::RootNode* scene);

		const RenderState* GetRenderState() const { return m_RenderState; }
		RenderState* GetRenderState() { return m_RenderState; }
		void SetRenderState(RenderState* renderState) { m_RenderState = renderState; }

	private:
		const Math::Matrix4& GetProjectionMatrix() const { return m_ProjectionMatrix; }
		const Math::Matrix4& GetModelMatrix() const { return m_ModelMatrix; }
		const Math::Matrix4& GetViewMatrix() const { return m_ViewMatrix; }

		Math::Matrix4 GetModelViewMatrix() const { return m_ViewMatrix * m_ModelMatrix; }

		void DrawObject(Scene::MeshNode* mesh);
		void DrawObjectArray(List<Scene::Node*>& renderArray);

		Math::Matrix4	m_ProjectionMatrix,
						m_ModelMatrix,
						m_ViewMatrix;
						
		Math::Matrix3	m_NormalMatrix;

		Math::Vector3	m_EyeDirection,
						m_EyePosition;

		Math::Vector2   m_NearFar;

		Ptr<RenderState> m_RenderState;

		Scene::CameraType m_ActualCameraType;
		
		List<Scene::LightNode*>* m_Lights;

		Scene::CameraNode* m_ReflectionCamera;

		Math::Vector3*	m_AmbientLightColor;

// 		Postprocess::HorizontalBlur* m_HBlurPP;
// 		Postprocess::VerticalBlur* m_VBlurPP;
// 		FullScreenQuad* m_HBlur, *m_VBlur;

		Renderer() {}

		static int CALLBACK SortByDepth(const void* p1, const void* p2)
		{
			const Scene::Node*const* obj1 = reinterpret_cast<const Scene::Node*const*>(p1),
						     *const* obj2 = reinterpret_cast<const Scene::Node*const*>(p2);

			if ((*obj1)->GetMatrix().GetTranslation().Z > (*obj2)->GetMatrix().GetTranslation().Z)
				return -1;
			else if ((*obj1)->GetMatrix().GetTranslation().Z < (*obj2)->GetMatrix().GetTranslation().Z)
				return 1;
			else
				return 0;
		}
};

} }
