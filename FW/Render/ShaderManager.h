#pragma once
#include "../Utils/LoadFile.h"
#include "../Singleton.h"
#include "../String.h"
#include "../Map.h"
#include "../Ptr.h"
#include "Shader.h"

namespace FW { namespace Render {

class ShaderManager : public Singleton<ShaderManager>, public Map<String, Ptr<Shader> >
{
	friend class Singleton<ShaderManager>;

	public:
		void Init()
		{
			m_Chunks.Clear();
			LoadShaderChunks();

			// Materials
			ReloadShader("phong",				"vp_phong",		"fp_phong");
			ReloadShader("sky",					"vp_sky",		"fp_sky");
			ReloadShader("noshading",			"vp_noshading", "fp_noshading");

			// RenderModes
			ReloadShader("normaldepth",			"vp_normaldepth",	"fp_normaldepth");
			ReloadShader("position",			"vp_position",		"fp_position");
			ReloadShader("depth",				"vp_depth",			"fp_depth");	// > depthLinear
			ReloadShader("vsmbuffer",			"vp_vsm",			"fp_vsm");

			// Postprocess
			ReloadShader("lightscattering",		"vp_ls",		"fp_ls",		true);
			ReloadShader("quad",				"vp_quad",		"fp_quad",		true);
			ReloadShader("ssao",				"vp_ssao",		"fp_ssao",		true);
			ReloadShader("bloom",				"vp_bloom",		"fp_bloom",		true);
			ReloadShader("hblur",				"vp_blur",		"fp_hblur",		true);
			ReloadShader("vblur",				"vp_blur",		"fp_vblur",		true);
			ReloadShader("radialblur",			"vp_rblur",		"fp_rblur",		true);
			ReloadShader("dof",					"vp_dof",		"fp_dof",		true);
			ReloadShader("volumelight",			"vp_vl",		"fp_vl",		true);
			ReloadShader("tonemapping",			"vp_tm",		"fp_tm",		true);
			ReloadShader("fxaa",				"vp_fxaa",		"fp_fxaa",		true);
			ReloadShader("filmgrain",			"vp_fg",		"fp_fg",		true);
		}

		Shader* GetShaderByName(const String& name) { return GetMappedElement(name); }

	private:
		Map<String, String> m_Chunks;

		ShaderManager() {}

		void ReloadShaderChunk(const String& name, const String& fileName)
		{
			String p;
			if (name == "fp_hblur")
				p = "#define HORIZONTAL_BLUR_5\r\n";
			else if (name == "fp_vblur")
				p = "#define VERTICAL_BLUR_5\r\n";

			m_Chunks.Add(name, p + FW::Utils::LoadFile(fileName));
		}

		const String& GetShaderChunk(const String& chunk)
		{
			return m_Chunks[chunk];
		}

		void LoadShaderChunks()
		{
			// Version for all shaders.
			ReloadShaderChunk("version",		"shaders\\version.glsl");

			// Headers contains declarations of important variables.
			ReloadShaderChunk("vp_header",		"shaders\\headervertexshader.glsl");
			ReloadShaderChunk("fp_header",		"shaders\\headerfragmentshader.glsl");

			// Headers for post processes. 
			ReloadShaderChunk("vp_fsq_header",	"shaders\\headerfullscreenquadvertex.glsl");
			ReloadShaderChunk("fp_fsq_header",	"shaders\\headerfullscreenquadfragment.glsl");

			// Phong shading.
			ReloadShaderChunk("vp_phong",		"shaders\\phong\\phong.vp");
			ReloadShaderChunk("fp_phong",		"shaders\\phong\\phong.fp");

			// Skydome.
			ReloadShaderChunk("vp_sky",		"shaders\\sky\\sky.vp");
			ReloadShaderChunk("fp_sky",		"shaders\\sky\\sky.fp");

			// No shading.
			ReloadShaderChunk("vp_noshading",	"shaders\\noshading\\noshading.vp");
			ReloadShaderChunk("fp_noshading",	"shaders\\noshading\\noshading.fp");

			// Light scattering.
			ReloadShaderChunk("vp_ls",			"shaders\\lightscattering\\lightscattering.vp");
			ReloadShaderChunk("fp_ls",			"shaders\\lightscattering\\lightscattering.fp");

			// Rendering to full screen quad.
			ReloadShaderChunk("vp_quad",		"shaders\\quad\\quad.vp");
			ReloadShaderChunk("fp_quad",		"shaders\\quad\\quad.fp");

			// Screen space ambient occlusion shader.
			ReloadShaderChunk("vp_ssao",		"shaders\\ssao\\ssao.vp");
			ReloadShaderChunk("fp_ssao",		"shaders\\ssao\\ssao.fp");

			// Screen space ambient occlusion shader.
			ReloadShaderChunk("vp_bloom",		"shaders\\bloom\\bloom.vp");
			ReloadShaderChunk("fp_bloom",		"shaders\\bloom\\bloom.fp");

			// Gaussian blur.
			ReloadShaderChunk("vp_blur",		"shaders\\blur\\blur.vp");
			ReloadShaderChunk("fp_hblur",	"shaders\\blur\\blur.fp");
			ReloadShaderChunk("fp_vblur",	"shaders\\blur\\blur.fp");

			// Radial blur.
			ReloadShaderChunk("vp_rblur",		"shaders\\radialblur\\radialblur.vp");
			ReloadShaderChunk("fp_rblur",		"shaders\\radialblur\\radialblur.fp");

			// Screen space ambient occlusion shader.
			ReloadShaderChunk("vp_dof",		"shaders\\dof\\dof.vp");
			ReloadShaderChunk("fp_dof",		"shaders\\dof\\dof.fp");

			// Rendering normals + depth to texture.
			ReloadShaderChunk("vp_normaldepth", "shaders\\normaldepth\\normaldepth.vp");
			ReloadShaderChunk("fp_normaldepth", "shaders\\normaldepth\\normaldepth.fp");

			// Rendering depth to texture.
			ReloadShaderChunk("vp_position",		"shaders\\position\\position.vp");
			ReloadShaderChunk("fp_position",		"shaders\\position\\position.fp");

			// Rendering depth to texture.
			ReloadShaderChunk("vp_depth",		"shaders\\depth\\depth.vp");
			ReloadShaderChunk("fp_depth",		"shaders\\depth\\depth.fp");

			ReloadShaderChunk("vp_vsm",			"shaders\\vsm\\vsm.vp");
			ReloadShaderChunk("fp_vsm",			"shaders\\vsm\\vsm.fp");

			ReloadShaderChunk("vp_vl",			"shaders\\volumelight\\volumelight.vp");
			ReloadShaderChunk("fp_vl",			"shaders\\volumelight\\volumelight.fp");

			ReloadShaderChunk("vp_tm",			"shaders\\tonemapping\\tonemapping.vp");
			ReloadShaderChunk("fp_tm",			"shaders\\tonemapping\\tonemapping.fp");

 			ReloadShaderChunk("vp_fxaa",		"shaders\\fxaa\\fxaa.vp");
 			ReloadShaderChunk("fp_fxaa",		"shaders\\fxaa\\fxaa.fp");

			ReloadShaderChunk("vp_fg",			"shaders\\filmgrain\\filmgrain.vp");
			ReloadShaderChunk("fp_fg",			"shaders\\filmgrain\\filmgrain.fp");
		}

		void ReloadShader(const String& name, const String& vert, const String& frag, bool fullScreenQuadEffect = false)
		{
			String vs, fs;
			Shader* shader;

			if ((shader = GetMappedElement(name)) == NULL)
			{
				FW_DEBUG_PRINT("[-] Compiling shader '%s'... ", name.CharArray());

				shader = new Render::Shader();
			}
			else
			{
				FW_DEBUG_PRINT("[-] Recompiling shader '%s'... ", name.CharArray());

				shader->End();
			}

			// vertex shader
			vs.AddLine(m_Chunks["version"]);
			vs.AddLine(m_Chunks["vp_header"]);
			if (fullScreenQuadEffect)
				vs.AddLine(m_Chunks["vp_fsq_header"]);
			vs.AddLine(m_Chunks[vert]);

			// fragment shader
			fs.AddLine(m_Chunks["version"]);
			fs.AddLine(m_Chunks["fp_header"]);
			if (fullScreenQuadEffect)
				fs.AddLine(m_Chunks["fp_fsq_header"]);
			fs.AddLine(m_Chunks[frag]);

			shader->Init(vs.CharArray(), fs.CharArray());

			Add(name, shader);

			FW_DEBUG_PRINT("done!\r\n");
		}
};

} }
