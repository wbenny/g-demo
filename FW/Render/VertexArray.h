#pragma once
#include "../GL/GL.h"
#include "../RefCounter.h"

namespace FW { namespace Render {

class VertexArray : public RefCounter
{
	public:
		VertexArray() :
			m_VertexArrayHandle(-1)
		{
			m_VertexArrayHandle = GL::GenVertexArray();
		}

		~VertexArray()
		{
			GL::DeleteVertexArray(m_VertexArrayHandle);
		}

		void Bind()
		{
			GL::BindVertexArray(m_VertexArrayHandle);
		}

		void Unbind()
		{
			GL::BindVertexArray(0);
		}

	protected:
		GLuint m_VertexArrayHandle;
};

} }
