#pragma once
#include "../Memory.h"
#include "../GL/GL.h"
#include "VertexFormats/VertexFormat.h"
#include "BufferObject.h"
#include "IBufferArray.h"

namespace FW { namespace Render {

class VertexBuffer : public IBufferArray
{
	public:
		VertexBuffer() :
			IBufferArray(BufferObjectTypes::ArrayBuffer)
		{

		}

		void Bind()
		{
			m_BufferObject.Bind();
		}

		void Unbind()
		{
			m_BufferObject.Unbind();
		}

		void UpdateBuffer()
		{
			m_BufferObject.Bind();
			m_BufferObject.Init(m_DataSize, m_Data);
		}

		u32 GetSize() const
		{
			return m_DataSize;
		}

		void SetSize(u32 newSize)
		{
			m_Data = Memory::Realloc(m_Data, m_DataSize, newSize);
			m_DataSize = newSize;
		}

		const void* GetDataPointer() const
		{
			return m_Data;
		}

		void SetData(const void* dataPtr, u32 count = 0)
		{
			if (!count)
				count = m_DataSize;

			Memory::Copy(m_Data, dataPtr, count);
		}

		VertexFormats::VertexFormat GetVertexFormat() const
		{
			return m_VertexFormat;
		}

		void SetVertexFormat(const VertexFormats::VertexFormat& vertexFormat)
		{
			m_VertexFormat = vertexFormat;
		}

	protected:
		VertexFormats::VertexFormat	m_VertexFormat;
};

} }
