#pragma once
#include "../Ptr.h"
#include "../Math/BoundingBox.h"
#include "../Content/Resource.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"

namespace FW { namespace Render {

class Geometry : public Content::Resource
{
	public:
		Geometry() : Content::Resource(Content::ResourceType::Geometry) {}
		~Geometry() {}

		void InitBuffers()
		{
			m_VertexBuffer = new VertexBuffer();
			m_IndexBuffer  = new IndexBuffer();

			m_VertexArray  = new VertexArray();
		}

		const VertexBuffer*	GetVertexBuffer() const { return m_VertexBuffer; }
		VertexBuffer*		GetVertexBuffer()		{ return m_VertexBuffer; }
		
		const IndexBuffer*	GetIndexBuffer() const	{ return m_IndexBuffer; }
		IndexBuffer*		GetIndexBuffer()		{ return m_IndexBuffer; }

		const VertexArray*  GetVertexArray() const  { return m_VertexArray; }
		VertexArray*		GetVertexArray()		{ return m_VertexArray; }

		void UpdateBuffers()
		{
			m_VertexBuffer->UpdateBuffer();
			m_IndexBuffer->UpdateBuffer();

			// Build vertex array.
			m_VertexArray->Bind();
			m_VertexBuffer->Bind();
			m_VertexBuffer->GetVertexFormat().Apply();
			m_IndexBuffer->Bind();
			m_VertexArray->Unbind();
		}

		const Math::BoundingBox& GetBoundingBox() const { return m_BoundingBox; }
		void SetBoundingBox(const Math::BoundingBox& boundingBox) { m_BoundingBox = boundingBox; }

		Geometry* Geometry::LoadFromFile(const String& fileName);

	protected:
		Ptr<VertexBuffer> m_VertexBuffer;
		Ptr<IndexBuffer>  m_IndexBuffer;

		Ptr<VertexArray>  m_VertexArray;

		Math::BoundingBox m_BoundingBox;
};

} }
