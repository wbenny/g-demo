#pragma once

namespace FW { namespace Utils {

template <class T>
__inline void Swap(T& v1, T& v2)
{
	T t = v1;
	v1 = v2;
	v2 = t;
}

} }
