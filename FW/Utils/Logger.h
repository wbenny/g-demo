#pragma once
#include "../Singleton.h"

#ifdef _DEBUG
#	define FW_LOG(f, ...)	FW::Utils::Logger::GetInstance().Log(f, __VA_ARGS__)
#else
#	define FW_LOG(f, ...)
#endif

namespace FW { namespace Utils {

class Logger : public Singleton<Logger>
{
	public:
		void Log(const char* f, ...)
		{
			va_list argList;
			va_start(argList, f);

			vprintf_s(format, argList);

			va_end(argList);
		}
};

} }
