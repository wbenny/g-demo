#pragma once
#include "../Types.h"
#include "../String.h"

namespace FW { namespace Utils {

static String LoadFile(const String& fileName)
{
	HANDLE hFile = CreateFile(
		fileName.CharArray(),
		GENERIC_READ,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL
	);

	DWORD fileSize = GetFileSize(hFile, NULL) + 1;

	char* buffer = new char[fileSize];
	
	DWORD dwBytesRead;
	ReadFile(hFile, static_cast<LPVOID>(buffer), fileSize, &dwBytesRead, NULL);
	CloseHandle(hFile);

	buffer[fileSize-1] = '\0';
	
	String result = buffer;
	
	// FIXME: Y U NO WORKIN PROPERLY?!?!?
// 	delete[] buffer;

	return result;
}

} } // namespace
