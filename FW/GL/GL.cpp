#include "GL.h"

namespace FW {

GLState								GL::m_GLState					= { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
GLState								GL::m_GLStateStack[16];
u8									GL::m_GLStateStackPointer       = 0;

// GL Arrays
PFNGLGENBUFFERSPROC					GL::m_GenBuffers				= NULL;
PFNGLBINDBUFFERPROC					GL::m_BindBuffer				= NULL;
PFNGLBUFFERDATAPROC					GL::m_BufferData				= NULL;
PFNGLBUFFERSUBDATAPROC				GL::m_BufferSubData				= NULL;
PFNGLDELETEBUFFERSPROC				GL::m_DeleteBuffers				= NULL;

// GL Shaders
PFNGLCREATESHADERPROC				GL::m_CreateShader				= NULL;
PFNGLSHADERSOURCEPROC				GL::m_ShaderSource				= NULL;
PFNGLCOMPILESHADERPROC				GL::m_CompileShader				= NULL;
PFNGLISSHADERPROC					GL::m_IsShader					= NULL;
PFNGLGETSHADERINFOLOGPROC			GL::m_GetShaderInfoLog			= NULL;
PFNGLGETPROGRAMINFOLOGPROC			GL::m_GetProgramInfoLog			= NULL;
PFNGLCREATEPROGRAMPROC				GL::m_CreateProgram				= NULL;
PFNGLATTACHSHADERPROC				GL::m_AttachShader				= NULL;
PFNGLLINKPROGRAMPROC				GL::m_LinkProgram				= NULL;
PFNGLUSEPROGRAMPROC					GL::m_UseProgram				= NULL;
PFNGLGETATTRIBLOCATIONPROC			GL::m_GetAttribLocation			= NULL;
PFNGLGETUNIFORMLOCATIONPROC			GL::m_GetUniformLocation		= NULL;
PFNGLDELETESHADERPROC				GL::m_DeleteShader				= NULL;
PFNGLDELETEPROGRAMPROC				GL::m_DeleteProgram				= NULL;
PFNGLUNIFORM1FPROC					GL::m_Uniform1f					= NULL;
PFNGLUNIFORM1IPROC					GL::m_Uniform1i					= NULL;
PFNGLUNIFORM2FVPROC					GL::m_Uniform2fv				= NULL;
PFNGLUNIFORM2IVPROC					GL::m_Uniform2iv				= NULL;
PFNGLUNIFORM3FVPROC					GL::m_Uniform3fv				= NULL;
PFNGLUNIFORM3IVPROC					GL::m_Uniform3iv				= NULL;
PFNGLUNIFORM4FVPROC					GL::m_Uniform4fv				= NULL;
PFNGLUNIFORM4IVPROC					GL::m_Uniform4iv				= NULL;
PFNGLUNIFORMMATRIX3FVPROC			GL::m_UniformMatrix3f			= NULL;
PFNGLUNIFORMMATRIX4FVPROC			GL::m_UniformMatrix4f			= NULL;

// VBO
PFNGLGENVERTEXARRAYSPROC			GL::m_GenVertexArrays			= NULL;
PFNGLBINDVERTEXARRAYPROC			GL::m_BindVertexArray			= NULL;
PFNGLDELETEVERTEXARRAYSPROC			GL::m_DeleteVertexArrays		= NULL;
PFNGLVERTEXATTRIBPOINTERPROC		GL::m_VertexAttribPointer		= NULL;
PFNGLENABLEVERTEXATTRIBARRAYPROC	GL::m_EnableVertexAttribArray	= NULL;
PFNGLDRAWARRAYSPROC					GL::m_DrawArrays				= NULL;
PFNGLDRAWELEMENTSPROC				GL::m_DrawElements				= NULL;

// FBO
PFNGLGENFRAMEBUFFERSPROC			GL::m_GenFramebuffers			= NULL;
PFNGLBINDFRAMEBUFFERPROC			GL::m_BindFramebuffer			= NULL;
PFNGLDELETEFRAMEBUFFERSPROC			GL::m_DeleteFramebuffers		= NULL;
PFNGLCHECKFRAMEBUFFERSTATUSPROC		GL::m_CheckFramebufferStatus	= NULL;
PFNGLFRAMEBUFFERTEXTUREPROC			GL::m_FramebufferTexture		= NULL;
PFNGLFRAMEBUFFERTEXTURE1DPROC		GL::m_FramebufferTexture1D		= NULL;
PFNGLFRAMEBUFFERTEXTURE2DPROC		GL::m_FramebufferTexture2D		= NULL;
PFNGLFRAMEBUFFERTEXTURE3DPROC		GL::m_FramebufferTexture3D		= NULL;
PFNGLFRAMEBUFFERRENDERBUFFERPROC	GL::m_FramebufferRenderbuffer	= NULL;

// Renderbuffer
PFNGLGENRENDERBUFFERSPROC			GL::m_GenRenderbuffers			= NULL;
PFNGLDELETERENDERBUFFERSPROC		GL::m_DeleteRenderbuffers		= NULL;
PFNGLBINDRENDERBUFFERPROC			GL::m_BindRenderbuffer			= NULL;
PFNGLRENDERBUFFERSTORAGEPROC		GL::m_RenderbufferStorage		= NULL;

// Textures
PFNGLACTIVETEXTUREPROC				GL::m_ActiveTexture				= NULL;
PFNGLTEXIMAGE3DPROC					GL::m_TexImage3D				= NULL;
PFNGLGENERATEMIPMAPPROC				GL::m_GenerateMipmap			= NULL;

// VSync
PFNWGLSWAPINTERVALEXTPROC			GL::m_SwapInterval				= NULL;
PFNWGLGETSWAPINTERVALEXTPROC		GL::m_GetSwapInterval			= NULL;

// GL Main & Deprecated
PFNGLBLENDEQUATIONPROC				GL::m_BlendEquation				= NULL;


bool GL::Init()
{
	// GL Arrays
	m_GenBuffers				= (PFNGLGENBUFFERSPROC)					wglGetProcAddress("glGenBuffers");
	m_BindBuffer				= (PFNGLBINDBUFFERPROC)					wglGetProcAddress("glBindBuffer");
	m_BufferData				= (PFNGLBUFFERDATAPROC)					wglGetProcAddress("glBufferData");
	m_BufferSubData				= (PFNGLBUFFERSUBDATAPROC)				wglGetProcAddress("glBufferSubData");
	m_DeleteBuffers				= (PFNGLDELETEBUFFERSPROC)				wglGetProcAddress("glDeleteBuffers");

	// GL Shaders
	m_CreateShader				= (PFNGLCREATESHADERPROC)				wglGetProcAddress("glCreateShader");
	m_ShaderSource				= (PFNGLSHADERSOURCEPROC)				wglGetProcAddress("glShaderSource");
	m_CompileShader				= (PFNGLCOMPILESHADERPROC)				wglGetProcAddress("glCompileShader");
	m_IsShader					= (PFNGLISSHADERPROC)					wglGetProcAddress("glIsShader");

	m_GetShaderInfoLog			= (PFNGLGETSHADERINFOLOGPROC)			wglGetProcAddress("glGetShaderInfoLog");
	m_GetProgramInfoLog			= (PFNGLGETPROGRAMINFOLOGPROC)			wglGetProcAddress("glGetProgramInfoLog");
	m_CreateProgram				= (PFNGLCREATEPROGRAMPROC)				wglGetProcAddress("glCreateProgram");
	m_AttachShader				= (PFNGLATTACHSHADERPROC)				wglGetProcAddress("glAttachShader");
	m_LinkProgram				= (PFNGLLINKPROGRAMPROC)				wglGetProcAddress("glLinkProgram");
	m_UseProgram				= (PFNGLUSEPROGRAMPROC)					wglGetProcAddress("glUseProgram");
	m_GetAttribLocation			= (PFNGLGETATTRIBLOCATIONPROC)			wglGetProcAddress("glGetAttribLocation");
	m_GetUniformLocation		= (PFNGLGETUNIFORMLOCATIONPROC)			wglGetProcAddress("glGetUniformLocation");
	m_DeleteShader				= (PFNGLDELETESHADERPROC)				wglGetProcAddress("glDeleteShader");
	m_DeleteProgram				= (PFNGLDELETEPROGRAMPROC)				wglGetProcAddress("glDeleteProgram");
	m_Uniform1f					= (PFNGLUNIFORM1FPROC)					wglGetProcAddress("glUniform1f");
	m_Uniform1i					= (PFNGLUNIFORM1IPROC)					wglGetProcAddress("glUniform1i");
	m_Uniform2fv				= (PFNGLUNIFORM2FVPROC)					wglGetProcAddress("glUniform2fv");
	m_Uniform2iv				= (PFNGLUNIFORM2IVPROC)					wglGetProcAddress("glUniform2iv");
	m_Uniform3fv				= (PFNGLUNIFORM3FVPROC)					wglGetProcAddress("glUniform3fv");
	m_Uniform3iv				= (PFNGLUNIFORM3IVPROC)					wglGetProcAddress("glUniform3iv");
	m_Uniform4fv				= (PFNGLUNIFORM4FVPROC)					wglGetProcAddress("glUniform4fv");
	m_Uniform4iv				= (PFNGLUNIFORM4IVPROC)					wglGetProcAddress("glUniform4iv");
	m_UniformMatrix3f			= (PFNGLUNIFORMMATRIX3FVPROC)			wglGetProcAddress("glUniformMatrix3fv");
	m_UniformMatrix4f			= (PFNGLUNIFORMMATRIX4FVPROC)			wglGetProcAddress("glUniformMatrix4fv");

	// VBO
	m_GenVertexArrays			= (PFNGLGENVERTEXARRAYSPROC)			wglGetProcAddress("glGenVertexArrays");
	m_BindVertexArray			= (PFNGLBINDVERTEXARRAYPROC)			wglGetProcAddress("glBindVertexArray");
	m_DeleteVertexArrays		= (PFNGLDELETEVERTEXARRAYSPROC)			wglGetProcAddress("glDeleteVertexArrays");
	m_VertexAttribPointer		= (PFNGLVERTEXATTRIBPOINTERPROC)		wglGetProcAddress("glVertexAttribPointer");
	m_EnableVertexAttribArray	= (PFNGLENABLEVERTEXATTRIBARRAYPROC)	wglGetProcAddress("glEnableVertexAttribArray");
	m_DrawArrays				= (PFNGLDRAWARRAYSPROC)					wglGetProcAddress("glDrawArrays");
	m_DrawElements				= glDrawElements; //(PFNGLDRAWELEMENTSPROC)				wglGetProcAddress("glDrawElements");

	// FBO
	m_GenFramebuffers			= (PFNGLGENFRAMEBUFFERSPROC)			wglGetProcAddress("glGenFramebuffers");
	m_BindFramebuffer			= (PFNGLBINDFRAMEBUFFERPROC)			wglGetProcAddress("glBindFramebuffer");
	m_DeleteFramebuffers		= (PFNGLDELETEFRAMEBUFFERSPROC)			wglGetProcAddress("glDeleteFramebuffers");
	m_CheckFramebufferStatus	= (PFNGLCHECKFRAMEBUFFERSTATUSPROC)		wglGetProcAddress("glCheckFramebufferStatus");
	m_FramebufferTexture		= (PFNGLFRAMEBUFFERTEXTUREPROC)			wglGetProcAddress("glFramebufferTexture");
	m_FramebufferTexture1D		= (PFNGLFRAMEBUFFERTEXTURE1DPROC)		wglGetProcAddress("glFramebufferTexture1D");
	m_FramebufferTexture2D		= (PFNGLFRAMEBUFFERTEXTURE2DPROC)		wglGetProcAddress("glFramebufferTexture2D");
	m_FramebufferTexture3D		= (PFNGLFRAMEBUFFERTEXTURE3DPROC)		wglGetProcAddress("glFramebufferTexture3D");
	m_FramebufferRenderbuffer	= (PFNGLFRAMEBUFFERRENDERBUFFERPROC)	wglGetProcAddress("glFramebufferRenderbuffer");

	// Renderbuffer
	m_GenRenderbuffers			= (PFNGLGENRENDERBUFFERSPROC)			wglGetProcAddress("glGenRenderbuffers");
	m_DeleteRenderbuffers		= (PFNGLDELETERENDERBUFFERSPROC)		wglGetProcAddress("glDeleteRenderbuffers");
	m_BindRenderbuffer			= (PFNGLBINDRENDERBUFFERPROC)			wglGetProcAddress("glBindRenderbuffer");
	m_RenderbufferStorage		= (PFNGLRENDERBUFFERSTORAGEPROC)		wglGetProcAddress("glRenderbufferStorage");

	// Textures
	m_ActiveTexture				= (PFNGLACTIVETEXTUREPROC)				wglGetProcAddress("glActiveTexture");
	m_TexImage3D				= (PFNGLTEXIMAGE3DPROC)					wglGetProcAddress("glTexImage3D");
	m_GenerateMipmap			= (PFNGLGENERATEMIPMAPPROC)				wglGetProcAddress("glGenerateMipmap");

	// VSync
	m_SwapInterval				= (PFNWGLSWAPINTERVALEXTPROC)			wglGetProcAddress("wglSwapIntervalEXT");
	m_GetSwapInterval			= (PFNWGLGETSWAPINTERVALEXTPROC)		wglGetProcAddress("wglGetSwapIntervalEXT");

	// GL Main & Deprecated
	m_BlendEquation				= (PFNGLBLENDEQUATIONPROC)				wglGetProcAddress("glBlendEquation");

	return true;
}

// GL Arrays
GLuint GL::GenBuffer()
{
	GLuint result;
	m_GenBuffers(1, &result);
	return result;
}

void GL::BindBuffer(GLenum target, GLuint buffer)
{
	if (buffer == m_GLState.LastBuffer)
		return;

	m_GLState.LastBuffer = buffer;
	m_BindBuffer(target, buffer);
}

void GL::BufferData(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage)
{
	m_BufferData(target, size, data, usage);
}


void GL::BufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid* data)
{
	m_BufferSubData(target, offset, size, data);
}

void GL::DeleteBuffer(GLuint buffer)
{
	if (m_GLState.LastBuffer == buffer)
		m_GLState.LastBuffer = 0;

	m_DeleteBuffers(1, &buffer);
}

// GL Shaders
GLuint GL::CreateShader(GLenum type)
{
	return m_CreateShader(type);
}

void GL::ShaderSource(GLuint shader, GLsizei count, const GLchar** string, const GLint* length)
{
	m_ShaderSource(shader, count, string, length);
}

void GL::CompileShader(GLuint shader)
{
	m_CompileShader(shader);
}

GLboolean GL::IsShader(GLuint shader)
{
	return m_IsShader(shader);
}

void GL::GetShaderInfoLog(GLuint shader, GLsizei buffSize, GLsizei* length, GLchar* infoLog)
{
	m_GetShaderInfoLog(shader, buffSize, length, infoLog);
}

GLuint GL::CreateProgram()
{
	return m_CreateProgram();
}

void GL::AttachShader(GLuint program, GLuint shader)
{
	m_AttachShader(program, shader);
}

void GL::LinkProgram(GLuint program)
{
	m_LinkProgram(program);
}

void GL::UseProgram(GLuint program)
{
	if (program == m_GLState.LastProgramUsed)
		return;

	m_GLState.LastProgramUsed = program;
	m_UseProgram(program);
}

void GL::GetProgramInfoLog(GLuint program, GLsizei buffSize, GLsizei* length, GLchar* infoLog)
{
	m_GetProgramInfoLog(program, buffSize, length, infoLog);
}

GLint GL::GetAttribLocation(GLuint program, const GLchar* name)
{
	return m_GetAttribLocation(program, name);
}

GLint GL::GetUniformLocation(GLuint program, const GLchar* name)
{
	return m_GetUniformLocation(program, name);
}

void GL::DeleteShader(GLuint shader)
{
	m_DeleteShader(shader);
}

void GL::DeleteProgram(GLuint program)
{
	m_DeleteShader(program);
}

void GL::Uniform1f(GLint location, GLfloat value)
{
	m_Uniform1f(location, value);
}

void GL::Uniform1i(GLint location, GLint value)
{
	m_Uniform1i(location, value);
}

void GL::Uniform2fv(GLint location, const GLfloat* value)
{
	m_Uniform2fv(location, 1, value);
}

void GL::Uniform2iv(GLint location, const GLint* value)
{
	m_Uniform2iv(location, 1, value);
}

void GL::Uniform3fv(GLint location, const GLfloat* value)
{
	m_Uniform3fv(location, 1, value);
}

void GL::Uniform3iv(GLint location, const GLint* value)
{
	m_Uniform3iv(location, 1, value);
}

void GL::Uniform4fv(GLint location, const GLfloat* value)
{
	m_Uniform4fv(location, 1, value);
}

void GL::Uniform4iv(GLint location, const GLint* value)
{
	m_Uniform4iv(location, 1, value);
}

void GL::UniformMatrix3f(GLint location, const GLfloat* value)
{
	m_UniformMatrix3f(location, 1, GL_FALSE, value);
}

void GL::UniformMatrix4f(GLint location, const GLfloat* value)
{
	m_UniformMatrix4f(location, 1, GL_FALSE, value);
}

// VBO
GLuint GL::GenVertexArray()
{
	GLuint result;
	m_GenVertexArrays(1, &result);
	return result;
}

void GL::BindVertexArray(GLuint array)
{
	if (array == m_GLState.LastVertexArray)
		return;

	m_GLState.LastVertexArray = array;
	m_BindVertexArray(array);
}

void GL::DeleteVertexArray(GLuint array)
{
	m_DeleteVertexArrays(1, &array);
}

void GL::VertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer)
{
	m_VertexAttribPointer(index, size, type, normalized, stride, pointer);
}

void GL::EnableVertexAttribArray(GLuint index)
{
	m_EnableVertexAttribArray(index);
}

void GL::DrawArrays(GLenum mode, GLint first, GLsizei count)
{
	m_DrawArrays(mode, first, count);
}

void GL::DrawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid* indices)
{
	m_DrawElements(mode, count, type, indices);
}

// FBO
GLuint GL::GenFramebuffer()
{
	GLuint result;
	m_GenFramebuffers(1, &result);
	return result;
}

void GL::BindFramebuffer(GLuint framebuffer)
{
	m_BindFramebuffer(GL_FRAMEBUFFER, framebuffer);
}

void GL::DeleteFramebuffer(GLuint framebuffer)
{
	m_DeleteFramebuffers(1, &framebuffer);
}

GLenum GL::CheckFramebufferStatus(GLenum target)
{
	return m_CheckFramebufferStatus(target);
}

void GL::FramebufferTexture(GLenum attachment, GLuint texture, GLint level)
{
	m_FramebufferTexture(GL_FRAMEBUFFER, attachment, texture, level);
}

void GL::FramebufferTexture1D(GLenum attachment, GLenum textarget, GLuint texture, GLint level)
{
	m_FramebufferTexture1D(GL_FRAMEBUFFER, attachment, textarget, texture, level);
}

void GL::FramebufferTexture2D(GLenum attachment, GLenum textarget, GLuint texture, GLint level)
{
	m_FramebufferTexture2D(GL_FRAMEBUFFER, attachment, textarget, texture, level);
}

void GL::FramebufferTexture3D(GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint zoffset)
{
	m_FramebufferTexture3D(GL_FRAMEBUFFER, attachment, textarget, texture, level, zoffset);
}

void GL::FramebufferRenderbuffer(GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer)
{
	m_FramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, renderbuffertarget, renderbuffer);
}

// Renderbuffer
GLuint GL::GenRenderbuffer()
{
	GLuint result;
	m_GenRenderbuffers(1, &result);
	return result;
}

void GL::BindRenderbuffer(GLuint renderbuffer)
{
	m_BindRenderbuffer(GL_RENDERBUFFER, renderbuffer);
}

void GL::DeleteRenderbuffer(GLuint renderbuffer)
{
	m_DeleteRenderbuffers(1, &renderbuffer);
}

void GL::RenderbufferStorage(GLenum internalformat, GLsizei width, GLsizei height)
{
	m_RenderbufferStorage(GL_RENDERBUFFER, internalformat, width, height);
}

// Textures
GLuint GL::GenTexture()
{
	GLuint result;
	glGenTextures(1, &result);
	return result;
}

void GL::BindTexture(GLenum target, GLuint texture)
{
// 	if (texture == m_GLState.LastTexture)
// 		return;
// 
// 	m_GLState.LastTexture = texture;
	glBindTexture(target, texture);
}

void GL::DeleteTexture(GLuint texture)
{
// 	if (texture == m_GLState.LastTexture)
// 		m_GLState.LastTexture = 0;

	glDeleteTextures(1, &texture);
}

void GL::ActiveTexture(GLenum texture)
{
	if (texture == m_GLState.ActiveTexture)
		return;

	m_GLState.ActiveTexture = texture;
	m_ActiveTexture(texture);
}

void GL::TexImage1D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLenum format, GLenum type, const GLvoid* pixels)
{
	glTexImage1D(target, level, internalformat, width, 0, format, type, pixels);
}

void GL::TexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid* pixels)
{
	glTexImage2D(target, level, internalformat, width, height, 0, format, type, pixels);
}

void GL::TexImage3D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const GLvoid* pixels)
{
	m_TexImage3D(target, level, internalformat, width, width, depth, 0, format, type, pixels);
}

void GL::TexParameterf(GLenum target, GLenum pname, GLfloat param)
{
	glTexParameterf(target, pname, param);
}

void GL::TexParameteri(GLenum target, GLenum pname, GLint param)
{
	glTexParameteri(target, pname, param);
}

void GL::GenerateMipmap(GLenum target)
{
	m_GenerateMipmap(target);
}

// VSync
BOOL GL::SwapInterval(int interval)
{
	return m_SwapInterval(interval);
}

int GL::GetSwapInterval()
{
	return m_GetSwapInterval();
}

// Main
void GL::Enable(GLenum cap)
{
	glEnable(cap);
}

void GL::Disable(GLenum cap)
{
	glDisable(cap);
}

void GL::CullFace(GLenum mode)
{
	if (mode == m_GLState.CullFace || mode == 0)
		return;

	m_GLState.CullFace = mode;
	glCullFace(mode);
}

void GL::FrontFace(GLenum mode)
{
	glFrontFace(mode);
}

void GL::Clear(GLbitfield mask)
{
	glClear(mask);
}

void GL::ClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha)
{
	glClearColor(red, green, blue, alpha);
}

void GL::ClearDepth(GLclampf depth)
{
	glClearDepth(depth);
}

void GL::DepthMask(GLboolean flag)
{
	glDepthMask(flag);
}

void GL::DepthFunc(GLenum func)
{
	if (func == m_GLState.DepthFunc || func == 0)
		return;

	m_GLState.DepthFunc = func;
	glDepthFunc(func);
}

void GL::BlendFunc(GLenum sfactor, GLenum dfactor)
{
	if ((m_GLState.BlendFuncSrcFactor == sfactor && m_GLState.BlendFuncDestFactor == dfactor) ||
		sfactor == 0 || dfactor == 0)
	{
		return;
	}

	m_GLState.BlendFuncSrcFactor = sfactor;
	m_GLState.BlendFuncDestFactor = dfactor;
	glBlendFunc(sfactor, dfactor);
}

void GL::BlendEquation(GLenum mode)
{
	if (mode == m_GLState.BlendEquation || mode == 0)
		return;

	m_GLState.BlendEquation = mode;
	m_BlendEquation(mode);
}

void GL::PointSize(GLfloat size)
{
	glPointSize(size);
}

void GL::PolygonMode(GLenum face, GLenum mode)
{
	if ((face == m_GLState.PolygonFace && mode == m_GLState.PolygonMode) ||
		face == 0 || mode == 0)
	{
		return;
	}

	m_GLState.PolygonFace = face;
	m_GLState.PolygonMode = mode;

	glPolygonMode(face, mode);
}

void GL::Viewport(GLint x, GLint y, GLsizei width, GLsizei height)
{
	if (m_GLState.ViewPortX == x &&
		m_GLState.ViewPortY == y &&
		m_GLState.ViewPortWidth == width &&
		m_GLState.ViewPortHeight == height)
	{
		return;
	}

	m_GLState.ViewPortX = x;
	m_GLState.ViewPortY = y;
	m_GLState.ViewPortWidth = width;
	m_GLState.ViewPortHeight = height;

	glViewport(x, y, width, height);
}

bool GL::PushState()
{
// 	if (m_GLStateStackPointer != 16)
// 	{
// 		m_GLStateStack[m_GLStateStackPointer] = m_GLState;
// 
// 		m_GLStateStackPointer++;
// 		return true;
// 	}
// 	else
// 	{
// 		return false;
// 	}
	return true;
}

bool GL::PopState()
{
// 	if (m_GLStateStackPointer != 0)
// 	{
// 		m_GLStateStackPointer--;
// 
// 		BlendEquation(m_GLStateStack[m_GLStateStackPointer].BlendEquation);
// 		BlendFunc(m_GLStateStack[m_GLStateStackPointer].BlendFuncSrcFactor, m_GLStateStack[m_GLStateStackPointer].BlendFuncDestFactor);
// 		DepthFunc(m_GLStateStack[m_GLStateStackPointer].DepthFunc);
// 		CullFace(m_GLStateStack[m_GLStateStackPointer].CullFace);
// 		Viewport(m_GLStateStack[m_GLStateStackPointer].ViewPortX,
// 			m_GLStateStack[m_GLStateStackPointer].ViewPortY,
// 			m_GLStateStack[m_GLStateStackPointer].ViewPortWidth,
// 			m_GLStateStack[m_GLStateStackPointer].ViewPortHeight);
// 
// 		return true;
// 	}
// 	else
// 	{
// 		return false;
// 	}
	return true;
}

}
