#pragma once
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "GLExt.h"
#include "GLWinExt.h"
#include "GLDefinitions.h"
#include "../Types.h"

namespace FW {

struct GLState
{
	GLenum	ActiveTexture;
	GLenum  CullFace;
	GLenum	DepthFunc;
	GLenum	BlendFuncSrcFactor;
	GLenum	BlendFuncDestFactor;
	GLenum	BlendEquation;
	GLint   ViewPortX;
	GLint   ViewPortY;
	GLsizei ViewPortWidth;
	GLsizei ViewPortHeight;
	GLenum  PolygonFace;
	GLenum	PolygonMode;

	GLuint  LastBuffer;
	GLuint  LastVertexArray;
	GLuint  LastTexture;
	GLuint  LastProgramUsed;
};

class GL
{
	public:
		static bool Init();

		// GL Arrays
		static GLuint GenBuffer();
		static void BindBuffer(GLenum target, GLuint buffer);
		static void BufferData(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage);
		static void BufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid* data);
		static void DeleteBuffer(GLuint buffer);

		// GL Shaders
		static GLuint CreateShader(GLenum type);
		static void ShaderSource(GLuint shader, GLsizei count, const GLchar** string, const GLint* length);
		static void CompileShader(GLuint shader);
		static GLboolean IsShader(GLuint shader);
		static void GetShaderInfoLog(GLuint shader, GLsizei buffSize, GLsizei* length, GLchar* infoLog);
		static GLuint CreateProgram();
		static void AttachShader(GLuint program, GLuint shader);
		static void LinkProgram(GLuint program);
		static void UseProgram(GLuint program);
		static void GetProgramInfoLog(GLuint program, GLsizei buffSize, GLsizei* length, GLchar* infoLog);
		static GLint GetAttribLocation(GLuint program, const GLchar* name);
		static GLint GetUniformLocation(GLuint program, const GLchar* name);
		static void DeleteShader(GLuint shader);
		static void DeleteProgram(GLuint program);
		static void Uniform1f(GLint location, GLfloat value);
		static void Uniform1i(GLint location, GLint value);
		static void Uniform2fv(GLint location, const GLfloat* value);
		static void Uniform2iv(GLint location, const GLint* value);
		static void Uniform3fv(GLint location, const GLfloat* value);
		static void Uniform3iv(GLint location, const GLint* value);
		static void Uniform4fv(GLint location, const GLfloat* value);
		static void Uniform4iv(GLint location, const GLint* value);
		static void UniformMatrix3f(GLint location, const GLfloat* value);
		static void UniformMatrix4f(GLint location, const GLfloat* value);

		// VBO
		static GLuint GenVertexArray();
		static void BindVertexArray(GLuint array);
		static void DeleteVertexArray(GLuint array);
		static void VertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid* pointer);
		static void EnableVertexAttribArray(GLuint index);
		static void DrawArrays(GLenum mode, GLint first, GLsizei count);
		static void DrawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid* indices);

		// FBO
		static GLuint GenFramebuffer();
		static void BindFramebuffer(GLuint framebuffer);
		static void DeleteFramebuffer(GLuint framebuffer);
		static GLenum CheckFramebufferStatus(GLenum target);
		static void FramebufferTexture(GLenum attachment, GLuint texture, GLint level);
		static void FramebufferTexture1D(GLenum attachment, GLenum textarget, GLuint texture, GLint level);
		static void FramebufferTexture2D(GLenum attachment, GLenum textarget, GLuint texture, GLint level);
		static void FramebufferTexture3D(GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint zoffset);
		static void FramebufferRenderbuffer(GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer);
	
		// Renderbuffer
		static GLuint GenRenderbuffer();
		static void BindRenderbuffer(GLuint renderbuffer);
		static void DeleteRenderbuffer(GLuint renderbuffer);
		static void RenderbufferStorage(GLenum internalformat, GLsizei width, GLsizei height);

		// Textures
		static GLuint GenTexture();
		static void BindTexture(GLenum target, GLuint texture);
		static void DeleteTexture(GLuint texture);
		static void ActiveTexture(GLenum texture);
		static void TexImage1D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLenum format, GLenum type, const GLvoid* pixels);
		static void TexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height,GLenum format, GLenum type, const GLvoid* pixels);
		static void TexImage3D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const GLvoid* pixels);
		static void TexParameterf(GLenum target, GLenum pname, GLfloat param);
		static void TexParameteri(GLenum target, GLenum pname, GLint param);
		static void GenerateMipmap(GLenum target);

		// VSync
		static BOOL SwapInterval(int interval);
		static int  GetSwapInterval();

		// Main
		static void Enable(GLenum cap);
		static void Disable(GLenum cap);
		static void CullFace(GLenum mode);
		static void FrontFace(GLenum mode);
		static void Clear(GLbitfield mask);
		static void ClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha);
		static void ClearDepth(GLclampf depth);
		static void DepthMask(GLboolean flag);
		static void DepthFunc(GLenum func);
		static void BlendFunc(GLenum sfactor, GLenum dfactor);
		static void BlendEquation(GLenum mode);
		static void PointSize(GLfloat size);
		static void PolygonMode(GLenum face, GLenum mode);
		static void Viewport(GLint x, GLint y, GLsizei width, GLsizei height);

		// Own
		static bool PushState();
		static bool PopState();

	private:
		static GLState m_GLState;
		static GLState m_GLStateStack[16];
		static u8	   m_GLStateStackPointer;

		// GL Arrays
		static PFNGLGENBUFFERSPROC				m_GenBuffers;
		static PFNGLBINDBUFFERPROC				m_BindBuffer;
		static PFNGLBUFFERDATAPROC				m_BufferData;
		static PFNGLBUFFERSUBDATAPROC			m_BufferSubData;
		static PFNGLDELETEBUFFERSPROC			m_DeleteBuffers;

		// GL Shaders
		static PFNGLCREATESHADERPROC			m_CreateShader;
		static PFNGLSHADERSOURCEPROC			m_ShaderSource;
		static PFNGLCOMPILESHADERPROC			m_CompileShader;
		static PFNGLISSHADERPROC				m_IsShader;
		static PFNGLGETSHADERINFOLOGPROC		m_GetShaderInfoLog;
		static PFNGLGETPROGRAMINFOLOGPROC		m_GetProgramInfoLog;
		static PFNGLCREATEPROGRAMPROC			m_CreateProgram;
		static PFNGLATTACHSHADERPROC			m_AttachShader;
		static PFNGLLINKPROGRAMPROC				m_LinkProgram;
		static PFNGLUSEPROGRAMPROC				m_UseProgram;
		static PFNGLGETATTRIBLOCATIONPROC		m_GetAttribLocation;
		static PFNGLGETUNIFORMLOCATIONPROC		m_GetUniformLocation;
		static PFNGLDELETESHADERPROC			m_DeleteShader;
		static PFNGLDELETEPROGRAMPROC			m_DeleteProgram;
		static PFNGLUNIFORM1FPROC				m_Uniform1f;
		static PFNGLUNIFORM1IPROC				m_Uniform1i;
		static PFNGLUNIFORM2FVPROC				m_Uniform2fv;
		static PFNGLUNIFORM2IVPROC				m_Uniform2iv;
		static PFNGLUNIFORM3FVPROC				m_Uniform3fv;
		static PFNGLUNIFORM3IVPROC				m_Uniform3iv;
		static PFNGLUNIFORM4FVPROC				m_Uniform4fv;
		static PFNGLUNIFORM4IVPROC				m_Uniform4iv;
		static PFNGLUNIFORMMATRIX3FVPROC		m_UniformMatrix3f;
		static PFNGLUNIFORMMATRIX4FVPROC		m_UniformMatrix4f;

		// VBO
		static PFNGLGENVERTEXARRAYSPROC			m_GenVertexArrays;
		static PFNGLBINDVERTEXARRAYPROC			m_BindVertexArray;
		static PFNGLDELETEVERTEXARRAYSPROC		m_DeleteVertexArrays;
		static PFNGLVERTEXATTRIBPOINTERPROC		m_VertexAttribPointer;
		static PFNGLENABLEVERTEXATTRIBARRAYPROC m_EnableVertexAttribArray;
		static PFNGLDRAWARRAYSPROC				m_DrawArrays;
		static PFNGLDRAWELEMENTSPROC			m_DrawElements;

		// FBO
		static PFNGLGENFRAMEBUFFERSPROC			m_GenFramebuffers;
		static PFNGLBINDFRAMEBUFFERPROC			m_BindFramebuffer;
		static PFNGLDELETEFRAMEBUFFERSPROC		m_DeleteFramebuffers;
		static PFNGLCHECKFRAMEBUFFERSTATUSPROC	m_CheckFramebufferStatus;
		static PFNGLFRAMEBUFFERTEXTUREPROC		m_FramebufferTexture;
		static PFNGLFRAMEBUFFERTEXTURE1DPROC	m_FramebufferTexture1D;
		static PFNGLFRAMEBUFFERTEXTURE2DPROC	m_FramebufferTexture2D;
		static PFNGLFRAMEBUFFERTEXTURE3DPROC	m_FramebufferTexture3D;
		static PFNGLFRAMEBUFFERRENDERBUFFERPROC	m_FramebufferRenderbuffer;

		// Renderbuffer
		static PFNGLGENRENDERBUFFERSPROC		m_GenRenderbuffers;
		static PFNGLDELETERENDERBUFFERSPROC		m_DeleteRenderbuffers;
		static PFNGLBINDRENDERBUFFERPROC		m_BindRenderbuffer;
		static PFNGLRENDERBUFFERSTORAGEPROC		m_RenderbufferStorage;

		// Textures
		static PFNGLACTIVETEXTUREPROC			m_ActiveTexture;
		static PFNGLTEXIMAGE3DPROC				m_TexImage3D;
		static PFNGLGENERATEMIPMAPPROC			m_GenerateMipmap;

		// VSync
		static PFNWGLSWAPINTERVALEXTPROC		m_SwapInterval;
		static PFNWGLGETSWAPINTERVALEXTPROC		m_GetSwapInterval;

		// GL Main & Deprecated
		static PFNGLBLENDEQUATIONPROC			m_BlendEquation;
};

} 
