/**
 * @file	fw/IList.h
 *
 * @brief	Declares the IList interface.
 */

#pragma once
#include "Array.h"

namespace FW {

/**
 * @class	IList
 *
 * @brief	IList interface.
 *
 * @date	1.10.2011
 */
template<class T>
class IList
{
	public:
		/**
		 * @fn	virtual void IList::Add(const T& object) = 0;
		 *
		 * @brief	Adds object..
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to add.
		 */
		virtual void Add(const T& object) = 0;

		/**
		 * @fn	virtual void IList::Clear() = 0;
		 *
		 * @brief	Clears this object to its blank state.
		 *
		 * @date	1.10.2011
		 */
		virtual void Clear() = 0;

		/**
		 * @fn	virtual void IList::CopyTo(Array<T>* array, u32 index) const = 0;
		 *
		 * @brief	Copies this collection to an array.
		 *
		 * @date	1.10.2011
		 *
		 * @param [in,out]	array	The array.
		 * @param	index		 	(optional) zero-based index of the first element
		 * 							in the array.
		 */
		virtual void CopyTo(Array<T>* array, u32 index) const = 0;

		/**
		 * @fn	virtual void IList::RemoveAt(u32 index) = 0;
		 *
		 * @brief	Removes object at described index.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 */
		virtual void RemoveAt(u32 index) = 0;

		/**
		 * @fn	virtual T& IList::At(u32 index) = 0;
		 *
		 * @brief	Return object at specified position.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		virtual T& At(u32 index) = 0;

		/**
		 * @fn	virtual const T& IList::At(u32 index) const = 0;
		 *
		 * @brief	Return object at specified position.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		virtual const T& At(u32 index) const = 0;

		/**
		 * @fn	virtual T& IList::operator[](u32 index) = 0;
		 *
		 * @brief	Array indexer operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		virtual T& operator[](u32 index) = 0;

		/**
		 * @fn	virtual const T& IList::operator[](u32 index) const = 0;
		 *
		 * @brief	Array indexer operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		virtual const T& operator[](u32 index) const = 0;

		/**
		 * @fn	virtual u32 IList::GetCount() const = 0;
		 *
		 * @brief	Gets the number of elements contained in the collection.
		 *
		 * @date	1.10.2011
		 *
		 * @return	Count of elements.
		 */
		virtual u32 GetCount() const = 0;

	protected:
		/**
		 * @fn	IList::IList()
		 *
		 * @brief	Default (protected) constructor.
		 *
		 * @date	1.10.2011
		 */
		IList()
		{

		}
};

} // namespace
