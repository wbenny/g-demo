#pragma once
#include "Types.h"
#include "String.h"
#include "Singleton.h"

namespace FW { 

class Console : public Singleton<Console>
{
	friend class Singleton<Console>;

	public:
		HANDLE GetConsoleOutputHandle()
		{
			return m_ConsoleOutputHandle;
		}

		CONSOLE_SCREEN_BUFFER_INFO GetBufferInfo()
		{
			CONSOLE_SCREEN_BUFFER_INFO csbi;
			GetConsoleScreenBufferInfo(m_ConsoleOutputHandle, &csbi);
			return csbi;
		}

		void Clear()
		{
			COORD dwWriteCoord = { 0, 0 };
			CONSOLE_SCREEN_BUFFER_INFO bufferInfo = GetBufferInfo();
			DWORD nLength = bufferInfo.dwSize.X * bufferInfo.dwSize.Y;
			DWORD pNumCharsWritten = 0;
			
			FillConsoleOutputCharacter(m_ConsoleOutputHandle, static_cast<TCHAR>(' '), nLength, dwWriteCoord, &pNumCharsWritten);

			pNumCharsWritten = 0;
			FillConsoleOutputAttribute(m_ConsoleOutputHandle, bufferInfo.wAttributes, nLength, dwWriteCoord, &pNumCharsWritten);
			SetConsoleCursorPosition(m_ConsoleOutputHandle, dwWriteCoord);
		}

		void Write(const char *format, ...) 
		{
			va_list argList;
			va_start(argList, format);

			vprintf(format, argList);

			va_end(argList);
		}

		void WriteLine() 
		{
			printf("\r\n");
		}

		void WriteLine(const char *format, ...) 
		{
			va_list argList;
			va_start(argList, format);

			vprintf(format, argList);

			va_end(argList);

			printf("\r\n");
		}

	private:
		Console() : m_ConsoleOutputHandle(0)
		{ 		
			m_ConsoleOutputHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		}

		HANDLE m_ConsoleOutputHandle;
};

}
