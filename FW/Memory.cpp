#include "Memory.h"

// void* __cdecl operator new(size_t n)
// {
// 	return FW::Memory::Alloc(n);
// }
// 
// void* __cdecl operator new[](size_t n)
// {
// 	return FW::Memory::Alloc(n);
// }
// 
// void __cdecl operator delete(void* ptr)
// {
// 	FW::Memory::Dealloc(ptr);
// }
// 
// void __cdecl operator delete[](void* ptr)
// {
// 	FW::Memory::Dealloc(ptr);
// }

namespace FW {

void* Memory::Alloc(u32 n)
{
	return reinterpret_cast<void *>(GlobalAlloc(GMEM_ZEROINIT, static_cast<SIZE_T>(n)));
}

void Memory::Dealloc(void* ptr)
{
	GlobalFree(reinterpret_cast<HGLOBAL>(ptr));
}

void* Memory::Realloc(void* ptr, u32 oldSize, u32 newSize)
{
	void* newPtr = reinterpret_cast<void*>(new u8[newSize]);

	if (ptr)
	{
		Copy(newPtr, ptr, newSize > oldSize ? oldSize : newSize);
		delete[] ptr;
	}

	return newPtr;
}

void* Memory::Set(void* dst, int val, u32 count)
{
	return ::memset(dst, val, count);
}

void* Memory::Copy(void* dst, const void* src, u32 count)
{
	return ::memcpy(dst, src, count);
}

int Memory::Compare(const void* ptr1, const void* ptr2, u32 count)
{
	return ::memcmp(ptr1, ptr2, count);
}

}
