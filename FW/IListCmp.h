#pragma once
//#include "IList.h"

namespace FW {

template <class T>
class IListCmp // : public IList<T>
{
	public:
		/**
		 * @fn	virtual bool IList::Contains(const T& object) const = 0;
		 *
		 * @brief	Query if this object contains the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to test for containment.
		 *
		 * @return	true if the object is in this collection, false if not.
		 */
		virtual bool Contains(const T& object) const = 0;

		/**
		 * @fn	virtual u32 IList::IndexOf(const T& object) const = 0;
		 *
		 * @brief	Index of the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object.
		 *
		 * @return	.
		 */
		virtual u32 IndexOf(const T& object) const = 0;

		/**
		 * @fn	virtual void IList::Remove(const T& object) = 0;
		 *
		 * @brief	Removes the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to remove.
		 */
		virtual void Remove(const T& object) = 0;


};

}
