#pragma once
#include "List.h"
#include "IListCmp.h"

namespace FW {

template <class T>
class ListCmp : public List<T>, public IListCmp<T>
{
	public:
		/**
		 * @fn	bool List::Contains(const T& object) const
		 *
		 * @brief	Query if this object contains the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to test for containment.
		 *
		 * @return	true if the object is in this collection, false if not.
		 */
		bool Contains(const T& object) const
		{
			return (std::find(m_Vector.cbegin(), m_Vector.cend(), object) !=
						m_Vector.cend());
		}

		/**
		 * @fn	u32 List::IndexOf(const T& object) const
		 *
		 * @brief	Index of the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object.
		 *
		 * @return	The index.
		 */
		u32 IndexOf(const T& object) const
		{
			std::vector<T>::const_iterator it;
			it = std::find(m_Vector.cbegin(), m_Vector.cend(), object);

			if (it == m_Vector.cend())
				return static_cast<u32>(-1);

			return (it - m_Vector.cbegin());
		}

		/**
		 * @fn	void List::Remove(const T& object)
		 *
		 * @brief	Removes the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to remove.
		 */
		void Remove(const T& object)
		{
			std::vector<T>::iterator it;
			it = std::find(m_Vector.begin(), m_Vector.end(), object);

			if (it != m_Vector.end())
				m_Vector.erase(it);
		}
};

}
