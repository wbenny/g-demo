/**
 * @file	fw/IPtrList.h
 *
 * @brief	Declares the IPtrList interface.
 */

#pragma once
#include "Array.h"

namespace FW {

/**
 * @class	IPtrList
 *
 * @brief	IPtrList interface.
 *
 * @date	1.10.2011
 */
template<class T>
class IPtrList
{
	public:
		/**
		 * @fn	virtual void IPtrList::Add(T* object) = 0;
		 *
		 * @brief	Adds object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to add.
		 */
		virtual void Add(T* object) = 0;

		/**
		 * @fn	virtual void IPtrList::Clear() = 0;
		 *
		 * @brief	Clears this object to its blank state.
		 *
		 * @date	1.10.2011
		 */
		virtual void Clear() = 0;

		/**
		 * @fn	virtual bool IPtrList::Contains(const T* object) const = 0;
		 *
		 * @brief	Query if this object contains the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to test for containment.
		 *
		 * @return	true if the object is in this collection, false if not.
		 */
		virtual bool Contains(const T* object) const = 0;

		/**
		 * @fn	virtual u32 IPtrList::IndexOf(const T &object) const = 0;
		 *
		 * @brief	Index of the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object.
		 *
		 * @return	.
		 */
		virtual u32 IndexOf(const T* object) const = 0;

		/**
		 * @fn	virtual void IPtrList::Remove(const T* object) = 0;
		 *
		 * @brief	Removes the given object.
		 *
		 * @date	1.10.2011
		 *
		 * @param	object	The object to remove.
		 */
		virtual void Remove(const T* object) = 0;

		/**
		 * @fn	virtual void IPtrList::RemoveAt(u32 index) = 0;
		 *
		 * @brief	Removes object at described index.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 */
		virtual void RemoveAt(u32 index) = 0;

		/**
		 * @fn	virtual T* IPtrList::At(u32 index) = 0;
		 *
		 * @brief	Return object at specified position.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		virtual T* At(u32 index) = 0;

		/**
		 * @fn	virtual const T* IPtrList::At(u32 index) const = 0;
		 *
		 * @brief	Return const object at specified position.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		virtual const T* At(u32 index) const = 0;

		/**
		 * @fn	virtual T* IPtrList::operator[](u32 index) = 0;
		 *
		 * @brief	Array indexer operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		virtual T* operator[](u32 index) = 0;

		/**
		 * @fn	virtual const T* IPtrList::operator[](u32 index) const = 0;
		 *
		 * @brief	Array indexer const operator.
		 *
		 * @date	1.10.2011
		 *
		 * @param	index	Zero-based index.
		 *
		 * @return	The object.
		 */
		virtual const T* operator[](u32 index) const = 0;

		/**
		 * @fn	virtual u32 IPtrList::GetCount() const = 0;
		 *
		 * @brief	Gets the number of elements contained in the collection.
		 *
		 * @date	1.10.2011
		 *
		 * @return	Count of elements.
		 */
		virtual u32 GetCount() const = 0;

	protected:
		/**
		 * @fn	IPtrList::IPtrList()
		 *
		 * @brief	Default (protected) constructor.
		 *
		 * @date	1.10.2011
		 */
		IPtrList()
		{

		}
};

} // namespace
