#pragma once
#include "Types.h"

// void* __cdecl operator new(size_t n);
// void* __cdecl operator new[](size_t n);
// void  __cdecl operator delete(void* ptr);
// void  __cdecl operator delete[](void* ptr);

namespace FW {

class Memory
{
	public:
		static void* Alloc(u32 n);
		static void  Dealloc(void* ptr);
		static void* Realloc(void* ptr, u32 oldSize, u32 newSize);
		static void* Set(void* dst, s32 val, u32 count);
		static void* Copy(void* dst, const void* src, u32 count);
		static int   Compare(const void* ptr1, const void* ptr2, u32 count);
};

}
