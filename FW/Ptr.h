/**
 * @file	fw/Ptr.h
 *
 * @brief	Declares the Ptr class.
 */

#pragma once
#include "RefCounter.h"

namespace FW {

/**
 * @class	Ptr<T>
 *
 * @brief	Pointer class for garbage collecting. 
 *
 * @date	30.9.2011
 */
template<class T>
class Ptr
{
	public:
		/**
		 * @typedef	T Type
		 *
		 * @brief	Defines an alias representing the type.
		 */
		typedef T Type;

		/**
		 * @fn	inline Ptr::Ptr()
		 *
		 * @brief	Default constructor.
		 *
		 * @date	30.9.2011
		 */
		inline Ptr() :
			m_Ptr(NULL)
		{
			
		}

		/**
		 * @fn	inline Ptr::Ptr(T* ptr)
		 *
		 * @brief	Constructor.
		 *
		 * @date	30.9.2011
		 *
		 * @param	ptr	The pointer.
		 */
		inline Ptr(T* ptr) :
			m_Ptr(ptr)
		{
			if (m_Ptr)
				m_Ptr->Ref();
		}

		/**
		 * @fn	inline Ptr::Ptr(const Ptr& rp)
		 *
		 * @brief	Copy constructor.
		 *
		 * @date	30.9.2011
		 *
		 * @param	rp	The pointer.
		 */
		inline Ptr(const Ptr& rp) :
			m_Ptr(rp.m_Ptr)
		{
			if (m_Ptr) 
				m_Ptr->Ref();
		}

		/**
		 * @fn	template <class T2> inline Ptr::Ptr(const Ptr<T2>& rp)
		 *
		 * @brief	Copy constructor (from pointer of different type).
		 *
		 * @date	30.9.2011
		 *
		 * @param	rp	The pointer.
		 */
		template <class T2>
		inline Ptr(const Ptr<T2>& rp) :
			m_Ptr(rp.m_Ptr)
		{
			if (m_Ptr)
				m_Ptr->Ref();
		}

		/**
		 * @fn	inline Ptr::~Ptr()
		 *
		 * @brief	Destructor.
		 *
		 * @date	30.9.2011
		 */
		inline ~Ptr()
		{
			if (m_Ptr)
				m_Ptr->Unref();
		}

		/**
		 * @fn	Ptr& Ptr::operator= (const Ptr& rp)
		 *
		 * @brief	Assignment operator.
		 *
		 * @date	30.9.2011
		 *
		 * @param	rp	The pointer.
		 *
		 * @return	A reference to this object.
		 */
		Ptr &operator = (const Ptr& rp)
		{
			Assign(rp);
			return *this;
		}

		/**
		 * @fn	template <class T2> Ptr &Ptr::operator= (const Ptr<T2> &rp)
		 *
		 * @brief	Assignment operator (of the different type pointer).
		 *
		 * @date	30.9.2011
		 *
		 * @param	rp	The pointer.
		 *
		 * @return	A reference to this object.
		 */
		template <class T2> 
		Ptr &operator = (const Ptr<T2> &rp)
		{
			Assign(rp);
			return *this;
		}

		/**
		 * @fn	inline Ptr &Ptr::operator= (T* ptr)
		 *
		 * @brief	Assignment operator.
		 *
		 * @date	30.9.2011
		 *
		 * @param	ptr	The pointer.
		 *
		 * @return	A reference to this object.
		 */
		inline Ptr &operator = (T* ptr)
		{
			if (m_Ptr == ptr)
				return *this;

			T* tmp_ptr = m_Ptr;
			m_Ptr = ptr;

			if (m_Ptr)
				m_Ptr->Ref();

			if (tmp_ptr)
				tmp_ptr->Unref();

			return *this;
		}

		/**
		 * @fn	operator Ptr::T* () const
		 *
		 * @brief	T* casting operator.
		 *
		 * @date	30.9.2011
		 */
		operator T* () const
		{
			return m_Ptr;
		}

		/**
		 * @fn	T& Ptr::operator*() const
		 *
		 * @brief	Indirection operator.
		 *
		 * @date	30.9.2011
		 *
		 * @return	The result of the operation.
		 */
		T& operator *() const
		{
			return *m_Ptr;
		}

		/**
		 * @fn	T* Ptr::operator->() const
		 *
		 * @brief	Member dereference operator.
		 *
		 * @date	30.9.2011
		 *
		 * @return	The dereferenced object.
		 */
		T* operator ->() const
		{
			return m_Ptr;
		}

		/**
		 * @fn	T* Ptr::Get() const
		 *
		 * @brief	Gets the original pointer.
		 *
		 * @date	30.9.2011
		 *
		 * @return	The pointer.
		 */
		T* Get() const
		{
			return m_Ptr;
		}

		/**
		 * @fn	bool Ptr::operator!() const
		 *
		 * @brief	Finaliser.
		 *
		 * @date	30.9.2011
		 */
		bool operator !() const
		{
			return m_Ptr == NULL;
		}

		/**
		 * @fn	bool Ptr::IsValid() const
		 *
		 * @brief	Query if this object is valid.
		 *
		 * @date	30.9.2011
		 *
		 * @return	true if valid, false if not.
		 */
		bool IsValid() const
		{ 
			return m_Ptr != NULL;
		}

	private:
		/**
		 * @fn	template <class T2> void Ptr::Assign(const Ptr<T2> &rp)
		 *
		 * @brief	Assigns the pointer of different type.
		 *
		 * @date	30.9.2011
		 *
		 * @param	rp	The pointer.
		 */
		template <class T2>
		void Assign(const Ptr<T2> &rp)
		{
			if (m_Ptr == rp.m_Ptr)
				return;

			T *tmp_ptr = m_Ptr;
			m_Ptr = rp.m_Ptr;

			if (m_Ptr)
				m_Ptr->Ref();

			if (tmp_ptr)
				tmp_ptr->Unref();
		}

		template <class T2> friend class Ptr;

		// Members.
		T *m_Ptr;	///< The pointer.
};

} // namespace
