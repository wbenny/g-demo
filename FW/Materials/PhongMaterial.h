#pragma once
#include "../Math/Vector3.h"
#include "../Math/Matrix4.h"
#include "../Scene/DirectionalLightNode.h"
#include "../Render/Texture.h"
#include "../Render/CubeMap.h"
#include "../Render/ShaderChunkManager.h"
#include "../String.h"
#include "MaterialBase.h"

#define PHONG_MAX_LIGHTS 4

namespace FW { namespace Materials {

class PhongMaterial : public MaterialBase
{
	public:
		PhongMaterial(Render::Texture* texture = NULL) :
			m_Texture(texture)
		{
			m_Color.Set(0.5f, 0.1f, 0.8f);
			m_SpecularShininess = 0.5f;
			m_SpecularReflection = 0.0f;
//			m_SpecularReflection = 0.9f;
			m_DiffuseReflection = 0.5f;
			m_EnvironmentMapShininess = 0.35f;

			m_ReflectionStrength = 0.3f;

			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("phong");

			InitShaderProperties();
		}

		void InitShaderProperties()
		{
			MaterialBase::InitShaderProperties();

			p_uCastShadow					.Init("uCastShadow");
			p_uUseTexture					.Init("uUseTexture");
			p_uUseEnvironmentMap			.Init("uUseEnvironmentMap");
			p_uUseNormalMap					.Init("uUseNormalMap");
			p_uUseHeightMap					.Init("uUseHeightMap");
			p_uIsReflective					.Init("uIsReflective");

			p_uTexture						.Init("uTexture");
			p_uNormalMap					.Init("uNormalMap");
			p_uHeightMap					.Init("uHeightMap");
			p_uEnvironmentMap				.Init("uEnvironmentMap");
			p_uReflectionMap				.Init("uReflectionMap");
			p_uEnvironmentMapShininess		.Init("uEnvironmentMapShininess");
			p_uReflectionStrength			.Init("uReflectionStrength");
			p_uParallaxFactor				.Init("uParallaxFactor");
			p_uAlpha						.Init("uAlpha");
			p_uAmbientLightColor			.Init("uAmbientLightColor");

			p_uShadowMap					.Init("uShadowMap");
			p_uShadowMatrix					.Init("uShadowMatrix");

			p_uMaterial.Color				.Init("uMaterial.Color");
			p_uMaterial.DiffuseReflection	.Init("uMaterial.DiffuseReflection");
			p_uMaterial.SpecularReflection	.Init("uMaterial.SpecularReflection");
			p_uMaterial.SpecularShininess	.Init("uMaterial.SpecularShininess");

			for (u32 i = 0; i < PHONG_MAX_LIGHTS; i++)
			{
				p_uLightSources[i].Type		.Init(String::Format("uLightSources[%i].Type",		i));
				p_uLightSources[i].FallOff	.Init(String::Format("uLightSources[%i].FallOff",	i));
				p_uLightSources[i].Location	.Init(String::Format("uLightSources[%i].Location",	i));
				p_uLightSources[i].Color	.Init(String::Format("uLightSources[%i].Color",		i));
			}
		}

		void Bind()
		{
			MaterialBase::Bind();
	
			if (m_RenderPassData->Mesh->IsReflective() && m_RenderPassData->ReflectionCamera)
				m_ReflectionMap = m_RenderPassData->ReflectionCamera->GetFramebuffer()->GetTexture();
			else
				m_ReflectionMap = NULL;
			
			if (m_Texture)
			{
				Render::Texture::SetActiveTexture(0);
				m_Texture->Bind();
				p_uTexture = 0;
			}

			if (m_NormalMap)
			{
				Render::Texture::SetActiveTexture(1);
				m_NormalMap->Bind();
				p_uNormalMap = 1;
			}

			if (m_HeightMap)
			{
				Render::Texture::SetActiveTexture(5);
				m_HeightMap->Bind();
				p_uHeightMap = 5;

				p_uParallaxFactor = 1.0f;
			}

			if (m_EnvironmentMap)
			{
				Render::CubeMap::SetActiveTexture(2);
				m_EnvironmentMap->Bind();
				p_uEnvironmentMap = 2;
			}

			if (m_ReflectionMap)
			{
				Render::Texture::SetActiveTexture(3);
				m_ReflectionMap->Bind();
				p_uReflectionMap = 3;

				p_uReflectionStrength = m_ReflectionStrength;
			}

			p_uAlpha = 1.0f; //m_RenderPassData->Mesh->GetOpacity();

			p_uUseTexture = m_Texture.IsValid();
			p_uUseNormalMap = m_NormalMap.IsValid();
			p_uUseEnvironmentMap = m_EnvironmentMap.IsValid();
			p_uIsReflective = m_ReflectionMap.IsValid();
			p_uUseHeightMap = m_HeightMap.IsValid();
			p_uEnvironmentMapShininess = m_EnvironmentMapShininess;

			p_uMaterial.Color				= m_Color;
			p_uMaterial.DiffuseReflection	= m_DiffuseReflection;
			p_uMaterial.SpecularReflection	= m_SpecularReflection;
			p_uMaterial.SpecularShininess	= m_SpecularShininess;

			p_uAmbientLightColor = m_RenderPassData->AmbientLightColor;

			bool shadowCast = false;

			for (u32 i = 0; i < PHONG_MAX_LIGHTS; i++)
			{
				if (i >= m_RenderPassData->Lights.GetCount())
				{
					p_uLightSources[i].Type		= 0;
					p_uLightSources[i].FallOff	= 0.0f;
					p_uLightSources[i].Location	= Math::Vector3::Zero;
					p_uLightSources[i].Color	= Math::Vector3::Zero;

					continue;
				}

				if (m_RenderPassData->Lights[i]->GetLightType() == Scene::LightType::Directional)
				{
					Math::Matrix4 shadowMatrix = Math::Matrix4(
							0.5f, 0.0f, 0.0f, 0.0f, 
							0.0f, 0.5f, 0.0f, 0.0f,
							0.0f, 0.0f, 0.5f, 0.0f,
							0.5f, 0.5f, 0.5f, 1.0f
						);

					Scene::DirectionalLightNode* dirLight = reinterpret_cast<Scene::DirectionalLightNode*>(m_RenderPassData->Lights[i]);

					// dirLight->GetLightCamera()->UpdateLightCamera();

					shadowMatrix = shadowMatrix * dirLight->GetLightCamera()->GetProjectionMatrix();
					shadowMatrix = shadowMatrix * dirLight->GetLightCamera()->GetWorldMatrix();
					//shadowMatrix = shadowMatrix * m_RenderPassData->ViewMatrix.Inverted();
					
					p_uShadowMatrix = shadowMatrix;

					Render::Texture::SetActiveTexture(4);
					dirLight->GetShadowMap()->Bind();
					p_uShadowMap = 4;

					shadowCast = true;
				}

				p_uLightSources[i].Type		= m_RenderPassData->Lights[i]->GetLightType();
				p_uLightSources[i].FallOff	= m_RenderPassData->Lights[i]->GetFallOffDistance();
				p_uLightSources[i].Location	= m_RenderPassData->Lights[i]->GetLocation();
				p_uLightSources[i].Color	= m_RenderPassData->Lights[i]->GetColor();
			}

			p_uCastShadow = shadowCast;
		}

		const Render::Texture* GetTexture() const { return m_Texture; }
		Render::Texture* GetTexture() { return m_Texture; }
		void SetTexture(Render::Texture* texture) { m_Texture = texture; }

		const Render::CubeMap* GetEnvironmentMap() const { return m_EnvironmentMap; }
		Render::CubeMap* GetEnvironmentMap() { return m_EnvironmentMap; }
		void SetEnvironmentMap(Render::CubeMap* cubeMap) { m_EnvironmentMap = cubeMap; }

		const Render::Texture* GetNormalMap() const { return m_NormalMap; }
		Render::Texture* GetNormalMap() { return m_NormalMap; }
		void SetNormalMap(Render::Texture* texture) { m_NormalMap = texture; }

		const Render::Texture* GetHeightMap() const { return m_HeightMap; }
		Render::Texture* GetHeightMap() { return m_HeightMap; }
		void SetHeightMap(Render::Texture* texture) { m_HeightMap = texture; }


		const Math::Vector3& GetColor() const { return m_Color; }
		float GetDiffuseReflection() const { return m_DiffuseReflection; }
		float GetSpecularReflection() const { return m_SpecularReflection; }
		float GetSpecularShininess() const { return m_SpecularShininess; }
		float GetEnvironmentMapShininess() const { return m_EnvironmentMapShininess; }
		float GetReflectionStrength() const { return m_ReflectionStrength; }
		float GetParallaxFactor() const { return m_ParallaxFactor; }

		void SetColor(const Math::Vector3& color) { m_Color = color; }
		void SetDiffuseReflection(float diffuseReflection) { m_DiffuseReflection = diffuseReflection; }
		void SetSpecularReflection(float specularReflection) { m_SpecularReflection = specularReflection; }
		void SetSpecularShininess(float specularShininess) { m_SpecularShininess = specularShininess; }
		void SetEnvironmentMapShininess(float shininess) { m_EnvironmentMapShininess = shininess; }
		void SetReflectionStrength(float strength) { m_ReflectionStrength = strength; }
		void SetParallaxFactor(float pf) { m_ParallaxFactor = pf; }

		//! Properties.
		Render::ShaderUniform p_uTexture, p_uNormalMap, p_uHeightMap, p_uAlpha, p_uAmbientLightColor;
		Render::ShaderUniform p_uShadowMap, p_uShadowMatrix, p_uReflectionMap;
		Render::ShaderUniform p_uCastShadow, p_uUseTexture, p_uUseEnvironmentMap, p_uUseNormalMap, p_uUseHeightMap, p_uIsReflective;
		Render::ShaderUniform p_uEnvironmentMap, p_uEnvironmentMapShininess, p_uReflectionStrength, p_uParallaxFactor;

		struct
		{
			Render::ShaderUniform Color, DiffuseReflection, SpecularReflection, SpecularShininess;
		} p_uMaterial;

		struct 
		{
			Render::ShaderUniform Type, FallOff, Location, Color;
		} p_uLightSources[PHONG_MAX_LIGHTS];				

	protected:
		float	m_DiffuseReflection,
				m_SpecularReflection,
				m_SpecularShininess,
				m_EnvironmentMapShininess,
				m_ReflectionStrength;

		float	m_ParallaxFactor;

		Math::Vector3	m_Color;
		
		Ptr<Render::Texture> m_Texture, m_NormalMap, m_HeightMap, m_ReflectionMap;
		Ptr<Render::CubeMap> m_EnvironmentMap;
};

} }
