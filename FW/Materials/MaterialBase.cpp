#include "MaterialBase.h"
#include "MaterialManager.h"

namespace FW { namespace Materials {

MaterialBase::~MaterialBase()
{
	if (m_Name.Length() > 0)
		MaterialManager::GetInstance().Remove(m_Name);
}

void MaterialBase::SetName(const String& name)
{
	Content::Resource::SetName(name);

	if (m_Name.Length() > 0)
		MaterialManager::GetInstance().Add(m_Name, this);
}

void MaterialBase::SetName(const char* name)
{
	Content::Resource::SetName(name);

	if (m_Name.Length() > 0)
		MaterialManager::GetInstance().Add(m_Name, this);
}

} }
