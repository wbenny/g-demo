#pragma once
#include "../Render/ScenePassContext.h"
#include "../Render/ShaderManager.h"
#include "../Render/ShaderProperty.h"
#include "../Content/Resource.h"
#include "../Ptr.h"

namespace FW { namespace Materials { 

enum MaterialType
{
	Any,
	SkyDome
};

class MaterialBase : public Content::Resource
{
	public:
		MaterialBase() : Content::Resource(Content::ResourceType::Material), m_MaterialType(MaterialType::Any) {}
		virtual ~MaterialBase();

		virtual void SetName(const String& name);
		virtual void SetName(const char* name);

		virtual void Bind() { m_Shader->Bind(); }
		virtual void Unbind() { m_Shader->Unbind(); }

		const Render::Shader* GetShader() const { return m_Shader; }
		Render::Shader* GetShader() { return m_Shader; }
		void SetShader(Render::Shader* shader) { m_Shader = shader; }

		virtual void InitShaderProperties()
		{
			// FIXME: fix this terrible hack :/
			// by uniform bufforz, fur exampol
			m_Shader->Bind();

			p_uProjectionMatrix.Init("uProjectionMatrix");
			p_uModelMatrix.Init("uModelMatrix");
			p_uViewMatrix.Init("uViewMatrix");
			p_uNormalMatrix.Init("uNormalMatrix");
			p_uProjectionViewModelMatrix.Init("uProjectionViewModelMatrix");
			p_uProjectionViewMatrix.Init("uProjectionViewMatrix");
			p_uEyePosition.Init("uEyePosition");
			p_uEyeDirection.Init("uEyeDirection");
			p_uWindowSize.Init("uWindowSize");
			p_uNearFar.Init("uNearFar");
		}

		MaterialType GetMaterialType() const { return m_MaterialType; }

		void SetScenePassContext(Render::ScenePassContext* data) { m_RenderPassData = data; }

		FW_AS_METHOD

		//! Properties.
		Render::ShaderUniform	p_uProjectionMatrix,
								p_uModelMatrix,
								p_uViewMatrix,
								p_uNormalMatrix,
								p_uProjectionViewModelMatrix,
								p_uProjectionViewMatrix,
								p_uEyePosition,
								p_uEyeDirection,
								p_uWindowSize,
								p_uNearFar;

	protected:
		MaterialType			  m_MaterialType;
		Render::ScenePassContext* m_RenderPassData;
		Ptr<Render::Shader>		  m_Shader;
};

} }
