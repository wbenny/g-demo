#pragma once
#include "../Math/Vector3.h"
#include "../Render/Texture.h"
#include "../Render/ShaderChunkManager.h"
#include "MaterialBase.h"

namespace FW { namespace Materials {

class SkyMaterial : public MaterialBase
{
	public:
		SkyMaterial()
		{
			m_MaterialType = MaterialType::SkyDome;
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("sky");

			m_Texture = Content::ResourceLoader<Render::Texture>::Read("textures\\sky.tga", true);

			m_Horizon = Math::Vector3(0.9f, 0.7f, 0.7f);
			m_SkydomeHeight = 700.0f;
			m_Time = 0.0f;

			InitShaderProperties();
		}

		void InitShaderProperties()
		{
			MaterialBase::InitShaderProperties();

			p_uTexture						.Init("uTexture");
			p_uHorizon						.Init("uHorizon");
			p_uSkydomeHeight				.Init("uSkydomeHeight");
			p_uTime							.Init("uTime");
		}

		void Bind()
		{
			MaterialBase::Bind();

			p_uHorizon = m_Horizon;
			p_uSkydomeHeight = m_SkydomeHeight;

			Render::Texture::SetActiveTexture(0);
			m_Texture->Bind();
			p_uTexture = 0;

			p_uTime = m_Time;
		}

		float GetSkydomeHeight() const { return m_SkydomeHeight; }
		void SetSkydomeHeight(float SkydomeHeight) { m_SkydomeHeight = SkydomeHeight; }

		float GetTime() const { return m_Time; }
		void SetTime(float time) { m_Time = time; }

		const Math::Vector3& GetHorizon() const { return m_Horizon; }
		void SetHorizon(const Math::Vector3& horizon) { m_Horizon = horizon; }

		//! Properties.
		Render::ShaderUniform p_uTexture,
							  p_uHorizon,
							  p_uSkydomeHeight,
							  p_uTime;

	protected:
		float m_SkydomeHeight, m_Time;
		Math::Vector3 m_Horizon;
		
		Ptr<Render::Texture> m_Texture;
};

} }
