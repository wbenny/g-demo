#pragma once
#include "../String.h"
#include "../Singleton.h"
#include "../Map.h"
#include "MaterialBase.h"

namespace FW { namespace Materials {

class MaterialManager : public Singleton<MaterialManager>, public Map<String, Ptr<MaterialBase> >
{
	friend class Singleton<MaterialManager>;

	public:
		MaterialBase* GetMaterial(const String& name)
		{
			return GetMappedElement(name);
		}

	private:
		MaterialManager() {}
};

} }
