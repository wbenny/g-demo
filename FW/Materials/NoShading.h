#pragma once
#include "../Ptr.h"
#include "../String.h"
#include "../Render/ShaderManager.h"

namespace FW { namespace Materials {

class NoShading : public MaterialBase
{
	public:
		NoShading()
		{
			m_Shader = Render::ShaderManager::GetInstance().GetShaderByName("noshading");

			InitShaderProperties();
		}

		void InitShaderProperties()
		{
			MaterialBase::InitShaderProperties();

			p_uColor.Init("uColor");
		}

		void Bind()
		{
			MaterialBase::Bind();

			p_uColor = m_Color;
		}

		const Math::Vector3& GetColor() const { return m_Color; }
		void SetColor(const Math::Vector3& color) { m_Color = color; }

		Render::ShaderUniform p_uColor;

	protected:
		Math::Vector3 m_Color;
};

} }
