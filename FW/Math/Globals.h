#pragma once

namespace FW { namespace Math {

const float PI			= 3.1415926535897932384626433832795f;
const float DEGTORAD    = PI / 180.0f;
const float RADTODEG    = 180.0f / PI;

__inline float DegToRad(float deg) { return deg * (PI / 180.0f); }
__inline float RadToDeg(float rad) { return rad * (180.0f / PI); }

} }

