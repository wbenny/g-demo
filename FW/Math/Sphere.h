#pragma once
#include "Vector3.h"
#include "Vector4.h"

namespace FW { namespace Math {

struct Sphere
{
  Sphere()
  {
    memset(this, 0, sizeof(*this));
  }

  Sphere(const Sphere& other)
  {
    memcpy(this, &other, sizeof(*this));
  }

  ~Sphere()
  {

  }

  Sphere& operator=(const Sphere& other)
  {
    memcpy(this, &other, sizeof(*this));
  }

	union
	{
		struct
		{
			Vector3 m_Position;
			float m_Radius;
		};

		struct
		{
			float Cell[4];
		};

		struct { Vector4 m_Vector4; };
	};
};

} }
