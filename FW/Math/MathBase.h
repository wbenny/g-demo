#pragma once
#include <cmath>

namespace FW { namespace Math {

/*
float Sin(float x);
float Cos(float x);
float Tan(float x);
float Cotg(float x);
void  SinCos(float x, float* sin, float* cos);
float Sqrt(float x);
float Exp(float x);
float Log2(float x);
float Mod(float x, float y);
float Pow(float x, float y);
float Floor(float x);
int   IFloor(float x);

float Random(int *seed);
*/

inline float Sin(float x) { return sin(x); }
inline float Cos(float x) { return cos(x); }
inline float Tan(float x) { return tan(x); }
inline float Cotg(float x) { return 1.0f/Tan(x); }
inline void  SinCos(float x, float* sin, float* cos) { *sin = Sin(x); *cos = Cos(x); }
inline float Sqrt(float x) { return sqrt(x); }
inline float Exp(float x) { return exp(x); }
inline float Log2(float x) { return log(x); }
inline float Mod(float x, float y) { return 0.0f; }
inline float Pow(float x, float y) { return pow(x, y); }
inline float Floor(float x) { return 0.0f; }
inline int   IFloor(float x) { return (int)x; }

inline float Random(int *seed)
{
	float res;

	seed[0] *= 16807;

	*((unsigned int *) &res) = ( ((unsigned int)seed[0])>>9 ) | 0x40000000;

	return( res-3.0f );
}

} }
