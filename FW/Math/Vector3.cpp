#include "Vector3.h"

namespace FW { namespace Math {

const Vector3 Vector3::Zero    = Vector3(0.0f, 0.0f, 0.0f);
const Vector3 Vector3::One     = Vector3(1.0f, 1.0f, 1.0f);
const Vector3 Vector3::NormalX = Vector3(1.0f, 0.0f, 0.0f);
const Vector3 Vector3::NormalY = Vector3(0.0f, 1.0f, 0.0f);
const Vector3 Vector3::NormalZ = Vector3(0.0f, 0.0f, 1.0f);

} }
