#include "Matrix3.h"

namespace FW { namespace Math {

const Matrix3 Matrix3::Zero     = Matrix3(0.0f, 0.0f, 0.0f,
										  0.0f, 0.0f, 0.0f,
										  0.0f, 0.0f, 0.0f);

const Matrix3 Matrix3::One      = Matrix3(1.0f, 1.0f, 1.0f,
										  1.0f, 1.0f, 1.0f,
									      1.0f, 1.0f, 1.0f);

const Matrix3 Matrix3::Identity = Matrix3(1.0f, 0.0f, 0.0f,
									      0.0f, 1.0f, 0.0f,
									      0.0f, 0.0f, 1.0f);

} }
