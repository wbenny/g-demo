#pragma once
#include "Vector2.h"
#include "Matrix4.h"

namespace FW { namespace Math {

struct Projection
{
	Math::Matrix4 Matrix;
	Math::Vector2 NearFar;

	static Projection CreatePerspectiveFieldOfView(float fieldOfView, float aspectRatio, float nearPlane, float farPlane)
	{
		Projection result = {
			Math::Matrix4::CreatePerspectiveFieldOfView(fieldOfView, aspectRatio, nearPlane, farPlane),
			Math::Vector2(nearPlane, farPlane)
		};

		return result;
	}

	static Projection CreateOrthographic(float width, float height, float nearPlane, float farPlane)
	{
		Projection result = {
			Math::Matrix4::CreateOrthographic(width, height, nearPlane, farPlane),
			Math::Vector2(nearPlane, farPlane)
		};

		return result;
	}
};

} }
