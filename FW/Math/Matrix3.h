#pragma once
#include "../Memory.h"
#include "../Types.h"

namespace FW { namespace Math {

class Matrix3
{
	public:
		union
		{
			struct
			{
				float M11, M21, M31;
				float M12, M22, M32;
				float M13, M23, M33;
			};

			struct
			{
				float Cell[9];
			};
		};

		Matrix3() :
			M11(1.0f), M21(0.0f), M31(0.0f),
			M12(0.0f), M22(1.0f), M32(0.0f),
			M13(0.0f), M23(0.0f), M33(1.0f)
		{

		}

		Matrix3(const Matrix3& m)
		{
			Memory::Copy(Cell, m.Cell, sizeof(Cell));
		}

		Matrix3(float m11, float m12, float m13,
				float m21, float m22, float m23,
				float m31, float m32, float m33) :
				M11(m11), M12(m12), M13(m13),
				M21(m21), M22(m22), M23(m23),
				M31(m31), M32(m32), M33(m33)
		{

		}

		Matrix3& operator = (const Matrix3& m)
		{
			Memory::Copy(Cell, m.Cell, sizeof(Cell));
			return *this;
		}

		float GetCell(int index) 
		{
			// if (x >= 0 && x <= 2)
			return Cell[index];
		}

		float GetCell(int x, int y)
		{
			// if (x >= 0 && y >= 0 && x <= 2 && y <= 2)
			return Cell[y * 3 + x];
		}

		void SetCell(int index, float value)
		{
			// if (x >= 0 && x <= 2)
			// return *(Cell + index) = value;
			Cell[index] = value;
		}

		void SetCell(int x, int y, float value)
		{
			// if (x >= 0 && y >= 0 && x <= 2 && y <= 2)
			Cell[y * 3 + x] = value;
		}

		void DebugPrint() const
		{
			printf("[%.3f %.3f %.3f]\r\n",   M11, M12, M13);
			printf("|%.3f %.3f %.3f|\r\n",   M21, M22, M23);
			printf("[%.3f %.3f %.3f]\r\n\n", M31, M32, M33);
		}

		static const Matrix3 Zero;
		static const Matrix3 One;
		static const Matrix3 Identity;
};

} }
