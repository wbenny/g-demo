#include "MathBase.h"

// References:
// [Tan, Cotg] (29.11.2011)
// http://svn2.assembla.com/svn/sunken/Core/Numeric/nuFLib.h
// [SinCos] (24.11.2011)
// http://stackoverflow.com/questions/2683588/what-is-the-fastest-way-to-compute-sin-and-cos-together

namespace FW { namespace Math {

// static short _op_round_down = 0x043f ;     // IFloor - round down control word
// 
// double Sin(double x)
// {
// 	__asm fld x;
// 	__asm fsin;
// 	__asm fstp x;
// 
// 	return x;
// }
// 
// double Cos(double x)
// {
// 	__asm fld x;
// 	__asm fcos;
// 	__asm fstp x;
// 
// 	return x;
// }
// 
// double Tan(double x)
// {
// 	__asm fld x;
// 	__asm fptan;
// 	__asm fstp st(0);
// 	__asm fstp x;
// 
// 	return x;
// }
// 
// double Cotg(double x)
// {
// 	__asm fld x;
// 	__asm fptan;
// 	__asm fdivrp st(1), st(0);
// 	__asm fstp x;
// 
// 	return x;
// }
// 
// void SinCos(double x, double* sin, double* cos)
// {
// 	__asm	fld x
// 	__asm	fsincos
// 	__asm	mov ebx, [cos]
// 	__asm	fstp [ebx]
// 	__asm	mov ebx, [sin]
// 	__asm	fstp [ebx]
// }
// 
// double Sqrt(double x)
// {
// 	__asm finit;
// 	__asm fld x;
// 	__asm fsqrt;
// 	__asm fstp x;
// 
// 	return x;
// }
// 
// double Exp(double x)
// {
// 	double res;
// 
// 	_asm fld     dword ptr [x]
// 	_asm fldl2e
// 	_asm fmulp   st(1), st(0)
// 	_asm fld1
// 	_asm fld     st(1)
// 	_asm fprem
// 	_asm f2xm1
// 	_asm faddp   st(1), st(0)
// 	_asm fscale
// 	_asm fxch    st(1)
// 	_asm fstp    st(0)
// 	_asm fstp    dword ptr [res]
// 
// 	return res;
// }
// 
// double Log2(double x)
// {
// 	double res;
// 
// 	_asm fld    dword ptr [x]
// 	_asm fld1
// 	_asm fxch   st(1)
// 	_asm fyl2x
// 	_asm fstp   dword ptr [res]
// 
// 	return res;
// }
// 
// double Mod(double x, double y)
// {
// 	double res;
// 
// 	_asm fld     dword ptr [y]
// 	_asm fld     dword ptr [x]
// 	_asm fprem
// 	_asm fxch    st(1)
// 	_asm fstp    st(0)
// 	_asm fstp    dword ptr [res]
// 
// 	return res;
// }
// 
// double Pow(double x, double y)
// {
// 	double res;
// 
// 	_asm fld     dword ptr [y]
// 	_asm fld     dword ptr [x]
// 	_asm fyl2x
// 	_asm fld1
// 	_asm fld     st(1)
// 	_asm fprem
// 	_asm f2xm1
// 	_asm faddp   st(1), st(0)
// 	_asm fscale
// 	_asm fxch
// 	_asm fstp    st(0)
// 	_asm fstp    dword ptr [res];
// 
// 	return res;
// }
// 
// double Floor(double x)
// {
// 	return static_cast<double>( (int)(x) - ( (x) < 0. ) );
// }
// 
// int IFloor(double x)
// {
// 	int res;
// 	short tmp;
// 
// 	_asm fstcw   word  ptr [tmp]
// 	_asm fld     dword ptr [x]
// 	_asm fldcw   word  ptr [_op_round_down]
// 	_asm fistp   dword ptr [res]
// 	_asm fldcw   word  ptr [tmp]
// 
// 	return res;
// }
// 
// double Random(int *seed)
// {
// 	double res;
// 
// 	seed[0] *= 16807;
// 
// 	*((unsigned int *) &res) = ( ((unsigned int)seed[0])>>9 ) | 0x40000000;
// 
// 	return( res-3.0f );
// }

} }
