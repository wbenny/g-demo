#pragma once
#include "../Types.h"
#include "MathBase.h"
#include "Vector2.h"
#include "Vector3.h"

namespace FW { namespace Math {

class Vector4
{
	public:
		union
		{
			struct { float X, Y, Z, W; };
			struct { float Cell[4]; };
		};

		Vector4():
			X(0.0f),
			Y(0.0f),
			Z(0.0f),
			W(0.0f)
		{

		}

		Vector4(float x, float y, float z, float w):
			X(x),
			Y(y),
			Z(z),
			W(w)
		{
				
		}

		Vector4(float* ptr):
			X(ptr[0]),
			Y(ptr[1]),
			Z(ptr[2]),
			W(ptr[3])
		{
			
		}

		Vector4(const Vector3& vec):
			X(vec.X),
			Y(vec.Y),
			Z(vec.Z),
			W(1.0f)
		{
			
		}

		Vector4(const Vector4& vec):
			X(vec.X),
			Y(vec.Y),
			Z(vec.Z),
			W(vec.W)
		{
			
		}

		void Set(float x, float y, float z, float w)
		{
			X = x;
			Y = y;
			Z = z;
			W = w;
		}

		float Length() const
		{
			return Math::Sqrt(X * X + Y * Y + Z * Z + W * W);
		}

		void Normalize()
		{
			float len = Length();

			if (len)
			{
				len = 1.0f / len;
				X *= len;
				Y *= len;
				Z *= len;
				W *= len;
			}
		}

		Vector4 Normalized() const
		{
			Vector4 result = (*this);
			result.Normalize();
			return result;
		}

		void DebugPrint() const
		{
			printf("(%.3f, %.3f, %.3f, %.3f)\r\n", X, Y, Z, W);
		}

		static float Distance(const Vector4& u, const Vector4& v)
		{
			return Math::Sqrt((u.X - v.X) * (u.X - v.X) + (u.Y - v.Y) * (u.Y - v.Y) + (u.Z - v.Z) * (u.Z - v.Z) + (u.W - v.W) * (u.W - v.W));
		}

		static float Dot(const Vector4& u, const Vector4& v)
		{
			return u.X * v.X + u.Y * v.Y + u.Z * v.Z + u.W * v.W;
		}


		float operator[] (int index) const
		{
			return Cell[index];
		}

		float& operator[] (int index)
		{
			return Cell[index];
		}

		Vector4 operator + (const Vector4& vec) const
		{
			Vector4 result = (*this);
			result.X += vec.X;
			result.Y += vec.Y;
			result.Z += vec.Z;
			result.W += vec.W;
			return result;
		}

		Vector4 operator - (const Vector4& vec) const
		{
			Vector4 result = (*this);
			result.X -= vec.X;
			result.Y -= vec.Y;
			result.Z -= vec.Z;
			result.W -= vec.W;
			return result;
		}

		Vector4 operator * (const Vector4& vec) const
		{
			Vector4 result = (*this);
			result.X *= vec.X;
			result.Y *= vec.Y;
			result.Z *= vec.Z;
			result.W *= vec.W;
			return result;
		}

		Vector4 operator / (const Vector4& vec) const
		{
			Vector4 result = (*this);
			result.X /= vec.X;
			result.Y /= vec.Y;
			result.Z /= vec.Z;
			result.W /= vec.W;
			return result;
		}

		Vector4 operator + (float v) const
		{
			Vector4 result = (*this);
			result.X += v;
			result.Y += v;
			result.Z += v;
			result.W += v;
			return result;
		}

		Vector4 operator - (float v) const
		{
			Vector4 result = (*this);
			result.X -= v;
			result.Y -= v;
			result.Z -= v;
			result.W -= v;
			return result;
		}

		Vector4 operator * (float v) const
		{
			Vector4 result = (*this);
			result.X *= v;
			result.Y *= v;
			result.Z *= v;
			result.W *= v;
			return result;
		}

		Vector4 operator / (float v) const
		{
			Vector4 result = (*this);
			result.X /= v;
			result.Y /= v;
			result.Z /= v;
			result.W /= v;
			return result;
		}

		friend Vector4 operator + (float v, const Vector4& u) 
		{
			return u + v;
		}

		friend Vector4 operator - (float v, const Vector4& u)
		{
			return u - v;
		}

		friend Vector4 operator * (float v, const Vector4& u)
		{
			return u * v;
		}

		friend Vector4 operator / (float v, const Vector4& u)
		{
			return u / v;
		}

		void operator += (const Vector4& v)
		{
			*this = *this + v;
		}

		void operator -= (const Vector4& v)
		{
			*this = *this - v;
		}

		void operator *= (const Vector4& v)
		{
			*this = *this * v;
		}

		void operator /= (const Vector4& v)
		{
			*this = *this / v;
		}

		void operator += (float v)
		{
			*this = *this + v;
		}

		void operator -= (float v)
		{
			*this = *this - v;
		}

		void operator *= (float v)
		{
			*this = *this * v;
		}

		void operator /= (float v)
		{
			*this = *this / v;
		}

		friend bool operator == (const Vector4& l, const Vector4& r)
		{
			return l.X == r.X && l.Y == r.Y && l.Z == r.Z && l.W == r.W;
		}

		Vector4 operator - () const
		{
			Vector4 result = *this;

			result.X = -result.X;
			result.Y = -result.Y;
			result.Z = -result.Z;
			result.W = -result.W;
				
			return result;
		}

		static const Vector4 Zero;
		static const Vector4 One;
		static const Vector4 NormalX;
		static const Vector4 NormalY;
		static const Vector4 NormalZ;
		static const Vector4 NormalW;
};

} }
