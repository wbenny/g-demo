#include "Vector4.h"

namespace FW { namespace Math {

const Vector4 Vector4::Zero    = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
const Vector4 Vector4::One     = Vector4(1.0f, 1.0f, 1.0f, 1.0f);
const Vector4 Vector4::NormalX = Vector4(1.0f, 0.0f, 0.0f, 0.0f);
const Vector4 Vector4::NormalY = Vector4(0.0f, 1.0f, 0.0f, 0.0f);
const Vector4 Vector4::NormalZ = Vector4(0.0f, 0.0f, 1.0f, 0.0f);
const Vector4 Vector4::NormalW = Vector4(0.0f, 0.0f, 0.0f, 1.0f);

} }
