#include "Vector2.h"

namespace FW { namespace Math {

const Vector2 Vector2::Zero    = Vector2(0.0f, 0.0f);
const Vector2 Vector2::One     = Vector2(1.0f, 1.0f);
const Vector2 Vector2::NormalX = Vector2(1.0f, 0.0f);
const Vector2 Vector2::NormalY = Vector2(0.0f, 1.0f);

} }
