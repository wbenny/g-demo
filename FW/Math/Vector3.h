#pragma once
#include "../Types.h"
#include "MathBase.h"
#include "Vector2.h"

namespace FW { namespace Math {

class Vector3
{
	public:
		union
		{
			struct { float X, Y, Z; };
			struct { float Cell[3]; };
		};

		Vector3():
			X(0.0f),
			Y(0.0f),
			Z(0.0f)
		{
				
		}

		Vector3(float x, float y, float z):
			X(x),
			Y(y),
			Z(z)
		{
				
		}

		Vector3(float* ptr):
			X(ptr[0]),
			Y(ptr[1]),
			Z(ptr[2])
		{
				
		}

		Vector3(const Vector3& vec):
			X(vec.X),
			Y(vec.Y),
			Z(vec.Z)
		{
			
		}

		void Set(float x, float y, float z)
		{
			X = x;
			Y = y;
			Z = z;
		}

		float Length() const
		{
			return Math::Sqrt(X * X + Y * Y + Z * Z);
		}

		Vector3& Normalize()
		{
			float len = Length();

			if (len)
			{
				len = 1.0f / len;
				X *= len;
				Y *= len;
				Z *= len;
			}

			return *this;
		}

		Vector3 Normalized() const
		{
			Vector3 result = (*this);
			result.Normalize();
			return result;
		}

		Vector3& Negate()
		{
			X = -X;
			Y = -Y;
			Z = -Z;

			return *this;
		}

		Vector3 Negated()
		{
			Vector3 result = (*this);
			result.Negate();
			return result;
		}

		void DebugPrint() const
		{
			printf("(%.3f, %.3f, %.3f)\r\n", X, Y, Z);
		}

		static float Dot(const Vector3& u, const Vector3& v)
		{
			return u.X * v.X + u.Y * v.Y + u.Z * v.Z;
		}

		static float Distance(const Vector3& u, const Vector3& v)
		{
			return Math::Sqrt((u.X - v.X) * (u.X - v.X) + (u.Y - v.Y) * (u.Y - v.Y) + (u.Z - v.Z) * (u.Z - v.Z));
		}

		static Vector3 Cross(const Vector3& u, const Vector3& v)
		{
			return Vector3(u.Y * v.Z - u.Z * v.Y, u.Z * v.X - u.X * v.Z, u.X * v.Y - u.Y * v.X);
		}

		float operator[] (int index) const
		{
			return Cell[index];
		}

		float& operator[] (int index)
		{
			return Cell[index];
		}

		Vector3 operator + (const Vector3& vec) const
		{
			Vector3 result = (*this);
			result.X += vec.X;
			result.Y += vec.Y;
			result.Z += vec.Z;
			return result;
		}

		Vector3 operator - (const Vector3& vec) const
		{
			Vector3 result = (*this);
			result.X -= vec.X;
			result.Y -= vec.Y;
			result.Z -= vec.Z;
			return result;
		}

		Vector3 operator * (const Vector3& vec) const
		{
			Vector3 result = (*this);
			result.X *= vec.X;
			result.Y *= vec.Y;
			result.Z *= vec.Z;
			return result;
		}

		Vector3 operator / (const Vector3& vec) const
		{
			Vector3 result = (*this);
			result.X /= vec.X;
			result.Y /= vec.Y;
			result.Z /= vec.Z;
			return result;
		}

		Vector3 operator + (float v) const
		{
			Vector3 result = (*this);
			result.X += v;
			result.Y += v;
			result.Z += v;
			return result;
		}

		Vector3 operator - (float v) const
		{
			Vector3 result = (*this);
			result.X -= v;
			result.Y -= v;
			result.Z -= v;
			return result;
		}

		Vector3 operator * (float v) const
		{
			Vector3 result = (*this);
			result.X *= v;
			result.Y *= v;
			result.Z *= v;
			return result;
		}

		Vector3 operator / (float v) const
		{
			Vector3 result = (*this);
			result.X /= v;
			result.Y /= v;
			result.Z /= v;
			return result;
		}

		friend Vector3 operator + (float v, const Vector3& u)
		{
			return u + v;
		}

		friend Vector3 operator - (float v, const Vector3& u) 
		{
			return u - v;
		}

		friend Vector3 operator * (float v, const Vector3& u)
		{
			return u * v;
		}

		friend Vector3 operator / (float v, const Vector3& u)
		{
			return u / v;
		}

		void operator += (const Vector3& v)
		{
			*this = *this + v;
		}

		void operator -= (const Vector3& v)
		{
			*this = *this - v;
		}
		void operator *= (const Vector3& v)
		{
			*this = *this * v;
		}

		void operator /= (const Vector3& v)
		{
			*this = *this / v;
		}

		void operator += (float v)
		{
			*this = *this + v;
		}

		void operator -= (float v)
		{
			*this = *this - v;
		}

		void operator *= (float v)
		{
			*this = *this * v;
		}

		void operator /= (float v)
		{
			*this = *this / v;
		}

		friend bool operator == (const Vector3& l, const Vector3& r)
		{
			return l.X == r.X && l.Y == r.Y && l.Z == r.Z;
		}

		Vector3 operator - () const
		{
			Vector3 result = *this;

			result.X = -result.X;
			result.Y = -result.Y;
			result.Z = -result.Z;
				
			return result;
		}

		static const Vector3 Zero;
		static const Vector3 One;
		static const Vector3 NormalX;
		static const Vector3 NormalY;
		static const Vector3 NormalZ;
};

} }
