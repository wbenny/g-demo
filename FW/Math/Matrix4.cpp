#include "Matrix4.h"

namespace FW { namespace Math {

const Matrix4 Matrix4::Zero     = Matrix4(0.0f, 0.0f, 0.0f, 0.0f,
										  0.0f, 0.0f, 0.0f, 0.0f,
										  0.0f, 0.0f, 0.0f, 0.0f,
										  0.0f, 0.0f, 0.0f, 0.0f);

const Matrix4 Matrix4::One      = Matrix4(1.0f, 1.0f, 1.0f, 1.0f,
										  1.0f, 1.0f, 1.0f, 1.0f,
									      1.0f, 1.0f, 1.0f, 1.0f,
									      1.0f, 1.0f, 1.0f, 1.0f);

const Matrix4 Matrix4::Identity = Matrix4(1.0f, 0.0f, 0.0f, 0.0f,
									      0.0f, 1.0f, 0.0f, 0.0f,
									      0.0f, 0.0f, 1.0f, 0.0f,
									      0.0f, 0.0f, 0.0f, 1.0f);

} }
