#pragma once
#include "../Memory.h"
#include "../Utils/Swap.h"
#include "../Types.h"
#include "Globals.h"
#include "MathBase.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Matrix3.h"

// References:
// [Matrix::Invert()] (29.11.2011)
// http://stackoverflow.com/questions/1148309/inverting-a-4x4-result
// [Orthographic, Perspective, PerspectiveFieldOfView] (29.11.2011)
// Decompiled XNA Matrix class
// http://nehe.ceske-hry.cz/cl_gl_matice2.php
// highly inspired by http://bartipan.net/vmath/ (24.1.2012)

namespace FW { namespace Math {

class Matrix4
{
	public:
		union
		{
			struct 
			{
				float M11, M21, M31, M41;
				float M12, M22, M32, M42;
				float M13, M23, M33, M43;
				float M14, M24, M34, M44;
			};

			struct  
			{
				float Cell[16];
			};
		};

		Matrix4() :
			M11(1.0f), M21(0.0f), M31(0.0f), M41(0.0f),
			M12(0.0f), M22(1.0f), M32(0.0f), M42(0.0f),
			M13(0.0f), M23(0.0f), M33(1.0f), M43(0.0f),
			M14(0.0f), M24(0.0f), M34(0.0f), M44(1.0f)
		{

		}

		Matrix4(const Matrix4& m)
		{
			// Memory::Copy(Cell, m.Cell, sizeof(Cell));
			M11 = m.M11; M21 = m.M21; M31 = m.M31; M41 = m.M41;
			M12 = m.M12; M22 = m.M22; M32 = m.M32; M42 = m.M42;
			M13 = m.M13; M23 = m.M23; M33 = m.M33; M43 = m.M43;
			M14 = m.M14; M24 = m.M24; M34 = m.M34; M44 = m.M44;
		}

		Matrix4(float m11, float m21, float m31, float m41,
				float m12, float m22, float m32, float m42,
				float m13, float m23, float m33, float m43,
				float m14, float m24, float m34, float m44) :
			M11(m11), M21(m21), M31(m31), M41(m41),
			M12(m12), M22(m22), M32(m32), M42(m42),
			M13(m13), M23(m23), M33(m33), M43(m43),
			M14(m14), M24(m24), M34(m34), M44(m44)
		{

		}

		Matrix4(float m[4][4])
		{
			M11 = m[0][0]; M21 = m[1][0]; M31 = m[2][0]; M41 = m[3][0];
			M12 = m[0][1]; M22 = m[1][1]; M32 = m[2][1]; M42 = m[3][1];
			M13 = m[0][2]; M23 = m[1][2]; M33 = m[2][2]; M43 = m[3][2];
			M14 = m[0][3]; M24 = m[1][3]; M34 = m[2][3]; M44 = m[3][3];
		}

		Matrix4& operator = (const Matrix4& m)
		{
			// Memory::Copy(Cell, m.Cell, sizeof(Cell));
			M11 = m.M11; M21 = m.M21; M31 = m.M31; M41 = m.M41;
			M12 = m.M12; M22 = m.M22; M32 = m.M32; M42 = m.M42;
			M13 = m.M13; M23 = m.M23; M33 = m.M33; M43 = m.M43;
			M14 = m.M14; M24 = m.M24; M34 = m.M34; M44 = m.M44;

			return *this;
		}

		Matrix4& operator = (float m[4][4])
		{
			M11 = m[0][0]; M21 = m[1][0]; M31 = m[2][0]; M41 = m[3][0];
			M12 = m[0][1]; M22 = m[1][1]; M32 = m[2][1]; M42 = m[3][1];
			M13 = m[0][2]; M23 = m[1][2]; M33 = m[2][2]; M43 = m[3][2];
			M14 = m[0][3]; M24 = m[1][3]; M34 = m[2][3]; M44 = m[3][3];

			return (*this);
		}

		friend bool operator == (const Matrix4& l, const Matrix4& r)
		{
			return !Memory::Compare(l.Cell, r.Cell, sizeof(r.Cell));
		}

		bool operator != (const Matrix4& m) const
		{
			return !!Memory::Compare(Cell, m.Cell, sizeof(Cell));
		}

		Vector3 operator * (const Vector3& v) const
		{
			return Vector3(
				Cell[ 0] * v.X + Cell[ 4] * v.Y + Cell[ 8] * v.Z,
				Cell[ 1] * v.X + Cell[ 5] * v.Y + Cell[ 9] * v.Z,
				Cell[ 2] * v.X + Cell[ 6] * v.Y + Cell[10] * v.Z);
		}

		Vector4 operator * (const Vector4& v) const
		{
			return Vector4(
				Cell[ 0] * v.X + Cell[ 4] * v.Y + Cell[ 8] * v.Z + Cell[12] * v.W,
				Cell[ 1] * v.X + Cell[ 5] * v.Y + Cell[ 9] * v.Z + Cell[13] * v.W,
				Cell[ 2] * v.X + Cell[ 6] * v.Y + Cell[10] * v.Z + Cell[14] * v.W,
				Cell[ 3] * v.X + Cell[ 7] * v.Y + Cell[11] * v.Z + Cell[15] * v.W);
		}

		Matrix4 operator * (const Matrix4& m) const
		{				
// 			Matrix4 result;
// 
// 			for (int i = 0; i < 4; i++)
// 			{
// 				for (int j = 0; j < 4; j++)
// 				{
// 					float n = 0.0f;
// 
// 					for (int k = 0; k < 4; k++)
// 						n += m.GetCell(i, k) * GetCell(k, j);
// 
// 					result.SetCell(i, j, n);
// 				}
// 			}
// 
// 			return result;

			Matrix4 result;

			result.M11 = (((M11 * m.M11) + (M12 * m.M21)) + (M13 * m.M31)) + (M14 * m.M41);
			result.M12 = (((M11 * m.M12) + (M12 * m.M22)) + (M13 * m.M32)) + (M14 * m.M42);
			result.M13 = (((M11 * m.M13) + (M12 * m.M23)) + (M13 * m.M33)) + (M14 * m.M43);
			result.M14 = (((M11 * m.M14) + (M12 * m.M24)) + (M13 * m.M34)) + (M14 * m.M44);
			result.M21 = (((M21 * m.M11) + (M22 * m.M21)) + (M23 * m.M31)) + (M24 * m.M41);
			result.M22 = (((M21 * m.M12) + (M22 * m.M22)) + (M23 * m.M32)) + (M24 * m.M42);
			result.M23 = (((M21 * m.M13) + (M22 * m.M23)) + (M23 * m.M33)) + (M24 * m.M43);
			result.M24 = (((M21 * m.M14) + (M22 * m.M24)) + (M23 * m.M34)) + (M24 * m.M44);
			result.M31 = (((M31 * m.M11) + (M32 * m.M21)) + (M33 * m.M31)) + (M34 * m.M41);
			result.M32 = (((M31 * m.M12) + (M32 * m.M22)) + (M33 * m.M32)) + (M34 * m.M42);
			result.M33 = (((M31 * m.M13) + (M32 * m.M23)) + (M33 * m.M33)) + (M34 * m.M43);
			result.M34 = (((M31 * m.M14) + (M32 * m.M24)) + (M33 * m.M34)) + (M34 * m.M44);
			result.M41 = (((M41 * m.M11) + (M42 * m.M21)) + (M43 * m.M31)) + (M44 * m.M41);
			result.M42 = (((M41 * m.M12) + (M42 * m.M22)) + (M43 * m.M32)) + (M44 * m.M42);
			result.M43 = (((M41 * m.M13) + (M42 * m.M23)) + (M43 * m.M33)) + (M44 * m.M43);
			result.M44 = (((M41 * m.M14) + (M42 * m.M24)) + (M43 * m.M34)) + (M44 * m.M44);

			return result;
		}

		Matrix4& operator *= (const Matrix4& m)
		{
			(*this) = (*this) * m;
			return (*this);
		}

		Matrix4 operator / (float n) const
		{
			Matrix4 result;

			for (int i = 0; i < 16; i++)
				result.Cell[i] = Cell[i] / n;

			return result;
		}

		Matrix4& operator /= (float n)
		{
			(*this) = (*this) / n;
			return (*this);
		}

		void SetIdentity()
		{
			(*this) = Identity;
		}

		Matrix4& Invert()
		{
			Matrix4 inverted;

			inverted.Cell[0]  =  Cell[5] *Cell[10]*Cell[15] -
								 Cell[5] *Cell[11]*Cell[14] -
								 Cell[9] *Cell[6] *Cell[15] +
								 Cell[9] *Cell[7] *Cell[14] +
								 Cell[13]*Cell[6] *Cell[11] -
								 Cell[13]*Cell[7] *Cell[10];

			inverted.Cell[1]  = -Cell[1] *Cell[10]*Cell[15] +
								 Cell[1] *Cell[11]*Cell[14] +
								 Cell[9] *Cell[2] *Cell[15] -
								 Cell[9] *Cell[3] *Cell[14] -
								 Cell[13]*Cell[2] *Cell[11] +
								 Cell[13]*Cell[3] *Cell[10];

			inverted.Cell[2]  =  Cell[1] *Cell[6] *Cell[15] -
								 Cell[1] *Cell[7] *Cell[14] -
								 Cell[5] *Cell[2] *Cell[15] +
								 Cell[5] *Cell[3] *Cell[14] +
								 Cell[13]*Cell[2] *Cell[7]  -
								 Cell[13]*Cell[3] *Cell[6];

			inverted.Cell[3]  = -Cell[1] *Cell[6] *Cell[11] +
								 Cell[1] *Cell[7] *Cell[10] +
								 Cell[5] *Cell[2] *Cell[11] -
								 Cell[5] *Cell[3] *Cell[10] -
								 Cell[9] *Cell[2] *Cell[7]  +
								 Cell[9] *Cell[3] *Cell[6];

			inverted.Cell[4]  = -Cell[4] *Cell[10]*Cell[15] +
								 Cell[4] *Cell[11]*Cell[14] +
								 Cell[8] *Cell[6] *Cell[15] -
								 Cell[8] *Cell[7] *Cell[14] -
								 Cell[12]*Cell[6] *Cell[11] +
								 Cell[12]*Cell[7] *Cell[10];

			inverted.Cell[5]  =  Cell[0] *Cell[10]*Cell[15] -
								 Cell[0] *Cell[11]*Cell[14] -
								 Cell[8] *Cell[2] *Cell[15] +
								 Cell[8] *Cell[3] *Cell[14] +
								 Cell[12]*Cell[2] *Cell[11] -
								 Cell[12]*Cell[3] *Cell[10];

			inverted.Cell[6]  = -Cell[0] *Cell[6] *Cell[15] +
								 Cell[0] *Cell[7] *Cell[14] +
								 Cell[4] *Cell[2] *Cell[15] -
								 Cell[4] *Cell[3] *Cell[14] -
								 Cell[12]*Cell[2] *Cell[7]  +
								 Cell[12]*Cell[3] *Cell[6];

			inverted.Cell[7]  =  Cell[0] *Cell[6] *Cell[11] -
								 Cell[0] *Cell[7] *Cell[10] -
								 Cell[4] *Cell[2] *Cell[11] +
								 Cell[4] *Cell[3] *Cell[10] +
								 Cell[8] *Cell[2] *Cell[7]  -
								 Cell[8] *Cell[3] *Cell[6];

			inverted.Cell[8]  =  Cell[4] *Cell[9] *Cell[15] -
								 Cell[4] *Cell[11]*Cell[13] -
								 Cell[8] *Cell[5] *Cell[15] +
								 Cell[8] *Cell[7] *Cell[13] +
								 Cell[12]*Cell[5] *Cell[11] -
								 Cell[12]*Cell[7] *Cell[9];

			inverted.Cell[9]  = -Cell[0] *Cell[9] *Cell[15] +
								 Cell[0] *Cell[11]*Cell[13] +
								 Cell[8] *Cell[1] *Cell[15] -
								 Cell[8] *Cell[3] *Cell[13] -
								 Cell[12]*Cell[1] *Cell[11] +
								 Cell[12]*Cell[3] *Cell[9];

			inverted.Cell[10] =  Cell[0] *Cell[5] *Cell[15] -
								 Cell[0] *Cell[7] *Cell[13] -
								 Cell[4] *Cell[1] *Cell[15] +
								 Cell[4] *Cell[3] *Cell[13] +
								 Cell[12]*Cell[1] *Cell[7]  -
								 Cell[12]*Cell[3] *Cell[5];

			inverted.Cell[11] = -Cell[0] *Cell[5] *Cell[11] +
								 Cell[0] *Cell[7] *Cell[9]  +
								 Cell[4] *Cell[1] *Cell[11]  -
								 Cell[4] *Cell[3] *Cell[9]   -
								 Cell[8] *Cell[1] *Cell[7]  +
								 Cell[8] *Cell[3] *Cell[5];

			inverted.Cell[12] = -Cell[4] *Cell[9] *Cell[14] +
								 Cell[4] *Cell[10]*Cell[13] +
								 Cell[8] *Cell[5] *Cell[14] -
								 Cell[8] *Cell[6] *Cell[13] -
								 Cell[12]*Cell[5] *Cell[10] +
								 Cell[12]*Cell[6] *Cell[9];

			inverted.Cell[13] =  Cell[0] *Cell[9] *Cell[14] -
								 Cell[0] *Cell[10]*Cell[13] -
								 Cell[8] *Cell[1] *Cell[14] +
								 Cell[8] *Cell[2] *Cell[13] +
								 Cell[12]*Cell[1] *Cell[10] -
								 Cell[12]*Cell[2] *Cell[9];

			inverted.Cell[14] = -Cell[0] *Cell[5] *Cell[14] +
								 Cell[0] *Cell[6] *Cell[13] +
								 Cell[4] *Cell[1] *Cell[14] -
								 Cell[4] *Cell[2] *Cell[13] -
								 Cell[12]*Cell[1] *Cell[6]  +
								 Cell[12]*Cell[2] *Cell[5];

			inverted.Cell[15] =  Cell[0] *Cell[5] *Cell[10] -
								 Cell[0] *Cell[6] *Cell[9]  -
								 Cell[4] *Cell[1] *Cell[10] +
								 Cell[4] *Cell[2] *Cell[9]  +
								 Cell[8] *Cell[1] *Cell[6]  -
								 Cell[8] *Cell[2] *Cell[5];

			float det =	Cell[0]*inverted.Cell[0] +
						Cell[1]*inverted.Cell[4] +
						Cell[2]*inverted.Cell[8] +
						Cell[3]*inverted.Cell[12];

			if (det == 0.0f)
				return (*this);

			det = 1.0f / det;

			for (int i = 0; i < 16; i++)
				Cell[i] = inverted.Cell[i] * det;

			return (*this);
		}

		Matrix4 Inverted() const
		{
			Matrix4 m = (*this);
			m.Invert();
			return m;
		}

		Matrix4& Transpose()
		{
			for (int x = 0, y;  x < 4; x++)
				for (y = x + 1; y < 4; y++)
					Utils::Swap(Cell[y * 4 + x], Cell[x * 4 + y]);

			return *this;
		}

		Matrix4 Transposed() const
		{
			Matrix4 m = (*this);
			m.Transpose();
			return m;
		}

		void RotateX(float angle)
		{
			angle = Math::DegToRad(angle);
			float p, _sin, _cos;

			Math::SinCos(-angle, &_sin, &_cos);

			p = Cell[ 4];
			Cell[ 4] = p * _cos - Cell[ 8] * _sin;
			Cell[ 8] = p * _sin + Cell[ 8] * _cos;

			p = Cell[ 5];
			Cell[ 5] = p * _cos - Cell[ 9] * _sin;
			Cell[ 9] = p * _sin + Cell[ 9] * _cos;

			p = Cell[ 6];
			Cell[ 6] = p * _cos - Cell[10] * _sin;
			Cell[10] = p * _sin + Cell[10] * _cos;
		}

		void RotateY(float angle)
		{
			angle = Math::DegToRad(angle);
			float p, _sin, _cos;

			FW::Math::SinCos(-angle, &_sin, &_cos);

			p = Cell[ 0];
			Cell[ 0] = p * _cos - Cell[ 8] * _sin;
			Cell[ 8] = p * _sin + Cell[ 8] * _cos;

			p = Cell[ 1];
			Cell[ 1] = p * _cos - Cell[ 9] * _sin;
			Cell[ 9] = p * _sin + Cell[ 9] * _cos;

			p = Cell[ 2];
			Cell[ 2] = p * _cos - Cell[10] * _sin;
			Cell[10] = p * _sin + Cell[10] * _cos;
		}

		void RotateZ(float angle)
		{
			angle = Math::DegToRad(angle);
			float p, _sin, _cos;

			FW::Math::SinCos(-angle, &_sin, &_cos);

			p = Cell[ 0];
			Cell[ 0] = p * _cos - Cell[ 4] * _sin;
			Cell[ 4] = p * _sin + Cell[ 4] * _cos;

			p = Cell[ 1];
			Cell[ 1] = p * _cos - Cell[ 5] * _sin;
			Cell[ 5] = p * _sin + Cell[ 5] * _cos;

			p = Cell[ 2];
			Cell[ 2] = p * _cos - Cell[ 6] * _sin;
			Cell[ 6] = p * _sin + Cell[ 6] * _cos;
		}

		void Rotate(float x, float y, float z)
		{
			RotateX(x);
			RotateY(y);
			RotateZ(z);
		}

		void Rotate(const Vector3& v)
		{
			RotateX(v.X);
			RotateY(v.Y);
			RotateZ(v.Z);
		}

		void Translate(float x, float y, float z)
		{
			float tmp[3];

			tmp[0] = Cell[12];
			tmp[1] = Cell[13];
			tmp[2] = Cell[14];

			for (int i = 0; i < 3; i++)
				tmp[i] += (x * Cell[i] + y * Cell[i + 4] + z * Cell[i + 8]);

			Cell[12] = tmp[0];
			Cell[13] = tmp[1];
			Cell[14] = tmp[2];
		}

		void Translate(const Vector3& v)
		{
			Translate(v.X, v.Y, v.Z);
		}

		void Scale(float x, float y, float z)
		{
			float tmp[3];

			tmp[0] = x;
			tmp[1] = y;
			tmp[2] = z;

			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					Cell[i * 4 + j] = Cell[j + 4 * i] * tmp[i];
		}

		void Scale(const Vector3& v)
		{
			Scale(v.X, v.Y, v.Z);
		}

		void Scale(float scale)
		{
			Scale(scale, scale, scale);
		}

		void LookAt(const Vector3& position, const Vector3& target, const Vector3& up)
		{
// 			Vector3 forward, side, upDir;
// 			
// 			forward = target - position;
// 			upDir = up;
// 
// 			forward.Normalize();
// 
// 			// Side = forward x up
// 			side = Vector3::Cross(forward, up);
// 			side.Normalize();
// 
// 			// Recompute up as: up = side x forward
// 			upDir = Vector3::Cross(side, forward);
// 
// 			Cell[ 0] = side.X;
// 			Cell[ 4] = side.Y;
// 			Cell[ 8] = side.Z;
// 
// 			Cell[ 1] = upDir.X;
// 			Cell[ 5] = upDir.Y;
// 			Cell[ 9] = upDir.Z;
// 
// 			Cell[ 2] = -forward.X;
// 			Cell[ 6] = -forward.Y;
// 			Cell[10] = -forward.Z;
// 
// 			(*this) *= Matrix4::CreateTranslation(-position.X, -position.Y, -position.Z);

			Vector3 zaxis = position - target;
			zaxis.Normalize();

			Vector3 xaxis = Vector3::Cross(up, zaxis);
			xaxis.Normalize();

			Vector3 yaxis = Vector3::Cross(zaxis, xaxis);

			Cell[0] = xaxis.X;
			Cell[1] = yaxis.X;
			Cell[2] = zaxis.X;
			Cell[3] = 0.0f;

			Cell[4] = xaxis.Y;
			Cell[5] = yaxis.Y;
			Cell[6] = zaxis.Y;
			Cell[7] = 0.0f;

			Cell[8] = xaxis.Z;
			Cell[9] = yaxis.Z;
			Cell[10] = zaxis.Z;
			Cell[11] = 0.0f;

			Cell[12] = 0.0f;
			Cell[13] = 0.0f;
			Cell[14] = 0.0f;
			Cell[15] = 1.0f;

			Vector3 rotPos = (*this) * position;

			Cell[12] = -rotPos.X;
			Cell[13] = -rotPos.Y;
			Cell[14] = -rotPos.Z;
			Cell[15] = 1.0f;

		}

		void LookAt(float posX, float posY, float posZ,
					float tarX, float tarY, float tarZ,
					float upX, float upY, float upZ)
		{
			LookAt(Vector3(posX, posY, posZ),
				   Vector3(tarX, tarY, tarZ),
				   Vector3(upX, upY, upZ));
		}

		Vector3 GetTranslation() const
		{
			return Vector3(Cell[12], Cell[13], Cell[14]);
		}

		Vector4 GetRow(int row)
		{
			return Vector4(Cell[row * 4 + 0], Cell[row * 4 + 1], Cell[row * 4 + 2], Cell[row * 4 + 3]);
		}

		Vector4 GetColumn(int column)
		{
			return Vector4(Cell[column], Cell[column + 4], Cell[column + 8], Cell[column + 12]);
		}

		float GetCell(int index) const
		{
			return Cell[index];
		}

		float GetCell(int x, int y) const
		{
			return Cell[x * 4 + y];
		}

		void SetCell(int index, float value)
		{
			Cell[index] = value;
		}

		void SetCell(int x, int y, float value)
		{
			Cell[x * 4 + y] = value;
		}

		Matrix3 ToMatrix3() const
		{
			return Matrix3(	M11, M21, M31,
							M12, M22, M32,
							M13, M23, M33);
		}

		void DebugPrint() const
		{
			printf("[%.3f %.3f %.3f %.3f]\r\n",   M11, M21, M31, M41);
			printf("|%.3f %.3f %.3f %.3f|\r\n",   M12, M22, M32, M42);
			printf("|%.3f %.3f %.3f %.3f|\r\n",   M13, M23, M33, M43);
			printf("[%.3f %.3f %.3f %.3f]\r\n\n", M14, M24, M34, M44);
		}

		// static
		static Matrix4 CreateRotationX(float angle)
		{
			Matrix4 result = Identity;
			result.RotateX(angle);
			return result;
		}

		static Matrix4 CreateRotationY(float angle)
		{
			Matrix4 result = Identity;
			result.RotateY(angle);
			return result;
		}

		static Matrix4 CreateRotationZ(float angle)
		{
			Matrix4 result = Identity;
			result.RotateZ(angle);
			return result;
		}

		static Matrix4 CreateRotation(float x, float y, float z)
		{
			Matrix4 result = Identity;
			result.Rotate(x, y, z);
			return result;
		}

		static Matrix4 CreateRotation(const Vector3& v)
		{
			Matrix4 result = Identity;
			result.Rotate(v);
			return result;
		}

		static Matrix4 CreateTranslation(float x, float y, float z)
		{
			Matrix4 result = Identity;
			result.Translate(x, y, z);
			return result;
		}

		static Matrix4 CreateTranslation(const Vector3& v)
		{
			Matrix4 result = Identity;
			result.Translate(v);
			return result;
		}

		static Matrix4 CreateScale(float x, float y, float z)
		{
			Matrix4 result = Identity;
			result.Scale(x, y, z);
			return result;
		}

		static Matrix4 CreateScale(const Vector3& v)
		{
			Matrix4 result = Identity;
			result.Scale(v);
			return result;
		}

		static Matrix4 CreateScale(float scale)
		{
			Matrix4 result = Identity;
			result.Scale(scale, scale, scale);
			return result;
		}

		static Matrix4 CreateLookAt(const Vector3& position, const Vector3& target, const Vector3& up)
		{
			Matrix4 result;
			result.LookAt(position, target, up);
			return result;
		}

		static Matrix4 CreateLookAt(float posX, float posY, float posZ,
								   float tarX, float tarY, float tarZ,
								   float upX, float upY, float upZ)
		{
			Matrix4 result;
			result.LookAt(	Vector3(posX, posY, posZ),
							Vector3(tarX, tarY, tarZ),
							Vector3(upX, upY, upZ));
			return result;
		}

		static Matrix4 CreateInverseLookAt(const Vector3& position, const Vector3& target, const Vector3& up, float planeY)
		{
			return CreateInverseLookAt(position.X, position.Y, position.Z, target.X, target.Y, target.Z, up.X, up.Y, up.Z, planeY);
		}

		static Matrix4 CreateInverseLookAt(float posX, float posY, float posZ,
			float tarX, float tarY, float tarZ,
			float upX, float upY, float upZ, float planeY)
		{
			Matrix4 result;

			result.LookAt(	Vector3(posX, 2.0f*planeY - posY, posZ),
							Vector3(tarX, 2.0f*planeY - tarY, tarZ),
							Vector3(-upX, -upY, -upZ));

			return CreateScale(-1.0f, 1.0f, 1.0f) * result;
		}

		static Matrix4 CreateOrthographic(float width, float height, float nearPlane, float farPlane)
		{
			float	depth  = farPlane - nearPlane,

					a = 2.0f / width,
					b = 2.0f / height,
					c = -2.0f / depth,
					z = -(farPlane + nearPlane) / depth;

			return Matrix4(	a,		0.0f,	0.0f,	0.0f,
							0.0f,	b,		0.0f,	0.0f,
							0.0f,	0.0f,	c,		z,
							0.0f,	0.0f,	0.0f,	1.0f);
		}

		static Matrix4 CreateFrustum(float left, float right, float bottom, float top, float nearVal, float farVal)
		{
			float	a = (right + left) / (right - left),
					b = (top + bottom) / (top - bottom),
					c = -(farVal + nearVal) / (farVal - nearVal),
					d = -(2.0f * farVal * nearVal) / (farVal - nearVal),
					x = (2.0f * nearVal) / (right - left),
					y = (2.0f * nearVal) / (top - bottom);

			
			return Matrix4(	x,		0.0f,	0.0f,	0.0f,
							0.0f,	y,		0.0f,	0.0f,
							a,		b,		c,		-1.0f,
							0.0f,	0.0f,	d,		0.0f);

		}

		static Matrix4 CreatePerspectiveFieldOfView(float fieldOfView, float aspectRatio, float nearPlane, float farPlane)
		{
			float minX, maxX, minY, maxY;

			// This is the FOV / 2 converted to radians
			// and then multiplied by the near value -
			// gives us our max y position at the front
			// of the frustum. The min is just the opposite
			maxY = nearPlane * Tan(fieldOfView * Math::PI / 360.0f);
			minY = -maxY;

			// the min and max x are then the same as
			// the min and max y multiplied by the
			// aspect ratio
			minX = minY * aspectRatio;
			maxX = maxY * aspectRatio;

			return CreateFrustum(
				minX, maxX, minY, maxY, nearPlane, farPlane
			);
		}

		static const Matrix4 Zero;
		static const Matrix4 One;
		static const Matrix4 Identity;
};

} }
