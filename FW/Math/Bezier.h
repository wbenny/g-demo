#pragma once
#include "Vector3.h"
#include "../List.h"

namespace FW { namespace Math {

class Bezier
{
	public:
		Bezier()
		{

		}

		void AddPoint(float p)
		{
			m_Points.Add(p);
		}

		bool RemovePointAt(u32 index)
		{
			if (index < 0 || index >= m_Points.GetCount())
				return false;

			m_Points.RemoveAt(index);
			return true;
		}

		void ClearPoints()
		{
			m_Points.Clear();
		}

		float operator[](int index)
		{
			return m_Points[index];
		}

		float Interpolate(float t)
		{
			if (m_Points.GetCount() == 0)
				return 0.0f;

			if (t < 0.0f) t = 0.0f;
			if (t >= 1.0f) t = 0.999f;

			int at = int(t*float(m_Points.GetCount()-1));
			float step = 1.0f/float(m_Points.GetCount());

			float diff = (t*float(m_Points.GetCount()-1) - at) * step * float(m_Points.GetCount());
			
			float a = m_Points[at];
			float b = m_Points[at+1];

			return a * (1.0f - diff) + b * diff;
		}

		u32 GetCount() const
		{
			return m_Points.GetCount();
		}

	protected:
		List<float> m_Points;

};

} }
