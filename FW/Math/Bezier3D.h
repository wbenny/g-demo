#pragma once
#include "Vector3.h"
#include "../List.h"

namespace FW { namespace Math {

class Bezier3D
{
	public:
		Bezier3D()
		{

		}

		void AddPoint(float x, float y, float z)
		{
			z = -z;	// :/

			m_Points.Add(Vector3(x, y, z));
			m_TempPoints.Add(Vector3());
		}

		void AddPoint(Vector3 point)
		{
			point.Z = -point.Z; // :/

			m_Points.Add(point);
			m_TempPoints.Add(Vector3());
		}

		bool RemovePointAt(u32 index)
		{
			if (index < 0 || index >= m_Points.GetCount())
				return false;

			m_Points.RemoveAt(index);
			m_TempPoints.RemoveAt(index);
			return true;
		}

		void ClearPoints()
		{
			m_Points.Clear();
			m_TempPoints.Clear();
		}

		const Vector3& operator[](int index)
		{
			return m_Points[index];
		}

		Vector3 Interpolate(float t)
		{
			if (m_Points.GetCount() == 0)
				return Vector3();

			if (t < 0.0f) t = 0.0f;
			if (t > 1.0f) t = 1.0f;

			for (u32 i = 0; i < m_Points.GetCount(); ++i)
				m_TempPoints[i] = m_Points[i];

			for (u32 i = 1; i < m_TempPoints.GetCount(); ++i)
			{
				for (u32 j = 0; j < m_TempPoints.GetCount() - i; ++j)
				{
					float x = m_TempPoints[j].X * (1.0f - t) + m_TempPoints[j + 1].X * t;
					float y = m_TempPoints[j].Y * (1.0f - t) + m_TempPoints[j + 1].Y * t;
					float z = m_TempPoints[j].Z * (1.0f - t) + m_TempPoints[j + 1].Z * t;

					m_TempPoints[j] = Vector3(x, y, z);
				}
			}

			return m_TempPoints[0];
		}

		u32 GetCount() const
		{
			return m_Points.GetCount();
		}

	protected:
		List<Vector3> m_Points;
		List<Vector3> m_TempPoints;

};

} }
