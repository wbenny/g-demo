#pragma once
#include "../Types.h"
#include "MathBase.h"

namespace FW { namespace Math {

class Vector2
{
	public:
		union
		{
			struct { float X, Y; };
			struct { float Cell[2]; };
		};

		Vector2():
			X(0),
			Y(0)
		{
			
		}

		Vector2(float x, float y):
			X(x),
			Y(y)
		{
				
		}

		Vector2(float* ptr):
			X(ptr[0]),
			Y(ptr[1])
		{
				
		}

		Vector2(const Vector2& vec):
			X(vec.X),
			Y(vec.Y)
		{
				
		}

		void Set(float x, float y)
		{
			X = x;
			Y = y;
		}

		float Length() const
		{
			return Math::Sqrt(X * X + Y * Y);
		}

		void Normalize()
		{
			float len = Length();

			if (len)
			{
				len = 1.0f / len;
				X *= len;
				Y *= len;
			}
		}

		Vector2 Normalized() const
		{
			Vector2 result = (*this);
			result.Normalize();
			return result;
		}

		void DebugPrint() const
		{
			printf("(%.3f, %.3f)\r\n", X, Y);
		}

		static float Dot(const Vector2& u, const Vector2& v)
		{
			return u.X * v.X + u.Y * v.Y;
		}

		static float Distance(const Vector2& u, const Vector2& v)
		{
			return Math::Sqrt((u.X - v.X) * (u.X - v.X) + (u.Y - v.Y) * (u.Y - v.Y));
		}

		float operator[] (int index) const
		{
			return Cell[index];
		}

		float& operator[] (int index)
		{
			return Cell[index];
		}

		Vector2 operator + (const Vector2& vec) const
		{
			Vector2 result = (*this);
			result.X += vec.X;
			result.Y += vec.Y;
			return result;
		}

		Vector2 operator - (const Vector2& vec) const
		{
			Vector2 result = (*this);
			result.X -= vec.X;
			result.Y -= vec.Y;
			return result;
		}

		Vector2 operator * (const Vector2& vec) const
		{
			Vector2 result = (*this);
			result.X *= vec.X;
			result.Y *= vec.Y;
			return result;
		}

		Vector2 operator / (const Vector2& vec) const
		{
			Vector2 result = (*this);
			result.X /= vec.X;
			result.Y /= vec.Y;
			return result;
		}

		Vector2 operator + (float v) const
		{
			Vector2 result = (*this);
			result.X += v;
			result.Y += v;
			return result;
		}

		Vector2 operator - (float v) const
		{
			Vector2 result = (*this);
			result.X -= v;
			result.Y -= v;
			return result;
		}

		Vector2 operator * (float v) const
		{
			Vector2 result = (*this);
			result.X *= v;
			result.Y *= v;
			return result;
		}

		Vector2 operator / (float v) const
		{
			Vector2 result = (*this);
			result.X /= v;
			result.Y /= v;
			return result;
		}

		friend Vector2 operator + (float v, const Vector2& u)
		{
			return u + v;
		}

		friend Vector2 operator - (float v, const Vector2& u)
		{
			return u - v;
		}

		friend Vector2 operator * (float v, const Vector2& u)
		{
			return u * v;
		}

		friend Vector2 operator / (float v, const Vector2& u)
		{
			return u / v;
		}

		void operator += (const Vector2& v)
		{
			*this = *this + v;
		}
		void operator -= (const Vector2& v)
		{
			*this = *this - v;
		}

		void operator *= (const Vector2& v)
		{
			*this = *this * v;
		}
		void operator /= (const Vector2& v)
		{
			*this = *this / v;
		}

		void operator += (float v)
		{
			*this = *this + v;
		}

		void operator -= (float v)
		{
			*this = *this - v;
		}

		void operator *= (float v)
		{
			*this = *this * v;
		}

		void operator /= (float v)
		{
			*this = *this / v;
		}

		friend bool operator == (const Vector2& l, const Vector2& r)
		{
			return l.X == r.X && l.Y == r.Y;
		}

		Vector2 operator - () const
		{
			Vector2 result = (*this);

			result.X = -result.X;
			result.Y = -result.Y;
				
			return result;
		}

		static const Vector2 Zero;
		static const Vector2 One;
		static const Vector2 NormalX;
		static const Vector2 NormalY;
};

} }
