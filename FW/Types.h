#pragma once
#pragma warning(disable : 4482)		// Umoznuje pouzivat enumerace jako jmenne prostory (namespace).
#pragma warning(disable : 4530)		// Potlacit hlasky o vypnuti vyjimek.

#include <windows.h>
#include <cstdio>

#ifndef NULL
#	define NULL 0
#endif


#ifdef _DEBUG
#	define FW_BREAK_IF(expr)			{ if(expr) DebugBreak(); }
#	define FW_DEBUG_PRINT(f, ...)		{ printf(f, __VA_ARGS__); }
#else
#	define FW_BREAK_IF(expr)
#	define FW_DEBUG_PRINT(f, ...)
#endif

#define FW_CLASS_FWD_DECL1(namespace1, name) namespace namespace1 { class name; }
#define FW_CLASS_FWD_DECL2(namespace1, namespace2, name) namespace namespace1 { namespace namespace2 { class name; } }
#define FW_CLASS_FWD_DECL3(namespace1, namespace2, namespace3, name) namespace namespace1 { namespace namespace2 { namespace namespace3 { class name; } } }
#define FW_CLASS_FWD_DECL4(namespace1, namespace2, namespace3, namespace4, name) namespace namespace1 { namespace namespace2 { namespace namespace3 { namespace namespace4 { class name; } } } }

#define FW_STRUCT_FWD_DECL1(namespace1, name) namespace namespace1 { struct name; }
#define FW_STRUCT_FWD_DECL2(namespace1, namespace2, name) namespace namespace1 { namespace namespace2 { struct name; } }
#define FW_STRUCT_FWD_DECL3(namespace1, namespace2, namespace3, name) namespace namespace1 { namespace namespace2 { namespace namespace3 { struct name; } } }
#define FW_STRUCT_FWD_DECL4(namespace1, namespace2, namespace3, namespace4, name) namespace namespace1 { namespace namespace2 { namespace namespace3 { namespace namespace4 { struct name; } } } }

#define FW_TYPE_FWD_DECL1(namespace1, type, name) namespace namespace1 { type name; }
#define FW_TYPE_FWD_DECL2(namespace1, namespace2, type, name) namespace namespace1 { namespace namespace2 { type name; } }
#define FW_TYPE_FWD_DECL3(namespace1, namespace2, namespace3, type, name) namespace namespace1 { namespace namespace2 { namespace namespace3 { type name; } } }
#define FW_TYPE_FWD_DECL4(namespace1, namespace2, namespace3, namespace4, type, name) namespace namespace1 { namespace namespace2 { namespace namespace3 { namespace namespace4 { type name; } } } }

#define FW_AS_METHOD	template<class T> T* As() { return reinterpret_cast<T*>(this); }\
						template<class T> T& AsReference() { return reinterpret_cast<T&>(*this); }

namespace FW {

typedef unsigned __int8		u8;
typedef unsigned __int16	u16;
typedef unsigned __int32	u32;
typedef unsigned __int64	u64;

typedef signed __int8		s8;
typedef signed __int16		s16;
typedef signed __int32		s32;
typedef signed __int64		s64;

typedef u8					byte_t;
typedef u16					word_t;
typedef u32					dword_t;
typedef u64					qword_t;

}
