/**
 * @file	fw/Assert.h
 *
 * @brief	Declares the FW_ASSERT macro.
 */

#pragma once
#include <windows.h>

// Assert macro.
// Using:
// FW_ASSERT(index != 0);
// or
// FW_ASSERT(index != 0, "Index is zero!");
#ifdef _DEBUG
#	define FW_ASSERT(expr, ...)	::FW::__Assert(expr, __FILE__, __LINE__, __FUNCTION__, (#expr), __VA_ARGS__)
#else
#	define FW_ASSERT(expr, ...)
#endif

namespace FW {
	
	/**
	 * @fn	void __Assert(int expression, const char* filename, int line, const char* fn,
	 * 		const char* expr, const char* msg = NULL)
	 *
	 * @brief	Assert.
	 *
	 * @date	30.9.2011
	 *
	 * @param	expression	The expression.
	 * @param	filename  	Filename of the file.
	 * @param	line	  	Line number.
	 * @param	fn		  	The function.
	 * @param	expr	  	The expression.
	 * @param	msg		  	(optional) The message.
	 */
	void __Assert(int expression, const char* filename, int line, const char* fn, const char* expr, const char* msg = NULL);

} // namespace
