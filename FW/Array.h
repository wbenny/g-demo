/**
 * @file	fw/Array.h
 *
 * @brief	Declares the Array class.
 */

#pragma once
#include "Assert.h"
#include "Types.h"

namespace FW {

/**
 * @class	Array<T>
 *
 * @brief	Array class. Contains a specified count of
 * 			specified elements. Not dynamic.
 *
 * @date	30.9.2011
 */
template<class T>
class Array
{
	public:
		/**
		 * @fn	inline explicit Array::Array()
		 *
		 * @brief	Default constructor.
		 *
		 * @date	30.9.2011
		 */
		inline explicit Array() :
			m_Array(NULL),
			m_Count(0)
		{
		
		}

		/**
		 * @fn	inline explicit Array::Array(int count)
		 *
		 * @brief	Constructor.
		 *
		 * @date	2.10.2011
		 *
		 * @param	count	Number of elements.
		 */
		inline explicit Array(int count) :
			m_Array(NULL),
			m_Count(0)
		{
			Renew(count);
		}

		/**
		 * @fn	inline Array::Array(const Array<T>& array)
		 *
		 * @brief	Copy constructor.
		 *
		 * @date	30.9.2011
		 *
		 * @param	array	The array to copy from.
		 */
		inline Array(const Array<T>& array) :
			m_Array(NULL),
			m_Count(0)
		{
			Renew(array.m_Count);
			FromArray(array.m_Array, array.m_Count);
		}

		/**
		 * @fn	inline Array::~Array()
		 *
		 * @brief	Destructor.
		 *
		 * @date	30.9.2011
		 */
		inline ~Array()
		{
			if (m_Count)
				delete[] m_Array;
		}

		/**
		 * @fn	inline Array<T>& Array::operator= (const Array<T>& array)
		 *
		 * @brief	Assignment operator.
		 *
		 * @date	30.9.2011
		 *
		 * @param	array	The array to copy from.
		 *
		 * @return	A reference to this object.
		 */
		inline Array<T>& operator = (const Array<T>& array)
		{
			m_Array = NULL;
			m_Count = 0;

			Renew(array.m_Count);
			FromArray(array.m_Array, array.m_Count);
		}

		/**
		 * @fn	void Array::Renew(int newSize)
		 *
		 * @brief	Renews the array size.
		 *
		 * @date	30.9.2011
		 *
		 * @param	newSize	Size of the newly created array.
		 */
		void Renew(int newSize)
		{
			FW_ASSERT(newSize >= 0, "Negative size of array");

			if (m_Array)
				delete[] m_Array;

			if (newSize > 0)
			{
				m_Array = new T[newSize];
			}

			m_Count = newSize;
		}

		/**
		 * @fn	inline Array<T>& Array::Empty()
		 *
		 * @brief	Clears the array and resize to 0.
		 *
		 * @date	30.9.2011
		 *
		 * @return	This.
		 */
		inline Array<T>& Empty()
		{
			Renew(0);
			return *this;
		}

		/**
		 * @fn	inline T& Array::At(int index)
		 *
		 * @brief	Gets the element at the specified index.
		 *
		 * @date	30.9.2011
		 *
		 * @param	index	Zero-based index of the.
		 *
		 * @return	The element.
		 */
		inline T& At(int index)
		{
			FW_ASSERT(index < m_Count, "Out of range");
			FW_ASSERT(index >= 0,		"Negative index");

			return m_Array[index];
		}

 		/**
 		 * @fn	inline const T& Array::At(int index) const
 		 *
		 * @brief	Gets the element at the specified index.
		 *
		 * @date	30.9.2011
		 *
		 * @param	index	Zero-based index of the.
		 *
		 * @return	The element.
 		 */
 		inline const T& At(int index) const
		{ 
			FW_ASSERT(index < m_Count, "Out of range");
			FW_ASSERT(index >= 0,		"Negative index");

			return m_Array[index];
		}

		/**
		 * @fn	inline T& Array::operator[] (int index)
		 *
		 * @brief	Gets the element at the specified index.
		 *
		 * @date	30.9.2011
		 *
		 * @param	index	Zero-based index of the.
		 *
		 * @return	The element.
		 */
		inline T& operator[] (int index)
		{
			return At(index);
		}

		/**
		 * @fn	inline const T& Array::operator[] (int index) const
		 *
		 * @brief	Gets the element at the specified index.
		 *
		 * @date	30.9.2011
		 *
		 * @param	index	Zero-based index of the.
		 *
		 * @return	The element.
		 */
		inline const T& operator[] (int index) const
		{
			return At(index);
		}

		/**
		 * @fn	inline T* Array::Data()
		 *
		 * @brief	Gets the pointer to data.
		 *
		 * @date	30.9.2011
		 *
		 * @return	NULL if it fails, else pointer to the first element.
		 */
		inline T* Data()
		{
			return m_Array;
		}

		/**
		 * @fn	inline T* Array::Data()
		 *
		 * @brief	Gets the pointer to data.
		 *
		 * @date	30.9.2011
		 *
		 * @return	NULL if it fails, else pointer to the first element.
		 */
		inline const T* Data() const
		{
			return m_Array;
		}

		/**
		 * @fn	Array<T>& Array::FromArray(const T* data, u32 count, u32 index = 0)
		 *
		 * @brief	Fills this array with elements from another array.
		 *
		 * @date	1.10.2011
		 *
		 * @param	data 	Pointer to the element of given index in array.
		 * @param	count	Number of elements to copy.
		 * @param	index	(optional) zero-based index of the first element to copy from.
		 *
		 * @return	This array.
		 */
		Array<T>& FromArray(const T* data, u32 count, u32 index = 0)
		{
			FW_ASSERT(count <= m_Count, "Buffer overflow");

			if (!count)
				return *this;

			if (!data)
			{
				FW_ASSERT(data != NULL, "NULL input data pointer");
				return *this;
			}

//			memcpy_s(m_pArray, m_iCount, data, count);

			for (u32 i = index; i < count; i++)
				m_Array[i] = data[i];

			return *this;
		}

		/**
		 * @fn	void Array::ToArray(T* data, u32 count)
		 *
		 * @brief	Fills specified array with data from this array.
		 *
		 * @date	30.9.2011
		 *
		 * @param [out]	data		Pointer to the data buffer.
		 * @param	count			Number of elements to copy.
		 */
		void ToArray(T* data, u32 count)
		{
			FW_ASSERT(data != NULL, "NULL input data pointer");

			if (!count)
				return;

// 			memcpy_s(data, count, m_pArray, m_iCount);

			for (int i = 0; i < count; i++)
				data[i] = m_Array[i];
		}

		/**
		 * @fn	inline void Array::Modify(u32 index, const T& val)
		 * 
		 * @brief	Modifies data on given with specified value.
		 *
		 * @date	30.9.2011
		 *
		 * @param	index	Zero-based index.
		 * @param	val  	The value.
		 */
		inline void Modify(u32 index, const T& val)
		{
			FW_ASSERT(index < m_Count,	"Buffer overflow");
			FW_ASSERT(index >= 0,		"Buffer underflow");

			m_Array[index] = val;
		}

		/**
		 * @fn	inline u32 Array::Count() const
		 *
		 * @brief	Gets the count of elements in array.
		 *
		 * @date	30.9.2011
		 *
		 * @return	.
		 */
		inline u32 Count() const
		{
			return m_Count;
		}

	private:
		// Members.
		T* m_Array;		///< Pointer to the first element in array.
		u32 m_Count;	///< Total count of elements in array.
};

} // namespace
