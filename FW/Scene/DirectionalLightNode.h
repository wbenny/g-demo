#pragma once
#include "LightNode.h"
#include "CameraNode.h"
#include "../Math/Vector3.h"
#include "../Math/Projection.h"

FW_CLASS_FWD_DECL2(FW, Render, Renderer);

namespace FW { namespace Scene {
	
class DirectionalLightNode : public LightNode
{
	friend class Render::Renderer;
	friend class CameraNode;

	public:
		DirectionalLightNode(const Math::Vector3& color, float intensity) :
			LightNode(LightType::Directional, color, intensity),
			m_CastShadow(false)
		{
			const u16 shadowMapWidth  = 1024*8;
			const u16 shadowMapHeight = 1024*8;
			
			m_LightCamera = new CameraNode();
			m_LightCamera->m_AssociatedLight = this;
			m_LightCamera->SetCameraType(CameraType::Shadow);
			m_LightCamera->GetFramebuffer()->Done();	// uninit fbo and init it again
			m_LightCamera->GetFramebuffer()->Init(Render::FramebufferPreset::DepthOnly, shadowMapWidth, shadowMapHeight);
			m_LightCamera->GetFramebuffer()->SetRenderState(new Render::RenderState(
					shadowMapWidth, shadowMapHeight,
					Math::Vector3(1.0f, 1.0f, 1.0f),
					Render::RenderModes::RenderModeBase::GetInstance(),
					Render::FaceCullingMode::Back
				));

			m_LightCamera->SetProjection(Math::Projection::CreatePerspectiveFieldOfView(
				45.0f,
				static_cast<float>(shadowMapWidth)/static_cast<float>(shadowMapHeight),
				100.0f,
				1800.0f
			));

// 			m_FSQHBlur = new Render::FullScreenQuad();
// 			m_FSQVBlur = new Render::FullScreenQuad();
// 			m_PPHBlur  = new Render::Postprocess::HorizontalBlur();
// 			m_PPVBlur  = new Render::Postprocess::VerticalBlur();
// 
// 			m_FSQHBlur->GetFramebuffer()->Done();
// 			m_FSQHBlur->GetFramebuffer()->Init(Render::FramebufferPreset::FloatBGR, shadowMapWidth, shadowMapHeight);
// 			m_FSQHBlur->GetFramebuffer()->SetRenderState(new Render::RenderState(shadowMapWidth, shadowMapHeight));
// 
// 			m_FSQVBlur->GetFramebuffer()->Done();
// 			m_FSQVBlur->GetFramebuffer()->Init(Render::FramebufferPreset::FloatBGR | Render::TextureBit::LinearFilteringWithMipmap, shadowMapWidth, shadowMapHeight);
// 			m_FSQVBlur->GetFramebuffer()->SetRenderState(new Render::RenderState(shadowMapWidth, shadowMapHeight));
		}

		const Math::Vector3& GetTarget() const { return m_Target; }
		void SetTarget(const Math::Vector3& target) { m_Target = target; }

		bool CastsShadow() const { return m_CastShadow; }
		void EnableShadowCasting(bool castShadow = true) { m_CastShadow = castShadow; }

// 		const Render::Texture* GetShadowMap() const { return m_FSQVBlur->GetOutputTexture(); }
// 		Render::Texture* GetShadowMap() { return m_FSQVBlur->GetOutputTexture(); }

		const Render::Texture* GetShadowMap() const { return m_LightCamera->GetFramebuffer()->GetTexture(); }
		Render::Texture* GetShadowMap() { return m_LightCamera->GetFramebuffer()->GetTexture(); }

		const Math::Vector3& GetLocation() const
		{
			m_Location = (GetWorldMatrix().GetTranslation().Normalized() - m_Target.Normalized()).Normalized();
			return m_Location;
		}

		void SetParent(Node* newParent);

		const CameraNode* GetLightCamera() const { return m_LightCamera; }
		CameraNode* GetLightCamera() { return m_LightCamera; }

	protected:
// 		Render::Postprocess::VerticalBlur* m_PPVBlur;
// 		Render::Postprocess::HorizontalBlur* m_PPHBlur;
// 		Render::FullScreenQuad* m_FSQVBlur, *m_FSQHBlur;

		bool			m_CastShadow;

		Math::Vector3	m_Target;

		Ptr<CameraNode>	m_LightCamera;

		mutable Math::Vector3 m_Location;
};

} }
