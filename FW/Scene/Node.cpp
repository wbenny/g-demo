#include "Node.h"
#include "NodeManager.h"

namespace FW { namespace Scene {

Node::~Node()
{
	if (m_Name.Length() > 0)
		NodeManager::GetInstance().Remove(m_Name);
}

void Node::SetName(const String& name)
{
	m_Name = name;

	if (m_Name.Length() > 0)
		NodeManager::GetInstance().Add(m_Name, this);
}

void Node::SetParent(Node* newParent)
{
	Ref();

	if (m_Parent)
	{
		m_Parent->m_Children.Remove(this);
	}

	m_Parent = newParent;

	if (m_Parent)
	{
		m_Parent->m_Children.Add(this);
	}

	MakeWorldMatrixDirty();

	Unref();
}

const Node* Node::GetRootNode() const
{
	const Node* result = this;

	while (true)
	{
		if (result->GetNodeType() == NodeType::Root)
			return result;

		if (result->GetParent() == NULL)
			return NULL;

		result = result->GetParent();
	}
}

Node* Node::GetRootNode()
{
	Node* result = this;

	while (true)
	{
		if (result->GetNodeType() == NodeType::Root)
			return result;

		if (result->GetParent() == NULL)
			return NULL;

		result = result->GetParent();
	}
}


void Node::SetMatrix(const Math::Matrix4& matrix)
{
	m_Matrix = matrix;

	MakeWorldMatrixDirty();
}

const Math::Matrix4& Node::GetWorldMatrix() const
{
	if (m_DirtyWorldMatrix)
	{
		if (m_Parent)
		{
			m_WorldMatrix = m_Parent->GetWorldMatrix() * m_Matrix;
		}
		else
		{
			m_WorldMatrix = m_Matrix;
		}

		m_DirtyWorldMatrix = false;
	}

	return m_WorldMatrix;
}

const Math::Matrix3& Node::GetNormalMatrix() const
{
	//if (m_DirtyWorldMatrix)
		m_NormalMatrix = GetWorldMatrix().Inverted().Transposed().ToMatrix3();

	return m_NormalMatrix;
}

const Math::Matrix4& Node::GetInverseMatrix() const
{
	if (m_DirtyWorldMatrix)
		m_InverseMatrix = GetWorldMatrix().Inverted();

	return m_InverseMatrix;
}

void Node::MakeWorldMatrixDirty()
{
	m_DirtyWorldMatrix = true;

	for (int i = 0; i < GetChildCount(); i++)
		GetChildren(i)->MakeWorldMatrixDirty();
}

} }
