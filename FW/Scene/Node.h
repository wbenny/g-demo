#pragma once
#include "../List.h"
#include "../Math/Vector3.h"
#include "../Math/Matrix4.h"
#include "../Math/Matrix3.h"
#include "../Math/BoundingBox.h"
#include "../RefCounter.h"
#include "../String.h"
#include "../PtrList.h"
#include "../Ptr.h"

FW_STRUCT_FWD_DECL2(FW, Render, ScenePassContext);

namespace FW { namespace Scene {

enum NodeType
{
	None,
	Root,
	Camera,
	Light,
	Mesh
};

class Node : public RefCounter
{
	public:
		Node() :
			m_NodeType(NodeType::None),
			m_Parent(NULL),
			m_DirtyWorldMatrix(true),
			m_Visible(true)
		{

		}

		Node(NodeType type) :
			m_NodeType(type),
			m_Parent(NULL),
			m_DirtyWorldMatrix(true),
			m_Visible(true)
		{

		}

		virtual ~Node();

		const String& GetName()				const	{ return m_Name; }
		void SetName(const String& name);

		bool IsVisible()					const	{ return m_Visible; }
		bool IsDirty()						const	{ return m_DirtyWorldMatrix; }

		NodeType GetNodeType()				const	{ return m_NodeType; }

		bool HasParent()					const	{ return !!m_Parent; }
		
		Node* GetParent()							{ return m_Parent; }
		const Node* GetParent()				const	{ return m_Parent; }
		virtual void SetParent(Node* newParent);

		virtual void Add(Node* node)				{ node->SetParent(this); }

		int GetChildCount()					const	{ return m_Children.GetCount(); }
		Node* GetChildren(int index)				{ return m_Children[index]; }
		const Node* GetChildren(int index)	const	{ return m_Children[index]; }

		const Node* GetRootNode() const;
		Node* GetRootNode();
	
		virtual void Draw(Render::ScenePassContext& context) { return; }
		virtual void Update(float time, float delta){ return; }

		void Show()									{ m_Visible = true; }
		void Hide()									{ m_Visible = false; }

		const Math::Matrix4& GetMatrix()	const	{ return m_Matrix; }
		void SetMatrix(const Math::Matrix4& matrix);

		const Math::Matrix4& GetWorldMatrix() const;
		const Math::Matrix3& GetNormalMatrix() const;
		const Math::Matrix4& GetInverseMatrix() const;

		const Math::BoundingBox& GetBoundingBox() const { return Math::BoundingBox::Empty; }

		FW_AS_METHOD

	protected:
		String					m_Name;

		PtrList<Node>			m_Children;

		Node*					m_Parent;
		
		NodeType				m_NodeType;

		Math::Matrix4			m_Matrix;

		bool					m_Visible;
		
		mutable Math::Matrix3	m_NormalMatrix;

		mutable Math::Matrix4	m_WorldMatrix,
								m_InverseMatrix;

		mutable bool			m_DirtyWorldMatrix;

		void MakeWorldMatrixDirty();
};

} }
