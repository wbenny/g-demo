#include "CameraNode.h"
#include "RootNode.h"
#include "DirectionalLightNode.h"

namespace FW { namespace Scene {

void CameraNode::SetParent(Node* newParent)
{
	Node::SetParent(newParent);

	RootNode* rootNode = reinterpret_cast<RootNode*>(newParent->GetRootNode());

	rootNode->m_Cameras.Add(this);
}

void CameraNode::UpdateLightCamera()
{
	if (m_AssociatedLight)
	{
		SetMatrix(
			Math::Matrix4::CreateLookAt(
				m_AssociatedLight->GetWorldMatrix().GetTranslation(),
				m_AssociatedLight->m_Target,
				Math::Vector3::NormalY
			)
		);
	}
}

} }
