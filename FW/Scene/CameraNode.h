#pragma once
#include "Node.h"
#include "../Math/Matrix4.h"
#include "../Math/Vector3.h"
#include "../Math/Projection.h"
#include "../Render/RenderTarget.h"
#include "../Ptr.h"

FW_CLASS_FWD_DECL2(FW, Render, Renderer)

namespace FW { namespace Scene {

enum CameraType
{
	Main,
	Shadow,
	Reflect
};

class DirectionalLightNode;

class CameraNode : public Node
{
	friend class DirectionalLightNode;
	friend class Render::Renderer;

	public:
		CameraNode() :
			Node(NodeType::Camera),
			m_CameraType(CameraType::Main),
			m_AssociatedLight(NULL),
			m_EnableLighting(true)
		{
			m_RenderTarget = new Render::RenderTarget(Render::FramebufferPreset::FloatBGRA | Render::TextureBit::LinearFilteringWithMipmap,
				FW_VIEWPORT_WIDTH,
				FW_VIEWPORT_HEIGHT
			);
		}

		CameraNode(const Math::Projection& projection) :
			Node(NodeType::Camera),
			m_CameraType(CameraType::Main),
			m_Projection(projection),
			m_AssociatedLight(NULL),
			m_EnableLighting(true)
		{
			m_RenderTarget = new Render::RenderTarget(Render::FramebufferPreset::FloatBGRA | Render::TextureBit::LinearFilteringWithMipmap,
				FW_VIEWPORT_WIDTH,
				FW_VIEWPORT_HEIGHT
			);
		}

		CameraType GetCameraType() const { return m_CameraType; }
		void SetCameraType(CameraType cameraType) { m_CameraType = cameraType; }

		const Math::Matrix4& GetProjectionMatrix() const { return m_Projection.Matrix; }
		const Math::Vector2& GetNearFar() const { return m_Projection.NearFar; }

		const Math::Vector3& GetTarget() const { return m_Target; }
		void SetTarget(const Math::Vector3& m) { m_Target = m; }

		void SetProjection(const Math::Projection& projection) { m_Projection = projection; }

		void SetParent(Node* newParent);

		const Render::RenderTarget* GetFramebuffer() const { return m_RenderTarget; }
		Render::RenderTarget* GetFramebuffer() { return m_RenderTarget; }
		void SetFramebuffer(Render::RenderTarget* renderTarget) { m_RenderTarget = renderTarget; }

		bool IsLightingEnabled() const { return m_EnableLighting; }
		void EnableLighting(bool enable = true) { m_EnableLighting = enable; }
		void DisableLighting() { EnableLighting(false); }

	protected:
		void UpdateLightCamera();

		bool						m_EnableLighting;

		CameraType					m_CameraType;

		Ptr<Render::RenderTarget>	m_RenderTarget;

		DirectionalLightNode*		m_AssociatedLight;

		Math::Projection			m_Projection;

		Math::Vector3				m_Target;
};

} }
