#pragma once
#include "LightNode.h"
#include "../Math/Vector3.h"

namespace FW { namespace Scene {
	
class PointLightNode : public LightNode
{
	public:
		PointLightNode(const Math::Vector3& color, float intensity) :
			LightNode(LightType::Point, color, intensity),
			m_FallOffDistance(2000.0f)
		{

		}

		float GetFallOffDistance() const { return m_FallOffDistance; }
		void SetFallOffDistance(float fod) { m_FallOffDistance = fod; }

		const Math::Vector3& GetLocation() const
		{
			m_Location = GetWorldMatrix().GetTranslation();
			return m_Location;
		}

	protected:
		float			m_FallOffDistance;

		mutable Math::Vector3	m_Location;
};

} }
