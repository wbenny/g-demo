#pragma once
#include "../Node.h"
#include "../../Math/Matrix4.h"
#include "../../Math/Vector3.h"

namespace FW { namespace Scene {

enum CameraType
{
	Normal,
	Shadow,
	Reflect
};

class CameraNode : public Node
{
	public:
		CameraNode() :
			Node(NodeType::Camera)
			m_CameraType(CameraType::Normal)
		{

		}

		CameraNode(const Math::Projection& projection) :
			Node(NodeType::Camera),
			m_CameraType(CameraType::Normal),
			m_Projection(projection)
		{

		}

		const Math::Matrix4& GetProjectionMatrix() const { return m_Projection.Matrix; }
		const Math::Vector2& GetNearFar() const { return m_Projection.NearFar; }

		const Math::Vector3& GetTarget() const { return m_Target; }
		void SetTarget(const Math::Vector3& m) { m_Target = m; }

		void SetProjection(const Math::Projection& projection) { m_Projection = projection; }

	protected:
		CameraType			m_CameraType;

		Math::Projection	m_Projection;
		Math::Vector3		m_Target;
};

} }
