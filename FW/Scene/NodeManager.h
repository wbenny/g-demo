#pragma once
#include "Node.h"
#include "../Singleton.h"
#include "../Map.h"

namespace FW { namespace Scene {

class NodeManager : public Singleton<NodeManager>, public Map<String, Ptr<Node> >
{
	friend class Singleton<NodeManager>;

	public:
		Node* GetNode(const String& name)
		{
			return GetMappedElement(name);
		}

	private:
		NodeManager() {}
};

} }
