#include "RootNode.h"

namespace FW { namespace Scene {

void RootNode::Add(Node* node)
{
	Node::Add(node);

	AddToMeshList(node);
}

void RootNode::AddToMeshList(Node* node)
{
	if(node->IsVisible() && node->GetNodeType() == NodeType::Mesh)
	{
		m_Meshes.Add(node);
	}

	if (node->GetChildCount())
	{
		u32 count = node->GetChildCount();

		while (count--)
		{
			AddToMeshList(node->GetChildren(count));
		}
	}
}

const CameraNode* RootNode::GetMainCamera() const
{
	for (u32 i = 0; i < m_Cameras.GetCount(); i++)
	{
		if (m_Cameras[i]->GetCameraType() == CameraType::Main)
			return m_Cameras[i];
	}

	return NULL;
}

CameraNode* RootNode::GetMainCamera()
{
	for (u32 i = 0; i < m_Cameras.GetCount(); i++)
	{
		if (m_Cameras[i]->GetCameraType() == CameraType::Main)
			return m_Cameras[i];
	}

	return NULL;
}

} }
