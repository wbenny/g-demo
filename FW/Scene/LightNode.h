#pragma once
#include "../Math/Vector3.h"
#include "Node.h"
#include "RootNode.h"

namespace FW { namespace Scene {

enum LightType
{
	Empty       = 0,
	Ambient     = (1 << 0),
	Directional = (1 << 1),
	Point       = (1 << 2),
	Spot		= (1 << 3)
};

class LightNode : public Node
{
	public:
		LightNode(LightType type) :
			Node(NodeType::Light),
			m_LightType(type)
		{

		}

		LightNode(LightType type, const Math::Vector3& color, float intensity) :
			Node(NodeType::Light),
			m_Color(color*intensity),
			m_LightType(type)
		{

		}

		const Math::Vector3& GetColor() const { return m_Color; }
		void SetColor(const Math::Vector3& color) { m_Color = color; }

		LightType GetLightType() const { return m_LightType; }

		virtual const Math::Vector3& GetLocation() const { return Math::Vector3::Zero; }

		virtual float GetFallOffDistance() const { return 0.0f; }
		virtual void SetFallOffDistance(float fod) { }

		virtual void SetParent(Node* newParent);

	protected:
		LightNode() :
			Node(NodeType::Light),
			m_LightType(LightType::Empty)
		{

		}

		Math::Vector3	m_Color;

		LightType		m_LightType;
};

} }
