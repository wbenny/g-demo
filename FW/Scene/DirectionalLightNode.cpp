#include "DirectionalLightNode.h"

namespace FW { namespace Scene {

void DirectionalLightNode::SetParent(Node* newParent)
{
	LightNode::SetParent(newParent);

	RootNode* rootNode = reinterpret_cast<RootNode*>(newParent->GetRootNode());

	rootNode->m_Cameras.Add(m_LightCamera);
}

} }
