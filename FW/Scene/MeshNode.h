#pragma once
#include "Node.h"
#include "../Math/Matrix4.h"
#include "../Math/BoundingBox.h"
#include "../GL/GL.h"
#include "../Render/BufferObject.h"
#include "../Render/Shader.h"
#include "../Render/Geometry.h"
#include "../Render/RenderState.h"
#include "../Types.h"

FW_CLASS_FWD_DECL2(FW, Materials, MaterialBase)

namespace FW { namespace Scene {

class MeshNode : public Node
{
	public:
		MeshNode();
		~MeshNode();

		const Materials::MaterialBase* GetMaterial() const;
		Materials::MaterialBase* GetMaterial();
		void SetMaterial(Materials::MaterialBase* material);

		Render::DepthTestMode GetDepthTestMode() { return m_DepthTestMode; };
		void SetDepthTestMode(Render::DepthTestMode depthTestMode) { m_DepthTestMode = depthTestMode; }

		Render::DrawMode GetDrawMode() const { return m_DrawMode; }
		void SetDrawMode(Render::DrawMode meshType) { m_DrawMode = meshType; }

		const Math::BoundingBox& GetBoundingBox() const { return m_Geometry->GetBoundingBox(); }

		const Render::Geometry* GetGeometry() const;
		Render::Geometry* GetGeometry();
		void SetGeometry(Render::Geometry* geometry);

		bool CastsShadow() const { return m_CastShadow; }
		void EnableShadowCasting(bool castShadow = true) { m_CastShadow = castShadow; }
		void DisableShadowCasting() { EnableShadowCasting(false); }

		bool ReceivesShadow() const { return m_ReceiveShadow; }
		void EnableShadowReceiving(bool receiveShadow = true) { m_ReceiveShadow = receiveShadow; }
		void DisableShadowReceiving() { EnableShadowReceiving(false); }

		bool IsReflective() const { return m_Reflective; }
		void EnableReflection(bool reflect = true) { m_Reflective = reflect; }
		void DisableReflection() { EnableReflection(false); }

	protected:
		Ptr<Render::Geometry>		m_Geometry;

		Ptr<Materials::MaterialBase>	m_Material;

		Render::DrawMode			m_DrawMode;

		Render::DepthTestMode		m_DepthTestMode;

		bool						m_Dynamic,
									m_CastShadow,
									m_ReceiveShadow,
									m_Reflective;
};

} }
