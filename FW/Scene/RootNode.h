#pragma once
#include "Node.h"
#include "LightNode.h"
#include "CameraNode.h"
#include "MeshNode.h"
#include "../Types.h"

FW_CLASS_FWD_DECL2(FW, Render, Renderer)
FW_CLASS_FWD_DECL2(FW, Render, Texture)

namespace FW { namespace Scene {

class DirectionalLightNode;

class RootNode : public Node
{
	// Kamaradime s kamerou, svetly a rendererem, takze nam mohou sahat do privatnich partii!
	friend class CameraNode;
	friend class LightNode;
	friend class DirectionalLightNode;
	friend class Render::Renderer;

	public:
		RootNode() : Node(NodeType::Root) { /*SetName("ROOT");*/ }

		void Add(Node* node);

		const CameraNode* GetMainCamera() const;
		CameraNode* GetMainCamera();

		//Render::Texture* Draw() { return Render::Renderer::GetInstance().Draw(this); }

	protected:
		void AddToMeshList(Node* node);

		List<CameraNode*> m_Cameras;
		List<LightNode*>  m_Lights;
		List<Node*>		  m_Meshes;
		Math::Vector3	  m_AmbientLightColor;

};

} }
