#pragma once
#include "LightNode.h"
#include "../Math/Vector3.h"

namespace FW { namespace Scene {
	
class AmbientLightNode : public LightNode
{
	public:
		AmbientLightNode(const Math::Vector3& color, float intensity) :
			LightNode(LightType::Ambient, color, intensity)
		{

		}
};

} }
