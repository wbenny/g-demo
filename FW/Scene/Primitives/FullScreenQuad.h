#pragma once
#include "../MeshNode.h"
#include "../../Math/Vector3.h"
#include "../../Render/GeometryBuilder.h"
#include "../../Render/VertexFormats/PositionTexture.h"

namespace FW { namespace Scene { namespace Primitives {

class FullScreenQuad : public MeshNode
{
	public:
		FullScreenQuad()
		{
			Render::GeometryBuilder<Render::VertexFormats::PositionTexture> builder;

			Render::VertexFormats::PositionTexture
					BL(Math::Vector3(-1.0f, -1.0f, 0.0f), Math::Vector2(0.0f, 0.0f)),
					BR(Math::Vector3( 1.0f, -1.0f, 0.0f), Math::Vector2(1.0f, 0.0f)),
					TL(Math::Vector3(-1.0f,  1.0f, 0.0f), Math::Vector2(0.0f, 1.0f)),
					TR(Math::Vector3( 1.0f,  1.0f, 0.0f), Math::Vector2(1.0f, 1.0f));

			builder.AddQuad(BR, TR, TL, BL);

			m_Geometry = builder.ToGeometry();
			m_Geometry->UpdateBuffers();
		}
};

} } }
