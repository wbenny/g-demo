#pragma once
#include "../../Content/ResourceLoader.h"
#include "../../Materials/SkyMaterial.h"
#include "../../Math/Matrix4.h"
#include "../CameraNode.h"

namespace FW { namespace Scene { namespace Primitives {

class Skydome : public MeshNode
{
	public:
		Skydome()
		{
			SetGeometry(Content::ResourceLoader<Render::Geometry>::Read("models\\skydome.vnt"));
			SetMaterial(new Materials::SkyMaterial());
			SetMatrix(Math::Matrix4::CreateTranslation(0.0f, -130.0f, 0.0f));
		}

		void Update(float time, float delta)
		{
			GetSkyMaterial()->SetSkydomeHeight(750.0f);
			GetSkyMaterial()->SetTime(time / 10.0f);

			Math::Vector3 skyPosition = m_Camera->GetInverseMatrix().GetTranslation();
			skyPosition.Y = -150.0f;
			SetMatrix(Math::Matrix4::CreateTranslation(skyPosition));

		}

		const Materials::SkyMaterial* GetSkyMaterial() const { return reinterpret_cast<const Materials::SkyMaterial*>(GetMaterial()); }
		Materials::SkyMaterial* GetSkyMaterial() { return reinterpret_cast<Materials::SkyMaterial*>(GetMaterial()); }

		const CameraNode* GetAssignedCamera() const { return m_Camera; }
		CameraNode* GetAssignedCamera() { return m_Camera; }
		void SetAssignedCamera(CameraNode* camera) { m_Camera = camera; }

	protected:
		Ptr<CameraNode> m_Camera;
};

} } }
