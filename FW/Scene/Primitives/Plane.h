#pragma once
#include "../MeshNode.h"
#include "../../Math/Vector3.h"
#include "../../Render/GeometryBuilder.h"
#include "../../Render/VertexFormats/PositionNormalTexture.h"
#include "../../Materials/PhongMaterial.h"

namespace FW { namespace Scene { namespace Primitives {

class Plane : public MeshNode
{
	public:
		Plane(float width = 1.0f, float height = 1.0f)
		{
			width *= 0.5f;
			height *= 0.5f;

			Render::GeometryBuilder<Render::VertexFormats::PositionNormalTexture> builder;

			Render::VertexFormats::PositionNormalTexture
					BL(Math::Vector3(-width, 0.0f, -height), Math::Vector2(0.0f, 0.0f)),
					BR(Math::Vector3( width, 0.0f, -height), Math::Vector2(1.0f, 0.0f)),
					FL(Math::Vector3(-width, 0.0f,  height), Math::Vector2(0.0f, 1.0f)),
					FR(Math::Vector3( width, 0.0f,  height), Math::Vector2(1.0f, 1.0f));

			builder.AddQuad(BL, FL, FR, BR);

			builder.CalculateNormals();

			m_Geometry = builder.ToGeometry();
			m_Geometry->UpdateBuffers();
		}
};

} } }
