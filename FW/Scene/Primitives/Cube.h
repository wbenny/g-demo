#pragma once
#include "../MeshNode.h"
#include "../../Math/Vector3.h"
#include "../../Render/GeometryBuilder.h"
#include "../../Render/VertexFormats/PositionNormalTexture.h"
#include "../../Materials/PhongMaterial.h"

namespace FW { namespace Scene { namespace Primitives {

class Cube : public MeshNode
{
	public:
		Cube(float size = 1.0f)
		{
			size *= 0.5f;

			Render::GeometryBuilder<Render::VertexFormats::PositionNormalTexture> builder;

			Render::VertexFormats::PositionNormalTexture faces[] = {
				// P�edn� st�na
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, -size, size), Math::Vector2(0.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, -size, size), Math::Vector2(1.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, size, size), Math::Vector2(1.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, size, size), Math::Vector2(0.0f, 1.0f)),
				// Zadn� st�na
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, -size, -size), Math::Vector2(1.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, size, -size), Math::Vector2(1.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, size, -size), Math::Vector2(0.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, -size, -size), Math::Vector2(0.0f, 0.0f)),
				// Vrchn� st�na
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, size, -size), Math::Vector2(0.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, size, size), Math::Vector2(0.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, size, size), Math::Vector2(1.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, size, -size), Math::Vector2(1.0f, 1.0f)),
				// Spodn� st�na
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, -size, -size), Math::Vector2(1.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, -size, -size), Math::Vector2(0.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, -size, size), Math::Vector2(0.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, -size, size), Math::Vector2(1.0f, 0.0f)),
				// Prav� st�na
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, -size, -size), Math::Vector2(1.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, size, -size), Math::Vector2(1.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, size, size), Math::Vector2(0.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3( size, -size, size), Math::Vector2(0.0f, 0.0f)),
				// Lev� st�na
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, -size, -size), Math::Vector2(0.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, -size, size), Math::Vector2(1.0f, 0.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, size, size), Math::Vector2(1.0f, 1.0f)),
				Render::VertexFormats::PositionNormalTexture(Math::Vector3(-size, size, -size), Math::Vector2(0.0f, 1.0f))
			};

			for (int i = 0; i < 6; i++)
			{
				builder.AddQuad(faces[0+i*4], faces[1+i*4], faces[2+i*4], faces[3+i*4]);
			}

			builder.CalculateNormals();

			m_Geometry = builder.ToGeometry();
			m_Geometry->UpdateBuffers();
		}
};

} } }
