#pragma once
#include "../MeshNode.h"
#include "../../Math/Vector2.h"
#include "../../Math/Vector3.h"
#include "../../Render/GeometryBuilder.h"
#include "../../Render/VertexFormats/PositionNormalTexture.h"

namespace FW { namespace Scene { namespace Primitives {

class Hemisphere : public MeshNode
{
	public:
		Hemisphere(	float radius = 1.0f,		// Polomer polokoule.
					u32 segmentationXZ = 64,	// Segmentace kruhu.
					u32 segmentationY = 64,		// Segmentace do vysky.
					u32 textureRepeat = 1)		// Opakovani textury.
		{
			Render::GeometryBuilder<Render::VertexFormats::PositionNormalTexture> builder;

			Render::VertexFormats::PositionNormalTexture top(
				Math::Vector3(0.0f, radius, 0.0f),	// Pozice.
				Math::Vector3(0.0f, 1.0f, 0.0f),	// Normala.
				Math::Vector2(
					0.0f,
					1.0f) // TexCoords.
				);

			builder.AddVertex(top);

			for (u32 i = 1; i <= segmentationY; i++)
			{
				float actualRadius = ((static_cast<float>(i) / static_cast<float>(segmentationY)) * radius);

				// Vypocitame Y souradnici pres rovnici koule r^2 = x^2 + y^2 + z^2
				float actualY = Math::Sqrt(radius*radius - actualRadius*actualRadius - 1.0f);

				if (actualRadius>=radius)
					actualY = 0.0f;

				for (float j = 0.0f; j < 2.0f*Math::PI - (0.0001f); j += (2.0f*Math::PI / static_cast<float>(segmentationXZ)))
				{
					Render::VertexFormats::PositionNormalTexture v;

					v.Position = Math::Vector3(Math::Sin(j) * actualRadius, actualY, Math::Cos(j) * actualRadius);
					v.Normal   = v.Position.Normalized();
					v.TexCoord = Math::Vector2(
						((1.0f + (v.Position.X / radius)) / 2.0f) * static_cast<float>(textureRepeat),
						((1.0f + (v.Position.Z / radius)) / 2.0f) * static_cast<float>(textureRepeat)
					);

					builder.AddVertex(v);
				}
			}

			// Spojime posledni pas s vrcholem.
			for (u32 i = 1; i <= segmentationXZ; i++)
			{
				// Pokud spojujeme posledni cast pasu.
				if ((i % segmentationXZ) == 0)
				{
// 					builder.AddFace3(0, i, i + 1 - segmentationXZ);
					builder.AddFace3(i + 1 - segmentationXZ, i, 0);
				}
				else
				{
// 					builder.AddFace3(0, i, i + 1);
					builder.AddFace3(i + 1, i, 0);
				}
			}

			// Spojime vrcholy do predposledniho pasu (posledni pas je spojen s "top").
			for (u32 i = 1; i <= segmentationXZ * (segmentationY - 1); i++)
			{
				// Pokud spojujeme posledni cast pasu.
				if ((i % segmentationXZ) == 0)
				{
// 					builder.AddFace4(i, i + segmentationXZ, i + 1, i + 1 - segmentationXZ);
					builder.AddFace4(i + 1 - segmentationXZ, i + 1, i + segmentationXZ, i);
				}
				else
				{
// 					builder.AddFace4(i, i + segmentationXZ, i + 1 + segmentationXZ, i + 1);
					builder.AddFace4(i + 1, i + 1 + segmentationXZ, i + segmentationXZ, i);
				}
			}

			m_Geometry = builder.ToGeometry();
			m_Geometry->UpdateBuffers();
		}
};

} } }
