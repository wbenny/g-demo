#pragma once
#include "MeshNode.h"
#include "../Materials/MaterialBase.h"
#include <cstdio>
#include "../Render/GeometryBuilder.h"
#include "../Render/VertexFormats/PositionNormalTexture.h"

namespace FW { namespace Scene {

MeshNode::MeshNode() :
	m_DrawMode(Render::DrawMode::Solid),
	m_DepthTestMode(Render::DepthTestMode::Enabled),
	m_Dynamic(false),
	m_CastShadow(true),
	m_ReceiveShadow(true),
	m_Reflective(false),
	m_Geometry(NULL),
	m_Material(NULL)
{
	m_NodeType = NodeType::Mesh;
}

MeshNode::~MeshNode()
{

}

const Materials::MaterialBase* MeshNode::GetMaterial() const { return m_Material; }
Materials::MaterialBase* MeshNode::GetMaterial() { return m_Material; }
void MeshNode::SetMaterial(Materials::MaterialBase* material) { m_Material = material; }

const Render::Geometry* MeshNode::GetGeometry() const { return m_Geometry; }
Render::Geometry* MeshNode::GetGeometry() { return m_Geometry; }
void MeshNode::SetGeometry(Render::Geometry* geometry) { m_Geometry = geometry; }

} }
