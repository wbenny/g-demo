#include "LightNode.h"

namespace FW { namespace Scene {

void LightNode::SetParent(Node* newParent)
{
	Node::SetParent(newParent);

	RootNode* rootNode = reinterpret_cast<RootNode*>(newParent->GetRootNode());

	if (m_LightType == LightType::Ambient)
	{
		rootNode->m_AmbientLightColor = m_Color;
	}
	else
	{
		rootNode->m_Lights.Add(this);
	}
}

} }
