#pragma once
#include "LightNode.h"
#include "CameraNode.h"
#include "../Math/Vector3.h"
#include "../Math/Projection.h"
#include "../Render/RenderModes/Depth.h"

FW_CLASS_FWD_DECL2(FW, Render, Renderer);

namespace FW { namespace Scene {
	
class SpotLightNode : public LightNode
{
	friend class Render::Renderer;
	friend class CameraNode;

	public:
		SpotLightNode(const Math::Vector3& color, float intensity) :
			LightNode(LightType::Spot, color, intensity),
			m_CastShadow(false)
		{
			const u16 shadowMapWidth  = 1024;
			const u16 shadowMapHeight = 1024;
			
			Render::Texture* depthMap = new Render::Texture();
			Render::Texture::InitData depthMapInitData(Render::Texture::InitData::TexturePreset::FramebufferFloatBGRA);

			depthMapInitData.Width  = shadowMapWidth;
			depthMapInitData.Height = shadowMapHeight;

			depthMap->Init(depthMapInitData);

			m_LightCamera = new CameraNode();
			m_LightCamera->m_AssociatedLight = this;
			m_LightCamera->SetCameraType(CameraType::Shadow);
			m_LightCamera->GetFramebuffer()->AttachTexture(depthMap);
  			m_LightCamera->GetFramebuffer()->SetFaceCullingMode(Render::FaceCullingMode::Front);
			m_LightCamera->GetFramebuffer()->SetRenderMode(Render::RenderModes::Depth::GetInstance());
			m_LightCamera->GetFramebuffer()->SetClearColor(Math::Vector3::One);
			m_LightCamera->SetProjection(Math::Projection::CreatePerspectiveFieldOfView(
				45.0f,
				static_cast<float>(shadowMapWidth)/static_cast<float>(shadowMapHeight),
				10.0f, // 0.1f
				3000.0f
			));
		}

		const Math::Vector3& GetTarget() const { return m_Target; }
		void SetTarget(const Math::Vector3& target) { m_Target = target; }

		bool CastsShadow() const { return m_CastShadow; }
		void EnableShadowCasting(bool castShadow = true) { m_CastShadow = castShadow; }

		const Math::Vector3& GetLocation() const
		{
			m_Location = (GetWorldMatrix().GetTranslation().Normalized() - m_Target.Normalized()).Normalized();
			return m_Location;
		}

		void SetParent(Node* newParent);

		const CameraNode* GetLightCamera() const { return m_LightCamera; }
		CameraNode* GetLightCamera() { return m_LightCamera; }

	protected:
		bool			m_CastShadow;

		Math::Vector3	m_Target;

		Ptr<CameraNode>	m_LightCamera;

		mutable Math::Vector3 m_Location;
};

} }
