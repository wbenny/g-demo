#pragma once
#include "FW/Render/IRendererHandler.h"
#include "FW/Scene/RootNode.h"
#include "FW/Scene/Primitives/Cube.h"
#include "FW/Scene/Primitives/Plane.h"
#include "FW/Scene/LightNode.h"
#include "FW/Scene/AmbientLightNode.h"
#include "FW/Scene/DirectionalLightNode.h"
#include "FW/Scene/PointLightNode.h"
#include "FW/Scene/CameraNode.h"
#include "FW/Scene/NodeManager.h"
#include "FW/Scene/Primitives/FullScreenQuad.h"
#include "FW/Materials/MaterialManager.h"
#include "FW/Render/FullScreenQuad.h"
#include "FW/Render/Postprocess/FullScreenQuadPostprocess.h"
#include "FW/Scene/Primitives/Skydome.h"
#include "FW/Render/Postprocess/RadialBlur.h"
#include "FW/Math/Bezier3D.h"

class Intro : public FW::Render::IRendererHandler
{
	public:
		Intro();

		bool Init();
		void End();

		bool Update(float time, float delta);

		void ProcessSponzaScene(float time, float delta);
		void ProcessChessScene(float time, float delta);

		void WriteInfo();
		
		void OnMouseMove(unsigned buttons, int x, int y, int dx, int dy);
		void OnMouseDown(unsigned button, int x, int y);
		void OnMouseUp(unsigned button, int x, int y);

		void OnKeyDown(char key, int x, int y);
		void OnKeyUp(char key, int x, int y);

	private:
		bool m_SSAO;

		FW::Math::Vector3	m_MoveVector;
		FW::Math::Vector2	m_RotationVector;

		float m_MoveDelta, m_MouseDelta;

		FW::Math::Bezier3D m_BEye[100], m_BTarget[100];

		// awww
		FW::Scene::Primitives::Skydome			*m_SkyDome;

		FW::Scene::RootNode						*m_SceneChess,
												*m_SceneSponza;

		FW::Scene::AmbientLightNode				*m_ChessAmbientLight,
												*m_SponzaAmbientLight;

		FW::Scene::PointLightNode				*m_ChessPointLight;
		FW::Scene::DirectionalLightNode			*m_SponzaDirectionalLight;


		FW::Scene::CameraNode					*m_ChessCameraMain,
												*m_ChessCameraLightScattering,
												*m_ChessCameraNormalDepth,
												*m_ChessCameraReflection;

		FW::Scene::CameraNode					*m_SponzaCameraMain,
												*m_SponzaCameraLightScattering,
												*m_SponzaCameraNormalDepth;

		FW::Scene::MeshNode						*m_MeshSponza,
												*m_MeshChess,
												*m_MeshTerrain,
												*m_MeshStatue,
												*m_MeshSunChess,
												*m_MeshSunSponza;
};
