#include "Loader.h"
#include "FW/Render/Renderer.h"
#include "FW/Render/Viewer.h"

void Loader::Callback(FW::u16 progress, char* text)
{
	const FW::Render::RendererInfo& Info = FW::Render::Viewer::GetInstance().GetInfo();
	const int XRES = Info.Width;
	const int YRES = Info.Height;

	const int xo = (( 28 * XRES) >> 8);
	const int y1 = ((200 * YRES) >> 8);
	const int yo = y1 - 8;

	FW::Render::Viewer::ProcessMessage();

	// Pozadi.
	SelectObject(Info.hDC, CreateSolidBrush(0x0045302c));
	Rectangle(Info.hDC, 0, 0, XRES, YRES);

	// Text.
	SetBkMode(Info.hDC, TRANSPARENT);
	SetTextColor(Info.hDC, 0x00ffffff);
	SelectObject(Info.hDC, CreateFont(44, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
										ANTIALIASED_QUALITY, 0, "arial"));
	TextOut(Info.hDC, (XRES - 318) >> 1, (YRES - 38) >> 1, "wait while loading...", 21);

	SelectObject(Info.hDC, CreateFont(20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
									ANTIALIASED_QUALITY, 0, "arial"));

	TextOut(Info.hDC, 142, (YRES + 305) >> 1, text, lstrlenA(text));

	// Proces nacitani.
	SelectObject(Info.hDC, CreateSolidBrush(0x00705030));
	Rectangle(Info.hDC, xo, yo, (228 * XRES) >> 8, y1);
	SelectObject(Info.hDC, CreateSolidBrush(0x00f0d0b0));
	Rectangle(Info.hDC, xo, yo, ((28 + progress*2) * XRES) >> 8, y1);

	FW::Render::Viewer::ProcessMessage();
}
