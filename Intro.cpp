#include "FW/Console.h"

#include "Intro.h"
#include "Loader.h"
#include "FW/GL/GL.h"
#include "FW/Render/RenderTarget.h"
#include "FW/Math/Projection.h"
#include "FW/Materials/NoShading.h"
#include "FW/Scene/Primitives/Plane.h"
#include "FW/Render/Postprocess/SSAO.h"
#include "FW/Scene/Primitives/Hemisphere.h"
#include "FW/Scene/Primitives/Skydome.h"
#include "FW/Content/ResourceLoader.h"
#include "FW/Render/Postprocess/HorizontalBlur.h"
#include "FW/Render/Postprocess/VerticalBlur.h"
#include "FW/Render/Postprocess/DepthOfField.h"
#include "FW/Render/ShaderManager.h"
#include "FW/Render/Postprocess/LightScattering.h"
#include "FW/Render/Postprocess/Bloom.h"
#include "FW/Render/RenderModes/NormalDepth.h"
#include "FW/Render/RenderModes/NoShading.h"
#include "FW/Render/Postprocess/VolumeLight.h"
#include "FW/Render/Postprocess/FXAA.h"
#include "FW/Render/Postprocess/ToneMapping.h"
#include "FW/Render/Postprocess/FilmGrain.h"
#include "fw/scene/NodeManager.h"
#include "FW/Math/Bezier3D.h"
#include "FW/Math/Bezier.h"

#define FW_CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))
#define IS_BETWEEN(x, a, b) ((x) >= (a) && (x) < (b))

int seed = 1;

void LoadShaders()
{
	FW_DEBUG_PRINT("[-] Loading shaders...\n");
	FW::Render::ShaderManager::GetInstance().Init();
	FW_DEBUG_PRINT("[-] Done...\n");

}

struct
{
	FW::Render::Postprocess::SSAO* SSAO;
	FW::Render::Postprocess::FullScreenQuadPostprocess* Blend1, *Blend2;
	FW::Render::Postprocess::HorizontalBlur* HBlur;
	FW::Render::Postprocess::VerticalBlur* VBlur;
	FW::Render::Postprocess::RadialBlur* RadialBlur;
	FW::Render::Postprocess::DepthOfField* DoF;
	FW::Render::Postprocess::Bloom* Bloom;
	FW::Render::Postprocess::LightScattering* LightScatteringChess;
	FW::Render::Postprocess::LightScattering* LightScatteringSponza;
	FW::Render::Postprocess::VolumeLight* VolumeLight;
	FW::Render::Postprocess::FXAA* FXAA;
	FW::Render::Postprocess::ToneMapping* ToneMapping;
	FW::Render::Postprocess::FilmGrain* FilmGrain;
} sPP;

float sunpos = 1.695f;

bool Intro::Init()
{
	Loader loader;

	// vsync (0-off, 1-on)
	FW::GL::SwapInterval(1);

	loader.Callback(20, "loading shaders...");


	LoadShaders();

	m_SSAO = true;
	m_MoveDelta = 3.50f;
	m_MouseDelta = 0.25f;
// 	m_MoveVector = FW::Math::Vector3(0.0f, 0.0f, 0.0f); //FW::Math::Vector3(-530.0f, 10.0f, -175.0f);
// 	m_RotationVector = FW::Math::Vector2(0.0f, 0.0f);
	m_MoveVector = FW::Math::Vector3(245.0f, 117.0f, -87.0f);	
//	m_MoveVector = FW::Math::Vector3(-16.001f, -18.649f, 62.960f);	// chess
	m_RotationVector = FW::Math::Vector2(0.0f, 0.0f);

	sPP.SSAO = new FW::Render::Postprocess::SSAO();
	sPP.DoF = new FW::Render::Postprocess::DepthOfField();
	sPP.VBlur = new FW::Render::Postprocess::VerticalBlur();
	sPP.HBlur = new FW::Render::Postprocess::HorizontalBlur();
	sPP.Blend1 = new FW::Render::Postprocess::FullScreenQuadPostprocess(1);
	sPP.Blend2 = new FW::Render::Postprocess::FullScreenQuadPostprocess(2);
	sPP.Bloom = new FW::Render::Postprocess::Bloom();
	sPP.LightScatteringChess = new FW::Render::Postprocess::LightScattering();
	sPP.LightScatteringSponza = new FW::Render::Postprocess::LightScattering();
	sPP.VolumeLight = new FW::Render::Postprocess::VolumeLight();
	sPP.RadialBlur = new FW::Render::Postprocess::RadialBlur();
	sPP.FXAA = new FW::Render::Postprocess::FXAA();
	sPP.ToneMapping = new FW::Render::Postprocess::ToneMapping();
	sPP.FilmGrain = new FW::Render::Postprocess::FilmGrain();

	sPP.LightScatteringSponza->SetExposure(0.0044f);
	sPP.LightScatteringSponza->SetWeight(50.65f);
	sPP.LightScatteringSponza->SetDecay(1.0f);
	sPP.LightScatteringSponza->SetDensity(0.739f);

// 	sPP.LightScattering->SetExposure(-0.00660f);
// 	sPP.LightScattering->SetWeight(-29.35000f);
// 	sPP.LightScattering->SetDecay(1.0f);
// 	sPP.LightScattering->SetDensity(0.74600f);

	// sponza
// 	sPP.LightScatteringSponza->SetExposure(0.0022f);
// 	sPP.LightScatteringSponza->SetWeight(4.65f);
// 	sPP.LightScatteringSponza->SetDecay(1.0f);
// 	sPP.LightScatteringSponza->SetDensity(0.84f);

	sPP.DoF->SetAutoFocusState(true);

	loader.Callback(35, "creating sponza scene...");


	//////////////////////////////////////////////////////////////////////////
	// Scene sponza
	m_SceneSponza = new FW::Scene::RootNode();

	// Light
	m_SponzaAmbientLight = new FW::Scene::AmbientLightNode(FW::Math::Vector3(0.70f, 0.7f, 0.70f), 0.68f);
	m_SceneSponza->Add(m_SponzaAmbientLight);

	m_SponzaDirectionalLight = new FW::Scene::DirectionalLightNode(FW::Math::Vector3(0.7f, 0.7f, 0.8f), 0.9f);
	m_SponzaDirectionalLight->SetTarget(FW::Math::Vector3(-90.0f, 0.0f, 0.0f));

	//////////////////////////////////////////////////////////////////////////
// 	FW::Scene::PointLightNode* pln;

// 	for (int i = 0; i < PHONG_MAX_LIGHTS; i++)
// 	{
// 		pln = new FW::Scene::PointLightNode(FW::Math::Vector3(FW::Math::Random(&seed)*0.5f+0.5f, FW::Math::Random(&seed)*0.5f+0.5f, FW::Math::Random(&seed)*0.5f+0.5f), 2.0f);
// 		pln->SetMatrix(FW::Math::Matrix4::CreateTranslation(FW::Math::Random(&seed)*230, FW::Math::Random(&seed)*113, FW::Math::Random(&seed)*55));
// 		pln->SetFallOffDistance(80.0f);
// 		m_SceneSponza->Add(pln);
// 	}

// 	pln = new FW::Scene::PointLightNode(FW::Math::Vector3(1.0f, 0.0f, 0.0f), 1.9f);
// 	pln->SetMatrix(FW::Math::Matrix4::CreateTranslation(230, 113, 55));
// 	pln->SetFallOffDistance(50.0f);
// 	m_SceneSponza->Add(pln);
// 
// 	pln = new FW::Scene::PointLightNode(FW::Math::Vector3(0.0f, 1.0f, 0.0f), 1.9f);
// 	pln->SetMatrix(FW::Math::Matrix4::CreateTranslation(230, 113, -55));
// 	pln->SetFallOffDistance(50.0f);
// 	m_SceneSponza->Add(pln);

	//////////////////////////////////////////////////////////////////////////

	// Camera
	m_SponzaCameraMain = new FW::Scene::CameraNode(FW::Math::Projection::CreatePerspectiveFieldOfView(60.0f, float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f));
	m_SponzaCameraMain->GetFramebuffer()->GetRenderState()->m_ClearColor = FW::Math::Vector3(0.0f, 0.0f, 0.0f);
	m_SceneSponza->Add(m_SponzaCameraMain);

	m_SponzaCameraNormalDepth = new FW::Scene::CameraNode(FW::Math::Projection::CreatePerspectiveFieldOfView(60.0f, float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f));
	m_SponzaCameraNormalDepth->GetFramebuffer()->Done();
	m_SponzaCameraNormalDepth->GetFramebuffer()->Init(FW::Render::FramebufferPreset::FloatBGRA, FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT);
	m_SponzaCameraNormalDepth->GetFramebuffer()->SetRenderState(
		new FW::Render::RenderState(
			FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT,
			FW::Math::Vector3(1.0f, 1.0f, 1.0f),
			FW::Render::RenderModes::NormalDepth::GetInstance(),
			FW::Render::FaceCullingMode::Back
		)
	);
	m_SceneSponza->Add(m_SponzaCameraNormalDepth);

	m_SponzaCameraLightScattering = new FW::Scene::CameraNode(FW::Math::Projection::CreatePerspectiveFieldOfView(60.0f, float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f));
	m_SponzaCameraLightScattering->DisableLighting();
	m_SponzaCameraLightScattering->GetFramebuffer()->Done();
	m_SponzaCameraLightScattering->GetFramebuffer()->Init(FW::Render::FramebufferPreset::ByteBGR, FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT);
	m_SponzaCameraLightScattering->GetFramebuffer()->SetRenderState(
		new FW::Render::RenderState(
			FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT,
			FW::Math::Vector3(0.2f, 0.2f, 0.28f)*0.2f,
			FW::Render::RenderModes::NoShading::GetInstance(),
			FW::Render::FaceCullingMode::Back
		)
	);
	m_SceneSponza->Add(m_SponzaCameraLightScattering);

	// Sun
	FW::Materials::NoShading* sunMat = new FW::Materials::NoShading();
	sunMat->SetColor(FW::Math::Vector3(1.0f, 1.0f, 1.0f));

	m_MeshSunSponza = new FW::Scene::MeshNode();
	m_MeshSunSponza->SetName("sun");
	m_MeshSunSponza->SetGeometry(FW::Content::ResourceLoader<FW::Render::Geometry>::Read("models\\sphere_centered.vnt"));
	m_MeshSunSponza->SetMaterial(sunMat);
	m_MeshSunSponza->SetMatrix(FW::Math::Matrix4::CreateTranslation(
		FW::Math::Vector3
		(
			200.0f,
			FW::Math::Sin(sunpos)*400.0f,
			FW::Math::Cos(sunpos)*100.0f
		) * 1.39f
	));
	m_MeshSunSponza->DisableShadowCasting();
	m_SceneSponza->Add(m_MeshSunSponza);
	m_SceneSponza->Add(m_SponzaDirectionalLight);
	//m_SponzaDirectionalLight->SetParent(m_MeshSunSponza);

	// Light scattering shader
	sPP.LightScatteringSponza->SetAssignedCamera(m_SponzaCameraMain);
	sPP.LightScatteringSponza->SetAssignedSunNode(m_MeshSunSponza);

	loader.Callback(45, "loading skydome model...");

	// Skydome
	m_SkyDome = new FW::Scene::Primitives::Skydome();
	m_SkyDome->SetAssignedCamera(m_SponzaCameraMain);
	m_SceneSponza->Add(m_SkyDome);

	loader.Callback(55, "loading sponza model...");

// 	// TABLE
// 	FW::Render::Texture* woodTexture = FW::Content::ResourceLoader<FW::Render::Texture>::Read("textures\\wood.tga");
// 	FW::Materials::PhongMaterial* woodMaterial = new FW::Materials::PhongMaterial(woodTexture);
// 
// 	FW::Scene::MeshNode* meshTable = FW::Content::ResourceLoader<FW::Scene::MeshNode>::Read("models\\table.3ds");
// 
// 	FW::Scene::NodeManager::GetInstance().GetNode("table")->As<FW::Scene::MeshNode>()->SetMaterial(woodMaterial);
// 
// 	meshTable->SetMatrix(FW::Math::Matrix4::CreateScale(0.1f) * FW::Math::Matrix4::CreateTranslation(0.0f, -200.0f, 0.0f));
// 
// 	m_SceneSponza->Add(meshTable);


	// SPONZA
	FW::Scene::MeshNode* meshSponza = FW::Content::ResourceLoader<FW::Scene::MeshNode>::Read("models\\sponza2.3ds");
	meshSponza->SetName("SPONZA");
// 	meshSponza->SetMatrix(FW::Math::Matrix4::CreateTranslation(0.0f, -20.0f, 0.0f));
	m_SceneSponza->Add(meshSponza);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Scene chess
	loader.Callback(70, "creating chess scene...");
	
	m_SceneChess = new FW::Scene::RootNode();

	// Light
	//m_ChessAmbientLight = new FW::Scene::AmbientLightNode(FW::Math::Vector3(0.70f, 0.7f, 0.70f), 0.68f);	//sponza
	m_ChessAmbientLight = new FW::Scene::AmbientLightNode(FW::Math::Vector3(0.7f, 0.7f, 0.8f), 0.5f);	//chess
	m_SceneChess->Add(m_ChessAmbientLight);
	
	m_ChessPointLight = new FW::Scene::PointLightNode(FW::Math::Vector3(0.7f, 0.7f, 0.8f), 1.0f);
	m_ChessPointLight->SetMatrix(FW::Math::Matrix4::CreateTranslation(0.0f, 0.0f, 0.0f));
	m_SceneChess->Add(m_ChessPointLight);

	// Camera
	m_ChessCameraMain = new FW::Scene::CameraNode(FW::Math::Projection::CreatePerspectiveFieldOfView(60.0f, float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f));
	m_ChessCameraMain->GetFramebuffer()->GetRenderState()->m_ClearColor = FW::Math::Vector3(0.0f, 0.0f, 0.0f);
	m_SceneChess->Add(m_ChessCameraMain);

	m_ChessCameraReflection = new FW::Scene::CameraNode(FW::Math::Projection::CreatePerspectiveFieldOfView(60.0f,float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f));
	m_ChessCameraReflection->SetCameraType(FW::Scene::CameraType::Reflect);
	m_ChessCameraReflection->GetFramebuffer()->GetRenderState()->m_ClearColor = FW::Math::Vector3(0.0f, 0.0f, 0.0f);
	m_ChessCameraReflection->GetFramebuffer()->GetRenderState()->m_FaceCullingMode = FW::Render::FaceCullingMode::NoCulling;
	m_SceneChess->Add(m_ChessCameraReflection);

	m_ChessCameraNormalDepth = new FW::Scene::CameraNode(FW::Math::Projection::CreatePerspectiveFieldOfView(60.0f, float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f));
	m_ChessCameraNormalDepth->GetFramebuffer()->Done();
	m_ChessCameraNormalDepth->GetFramebuffer()->Init(FW::Render::FramebufferPreset::FloatBGRA, FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT);
	m_ChessCameraNormalDepth->GetFramebuffer()->SetRenderState(
		new FW::Render::RenderState(
			FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT,
			FW::Math::Vector3(1.0f, 1.0f, 1.0f),
			FW::Render::RenderModes::NormalDepth::GetInstance(),
			FW::Render::FaceCullingMode::Back
		)
	);
	m_SceneChess->Add(m_ChessCameraNormalDepth);

	m_ChessCameraLightScattering = new FW::Scene::CameraNode(FW::Math::Projection::CreatePerspectiveFieldOfView(60.0f, float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f));
	m_ChessCameraLightScattering->DisableLighting();
	m_ChessCameraLightScattering->GetFramebuffer()->Done();
	m_ChessCameraLightScattering->GetFramebuffer()->Init(FW::Render::FramebufferPreset::ByteBGR, FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT);
	m_ChessCameraLightScattering->GetFramebuffer()->SetRenderState(
		new FW::Render::RenderState(
			FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT,
			FW::Math::Vector3(0.2f, 0.2f, 0.28f)*0.2f,
			FW::Render::RenderModes::NoShading::GetInstance(),
			FW::Render::FaceCullingMode::Back
		)
	);
	m_SceneChess->Add(m_ChessCameraLightScattering);
	
	// Light scattering shader
	sPP.LightScatteringChess->SetAssignedCamera(m_ChessCameraMain);

	//////////////////////////////////
	// Mesh
	loader.Callback(75, "loading chess scene...");

	m_MeshChess = FW::Content::ResourceLoader<FW::Scene::MeshNode>::Read("models\\chess2.3ds");
	m_MeshChess->SetName("CHESS");
	m_MeshChess->SetMatrix(FW::Math::Matrix4::CreateTranslation(0.0f, 0.0f, 0.0f));
	m_SceneChess->Add(m_MeshChess);
	
	FW::Render::Texture* marble = FW::Content::ResourceLoader<FW::Render::Texture>::Read("textures\\marble.tga", true);

// 	FW::Scene::NodeManager::GetInstance().GetNode("pole")->As<FW::Scene::MeshNode>()->EnableReflection();

	FW::Render::CubeMap* cm = FW::Content::ResourceLoader<FW::Render::CubeMap>::Read("textures\\cubemaps\\miramar");

	FW::Materials::MaterialManager::GetInstance().GetMaterial("cerna")->As<FW::Materials::PhongMaterial>()->SetEnvironmentMap(cm);
	FW::Materials::MaterialManager::GetInstance().GetMaterial("bila")->As<FW::Materials::PhongMaterial>()->SetEnvironmentMap(cm);

	FW::Materials::MaterialManager::GetInstance().GetMaterial("cerna")->As<FW::Materials::PhongMaterial>()->SetSpecularReflection(0.90f);
	FW::Materials::MaterialManager::GetInstance().GetMaterial("bila")->As<FW::Materials::PhongMaterial>()->SetSpecularReflection(0.90f);

// 	FW::Materials::MaterialManager::GetInstance().GetMaterial("pole")->As<FW::Materials::PhongMaterial>()->SetTexture(marble);
	//////////////////////////////////

// 	m_MeshChess->SetMatrix(FW::Math::Matrix4::CreateScale(0.03f) * FW::Math::Matrix4::CreateTranslation(0.0f, -350.0f, 0.0f));
// 	m_SceneSponza->Add(m_MeshChess);

	loader.Callback(95, "setting up renderer...");

	FW::Render::Renderer::GetInstance().Init();
	
	loader.Callback(100, "done...");
	Sleep(500);

	return true;
}

void Intro::End()
{

}

static float GetFract(float f)
{
	float num = f;
	int intpart = (int)f;
	return (num - float(intpart));
}

static FW::Math::Vector3 VecTransform(const FW::Math::Vector3& position, const FW::Math::Matrix4& matrix)
{
	FW::Math::Vector3 vector;
	float num3 = (((position.X * matrix.M11) + (position.Y * matrix.M21)) + (position.Z * matrix.M31)) + matrix.M14;
	float num2 = (((position.X * matrix.M21) + (position.Y * matrix.M22)) + (position.Z * matrix.M32)) + matrix.M24;
	float num  = (((position.X * matrix.M31) + (position.Y * matrix.M23)) + (position.Z * matrix.M33)) + matrix.M34;
	vector.X = num3;
	vector.Y = num2;
	vector.Z = num;
	return vector;
}

bool updated = false;

struct s_sceneProp
{
	float start;
	FW::Math::Bezier bDof, bFov, bSun, bBlur;
	FW::Math::Bezier3D bEye, bTarget;
	bool fadeIn, fadeOut, endFlag;
	bool sponza;

	bool specialSunPosition;
	bool higherLighting;
	bool moreLightScattering;

	s_sceneProp()
	{
		start = 0.0f; fadeIn = fadeOut = false; endFlag = false; sponza = false;
		specialSunPosition = false;
		higherLighting = false;
		moreLightScattering = false;
	}

	void AddPoint(const FW::Math::Vector3& e, const FW::Math::Vector3& t, float dof = 60.0f, float fov = 60.0f, float sun = 1.695f, float blur = 3.2f)
	{
		if (blur == 3.2f && sponza)
			blur = 1.0f;

		bEye.AddPoint(e);
		bTarget.AddPoint(t);
		bDof.AddPoint(dof);
		bFov.AddPoint(fov);
		bSun.AddPoint(sun);
		bBlur.AddPoint(blur);
	}
};

FW::List<s_sceneProp*> l_scene;
int l_sceneId = 0;
float time_omega = 0.0f;

void GenRandomScene(float start, int type = -1)
{
	s_sceneProp* s;

	s = new s_sceneProp;
	s->start = start;
	s->fadeIn = s->fadeOut = true;

	if (type == -1)
		s->sponza = !!(rand()&1);
	else
		s->sponza = !!type;

	for (FW::u32 i = 0; i < FW::u32(FW::Math::Random(&seed)*2.0f+7.0f); i++)
	{
		if (s->sponza)
		{
			s->AddPoint(FW::Math::Vector3(FW::Math::Random(&seed)*260.0f, (FW::Math::Random(&seed)+1.0f)*110.0f, FW::Math::Random(&seed)*100.0f),
						FW::Math::Vector3(FW::Math::Random(&seed)*260.0f, (FW::Math::Random(&seed)+1.0f)*110.0f, FW::Math::Random(&seed)*100.0f), -1.0f, 60.0f, 1.695f, 1.0f);
		}
		else
		{
			s->AddPoint(FW::Math::Vector3(FW::Math::Random(&seed)*130.0f, (FW::Math::Random(&seed)+1.0f)*30.0f, FW::Math::Random(&seed)*100.0f),
						FW::Math::Vector3(FW::Math::Random(&seed)*130.0f, (FW::Math::Random(&seed)+1.0f)*30.0f, FW::Math::Random(&seed)*100.0f), -1.0f, 60.0f, 1.695f, 1.0f);

		}
	}

	l_scene.Add(s);
}

Intro::Intro()
{
	seed = 6;

	s_sceneProp* s;

	// sponza big prulet
	s = new s_sceneProp;
	s->sponza = true;
	s->higherLighting = true;
	s->specialSunPosition = true;
	s->start = 0.0f;
	s->fadeIn = s->fadeOut = true;

	s->AddPoint(FW::Math::Vector3(0.0f, 127.0f, 0.0f), FW::Math::Vector3(0.0f, 129.0f,  0.0f), 60.0f, 60.0f, 1.53f);
	s->AddPoint(FW::Math::Vector3(0.0f, 127.0f, 0.0f), FW::Math::Vector3(0.0f, 129.0f,  0.0f), 60.0f, 60.0f, 1.56f);
	s->AddPoint(FW::Math::Vector3(33.0f, 9.0f, 2.0f), FW::Math::Vector3(0.0f, 129.0f,  0.0f), 60.0f, 60.0f, 1.60f);
	s->AddPoint(FW::Math::Vector3(94.0f, 15.0f,  26.0f), FW::Math::Vector3(0.0f, 12.0f, 12.5f), 60.0f, 60.0f, 1.62f);
	s->AddPoint(FW::Math::Vector3(94.0f, 15.0f,  26.0f), FW::Math::Vector3(0.0f, 8.0f, 12.5f), 60.0f, 60.0f, 1.65f);
	s->AddPoint(FW::Math::Vector3(-70.0f, 25.0f, 80.0f), FW::Math::Vector3(-95.0f, 5.0f, 68.5f), 60.0f, 60.0f, 1.68f);

	s->AddPoint(FW::Math::Vector3(-241.0f, 29.0f, 95.0f), FW::Math::Vector3(-223.0f, 28.0f, 27.5f), 60.0f, 60.0f, 1.70f);
	s->AddPoint(FW::Math::Vector3(-264.0f, 28.0f, -5.0f), FW::Math::Vector3(-187.0f, 35.0f, 9.5f), 60.0f, 60.0f, 1.73f);
	s->AddPoint(FW::Math::Vector3(-166.0f, 53.0f, -37.0f), FW::Math::Vector3(-59.0f, 118.0f, 85.5f), 60.0f, 60.0f, 1.77f);
	s->AddPoint(FW::Math::Vector3(-56.0f, 123.0f, -6.0f), FW::Math::Vector3(-59.0f, 118.0f, 85.5f), 60.0f, 60.0f, 1.78f);
	s->AddPoint(FW::Math::Vector3(-5.0f, 128.0f, 94.0f), FW::Math::Vector3(-69.0f, 117.0f, 92.5f), 60.0f, 60.0f, 1.80f);

	s->AddPoint(FW::Math::Vector3(-126.0f, 119.0f, 90.0f), FW::Math::Vector3(-127.0f, 120.0f, 76.5f), 60.0f, 60.0f, 1.83f);
	s->AddPoint(FW::Math::Vector3(-176.0f, 126.0f, -100.0f), FW::Math::Vector3(-127.0f, 119.0f, -95.5f), 60.0f, 60.0f, 1.86f);
	s->AddPoint(FW::Math::Vector3(100.0f, 128.0f, -95.0f), FW::Math::Vector3(110.0f, 120.0f, -95.5f), 60.0f, 60.0f, 1.86f);

	l_scene.Add(s);


// 	// sponza prulet
	s = new s_sceneProp;
	s->sponza = true;
	s->moreLightScattering = true;
	s->specialSunPosition = true;
	s->start = 26.5f;
	s->fadeIn = s->fadeOut = true;

	s->AddPoint(FW::Math::Vector3(227.0f, 107.0f, -100.0f), FW::Math::Vector3(160.0f, 140.0f,  200.0f));
	s->AddPoint(FW::Math::Vector3(232.0f, 121.0f,  100.0f), FW::Math::Vector3(120.0f, 150.0f, -70.0f));

	l_scene.Add(s);

	// scene
// 	s = new s_sceneProp;
// 	s->start = 46.2f;
// 	s->fadeIn = s->fadeOut = true;
// 
// 	s->AddPoint(FW::Math::Vector3(-22.27f,  52.40f, -69.67f),
// 				FW::Math::Vector3(-43.33f,  305.65f,  57.00f));
// 
// 	s->AddPoint(FW::Math::Vector3(113.27f,  20.40f,  -95.67f),
// 				FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
// 
// 	l_scene.Add(s);

	// scene 0
	s = new s_sceneProp;
	s->start = 39.7f;
	s->fadeIn = s->fadeOut = true;

	s->AddPoint(FW::Math::Vector3(-22.27f,  52.40f, -69.67f),
				FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	s->AddPoint(FW::Math::Vector3( 26.25f,  58.40f, -50.58f),
				FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	s->AddPoint(FW::Math::Vector3( 26.25f,  58.40f, -50.58f),
						FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	s->AddPoint(FW::Math::Vector3( 26.25f,  58.40f, -80.58f),
						FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	s->AddPoint(FW::Math::Vector3( 26.25f,  58.40f, -50.58f),
						FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	s->AddPoint(FW::Math::Vector3(136.25f,  7.40f,   117.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(136.25f,  7.40f,   117.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(136.25f,  7.40f,   117.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(136.25f,  7.40f,   117.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(136.25f,  7.40f,   117.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(136.25f,  7.40f,   117.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(136.25f,  7.40f,   117.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));


	s->AddPoint(FW::Math::Vector3(130.25f,  7.40f,   107.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	// strelec
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	// zaostrime na strelce
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f), 15.0f);
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f), 15.0f);
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f), 15.0f);
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f), 15.0f);
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f), 15.0f);
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f), 15.0f);

	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f), 15.0f);
	s->AddPoint(FW::Math::Vector3(-43.25f,  47.40f,   127.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f), 15.0f);

	// rozostrime
	s->AddPoint(FW::Math::Vector3(-50.00f,  50.40f,  126.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(-50.00f,  50.40f,  126.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	
	s->AddPoint(FW::Math::Vector3(-50.00f,  50.40f,  126.58f),
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));
	s->AddPoint(FW::Math::Vector3(-50.00f,  50.40f,  126.58f)*1.3f,
		FW::Math::Vector3(-43.33f,  35.65f,  57.00f));

	l_scene.Add(s);

	// zustaneme dele dole
	s = new s_sceneProp;
	s->start = 59.0f;
	s->moreLightScattering = true;
	s->fadeIn = s->fadeOut = true;

	s->AddPoint(FW::Math::Vector3(-135.00f, 9.40f,   38.58f),
		FW::Math::Vector3(-49.33f,  27.65f,  70.00f), 150.0f);
	s->AddPoint(FW::Math::Vector3(-135.00f, 9.40f,   38.58f),
		FW::Math::Vector3(-49.33f,  27.65f,  70.00f), 150.0f);
	s->AddPoint(FW::Math::Vector3(-135.00f, 9.40f,   38.58f),
		FW::Math::Vector3(-49.33f,  27.65f,  70.00f), 150.0f);

	// zustaneme dele zaostreni na damu
	s->AddPoint(FW::Math::Vector3(-77.00f,  58.40f,  -1.58f),
						FW::Math::Vector3(-49.33f,  52.65f,  51.00f), 40.0f, 20.0f);

	s->AddPoint(FW::Math::Vector3(-77.00f,  58.40f,  -1.58f),
						FW::Math::Vector3(-49.33f,  52.65f,  51.00f), 40.0f, 22.0f);

	s->AddPoint(FW::Math::Vector3(-77.00f,  58.40f,  -1.58f),
						FW::Math::Vector3(-49.33f,  52.65f,  51.00f), 40.0f, 25.0f);
	
	l_scene.Add(s);

	// scene 
	GenRandomScene(72.0f, 0);
	GenRandomScene(85.0f, 1);

	//////////////////////////////////////////////////////////////////////////
	s = new s_sceneProp;
	s->fadeIn = s->fadeOut = true;
	s->sponza = true;
	s->higherLighting = true;
	s->start = 93.2f;

	s->AddPoint(FW::Math::Vector3(-226.0f, 118.0f, -96.0f), FW::Math::Vector3(-190.0f, 115.0f,  60.0f), 160.0f, 60.0f, 1.4f);
	s->AddPoint(FW::Math::Vector3(168.0f, 121.0f, -102.0f), FW::Math::Vector3(140.0f, 115.0f,  60.0f), 160.0f, 60.0f, 1.85f);

	l_scene.Add(s);

	//////////////////////////////////////////////////////////////////////////
	s = new s_sceneProp;
	s->fadeIn = s->fadeOut = true;
	s->sponza = true;
	s->higherLighting = true;
	s->specialSunPosition = true;
	s->start = 115.0f;

	s->AddPoint(FW::Math::Vector3(234.0f, 130.0f, 93.0f), FW::Math::Vector3(200.0f, 100.0f,  1.0f), 60.0f, 60.0f, 1.4f);
	s->AddPoint(FW::Math::Vector3(145.0f, 124.0f, -98.0f), FW::Math::Vector3(200.0f, 1.00f,  -6.0f), 60.0f, 60.0f, 1.85f);

	l_scene.Add(s);


	GenRandomScene(129.0, 0);

	s = new s_sceneProp;
	s->fadeIn = s->fadeOut = true;
	s->sponza = true;
	s->higherLighting = true;
	s->specialSunPosition = true;
	s->start = 140.0f;

	s->AddPoint(FW::Math::Vector3(29.0f, 75.0f, -15.0f), FW::Math::Vector3(58.0f, 98.0f,  -5.6f), 60.0f, 60.0f);
	s->AddPoint(FW::Math::Vector3(32.0f, 70.0f, 12.0f), FW::Math::Vector3(58.0f, 98.0f,  -5.6f), 60.0f, 60.0f);
	s->AddPoint(FW::Math::Vector3(84.0f, 73.0f, 14.0f), FW::Math::Vector3(58.0f, 98.0f,  -5.6f), 60.0f, 60.0f);
	s->AddPoint(FW::Math::Vector3(84.0f, 70.0f, -23.0f), FW::Math::Vector3(58.0f, 98.0f,  -5.6f), 60.0f, 60.0f);
	s->AddPoint(FW::Math::Vector3(75.0f, 80.0f, -6.0f), FW::Math::Vector3(58.0f, 98.0f,  -5.6f), 60.0f, 60.0f);
	s->AddPoint(FW::Math::Vector3(209.0f, 4.0f, -11.0f), FW::Math::Vector3(121.0f, 85.0f,  -11.6f), 60.0f, 60.0f);

	l_scene.Add(s);


	//////////////////////////////////////////////////////////////////////////
	// END SCENE
	s = new s_sceneProp;

	s->start = 155.0f;
	s->endFlag = true;

	l_scene.Add(s);
}

void Intro::ProcessChessScene(float time, float delta)
{
	float pos, at, tt;
	FW::Math::Vector3 _u1, _u2, _u3;
	_u3 = FW::Math::Vector3(0.0f, 1.0f, 0.0f);

	time -= time_omega;
	
	// scene managing
	{
		if (!IS_BETWEEN(time, l_scene[l_sceneId]->start, l_scene[l_sceneId+1]->start))
		{
			// step to next scene
			l_sceneId++;

			if (l_scene[l_sceneId]->endFlag)
			{
				time_omega += l_scene[l_sceneId]->start;
				l_sceneId = 0;
			}

			printf("playing scene [%i]\n", l_sceneId);
		}

		// fading
		{
			float diff1 = 1.0f, diff2 = 1.0f;

			if (l_scene[l_sceneId]->fadeIn) // in
			{
				diff1 = time - l_scene[l_sceneId]->start;
			}
			
			if (l_scene[l_sceneId]->fadeOut) // out
			{
				diff2 = l_scene[l_sceneId+1]->start - time;
			}

			diff1 = FW_CLAMP(diff1, 0.0f, 1.0f);
			diff2 = FW_CLAMP(diff2, 0.0f, 1.0f);

			sPP.FilmGrain->SetIntensity(min(diff1, diff2));
		}

		pos = l_scene[l_sceneId+1]->start - l_scene[l_sceneId]->start;
		at = l_scene[l_sceneId+1]->start - time;
		tt = 1.0f - at/pos;

		_u1 = l_scene[l_sceneId]->bEye.Interpolate(tt);
		_u2 = l_scene[l_sceneId]->bTarget.Interpolate(tt);

		sPP.DoF->SetAutoFocusState(l_scene[l_sceneId]->bDof.Interpolate(tt) <= 0.0f);
		sPP.DoF->SetFocalDepth(l_scene[l_sceneId]->bDof.Interpolate(tt));

		sPP.DoF->SetBlur(l_scene[l_sceneId]->bBlur.Interpolate(tt));
	}

	// LS
	if (l_scene[l_sceneId]->moreLightScattering)
	{
		sPP.LightScatteringChess->SetExposure(0.0022f);
		sPP.LightScatteringChess->SetWeight(34.65f);
		sPP.LightScatteringChess->SetDecay(1.0f);
		sPP.LightScatteringChess->SetDensity(0.84f);
	}
	else
	{
		sPP.LightScatteringChess->SetExposure(0.0022f);
		sPP.LightScatteringChess->SetWeight(4.65f);
		sPP.LightScatteringChess->SetDecay(1.0f);
		sPP.LightScatteringChess->SetDensity(0.84f);
	}

	FW::Math::Matrix4 _lookAtMatrix, _invLookAt;

	_lookAtMatrix = FW::Math::Matrix4::CreateLookAt(_u1, _u2, _u3);
	_invLookAt = FW::Math::Matrix4::CreateInverseLookAt(_u1, _u2, _u3, 0.0f);

	// Set up the view matrix and projection matrix.
	FW::Math::Projection prj = FW::Math::Projection::CreatePerspectiveFieldOfView(l_scene[l_sceneId]->bFov.Interpolate(tt), float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f);
	
	m_ChessCameraMain->SetProjection(prj);
	m_ChessCameraLightScattering->SetProjection(prj);
	m_ChessCameraNormalDepth->SetProjection(prj);
	m_ChessCameraReflection->SetProjection(prj);

	m_ChessCameraMain->SetMatrix(_lookAtMatrix);
	m_ChessCameraLightScattering->SetMatrix(_lookAtMatrix);
	m_ChessCameraNormalDepth->SetMatrix(_lookAtMatrix);
	m_ChessCameraReflection->SetMatrix(_invLookAt);

	sPP.FilmGrain->SetTime(time);

	// Render scene
	FW::Render::Renderer::GetInstance().Draw(m_SceneChess);

	// Light scattering shader on the special framebuffer
	sPP.LightScatteringChess->SetInputTexture(m_ChessCameraLightScattering->GetFramebuffer()->GetTexture());
	sPP.LightScatteringChess->Draw();

	// SSAO
	sPP.SSAO->SetInputTexture(m_SSAO ? m_ChessCameraMain->GetFramebuffer()->GetTexture() : NULL);
	sPP.SSAO->SetNormalDepthTexture(m_ChessCameraNormalDepth->GetFramebuffer()->GetTexture());
	sPP.SSAO->Draw();

	// Depth of Field (main camera + need depth texture)
	sPP.DoF->SetRenderedTexture(sPP.SSAO->GetOutputTexture());
	sPP.DoF->SetDepthTexture(m_ChessCameraNormalDepth->GetFramebuffer()->GetTexture());
	sPP.DoF->Draw();

	// Blend result from SSAO with Light scattering pass
	sPP.Blend2->SetInputTexture(sPP.DoF->GetOutputTexture());
	sPP.Blend2->SetInputTexture(sPP.LightScatteringChess->GetOutputTexture(), 1);
	sPP.Blend2->Draw();
	
	// Bloom intro special framebuffer
	sPP.Bloom->SetInputTexture(sPP.Blend2->GetOutputTexture());
	sPP.Bloom->Draw();

	// Tone mapping with bloom + almost final render
	sPP.ToneMapping->SetInputTexture(sPP.Blend2->GetOutputTexture());
	sPP.ToneMapping->SetBloomTexture(sPP.Bloom->GetOutputTexture());
	sPP.ToneMapping->Draw();

	// Antialiasing
	sPP.FXAA->SetInputTexture(sPP.ToneMapping->GetOutputTexture());
	sPP.FXAA->Draw();

	// Film graing... and we're done!
	sPP.FilmGrain->SetInputTexture(sPP.FXAA->GetOutputTexture());
	sPP.FilmGrain->SetAsFinal();
	sPP.FilmGrain->Draw();
}

void Intro::ProcessSponzaScene(float time, float delta)
{
	float pos, at, tt;
	FW::Math::Vector3 _u1, _u2, _u3;
	_u3 = FW::Math::Vector3(0.0f, 1.0f, 0.0f);

	time -= time_omega;
	//time += 10.0f;

	// scene managing
	{
		if (!IS_BETWEEN(time, l_scene[l_sceneId]->start, l_scene[l_sceneId+1]->start))
		{
			// step to next scene
			l_sceneId++;

			if (l_scene[l_sceneId]->endFlag)
			{
				Sleep(1000);
				ExitProcess(0);

				time_omega += l_scene[l_sceneId]->start;
				l_sceneId = 0;
			}

			printf("playing scene [%i]\n", l_sceneId);
		}

		// fading
		{
			float diff1 = 1.0f, diff2 = 1.0f;

			if (l_scene[l_sceneId]->fadeIn) // in
			{
				diff1 = time - l_scene[l_sceneId]->start;
			}

			if (l_scene[l_sceneId]->fadeOut) // out
			{
				diff2 = l_scene[l_sceneId+1]->start - time;
			}

			diff1 = FW_CLAMP(diff1, 0.0f, 1.0f);
			diff2 = FW_CLAMP(diff2, 0.0f, 1.0f);

			sPP.FilmGrain->SetIntensity(min(diff1, diff2));
		}


		pos = l_scene[l_sceneId+1]->start - l_scene[l_sceneId]->start;
		at = l_scene[l_sceneId+1]->start - time;
		tt = 1.0f - at/pos;

		_u1 = l_scene[l_sceneId]->bEye.Interpolate(tt);
		_u2 = l_scene[l_sceneId]->bTarget.Interpolate(tt);

		sPP.DoF->SetAutoFocusState(l_scene[l_sceneId]->bDof.Interpolate(tt) <= 0.0f);
		sPP.DoF->SetFocalDepth(l_scene[l_sceneId]->bDof.Interpolate(tt));

		sPP.DoF->SetBlur(l_scene[l_sceneId]->bBlur.Interpolate(tt));
	}

	sunpos = l_scene[l_sceneId]->bSun.Interpolate(tt);

	// LS
	if (l_scene[l_sceneId]->moreLightScattering)
	{
		sPP.LightScatteringSponza->SetExposure(0.0044f);
		sPP.LightScatteringSponza->SetWeight(50.65f);
		sPP.LightScatteringSponza->SetDecay(1.0f);
		sPP.LightScatteringSponza->SetDensity(0.739f);
	}
	else
	{
		sPP.LightScatteringSponza->SetExposure(0.0054f);
		sPP.LightScatteringSponza->SetWeight(5.65f);
		sPP.LightScatteringSponza->SetDecay(1.0f);
		sPP.LightScatteringSponza->SetDensity(0.84f);
	}

	// more lighting
	if (l_scene[l_sceneId]->higherLighting)
	{
		m_SponzaDirectionalLight->SetColor(FW::Math::Vector3(0.7f, 0.7f, 0.8f) * 1.7f);
	}
	else
	{
		m_SponzaDirectionalLight->SetColor(FW::Math::Vector3(0.7f, 0.7f, 0.8f) * 0.9f);
	}

	if (l_scene[l_sceneId]->specialSunPosition)
	{
		m_SponzaDirectionalLight->SetMatrix(FW::Math::Matrix4::CreateTranslation(
			FW::Math::Vector3(
				-250.0f,
				FW::Math::Sin(sunpos)*400.0f+100.0f,
				-FW::Math::Cos(sunpos)*500.0f-80.0f
			) * 1.9f
		));

		m_MeshSunSponza->SetMatrix(FW::Math::Matrix4::CreateTranslation(
			FW::Math::Vector3(
				-250.0f,
				FW::Math::Sin(sunpos)*400.0f+100.0f,
				-FW::Math::Cos(sunpos)*500.0f-80.0f
			) * 0.9f
		));
	}
	else
	{
		m_SponzaDirectionalLight->SetMatrix(FW::Math::Matrix4::CreateTranslation(
			FW::Math::Vector3(
				-25.0f,
				FW::Math::Sin(sunpos)*400.0f+100.0f,
				-FW::Math::Cos(sunpos)*500.0f-80.0f
			) * 1.9f
		));

		m_MeshSunSponza->SetMatrix(FW::Math::Matrix4::CreateTranslation(
			FW::Math::Vector3(
				-25.0f,
				FW::Math::Sin(sunpos)*400.0f+100.0f,
				-FW::Math::Cos(sunpos)*500.0f
			) * 0.9f
		));
	}

	/*
	//////////////////////////////////////////////
	////// RANDOM MOVEMENT ALONG BEZIER CURVE
	///////////////////////////////////////////////
#define F_TIME 15

	FW::Math::Vector3 _u1, _u2, _u3;

	if (!(int(time) % F_TIME) && !updated)
	{
		seed = timeGetTime();

		updated = true;

		m_BEye[0].ClearPoints();
		m_BTarget[0].ClearPoints();

		m_BEye[0]	.AddPoint(227.0f, 107.0f,  100.0f);
		m_BTarget[0].AddPoint(160.0f, 140.0f, -200.0f);
		m_BEye[0]	.AddPoint(232.0f, 121.0f, -100.0f);
		m_BTarget[0].AddPoint(120.0f, 150.0f,  70.0f);
// 
// 		for (FW::u32 i = 0; i < FW::u32(FW::Math::Random(&seed)*2.0f+7.0f); i++)
// 		{
// 			gBEye[0].   AddPoint(FW::Math::Vector3(FW::Math::Random(&seed)*230.0f, FW::Math::Random(&seed)*60.0f + 70.0f, FW::Math::Random(&seed)*100.0f));
// 			gBTarget[0].AddPoint(FW::Math::Vector3(FW::Math::Random(&seed)*230.0f, FW::Math::Random(&seed)*60.0f + 70.0f, FW::Math::Random(&seed)*100.0f));
// 		}
	}

	// we already defined the paths
	if (!(int(time + 1.0f) % F_TIME)) { updated = false; }

	if (!(int(time + 1.0f) % F_TIME)) { sPP.FilmGrain->SetIntensity(1.0f - GetFract(time)); }	// fade out
	if (!(int(time)        % F_TIME)) { sPP.FilmGrain->SetIntensity(       GetFract(time)); }	// fade in


	_u1 = m_BEye[0]		.Interpolate(GetFract(time/float(F_TIME)));
	_u2 = m_BTarget[0]	.Interpolate(GetFract(time/float(F_TIME)));
	_u3 = FW::Math::Vector3(0.0f, 1.0f, 0.0f);

	FW::Math::Matrix4 _lookAtMatrix, _invLookAt;

	_lookAtMatrix = FW::Math::Matrix4::CreateLookAt(_u1, _u2, _u3);
	*/
	////////////////////////////////////////////////////////////
	/// FPS CAMERA /////////////////////////
	////////////////////////////////////////////////////////////
	
// 	// Calculate the camera's current position.
// 	FW::Math::Vector3 cameraPosition = m_MoveVector;
// 	//cameraPosition.Y = -cameraPosition.Y;
// 
// 	FW::Math::Matrix4 rotationMatrix =	FW::Math::Matrix4::CreateRotationX(-m_RotationVector.X) *
// 										FW::Math::Matrix4::CreateRotationY(m_RotationVector.Y);
// 
// 	// Create a vector pointing the direction the camera is facing.
// 	FW::Math::Vector3 transformedReference = VecTransform(FW::Math::Vector3(0.0f, 0.0f, 1.0f), rotationMatrix);
// 
// 	// Calculate the position the camera is looking at.
// 	FW::Math::Vector3 cameraLookat = cameraPosition + transformedReference;
// 
// 	FW::Math::Matrix4 _lookAtMatrix = FW::Math::Matrix4::CreateLookAt(
// 		cameraPosition,
// 		cameraLookat,
// 		FW::Math::Vector3::NormalY
// 		);

	//////////////////////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////////////////////

	FW::Math::Matrix4 _lookAtMatrix, _invLookAt;

	_lookAtMatrix = FW::Math::Matrix4::CreateLookAt(_u1, _u2, _u3);
	_invLookAt = FW::Math::Matrix4::CreateInverseLookAt(_u1, _u2, _u3, 0.0f);

	// Set up the view matrix and projection matrix.
	FW::Math::Projection prj = FW::Math::Projection::CreatePerspectiveFieldOfView(l_scene[l_sceneId]->bFov.Interpolate(tt), float(FW_VIEWPORT_WIDTH)/float(FW_VIEWPORT_HEIGHT), 0.1f, 3000.0f);

	m_SponzaCameraMain->SetProjection(prj);
	m_SponzaCameraLightScattering->SetProjection(prj);
	m_SponzaCameraNormalDepth->SetProjection(prj);

	m_SponzaCameraMain->SetMatrix(_lookAtMatrix);
	m_SponzaCameraLightScattering->SetMatrix(_lookAtMatrix);
	m_SponzaCameraNormalDepth->SetMatrix(_lookAtMatrix);

	sPP.FilmGrain->SetTime(time);

	FW::Render::Renderer::GetInstance().Draw(m_SceneSponza);

	sPP.LightScatteringSponza->SetInputTexture(m_SponzaCameraLightScattering->GetFramebuffer()->GetTexture());
	sPP.LightScatteringSponza->Draw();

	// SSAO
	sPP.SSAO->SetInputTexture(m_SSAO ? m_SponzaCameraMain->GetFramebuffer()->GetTexture() : NULL);
	sPP.SSAO->SetNormalDepthTexture(m_SponzaCameraNormalDepth->GetFramebuffer()->GetTexture());
	sPP.SSAO->Draw();

	// Depth of Field (main camera + need depth texture)
	sPP.DoF->SetRenderedTexture(sPP.SSAO->GetOutputTexture());
	sPP.DoF->SetDepthTexture(m_SponzaCameraNormalDepth->GetFramebuffer()->GetTexture());
	sPP.DoF->Draw();

	// Blend result from SSAO with Light scattering pass
	sPP.Blend2->SetInputTexture(sPP.DoF->GetOutputTexture());
	sPP.Blend2->SetInputTexture(sPP.LightScatteringSponza->GetOutputTexture(), 1);
	sPP.Blend2->Draw();

	// Bloom intro special framebuffer
	sPP.Bloom->SetInputTexture(sPP.Blend2->GetOutputTexture());
	sPP.Bloom->Draw();

	// Tone mapping with bloom + almost final render
	sPP.ToneMapping->SetInputTexture(sPP.Blend2->GetOutputTexture());
	sPP.ToneMapping->SetBloomTexture(sPP.Bloom->GetOutputTexture());
	sPP.ToneMapping->Draw();

	// Antialiasing
	sPP.FXAA->SetInputTexture(sPP.ToneMapping->GetOutputTexture());
	sPP.FXAA->Draw();

	// Film graing... and we're done!
	sPP.FilmGrain->SetInputTexture(sPP.FXAA->GetOutputTexture());
	sPP.FilmGrain->SetAsFinal();
	sPP.FilmGrain->Draw();
}

bool Intro::Update(float time, float delta)
{
	if (l_scene[l_sceneId]->sponza)
		ProcessSponzaScene(time, delta);
	else
		ProcessChessScene(time, delta);

//  	Sleep(40);

	return true;
}

void Intro::WriteInfo()
{
	FW::Console::GetInstance().Clear();
	FW::Console::GetInstance().Write("Position: ");
	m_MoveVector.DebugPrint();
	FW::Console::GetInstance().Write("Rotation: ");
	m_RotationVector.DebugPrint();

	FW::Console::GetInstance().WriteLine();
	FW::Console::GetInstance().WriteLine("[H/J] Light scattering - weight: %5.5f", sPP.LightScatteringChess->GetWeight());

	FW::Console::GetInstance().WriteLine();
	FW::Console::GetInstance().WriteLine("[T/Z] SSAO - AO init color: %5.5f", sPP.SSAO->GetAOInitColor());
	FW::Console::GetInstance().WriteLine("[U/I] SSAO - AO scale:      %5.5f", sPP.SSAO->GetAOScale());
	FW::Console::GetInstance().WriteLine("[O/P] SSAO / Final render");

	FW::Console::GetInstance().WriteLine();
	FW::Console::GetInstance().WriteLine("[F/G] Sun position: %5.5f", sunpos);

	FW::Console::GetInstance().WriteLine();
	FW::Console::GetInstance().WriteLine("[Y/X] DoF AutoFocus: %s", sPP.DoF->IsAutoFocused() ? "true" : "false");
	FW::Console::GetInstance().WriteLine("[C/V] DoF ShowFocus: %s", sPP.DoF->IsFocusShown() ? "true" : "false");
	FW::Console::GetInstance().WriteLine("[B/N] DoF Focal plane: %5.5f", sPP.DoF->GetFocalDepth());

	FW::Console::GetInstance().WriteLine();
	FW::Console::GetInstance().WriteLine("[M] Dump bezier curve");

	m_MeshSunSponza->GetWorldMatrix().GetTranslation().DebugPrint();
}

void Intro::OnMouseMove(unsigned buttons, int x, int y, int dx, int dy)
{
	if (buttons == FW::Render::Events::MouseButton::Left)
	{
		m_RotationVector.X += dy * m_MouseDelta;
		m_RotationVector.Y -= dx * m_MouseDelta;

		if (m_RotationVector.X >= 360.0f)
			m_RotationVector.X -= 360.0f;

		if (m_RotationVector.Y >= 360.0f)
			m_RotationVector.Y -= 360.0f;


		if (m_RotationVector.X < 0.0f)
			m_RotationVector.X += 360.0f;

		if (m_RotationVector.Y < 0.0f)
			m_RotationVector.Y += 360.0f;

		WriteInfo();
	}
}

void Intro::OnMouseDown(unsigned button, int x, int y)
{

}

void Intro::OnMouseUp(unsigned button, int x, int y)
{

}

void Intro::OnKeyDown(char key, int x, int y)
{
	switch (key)
	{
		// Lights
		case 'K':
			m_ChessPointLight->SetColor(m_ChessPointLight->GetColor() + (FW::Math::Vector3::One * 0.1f));
			m_SponzaDirectionalLight->SetColor(m_SponzaDirectionalLight->GetColor() + (FW::Math::Vector3::One * 0.1f));
			break;

		case 'L':
			m_ChessPointLight->SetColor(m_ChessPointLight->GetColor() - (FW::Math::Vector3::One * 0.1f));
			m_SponzaDirectionalLight->SetColor(m_SponzaDirectionalLight->GetColor() - (FW::Math::Vector3::One * 0.1f));
			break;

		// DoF
		case 'Y':
			sPP.DoF->SetAutoFocusState(true);
			break;

		case 'X':
			sPP.DoF->SetAutoFocusState(false);
			break;

		case 'C':
			sPP.DoF->SetFocusVisibility(true);
			break;

		case 'V':
			sPP.DoF->SetFocusVisibility(false);
			break;

		case 'B':
			sPP.DoF->SetFocalDepth(sPP.DoF->GetFocalDepth() + 1.0f);
			break;

		case 'N':
			sPP.DoF->SetFocalDepth(sPP.DoF->GetFocalDepth() - 1.0f);
			break;

		// Sun position
		case 'F':
			sunpos += 0.005f;
			break;

		case 'G':
			sunpos -= 0.005f;
			break;

		// Light scattering
		case 'H':
			sPP.LightScatteringChess->SetWeight(sPP.LightScatteringChess->GetWeight() + 1.1f);
			sPP.LightScatteringSponza->SetWeight(sPP.LightScatteringSponza->GetWeight() + 1.1f);
			break;

		case 'J':
			sPP.LightScatteringChess->SetWeight(sPP.LightScatteringChess->GetWeight() - 1.1f);
			sPP.LightScatteringSponza->SetWeight(sPP.LightScatteringSponza->GetWeight() - 1.1f);
			break;

		// SSAO
		case 'T':
			sPP.SSAO->SetAOInitColor(sPP.SSAO->GetAOInitColor() + 0.05f);
			break;

		case 'Z':
			sPP.SSAO->SetAOInitColor(sPP.SSAO->GetAOInitColor() - 0.05f);
			break;

		case 'U':
			sPP.SSAO->SetAOScale(sPP.SSAO->GetAOScale() + 0.005f);
			break;

		case 'I':
			sPP.SSAO->SetAOScale(sPP.SSAO->GetAOScale() - 0.005f);
			break;

		case 'O':
			m_SSAO = true;
			break;

		case 'P':
			m_SSAO = false;
			break;

		case 'Q':
			FW::Render::ShaderManager::GetInstance().Init();
			break;

		case 'W':
			{
				FW::Math::Vector2 rotationRad = m_RotationVector * FW::Math::DEGTORAD;

				m_MoveVector.X += FW::Math::Sin(rotationRad.Y) * m_MoveDelta;
				m_MoveVector.Y -= FW::Math::Sin(rotationRad.X) * m_MoveDelta;
				m_MoveVector.Z += FW::Math::Cos(rotationRad.Y) * m_MoveDelta;
			}
			break;

		case 'S':
			{
				FW::Math::Vector2 rotationRad = m_RotationVector * FW::Math::DEGTORAD;

				m_MoveVector.X -= FW::Math::Sin(rotationRad.Y) * m_MoveDelta;
				m_MoveVector.Y += FW::Math::Sin(rotationRad.X) * m_MoveDelta;
				m_MoveVector.Z -= FW::Math::Cos(rotationRad.Y) * m_MoveDelta;
			}
			break;

		case 'A':
			{
				FW::Math::Vector2 rotationRad = m_RotationVector * FW::Math::DEGTORAD;

				m_MoveVector.X += FW::Math::Cos(rotationRad.Y) * m_MoveDelta;
				m_MoveVector.Z -= FW::Math::Sin(rotationRad.Y) * m_MoveDelta;
			}
			break;

		case 'D':
			{
				FW::Math::Vector2 rotationRad = m_RotationVector * FW::Math::DEGTORAD;

				m_MoveVector.X -= FW::Math::Cos(rotationRad.Y) * m_MoveDelta;
				m_MoveVector.Z += FW::Math::Sin(rotationRad.Y) * m_MoveDelta;
			}
			break;

		case 'R':
			{
				m_MoveVector = FW::Math::Vector3::Zero;
				m_RotationVector = FW::Math::Vector2::Zero;
			}
			break;
	}
	
	WriteInfo();
}

void Intro::OnKeyUp(char key, int x, int y)
{
	switch (key)
	{
		// Dump bezier curve
		case 'M':
			{
				FILE* f = fopen(FW::String::Format("%u.txt", timeGetTime()).CharArray(), "w+");
				
				fprintf(f, "Eye:\r\n");

				for (FW::u32 i = 0; i < m_BEye[0].GetCount(); i++)
				{
					fprintf(f, "%ff, %ff, %ff\r\n", m_BEye[0][i].X, m_BEye[0][i].Y, m_BEye[0][i].Z);
				}

				fprintf(f, "\r\nTarget:\r\n");

				for (FW::u32 i = 0; i < m_BTarget[0].GetCount(); i++)
				{
					fprintf(f, "%ff, %ff, %ff\r\n", m_BTarget[0][i].X, m_BTarget[0][i].Y, m_BTarget[0][i].Z);
				}

				fclose(f);
			}
			break;
	}
}
