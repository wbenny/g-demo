#include "FW/Render/Viewer.h"
#include "Intro.h"

// 0 for test mode
#if 1

int __cdecl main()
{
	FW::Render::Viewer::GetInstance().SetHandler(new Intro());
	FW::Render::Viewer::GetInstance().Init(FW_VIEWPORT_WIDTH, FW_VIEWPORT_HEIGHT, false);
	FW::Render::Viewer::GetInstance().Run();

	return 0;
}

#else

int __cdecl main()
{
	FW::String str1 = "text";
	FW::String str2 = 'f';

	printf("%i", (int)(str1 == "text"));
	printf("%i", (int)(str2 == 'f'));
	printf("%i", (int)(str2 == "f"));
	str1 = str2;
	printf("%i", (int)(str1 == str2));
}


#endif