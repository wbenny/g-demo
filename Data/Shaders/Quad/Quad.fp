uniform int uTextureCount;

void main()
{
    if      (uTextureCount == 1)
        vFragColor = texture(uInputTexture0, vTexCoords);
    else if (uTextureCount == 2)
        vFragColor = texture(uInputTexture0, vTexCoords) +
                     texture(uInputTexture1, vTexCoords);
    else if (uTextureCount == 3)
        vFragColor = texture(uInputTexture0, vTexCoords) *
                     texture(uInputTexture1, vTexCoords) *
                     texture(uInputTexture2, vTexCoords);
    else if (uTextureCount == 4)
        vFragColor = texture(uInputTexture0, vTexCoords) *
                     texture(uInputTexture1, vTexCoords) *
                     texture(uInputTexture2, vTexCoords) * 
                     texture(uInputTexture3, vTexCoords);
}
