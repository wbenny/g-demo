// varying
in vec3 vVertexEyeNormal;

void main()
{
    const float near = uNearFar.x; 
    const float far  = uNearFar.y;

    //float d = ((gl_FragCoord.z / gl_FragCoord.w) - near)/(far-near); // Linearni interpolace.

    //float d = ((gl_FragCoord.z / gl_FragCoord.w) * 0.5) + 0.5;

    float d = gl_FragCoord.z;

    vFragColor = vec4(normalize(vVertexEyeNormal), d);
}
