// defines
#define SAMPLES 8.0

// uniforms
uniform sampler2D uNoiseTexture;
uniform sampler2D uNormalDepthTexture;
uniform bool uUseColorTexture;
/*
// TODO: as uniforms
const float totStrength = 1.5;      // 1.5
const float strength = 0.25;        // 0.25
*/
const float offset = 12.5;          // 12.5
/*
const float falloff = 0.00001;
const float rad = 0.00125*0.8;      // * 0.8
const float invSamples = -totStrength/SAMPLES;
*/

const vec3 pSphere[10] = vec3[]
(
	vec3(-0.010735935, 0.01647018, 0.0062425877),
	vec3(-0.06533369, 0.3647007, -0.13746321),
	vec3(-0.6539235, -0.016726388, -0.53000957),
	vec3(0.40958285, 0.0052428036, -0.5591124),
	vec3(-0.1465366, 0.09899267, 0.15571679),
	vec3(-0.44122112, -0.5458797, 0.04912532),
	vec3(0.03755566, -0.10961345, -0.33040273),
	vec3(0.019100213, 0.29652783, 0.066237666),
	vec3(0.8765323, 0.011236004, 0.28265962),
	vec3(0.29264435, -0.40794238, 0.15964167)
);

void main()
{
	vec2 screenPos = vTexCoords.xy;

	float depth = texture(uNormalDepthTexture, vTexCoords).z;
	
    if (depth < -1000.0) discard;
	
	vec3 fres = normalize((texture(uNoiseTexture, vTexCoords * offset).xyz * 2.0) - vec3(1.0));
	vec3 normal = texture(uNormalDepthTexture, vTexCoords).xyz;

	float bl = 0.0;
 
	float depthDifference;
	vec3 occluderFragment;
	vec3 ray;
   
	for (int i = 0; i < SAMPLES; ++i)
	{
		ray = reflect(
                //(texture(uNoiseTexture,vec2(i / SAMPLES)).xyz * 2.0) - vec3(1.0),
                pSphere[i],
            fres);

		vec2 occluderPos = screenPos + sign(dot(ray, normal)) * ray.xy * 0.0125;
		
		occluderFragment = texture(uNormalDepthTexture, occluderPos).xyz;
		depthDifference = clamp((texture(uNormalDepthTexture, occluderPos).z - depth) * length(occluderFragment), 0.0, 0.0625);
	
		bl += depthDifference;
	}
	
	bl = clamp(1.0 - (bl / SAMPLES) * 10.0, 0.0, 1.0);
	
    if (uUseColorTexture)
    {
	    vFragColor = vec4(bl * texture(uInputTexture0, screenPos).xyz, 1.0);
    }
    else
    {
        vFragColor = vec4(vec3(bl), 1.0);
    }
}
