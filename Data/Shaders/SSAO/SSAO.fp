uniform sampler2D uNormalDepthTexture;
uniform bool uUseColorTexture;

uniform float uAOInitColor;
uniform float uAOScale;

uniform float camx = 0.1;
uniform float camy = 3000.0;

float readDepth(in vec2 coord)
{
    return (2.0 * camx) / (camy + camx - texture2D(uNormalDepthTexture, coord ).a * (camy - camx)); 	
}

float compareDepths(in float depth1, in float depth2)
{
	float aoCap = 1.0;
	float aoMultiplier = 100.0;
	float depthTolerance = 0.0001;
	float aorange = 5.0;// units in space the AO effect extends to (this gets divided by the camera far range
	float diff = sqrt(clamp(1.0 - (depth1 - depth2) / (aorange / (camy - camx)), 0.0, 1.0));
	float ao = min(aoCap, max(0.0, depth1 - depth2 - depthTolerance) * aoMultiplier) * diff;
	return ao;
}

void main(void)
{	
	float depth = readDepth(vTexCoords);
	float d;
	
	float pw = 1.0 / uWindowSize.x;
	float ph = 1.0 / uWindowSize.y;

	float ao = uAOInitColor;// 1.0;
		
	float aoscale = uAOScale;// 0.1;
	
	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;
	
	pw *= 2.0;
	ph *= 2.0;
	aoscale *= 1.2;
	
	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;

	pw *= 2.0;
	ph *= 2.0;
	aoscale *= 1.4;
	
	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;
	
	pw *= 2.0;
	ph *= 2.0;
	aoscale *= 1.6;
	
	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;
	
	pw *= 2.0;
	ph *= 2.0;
	aoscale *= 1.8;
	
	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t + ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s + pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;

	d = readDepth(vec2(vTexCoords.s - pw,vTexCoords.t - ph));
	ao += compareDepths(depth, d) / aoscale;
	
	ao /= 15.0;
    ao = 1.0 - ao;
    	
    if (uUseColorTexture)
    {
	    vFragColor = vec4(ao * texture(uInputTexture0, vTexCoords).xyz, 1.0);
    }
    else
    {
        vFragColor = vec4(vec3(ao), 1.0);
    }
}
