#define NUM_SAMPLES  100

uniform float uExposure;
uniform float uDecay;
uniform float uDensity;
uniform float uWeight;
uniform vec2  uLightPositionOnScreen;

// uniformExposure = 0.0034f;
// uniformDecay = 1.0f;
// uniformDensity = 0.84f;
// uniformWeight = 5.65f;	

void main()
{
    vFragColor = vec4(0.0);
    vec2 deltaTextCoord = vec2(vTexCoords - uLightPositionOnScreen);
    vec2 textCoo = vTexCoords;
    float illuminationDecay = 1.0;

    deltaTextCoord *= 1.0 / float(NUM_SAMPLES) * uDensity;
    
    for(int i = 0; i < NUM_SAMPLES; i++)
    {
        textCoo -= deltaTextCoord;
        vec4 t = texture(uInputTexture0, textCoo);
        t *= illuminationDecay * uWeight;
        vFragColor += t;
        illuminationDecay *= uDecay;
    }

    vFragColor *= uExposure;
    vFragColor.a = 1.0;
}

