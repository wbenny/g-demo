uniform float uTime;
uniform float uStrength;
uniform float uIntensity;
 
void main()
{
    // Random, adding values to get rid of edge errors  and mods that return 0
    float x = (vTexCoords.x + 4.0) * (vTexCoords.y + 4.0) * (uTime * 10.0);
    vec4 grain = vec4(mod((mod(x, 13.0) + 1.0) * (mod(x, 123.0) + 1.0), 0.01) - 0.005) * uStrength;
 
    vFragColor = (texture(uInputTexture0, vTexCoords) + grain) * uIntensity;

    vFragColor.a = 1.0;
}