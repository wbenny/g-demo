uniform vec3 uColor;

void main()
{
    vFragColor = vec4(uColor, 1.0);
}
