// varying
out vec4 vFragColor;

uniform mat4 uProjectionMatrix, uModelMatrix, uViewMatrix;
uniform mat3 uNormalMatrix;

uniform mat4 uProjectionViewMatrix;
uniform mat4 uProjectionViewModelMatrix;

uniform vec3 uEyeDirection, uEyePosition;

uniform vec2 uWindowSize;
uniform vec2 uNearFar;

float random(in vec2 n)
{
    return 0.5 + 0.5 * fract(sin(dot(n.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

