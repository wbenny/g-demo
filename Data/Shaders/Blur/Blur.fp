/*
uniform float uSigma = 4.5;    // The uSigma value for the gaussian function: higher value means more blur
                         // A good value for 9x9 is around 3 to 5
                         // A good value for 7x7 is around 2.5 to 4
                         // A good value for 5x5 is around 2 to 3.5
                         // ... play around with this based on what you need :)

uniform float uBlurSize = 1.0 / 512.0; // This should usually be equal to
                         // 1.0f / texture_pixel_width for a horizontal blur, and
                         // 1.0f / texture_pixel_height for a vertical blur.

const float pi = 3.14159265;

// The following are all mutually exclusive macros for various 
// seperable blurs of varying kernel size
#if defined(VERTICAL_BLUR_9)
const float numBlurPixelsPerSide = 4.0;
const vec2  blurMultiplyVec      = vec2(0.0, 1.0);
#elif defined(HORIZONTAL_BLUR_9)
const float numBlurPixelsPerSide = 4.0;
const vec2  blurMultiplyVec      = vec2(1.0, 0.0);
#elif defined(VERTICAL_BLUR_7)
const float numBlurPixelsPerSide = 3.0;
const vec2  blurMultiplyVec      = vec2(0.0, 1.0);
#elif defined(HORIZONTAL_BLUR_7)
const float numBlurPixelsPerSide = 3.0;
const vec2  blurMultiplyVec      = vec2(1.0, 0.0);
#elif defined(VERTICAL_BLUR_5)
const float numBlurPixelsPerSide = 2.0;
const vec2  blurMultiplyVec      = vec2(0.0, 1.0);
#elif defined(HORIZONTAL_BLUR_5)
const float numBlurPixelsPerSide = 2.0;
const vec2  blurMultiplyVec      = vec2(1.0, 0.0);
#else
// This only exists to get this shader to compile when no macros are defined
const float numBlurPixelsPerSide = 0.0;
const vec2  blurMultiplyVec      = vec2(0.0, 0.0);
#endif

void main()
{
  // Incremental Gaussian Coefficent Calculation (See GPU Gems 3 pp. 877 - 889)
  vec3 incrementalGaussian;
  incrementalGaussian.x = 1.0 / (sqrt(2.0 * pi) * uSigma);
  incrementalGaussian.y = exp(-0.5 / (uSigma * uSigma));
  incrementalGaussian.z = incrementalGaussian.y * incrementalGaussian.y;

  vec4 avgValue = vec4(0.0, 0.0, 0.0, 0.0);
  float coefficientSum = 0.0;

  // Take the central sample first...
  avgValue += texture(uInputTexture0, vTexCoords.xy) * incrementalGaussian.x;
  coefficientSum += incrementalGaussian.x;
  incrementalGaussian.xy *= incrementalGaussian.yz;

  // Go through the remaining 8 vertical samples (4 on each side of the center)
  for (float i = 1.0; i <= numBlurPixelsPerSide; i++)
  { 
    avgValue += texture(uInputTexture0, vTexCoords.xy - i * uBlurSize * 
                          blurMultiplyVec) * incrementalGaussian.x;         
    avgValue += texture(uInputTexture0, vTexCoords.xy + i * uBlurSize * 
                          blurMultiplyVec) * incrementalGaussian.x;         
    coefficientSum += 2.0 * incrementalGaussian.x;
    incrementalGaussian.xy *= incrementalGaussian.yz;
  }

  vFragColor = avgValue / coefficientSum;
}
*/

uniform vec2 vPixelSize = vec2(1.0/1024.0, 1.0/1024.0);

float vGaussianBlur[10] ;

//= { 0.0882357, 0.0957407, 0.101786, 0.106026, 0.108212, 0.108212, 0.106026, 0.101786, 0.0957407, 0.0882357 };

float log_conv ( float x0, float X, float y0, float Y )
{
    return ( X + log( x0 + (y0 * exp(Y - X) ) ) );
}

vec4 LogGaussianFilter()
{
	
	vGaussianBlur[0] = 0.0882357;
	vGaussianBlur[1] = 0.0957407;
	vGaussianBlur[2] = 0.101786;
	vGaussianBlur[3] = 0.106026;
	vGaussianBlur[4] = 0.108212;
	vGaussianBlur[5] = 0.108212;
	vGaussianBlur[6] = 0.106026;
	vGaussianBlur[7] = 0.101786;
	vGaussianBlur[8] = 0.0957407;
	vGaussianBlur[9] = 0.0882357;
	
	float vSample[ 10 ];

    for (int i = 0; i < 10; i++)
    {
		float fOffSet = i - 4.5;		
		vec2 texCoord = vec2( vTexCoords.x + fOffSet * vPixelSize.x, vTexCoords.y + fOffSet * vPixelSize.y );
		vSample[i] = texture( uInputTexture0, texCoord ).r;
	}
 
    float fAccum;
    fAccum = log_conv( vGaussianBlur[0], vSample[0], vGaussianBlur[1], vSample[1] );
    for (int i = 2; i < 10; i++)
    {
        fAccum = log_conv( 1.0, fAccum, vGaussianBlur[i], vSample[i] );
    }        
    
    return vec4( fAccum, 0.0, 0.0, 0.0);
}

void main()
{
	vFragColor = LogGaussianFilter();
}
