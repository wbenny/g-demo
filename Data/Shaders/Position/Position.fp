in vec3 vPosition;

void main()
{
    vFragColor = vec4(vPosition, 1.0);
}
