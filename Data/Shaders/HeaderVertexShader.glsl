// uniforms
uniform mat4 uProjectionMatrix, uModelMatrix, uViewMatrix;
uniform mat3 uNormalMatrix;

uniform mat4 uProjectionViewMatrix;
uniform mat4 uProjectionViewModelMatrix;

uniform vec3 uEyeDirection, uEyePosition;

uniform vec2 uWindowSize;
uniform vec2 uNearFar;
