// attributes
layout(location = 0) in vec3 aVertexPosition;
layout(location = 1) in vec2 aVertexUV;

// varying
out vec2 vTexCoords;
