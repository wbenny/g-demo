#define MAX_STEPS 2000

// uniforms
uniform sampler2D uCameraDepthTexture;
uniform sampler2D uLightDepthTexture;
uniform sampler2D uNoiseTexture;

uniform mat4 uLightProjectionModel;
uniform vec3 uLightPosition;
uniform vec3 uLightForward;
uniform vec3 uLightRight, uLightUp;

uniform float uSamplingRate = 3.0;
uniform float uCoarseDepthTexelSize;

float saturate(float n)
{
    return clamp(n, 0.0, 1.0);
}

void main()
{
/*
    float sceneDepth = texture(uCameraDepthTexture, vTexCoord).a; 
 
    vec4 clipPos; 
    clipPos.x = 2.0 * vTexCoord.x - 1.0; 
    clipPos.y = -2.0 * vTexCoord.y + 1.0; 
    clipPos.z = sceneDepth; 
    clipPos.w = 1.0; 
       
    vec4 positionWS = inverse(uProjectionViewModelMatrix) * clipPos; 
    positionWS.w = 1.0 / positionWS.w; 
    positionWS.xyz *= positionWS.w; 
 
    /////////////////////////////

 	vec3 vecForward = normalize( positionWS.xyz - uEyePosition.xyz );
	float traceDistance = dot( positionWS.xyz - ( uEyePosition.xyz + vecForward * uNearFar.x ), vecForward );
	traceDistance = clamp( traceDistance, 0.0, 2500.0 ); // Far trace distance
	
	positionWS.xyz = uEyePosition.xyz + vecForward * uNearFar.x;

    /*
	if( g_UseAngleOptimization )
	{
		float dotViewLight = dot( vecForward, uLightForward );
		vecForward *= exp( dotViewLight * dotViewLight );
	}
    */
	/*	
	vecForward *= uSamplingRate * 2.0;
	int stepsNum = min( traceDistance / length( vecForward ), MAX_STEPS );

	// Add jittering
	float jitter = texture(uNoiseTexture, vTexCoord).x;

	float step = length( vecForward );
	float scale = step * 0.0005; // Set base brightness factor
	vec4 shadowUV;
	vec3 coordinates;
	
	// Calculate coordinate delta ( coordinate step in ligh space )
	vec3 curPosition = positionWS.xyz + vecForward * jitter;
	shadowUV = uLightProjectionModel * vec4( curPosition, 1.0 );
	coordinates = shadowUV.xyz / shadowUV.w;
	coordinates.x = ( coordinates.x + 1.0 ) * 0.5;
	coordinates.y = ( 1.0 - coordinates.y ) * 0.5;
	coordinates.z = dot( curPosition - uLightPosition, uLightForward );

	curPosition = positionWS.xyz + vecForward * ( 1.0 + jitter );
	shadowUV = uLightProjectionModel * vec4( curPosition, 1.0 );
	vec3 coordinateEnd = shadowUV.xyz / shadowUV.w;
	coordinateEnd.x = ( coordinateEnd.x + 1.0 ) * 0.5;
	coordinateEnd.y = ( 1.0 - coordinateEnd.y ) * 0.5;
	coordinateEnd.z = dot( curPosition - uLightPosition, uLightForward );

	vec3 coordinateDelta = coordinateEnd - coordinates;

	vec2 vecForwardProjection;
	vecForwardProjection.x = dot( uLightRight, vecForward );
	vecForwardProjection.y = dot( uLightUp, vecForward );

	// Calculate coarse step size
	float longStepScale = int( uCoarseDepthTexelSize / length( vecForwardProjection ) );
	longStepScale = max( longStepScale, 1 );
	
	float sampleFine;
	vec2 sampleMinMax;
	float light = 0.0;
	float coordinateZ_end;
	float isLongStep;
	float longStepScale_1 = longStepScale - 1;

	float longStepsNum = 0; 
	float realStepsNum = 0;
	
	//[loop]
	for( int i = 0; i < stepsNum; i++ )
    {
		sampleMinMax = s0.SampleLevel( samplerDepthMinMax, coordinates.xy, 0 ).xy;
		
		// Use point sampling. Linear sampling can cause the whole coarse step being incorrect
		sampleFine = DepthTexture.SampleCmpLevelZero( samplerPoint_Less, coordinates.xy, coordinates.z );

		float zStart = s1.SampleLevel( samplerPoint, coordinates.xy, 0 );
		
		const float transactionScale = 100.0f;
		
		// Add some attenuation for smooth light fading out
		float attenuation = ( coordinates.z - zStart ) / ( ( sampleMinMax.y + transactionScale ) - zStart );
		attenuation = saturate( attenuation );
		attenuation = 1.0 - attenuation;
		attenuation *= attenuation;

		float attenuation2 = ( ( zStart + transactionScale ) - coordinates.z ) * ( 1.0 / transactionScale );
		attenuation2 = 1.0 - saturate( attenuation2 );

		attenuation *= attenuation2;
		
		// Use this value to incerase light factor for "indoor" areas
		float density = s1.SampleCmpLevelZero( samplerPoint_Greater, coordinates.xy, coordinates.z );
		density *= 10.0 * attenuation;
		density += 0.25;
		sampleFine *= density;
		
		coordinateZ_end = coordinates.z + coordinateDelta.z * longStepScale;
		
		float comparisonValue = max( coordinates.z, coordinateZ_end );
		float isLight = comparisonValue < sampleMinMax.x; // .x stores min depth values
		
		comparisonValue = min( coordinates.z, coordinateZ_end );
		float isShadow = comparisonValue > sampleMinMax.y; // .y stores max depth values
		
		// We can perform coarse step if all samples are in light or shadow
		isLongStep = isLight + isShadow;

		longStepsNum += isLongStep;
		realStepsNum += 1.0;

        /*
		if( useZOptimizations )
		{
            light += scale * sampleFine * ( 1.0 + isLongStep * longStepScale_1 ); // longStepScale should be >= 1 if we use a coarse step

			coordinates += coordinateDelta * ( 1.0 + isLongStep * longStepScale_1 );
			i += isLongStep * longStepScale_1;
		}
        */
	/*	else
		{
			light += scale * sampleFine;
			coordinates += coordinateDelta;
		}
    }
    */
    /*
	// Do correction for final coarse steps.
	if( useZOptimizations )
	{
		light -= scale * sampleFine * ( i - stepsNum );
	}
    */
	//return longStepsNum / realStepsNum;
	//return light * cos( light );
    //return light;
    

    vFragColor = vec4(vec3(0.8), 1.0);
}
