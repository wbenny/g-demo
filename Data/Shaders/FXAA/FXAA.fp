float lumRGB(vec3 v)
{  
    return dot(v, vec3(0.212, 0.716, 0.072));
}
 
void main()
{
    vec2 inverse_buffer_size = vec2(1.0/uWindowSize.x, 1.0/uWindowSize.y);
    vec2 UV = gl_FragCoord.xy * inverse_buffer_size;
 
    float w = 1.75;
 
    float t = lumRGB(texture(uInputTexture0, UV + vec2( 0.0, -1.0) * w * inverse_buffer_size).xyz),
	      l = lumRGB(texture(uInputTexture0, UV + vec2(-1.0,  0.0) * w * inverse_buffer_size).xyz),
	      r = lumRGB(texture(uInputTexture0, UV + vec2( 1.0,  0.0) * w * inverse_buffer_size).xyz),
	      b = lumRGB(texture(uInputTexture0, UV + vec2( 0.0,  1.0) * w * inverse_buffer_size).xyz);
 
    vec2  n = vec2(-(t - b), r - l);
    float nl = length(n);
 
    if	(nl < (1.0 / 16.0))
    {
	    vFragColor = texture(uInputTexture0, UV);
    }
    else
    {
	    n *= inverse_buffer_size / nl;
 
	    vec4 o  = texture(uInputTexture0, UV),
		     t0 = texture(uInputTexture0, UV + n  * 0.5) * 0.9,
		     t1 = texture(uInputTexture0, UV - n  * 0.5) * 0.9,
		     t2 = texture(uInputTexture0, UV + n) * 0.75,
		     t3 = texture(uInputTexture0, UV - n) * 0.75;
 
	    vFragColor = (o + t0 + t1 + t2 + t3) / 4.3;
    }
}