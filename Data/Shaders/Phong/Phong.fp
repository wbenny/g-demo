// defines
#define LIGHT_NONE          0
#define LIGHT_AMBIENT       1
#define LIGHT_DIRECTIONAL   2
#define LIGHT_POINT         4
#define LIGHT_SPOT          8

#define LIGHT_COUNT         4

// structs
struct sLight
{
    int   Type;
    bool  Active;
    float FallOff;
    vec3  Location; // Location jiz pronasobena s View matici.
    vec3  Color;
};

struct sMaterial
{
    vec3  Color;
    float DiffuseReflection;
	float SpecularReflection;
	float SpecularShininess;
};

// uniforms
uniform vec3        uAmbientLightColor;
uniform sLight      uLightSources[LIGHT_COUNT];
uniform sMaterial   uMaterial;
uniform float       uEnvironmentMapShininess;
uniform float       uReflectionStrength;
uniform float       uAlpha;

uniform sampler2D   uTexture;
uniform sampler2D   uNormalMap;
uniform sampler2D   uShadowMap;
uniform sampler2D   uReflectionMap;
uniform samplerCube uEnvironmentMap;

uniform bool uUseTexture, uUseEnvironmentMap, uUseNormalMap, uIsReflective;

// varying
in vec3 vEyeDirection;
in vec4 vWorldVertexPosition;
in vec4 vViewVertexPosition;
in vec3 vVertexEyeNormal;
in vec2 vTexCoords;
in vec3 vVertexRef;

in vec4 vShadowVertexPosition;

// global variables
vec3 gDiffuseColor  = vec3(0.0);
float gSpecular = 0.0;
float gShadowColor = 1.0;

mat3 computeTangentFrame(vec3 normal, vec3 position, vec2 texCoord)
{
    vec3 dpx = dFdx(position);
    vec3 dpy = dFdy(position);
    vec2 dtx = dFdx(texCoord);
    vec2 dty = dFdy(texCoord);
    
    vec3 tangent = normalize(dpy * dtx.t - dpx * dty.t);
    vec3 binormal = cross(tangent, normal);
   
    return mat3(tangent, binormal, normal);
}

void AddLight(in sLight light)
{
    vec3 normal;
    
    if (uUseNormalMap)
    {
	    vec3 n = normalize(vVertexEyeNormal);
	    vec3 pos = vViewVertexPosition.xyz;
	    vec3 v = normalize(pos);

	    mat3 tbn = computeTangentFrame(n, v, vTexCoords);

        vec3 bump;

        bump = texture(uNormalMap, vTexCoords).xyz * 2.0 - 1.0;
	    bump.z *= 0.7;

	    normal = normalize(normalize(tbn * bump) * 0.5 + 0.5);
    } 
    else
    {
        normal = normalize(vVertexEyeNormal);
    }   

    // No light.
    if(light.Type == LIGHT_NONE)
    {
        return;
    }
    // Directional or point light.
    else if (light.Type == LIGHT_DIRECTIONAL)
    {
        vec3 eye = normalize(vWorldVertexPosition.xyz);
        vec3 h = normalize(light.Location);

        float NdotL = max(dot(light.Location, normal), 0.0);
        float specular = pow(max(dot(normal, h), 0.0), uMaterial.SpecularShininess);

        gDiffuseColor += NdotL * light.Color * uMaterial.DiffuseReflection;
        gSpecular     += specular * uMaterial.SpecularReflection;
    }
    else if (light.Type == LIGHT_POINT)
    {
        vec3 dir = light.Location - vWorldVertexPosition.xyz;
	    float attenuation = 1.0 - clamp(length(dir) / light.FallOff, 0.0, 1.0);
	
	    vec3 eye = vWorldVertexPosition.xyz;
	    vec3 h = normalize(dir - eye);

	    float NdotL = max(dot(normalize(dir), normal), 0.0) * attenuation;
	    float specular = pow(max(dot(normal, h), 0.0), uMaterial.SpecularShininess);

        gDiffuseColor += NdotL * light.Color * uMaterial.DiffuseReflection;
        gSpecular     += specular * uMaterial.SpecularReflection;
	}
}

void AddShadow(in float sigma)
{
    vec4 shadowCoord = vShadowVertexPosition / vShadowVertexPosition.w;
    shadowCoord.z += sigma;

    if ((shadowCoord.z > 0.99999999))
        return;

    float shadow = texture(uShadowMap, shadowCoord.xy).z;

    gShadowColor = shadow < shadowCoord.z ? 0.4 : 1.0;
}

void main()
{
    gDiffuseColor = uAmbientLightColor;

    for (int i = 0; i < LIGHT_COUNT; i++)
    {
        AddLight(uLightSources[i]);
	}
    
    AddShadow(-0.00025);
    
    vec3 c = (uUseTexture ? texture(uTexture, vTexCoords).rgb : uMaterial.Color);
    vFragColor = vec4((vec3(gSpecular) + (gDiffuseColor * c)) * gShadowColor, 1.0);

    if (uUseEnvironmentMap)
        vFragColor += (texture(uEnvironmentMap, vVertexRef) * uEnvironmentMapShininess) * gShadowColor;

    if (uIsReflective)
    {
    	vec2 uvReflection = vec2(gl_FragCoord.x/uWindowSize.x, gl_FragCoord.y/uWindowSize.y);

        vFragColor.rgb += texture(uReflectionMap, uvReflection).rgb * uReflectionStrength * gShadowColor;
    }

    vFragColor.a = uAlpha;    
}
