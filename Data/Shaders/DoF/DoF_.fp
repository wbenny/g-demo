uniform sampler2D uRenderedTexture;
uniform sampler2D uDepthTexture;

const float blurclamp = 1.5;    // max blur amount
const float bias = 0.04;	    // aperture - bigger values for shallower depth of field
uniform float focus = 0.08;   // this value comes from ReadDepth script.
 
void main() 
{

	float aspectratio = uWindowSize.x/uWindowSize.y;
	vec2 aspectcorrect = vec2(1.0, aspectratio);
	
	vec4 depth1 = texture(uDepthTexture,vTexCoords.xy );

	float factor = (depth1.x - focus);
	 
	vec2 dofblur = vec2(clamp(factor * bias, -blurclamp, blurclamp));

	vec4 col = vec4(0.0);
	
	col += texture(uRenderedTexture, vTexCoords.xy);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.0 , 0.4  )*aspectcorrect) * dofblur);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.15, 0.37 )*aspectcorrect) * dofblur);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.29, 0.29 )*aspectcorrect) * dofblur);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.37, 0.15 )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.4 , 0.0  )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.37,-0.15 )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.29,-0.29 )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.15,-0.37 )*aspectcorrect) * dofblur);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.0 ,-0.4  )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.15, 0.37 )*aspectcorrect) * dofblur);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.29, 0.29 )*aspectcorrect) * dofblur);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.37, 0.15 )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.4 , 0.0  )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.37,-0.15 )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.29,-0.29 )*aspectcorrect) * dofblur);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.15,-0.37 )*aspectcorrect) * dofblur);
	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.15, 0.37 )*aspectcorrect) * dofblur * 0.9);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.37, 0.15 )*aspectcorrect) * dofblur * 0.9);		
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.37,-0.15 )*aspectcorrect) * dofblur * 0.9);		
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.15,-0.37 )*aspectcorrect) * dofblur * 0.9);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.15, 0.37 )*aspectcorrect) * dofblur * 0.9);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.37, 0.15 )*aspectcorrect) * dofblur * 0.9);		
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.37,-0.15 )*aspectcorrect) * dofblur * 0.9);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.15,-0.37 )*aspectcorrect) * dofblur * 0.9);	
	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.29, 0.29 )*aspectcorrect) * dofblur * 0.7);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.4 , 0.0  )*aspectcorrect) * dofblur * 0.7);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.29,-0.29 )*aspectcorrect) * dofblur * 0.7);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.0 ,-0.4  )*aspectcorrect) * dofblur * 0.7);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.29, 0.29 )*aspectcorrect) * dofblur * 0.7);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.4 , 0.0  )*aspectcorrect) * dofblur * 0.7);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.29,-0.29 )*aspectcorrect) * dofblur * 0.7);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.0 , 0.4  )*aspectcorrect) * dofblur * 0.7);
			 
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.29, 0.29 )*aspectcorrect) * dofblur * 0.4);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.4 , 0.0  )*aspectcorrect) * dofblur * 0.4);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.29,-0.29 )*aspectcorrect) * dofblur * 0.4);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.0 ,-0.4  )*aspectcorrect) * dofblur * 0.4);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.29, 0.29 )*aspectcorrect) * dofblur * 0.4);
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.4 , 0.0  )*aspectcorrect) * dofblur * 0.4);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2(-0.29,-0.29 )*aspectcorrect) * dofblur * 0.4);	
	col += texture(uRenderedTexture, vTexCoords.xy + (vec2( 0.0 , 0.4  )*aspectcorrect) * dofblur * 0.4);	
			
	vFragColor = col/41.0;
	vFragColor.a = 1.0;
}
