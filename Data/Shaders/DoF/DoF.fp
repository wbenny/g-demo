/*
DoF with bokeh GLSL shader v2.4
by Martins Upitis (martinsh) (devlog-martinsh.blogspot.com)

----------------------
The shader is Blender Game Engine ready, but it should be quite simple to adapt for your engine.

This work is licensed under a Creative Commons Attribution 3.0 Unported License.
So you are free to share, modify and adapt it for your needs, and even use it for commercial use.
I would also love to hear about a project you are using it.

Have fun,
Martins
----------------------
*/

#define PI  3.14159265

uniform sampler2D uRenderedTexture;
uniform sampler2D uDepthTexture;

// uniform variables from external script
uniform bool  uShowFocus;           // show debug focus point and focal range (red = focal point, green = focal range)

uniform bool  uAutoFocus;           // use autofocus in shader? disable if you use external uFocalDepth value
uniform vec2  uFocus;               // autofocus point on screen (0.0,0.0 - left lower corner, 1.0,1.0 - upper right)

uniform float uFocalDepth;          // focal distance value in meters, but you may use autofocus option below

uniform float uNearDofStart;        // near dof blur start
uniform float uNearDofDistance;     // near dof blur falloff distance
uniform float uFarDofStart;         // far dof blur start
uniform float uFarDofDistance;      // far dof blur falloff distance


// --------------------------------------------------------------------

float width  = uWindowSize.x; // texture width
float height = uWindowSize.y; // texture height

vec2  texel = vec2(1.0/width, 1.0/height);

// make sure that these two values are the same for your camera, otherwise distances will be wrong.
float uNear = 0.1;              // camera clipping start
float uFar  = 3000.0;           // camera clipping end

// user variables
int   samples   = 4;            // samples on the first ring
int   rings     = 3;            // ring count

float maxblur   = 1.35;         // clamp value of max blur (0.0 = no blur,1.0 default)

float threshold = 2.20;         // highlight threshold;
float gain      = 1.5;          // highlight gain;

float bias      = 1.5;          // bokeh edge bias
float fringe    = 0.7;          // bokeh chromatic aberration/fringing

bool  noise     = true;         // use noise instead of pattern for sample dithering
float namount   = 0.0001;       // dither amount

// --------------------------------------------------------------------

// processing the sample
vec3 SampleProcess(vec2 coords, float blur) 
{
	vec3 col = vec3(0.0);
	
	col.r = texture(uRenderedTexture, coords + vec2( 0.000,  1.0) * texel * fringe * blur).r;
	col.g = texture(uRenderedTexture, coords + vec2(-0.866, -0.5) * texel * fringe * blur).g;
	col.b = texture(uRenderedTexture, coords + vec2( 0.866, -0.5) * texel * fringe * blur).b;
	
	vec3 lumcoeff = vec3(0.299, 0.587, 0.114);
	float lum = dot(col.rgb, lumcoeff);
	float thresh = max((lum - threshold) * gain, 0.0);
	return col + mix(vec3(0.0), col, thresh * blur);
}

// generating noise/pattern texture for dithering
vec2 RandomVec2(vec2 coord)
{
	float noiseX = ((fract(1.0 - coord.s * (width/2.0)) * 0.25) + (fract(coord.t * (height/2.0)) * 0.75)) * 2.0 - 1.0;
	float noiseY = ((fract(1.0 - coord.s * (width/2.0)) * 0.75) + (fract(coord.t * (height/2.0)) * 0.25)) * 2.0 - 1.0;
	
	if (noise)
	{
		noiseX = clamp(fract(sin(dot(coord ,vec2(12.9898, 78.233)))       * 43758.5453), 0.0, 1.0) * 2.0 - 1.0;
		noiseY = clamp(fract(sin(dot(coord ,vec2(12.9898, 78.233) * 2.0)) * 43758.5453), 0.0, 1.0) * 2.0 - 1.0;
	}

	return vec2(noiseX, noiseY);
}

vec3 DebugFocus(vec3 col, float blur, float depth)
{
	float edge = 0.002 * depth; //distance based edge smoothing
	float m = clamp(smoothstep(0.0,        edge, blur), 0.0, 1.0);
	float e = clamp(smoothstep(1.0 - edge, 1.0,  blur), 0.0, 1.0);
	
	col = mix(col, vec3(1.0,0.5,0.0), ( 1.0 - m) *  0.6);
	col = mix(col, vec3(0.0,0.5,1.0), ((1.0 - e) - (1.0 - m)) * 0.2);

	return col;
}

float Linearize(float depth)
{
	return -uFar * uNear / (depth * (uFar - uNear) - uFar);
}

void main() 
{
	//scene depth calculation
	float depth = Linearize(texture(uDepthTexture,vTexCoords).a);
	
	//focal plane calculation
	float fDepth = uFocalDepth;
	
	if (uAutoFocus)
	{
		fDepth = Linearize(texture(uDepthTexture, uFocus).a);
	}
	
	//dof blur factor calculation
	float blur = 0.0;
	
	float a = depth - fDepth; //focal plane
	float b = ( a - uFarDofStart ) / uFarDofDistance; //far DoF
	float c = (-a - uNearDofStart) / uNearDofDistance; //near Dof
	blur = clamp((a > 0.0) ? b : c, 0.0, 1.0);
		
	// calculation of pattern for ditering
	vec2 noise = RandomVec2(vTexCoords.xy) * namount * blur;
	
	// getting blur x and y step factor
	float w = (1.0 / width ) * blur * maxblur + noise.x;
	float h = (1.0 / height) * blur * maxblur + noise.y;
	
	// calculation of final color
	vec3 col = vec3(0.0);
	
    // some optimization thingy
	if(blur < 0.05)
	{
		col = texture(uRenderedTexture, vTexCoords.xy).rgb;
	}
	else
	{
		col = texture(uRenderedTexture, vTexCoords.xy).rgb;
		float s = 1.0;
		int ringsamples;
		
		for (int i = 1; i <= rings; i++)
		{   
			ringsamples = i * samples;
			
			for (int j = 0 ; j < ringsamples ; j++)   
			{
				float step = PI*2.0 / float(ringsamples);
				float pw = (cos(float(j) * step) * float(i));
				float ph = (sin(float(j) * step) * float(i));
				float p  = 1.0;

				col += SampleProcess(vTexCoords.xy + vec2(pw * w, ph * h), blur) * mix(1.0, (float(i)) / (float(rings)), bias) * p;  
				s += mix(1.0, (float(i)) / (float(rings)), bias) * p;   
			}
		}
		col /= s; //divide by sample count
	}
	
	if (uShowFocus)
	{
		col = DebugFocus(col, blur, depth);
	}
	
	vFragColor.rgb = col;
	vFragColor.a = 1.0;
}