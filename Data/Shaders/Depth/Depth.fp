float unpack(sampler2D TEX, vec2 co)
{
    float value = dot(texture(TEX, co), vec4(0.000000059604644775390625, 0.0000152587890625, 0.00390625, 1.0));
    return value;
}

vec2 unpack2(sampler2D TEX, vec2 co)
{
    vec4 color = texture(TEX, co);
    float value1 = dot(color.rg, vec2(0.00390625, 1.0));
    float value2 = dot(color.ba, vec2(0.00390625, 1.0));
    return vec2(value1, value2);
}

vec4 pack(float value)
{
    vec4 rgba = fract(value * vec4(16777216.0, 65536.0, 256.0, 1.0));
    return rgba - rgba.rrgb * vec4(0.0, 0.00390625, 0.00390625, 0.00390625);
}

vec2 pack2(float value)
{
    vec2 rg = fract(value * vec2(256.0, 1.0));
    return rg-rg.rr * vec2(0.0, 0.00390625);
}

void main()
{
    const float near = uNearFar.x; 
    const float far  = uNearFar.y;

    float d = (((gl_FragCoord.z / gl_FragCoord.w) - near)/(far-near)); // Linearni interpolace.

    //float d = gl_FragCoord.z;

    vFragColor = vec4(d, d, d, 1.0);
}
