// uniforms
uniform sampler2D uTexture;
uniform vec3 uHorizon;

// varying
in float vIntensitySq, vIntensity;
in vec2 vTexCoord0, vTexCoord1;

void main()
{
    vec3 nHorizon = vec3(0.1, 0.1, 0.1);

    vec3 clouds0 = vec3(0.1, 0.1, 0.9); //texture(uTexture, vTexCoord0).xyz;
    vec3 clouds1 = vec3(0.7, 0.7, 0.9); //texture(uTexture, vTexCoord1).xyz;
	
    vec3 clouds = (clouds0 + clouds1) * vIntensitySq;

    vec3 cloudColor = vec3((1.0 - vIntensity) * nHorizon.x, (1.0 - vIntensity) * nHorizon.y, vIntensity * nHorizon.z);

    vFragColor = clamp(vec4(cloudColor * (1.0 - clouds.x) + clouds, 1.0), 0.0, 1.0);
}
