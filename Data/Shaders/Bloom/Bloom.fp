void main()
{
   vec4 sum = vec4(0.0);
   vec4 bum = vec4(0.0);
   int j;
   int i;

   for(i = -4; i < 4; i++)
   {
        for (j = -1; j < 1; j++)
        {
            sum += texture(uInputTexture0, vTexCoords + vec2(-i, j)*0.001) * 0.60;
			bum += texture(uInputTexture0, vTexCoords + vec2( j, i)*0.000) * 0.10;            
        }
   }
   
   vFragColor = clamp(sum*sum*sum*0.0080+bum*bum*bum*0.0080+ texture(uInputTexture0, vTexCoords), 0.0, 1.0);
   vFragColor.a = 1.0;
}
