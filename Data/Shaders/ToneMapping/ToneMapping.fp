uniform sampler2D uBloomTexture;
uniform float uExposure = 0.30;
uniform float uGamma = 0.9;
 
void main(void)
{
	vec4 bloom = clamp(texture(uBloomTexture,  vTexCoords.xy), 0.0, 1.0);
	vec4 color = clamp(texture(uInputTexture0, vTexCoords.xy), 0.0, 1.0);
	
	float luminance = dot(vec4(0.299, 0.587, 0.114, 0.000), vec4(uExposure) * color * bloom);
	
	float tone = 0.3 + luminance;
	
	vFragColor = pow(color * tone + tone * tone * bloom, vec4(1.0/uGamma));
    vFragColor.a = luminance * 4.0;
}
