// uniforms
uniform sampler2D uInputTexture0;
uniform sampler2D uInputTexture1;
uniform sampler2D uInputTexture2;
uniform sampler2D uInputTexture3;

// varying
in vec2 vTexCoords;
